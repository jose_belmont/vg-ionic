export interface IConsole {
    id_consoles?: number,
    name_console: string,
    platform?: number,
    plataforma?: string,
    img_console?: string
}