export interface IMyGame {
    id_game: number,
    id_user?: number,
    platform?: string,
    name_console?: string,
    genre?: string,
    title?: string,
    companyia?: string,
    year?: number,
    description?: string,
    cover?: string,
    region?: string,
    local_ubication?: string,
    gamerscore?: number,
    trophies?: number,
    box?: string,
    manual?: string,
    date_finish?: Date | String,
    annotations?: string,
    my_cover?: string,
    mybarcode?: string,
    youtube?: string,
    sellLat?: number,
    sellLng?: number,
}

