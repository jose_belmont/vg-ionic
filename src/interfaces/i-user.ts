export interface IUser {
    id_user?: number,
    user: string,
    email: string,
    password: string,
    password2?: string,
    name?: string,
    surname?: string,
    avatar: string,
    id_steam?: string
    lat?: number,
    lng?: number
}