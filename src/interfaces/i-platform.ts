export interface IPlatform {
    id_platform?: number,
    name_platform: string,
    img_platform: string,
}