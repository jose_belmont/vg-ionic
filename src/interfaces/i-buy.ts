export interface IBuy {
    id_trans_buy: number,
    id_trans_sell: number,
    id_trans_user: number,
    date_buy: Date,
    payed: boolean
}