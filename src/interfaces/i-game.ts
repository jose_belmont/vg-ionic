export interface IGame { 
    id_game?: number,
    id_user?: number,
    id_console?: number,
    id_genre?: number,
    name_console?: string,
    genre?: string,
    title?: string,
    companyia?: string,
    year?: number,
    description?: string,
    cover?: string,
    region?: string,
    barcode?: string,
    youtube?:string,
    add?: boolean
}