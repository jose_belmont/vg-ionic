export interface ISell {
    id_trans_sell?: number,
    id_game?: number,
    id_user?: number,
    date_sell?: Date | String,
    price?: number,
    description?: string,
    selled?: boolean,
    send?: boolean,
    cover?: string,
    title?: string,
    consola?: string,
    sellLat?: number,
    sellLng?: number,
    avatar?: string,
    user?: string
}