export interface ISteam {
    response: {
        game_count: number
        games: [
            {
                appid: number,
                name: string
            }
        ]
    }
}