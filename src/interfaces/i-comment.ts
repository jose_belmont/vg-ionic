export interface IComment {
    id_comment?: number,
    id_game: number,
    id_user: number,
    title: string,
    description: string,
    date: Date,
    consola?: string,
    juego?: string,
    user?: string,
    avatar?: string
}

