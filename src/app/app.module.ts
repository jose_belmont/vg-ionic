import { PayPal } from '@ionic-native/paypal';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { ModalRegionsPage } from './../pages/modal-regions/modal-regions';
import { ModalCompanyiasPage } from './../pages/modal-companyias/modal-companyias';
import { ModalGenresPage } from './../pages/modal-genres/modal-genres';
import { ModalConsolasPage } from './../pages/modal-consolas/modal-consolas';

import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';

import { YoutubeVideoPlayer } from '@ionic-native/youtube-video-player';
import { HttpModule } from '@angular/http';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AuthProvider } from '../providers/auth/auth';
import { IonicStorageModule } from '@ionic/storage';
import { Geolocation } from '@ionic-native/geolocation';
import { AuthInterceptor } from '../interceptors/auth.interceptor';
import { Camera } from '@ionic-native/camera';
import { UserProvider } from '../providers/user/user';
import { AgmCoreModule } from '@agm/core';
import { CommentsProvider } from '../providers/comments/comments';
import { GamesProvider } from '../providers/games/games';
import { MygamesProvider } from '../providers/mygames/mygames';
import { MarketProvider } from '../providers/market/market';
import { YoutubeProvider } from '../providers/youtube/youtube';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    ModalConsolasPage,
    ModalGenresPage,
    ModalCompanyiasPage,
    ModalRegionsPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule,
    HttpModule,
    IonicStorageModule.forRoot(),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    ModalConsolasPage,
    ModalGenresPage,
    ModalCompanyiasPage,
    ModalRegionsPage
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
    },
    Geolocation,
    BarcodeScanner,
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthProvider,
    Camera,
    YoutubeVideoPlayer,
    UserProvider,
    CommentsProvider,
    GamesProvider,
    MygamesProvider,
    MarketProvider,
    InAppBrowser,
    YoutubeProvider,
    PayPal
  ]
})
export class AppModule {}
