export const constants = {
    //for working in mobile with devapp
    server: 'http://192.168.1.150:8080/',
    imgGames: "http://192.168.1.150:8080/public/img/covers/",
    imgUsers: "http://192.168.1.150:8080/public/img/users/",
    imgPlatfomrs: "http://192.168.1.150:8080/public/img/platforms/",
    imgConsoles: "http://192.168.1.150:8080/public/img/consoles/",
    imgMenu: "http://192.168.1.150:8080/public/img/menu/"

    //for working in local browser
    // server: 'http://localhost:8080/',
    // imgGames: "http://localhost:8080/public/img/covers/",
    // imgUsers: "http://localhost:8080/public/img/users/",
    // imgPlatfomrs: "http://localhost:8080/public/img/platforms/",
    // imgConsoles: "http://localhost:8080/public/img/consoles/",
    // imgMenu: "http://localhost:8080/public/img/menu/"
};