import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { IGame } from '../../interfaces/i-game';

/**
 * Generated class for the ModalCompanyiasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-modal-companyias',
  templateUrl: 'modal-companyias.html',
})
export class ModalCompanyiasPage {

  game: IGame = {
    id_game: -1,
    id_user: -1,
    id_console: -1,
    id_genre: -1,
    title: "",
    companyia: "",
    year: 1981,
    description: "",
    cover: "",
    region: ""
  }
  
  companyia = [
    "Sunsoft",
    "Konami",
    "Capcom",
    "SquareSoft",
    "Square-Enix",
    "Enix",
    "Taito",
    "Hudson-Soft",
    "Akklaim",
    "Nintendo",
    "Sega",
    "Jaleco",
    "Tecmo",
    "Bandai",
    "Namco",
    "Bandai-Namco",
    "Kemco",
    "Koei",
    "NIS",
    "Level 5",
    "Atlus"
  ];

  constructor(
    public navCtrl: NavController,
    public viewCtrl: ViewController,
    public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalCompanyiasPage');
  }

  closeModal() {
    this.viewCtrl.dismiss(this.game.companyia);
  }

  goBack() {
    this.navCtrl.pop();
  }
}
