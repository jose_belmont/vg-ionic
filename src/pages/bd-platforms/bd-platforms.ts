import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, ActionSheetController } from 'ionic-angular';
import { IPlatform } from '../../interfaces/i-platform';
import { GamesProvider } from '../../providers/games/games';

/**
 * Generated class for the BdPlatformsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-bd-platforms',
  templateUrl: 'bd-platforms.html',
})
export class BdPlatformsPage {

  platform: IPlatform = {
    id_platform: -1,
    name_platform: "",
    img_platform: ""
  }
  platforms: IPlatform[];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private gamesProvider: GamesProvider,
    private platforma: Platform,
    private actionSheetCtrl: ActionSheetController
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyPlatformsPage');
    this.getPlatforms();
  }

  getPlatforms() {
    this.gamesProvider.getAllPlatforms().subscribe(pl => {
      this.platforms = pl;
      console.log(this.platforms);
    });
  }

  goConsoles(plat) {
    this.navCtrl.push('BdConsolesPage', plat);
  }

  openASNewGame() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Opciones',
      cssClass: 'page-my-games-detail',
      buttons: [
        {
          text: 'Codigo de Barras',
          icon: !this.platforma.is('ios') ? 'list-box' : null,
          handler: () => {
            this.navCtrl.setRoot('BarcodePage');
          }
        },
        {
          text: 'Ir a mi Colección',
          icon: !this.platforma.is('ios') ? 'albums' : null,
          handler: () => {
            console.log('Ir a mi coleccion clicked');
            this.navCtrl.setRoot('MyPlatformsPage');
          }
        },
        {
          text: 'Crear nuevo Juego',
          icon: !this.platforma.is('ios') ? 'add' : null,
          handler: () => {
            console.log('Nuevo juego clicked');
            this.navCtrl.push('AddGamePage');
          }
        },
        {
          text: 'Cancel',
          role: 'cancel', // will always sort to be on the bottom
          icon: !this.platforma.is('ios') ? 'close' : null,
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }
}
