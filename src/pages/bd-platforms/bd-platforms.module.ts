import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BdPlatformsPage } from './bd-platforms';

@NgModule({
  declarations: [
    BdPlatformsPage,
  ],
  imports: [
    IonicPageModule.forChild(BdPlatformsPage),
  ],
})
export class BdPlatformsPageModule {}
