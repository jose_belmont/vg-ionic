export class SteamProfile {

  constructor() { }
  static getPerfil () {
    let todo =
    {
      "response": {
        "game_count": 1041,
        "games": [{
          "appid": 8000,
          "name": "Tomb Raider: Anniversary",
          "playtime_forever": 0,
          "img_icon_url": "13243b33c45bbde67ed6538641173e556548aac3",
          "img_logo_url": "8b05ccbaa0891984239414b0fdfbb6b5b4590022"
        },
        {
          "appid": 400,
          "name": "Portal",
          "playtime_forever": 0,
          "img_icon_url": "cfa928ab4119dd137e50d728e8fe703e4e970aff",
          "img_logo_url": "4184d4c0d915bd3a45210667f7b25361352acd8f",
          "has_community_visible_stats": true
        },
        {
          "appid": 12140,
          "name": "Max Payne",
          "playtime_forever": 0,
          "img_icon_url": "341d6de6e749fb2889ea046f57702f547853a464",
          "img_logo_url": "d8517c3e945349456294f450ac3fb1e53886cd5a"
        },
        {
          "appid": 12150,
          "name": "Max Payne 2: The Fall of Max Payne",
          "playtime_forever": 0,
          "img_icon_url": "4eba5db8324e043569742f7b1d50a8975b88bf52",
          "img_logo_url": "0514b7ef8e2fd6b56a9511d791fe242a886351f6"
        },
        {
          "appid": 13520,
          "name": "Far Cry",
          "playtime_forever": 0,
          "img_icon_url": "76f5a80c061ffb5553619fc2b54e6e3a44dfd0fb",
          "img_logo_url": "85cd787d6a73c2aaba39df0e53c8a75afcca6d68"
        },
        {
          "appid": 15100,
          "name": "Assassin's Creed",
          "playtime_forever": 0,
          "img_icon_url": "cd8f7a795e34e16449f7ad8d8190dce521967917",
          "img_logo_url": "5450218e6f8ea246272cddcb2ab9a453b0ca7ef5"
        },
        {
          "appid": 11230,
          "name": "Gumboy Tournament",
          "playtime_forever": 0,
          "img_icon_url": "ecafd8f5107c3892dbe6c74b4c60a0b259b83ca1",
          "img_logo_url": "aa3b7184db34e045361ca0c226bdbfc86f85aad4"
        },
        {
          "appid": 15270,
          "name": "Cold Fear",
          "playtime_forever": 0,
          "img_icon_url": "4e0e0dcc3c882910f0b4aa0e91ce4c233502cbc3",
          "img_logo_url": "a5104045a201615857cb92794d9f15bff3e2e972"
        },
        {
          "appid": 15130,
          "name": "Beyond Good & Evil",
          "playtime_forever": 0,
          "img_icon_url": "225272b8dcf7c8c107f8a95442f67f7ce40e3a1f",
          "img_logo_url": "4e149d3a7f681c9de594beee2b9bf3ea90b47de0"
        },
        {
          "appid": 15080,
          "name": "Rayman: Raving Rabbids",
          "playtime_forever": 0,
          "img_icon_url": "470f5b98ba7c81aebb4df47859df196e62475767",
          "img_logo_url": "cb4e98ca8729ae4dea749b57108d4f4daee75158"
        },
        {
          "appid": 19900,
          "name": "Far Cry 2",
          "playtime_forever": 0,
          "img_icon_url": "c73f0c967f77518128c6f2a3f0c4069ff49b1441",
          "img_logo_url": "3fa04cb86e9512e8ff4f520860db163e86243dd8"
        },
        {
          "appid": 16450,
          "name": "F.E.A.R. 2: Project Origin",
          "playtime_forever": 0,
          "img_icon_url": "6611d8b01c7a2cc3538c478c044d1e09f3140eaa",
          "img_logo_url": "41734347c3f05fe7dd797570130f5069c08f9d1b"
        },
        {
          "appid": 24420,
          "name": "Aquaria",
          "playtime_forever": 0,
          "img_icon_url": "8da675ae3499a9de4d1c4edcf395908dad0958a5",
          "img_logo_url": "d6ebe90b8bbb2eccfa28616a283e9781694cdba0",
          "has_community_visible_stats": true
        },
        {
          "appid": 17460,
          "name": "Mass Effect",
          "playtime_forever": 0,
          "img_icon_url": "57be81f70afa48c65437df93d75ba167a29687bc",
          "img_logo_url": "7501ea5009533fa5c017ec1f4b94725d67ad4936"
        },
        {
          "appid": 17470,
          "name": "Dead Space",
          "playtime_forever": 0,
          "img_icon_url": "45e682594687a6c92037631225340d26c783362f",
          "img_logo_url": "01400b8d7847c309e5d47d7ceb3f885f47247501"
        },
        {
          "appid": 17410,
          "name": "Mirror's Edge",
          "playtime_forever": 0,
          "img_icon_url": "cfea4731163004b2e5117c3b42a798c48c483d8f",
          "img_logo_url": "8c5a900802fabf20a7922c6a69cec9320c940514"
        },
        {
          "appid": 23310,
          "name": "The Last Remnant",
          "playtime_forever": 0,
          "img_icon_url": "295601b8f28db268004564eb1ff77ef6a0b23bfe",
          "img_logo_url": "8f1ba5318378e32bdd7489178e825bf581c0c207"
        },
        {
          "appid": 19000,
          "name": "Silent Hill: Homecoming",
          "playtime_forever": 0,
          "img_icon_url": "4a3250c04b4a310ee6fa1abc2996c39b28bf2b7a",
          "img_logo_url": "1b35abb6a3c02c3334e0e566d4a2eeae7b2f63c0"
        },
        {
          "appid": 27000,
          "name": "The Path",
          "playtime_forever": 0,
          "img_icon_url": "3921031bd7f10f594fb248d6378dd248c233a996",
          "img_logo_url": "382d3d4bf9ef5233c472fca7a87dc18ebc66f126"
        },
        {
          "appid": 22330,
          "name": "The Elder Scrolls IV: Oblivion ",
          "playtime_forever": 0,
          "img_icon_url": "e33402f6e1e4ed6dd001c7add26934f345cbd31e",
          "img_logo_url": "531dd052b9816ce1484a2c4b46a4f271963c527f"
        },
        {
          "appid": 1250,
          "name": "Killing Floor",
          "playtime_forever": 0,
          "img_icon_url": "d8a2d777cb4c59cf06aa244166db232336520547",
          "img_logo_url": "354c07a75cc16f6bf551b81d27f4eee3436fc2fb",
          "has_community_visible_stats": true
        },
        {
          "appid": 35420,
          "name": "Killing Floor Mod: Defence Alliance 2",
          "playtime_forever": 0,
          "img_icon_url": "ae7580a60cf77b754c723c72d5e31d530fbe7804",
          "img_logo_url": "45178d4b1999cf466fdf1ee4551f29b7e02b2bdf",
          "has_community_visible_stats": true
        },
        {
          "appid": 21680,
          "name": "Bionic Commando Rearmed",
          "playtime_forever": 0,
          "img_icon_url": "eae797bd2aec6229f9bd221f5bd9612b2fe0ae4a",
          "img_logo_url": "e4cd5c221f95bcb58518543ab17839bf30ca6a09"
        },
        {
          "appid": 35300,
          "name": "Warfare",
          "playtime_forever": 0,
          "img_icon_url": "2b027f04b2a74f9493a4c8fadfd0f7d381383514",
          "img_logo_url": "cb8780453208f99d3245e5bfda87c0c3ffbafa35"
        },
        {
          "appid": 21670,
          "name": "Bionic Commando",
          "playtime_forever": 0,
          "img_icon_url": "ec4706a0a25a9d896d508dfda9892abf647e505c",
          "img_logo_url": "e6e7dd4fd7630a1650e31a960140f27272f28950"
        },
        {
          "appid": 26800,
          "name": "Braid",
          "playtime_forever": 0,
          "img_icon_url": "1468d086e684d6a3067bd4bdc63f699fc0aede3d",
          "img_logo_url": "dc76bb847fbb82ca81e122927541398d41638ac8",
          "has_community_visible_stats": true
        },
        {
          "appid": 23380,
          "name": "Gyromancer",
          "playtime_forever": 0,
          "img_icon_url": "7b348629c3eabb017d0f49b755e9ce48d27c1e76",
          "img_logo_url": "d601ab1fb24af771e7c0368fed6b8634a725b135"
        },
        {
          "appid": 6010,
          "name": "Indiana Jones and the Fate of Atlantis",
          "playtime_forever": 0,
          "img_icon_url": "672d68bdaf3d61e8333ad644a916ed00e46920ed",
          "img_logo_url": "481019ba031ab5ee66221a034569c7d9eb08e351",
          "has_community_visible_stats": true
        },
        {
          "appid": 6040,
          "name": "The Dig",
          "playtime_forever": 0,
          "img_icon_url": "505eccf64a16a863435d676e0fc8b803bdaa983b",
          "img_logo_url": "ed476a8814963170be2ac35113fabfe4359a52f5",
          "has_community_visible_stats": true
        },
        {
          "appid": 32310,
          "name": "Indiana Jones and the Last Crusade",
          "playtime_forever": 0,
          "img_icon_url": "be7dc2a9e63cd80d2c0072c803b0cb80f6c38239",
          "img_logo_url": "a05c57d99c5a1fe433e9934f2b86ebe76a6f4944",
          "has_community_visible_stats": true
        },
        {
          "appid": 32340,
          "name": "Loom",
          "playtime_forever": 0,
          "img_icon_url": "9f14105aecd53492d06a4880a0dd89c86a2e4849",
          "img_logo_url": "3d3f64ca90dd12af8e2dc5ac44aa7fb154279ae1",
          "has_community_visible_stats": true
        },
        {
          "appid": 40700,
          "name": "Machinarium",
          "playtime_forever": 0,
          "img_icon_url": "33e11d52f0a2335671d7bf73341c14ae6e596809",
          "img_logo_url": "5101b896d69a6eafc32667689a7f5d8756a4ac87",
          "has_community_visible_stats": true
        },
        {
          "appid": 20900,
          "name": "The Witcher: Enhanced Edition",
          "playtime_forever": 0,
          "img_icon_url": "746d1cd48fb2e57d579b05b6e9eccba95859e549",
          "img_logo_url": "d37dedbc4104d1683385d022314d1316399b3dce"
        },
        {
          "appid": 17450,
          "name": "Dragon Age: Origins",
          "playtime_forever": 0,
          "img_icon_url": "8cc583d72b84ae9a4026c762e130869127dd05e4",
          "img_logo_url": "c313ab7941ba8375dd17a0531b724ef94ca54dea"
        },
        {
          "appid": 37000,
          "name": "The Void",
          "playtime_forever": 0,
          "img_icon_url": "1e298833d48581a95456a869cf11c9fcbe6b280d",
          "img_logo_url": "cbff6e1dbba3bda779f40b9c8bd2c0cbdc0af585"
        },
        {
          "appid": 37010,
          "name": "Ninja Blade",
          "playtime_forever": 0,
          "img_icon_url": "8490a7222443b3e6a485ddc308fab881c8dce6a5",
          "img_logo_url": "dcd484ea61c4cba5378676384d27f8002959009c"
        },
        {
          "appid": 37030,
          "name": "UFO: Extraterrestrials Gold",
          "playtime_forever": 0,
          "img_icon_url": "dd3541c47e407ab754d9318bb9f9c378f46e6ab9",
          "img_logo_url": "fbe76be0d48fb11decb4f6144149a26c213241e9"
        },
        {
          "appid": 500,
          "name": "Left 4 Dead",
          "playtime_forever": 0,
          "img_icon_url": "428df26bc35b09319e31b1ffb712487b20b3245c",
          "img_logo_url": "0f67ee504d8f04ecd83986dd7855821dc21f7a78",
          "has_community_visible_stats": true
        },
        {
          "appid": 550,
          "name": "Left 4 Dead 2",
          "playtime_forever": 0,
          "img_icon_url": "7d5a243f9500d2f8467312822f8af2a2928777ed",
          "img_logo_url": "205863cc21e751a576d6fff851984b3170684142",
          "has_community_visible_stats": true
        },
        {
          "appid": 223530,
          "name": "Left 4 Dead 2 Beta",
          "playtime_forever": 0,
          "img_icon_url": "",
          "img_logo_url": ""
        },
        {
          "appid": 31170,
          "name": "Tales of Monkey Island: Chapter 1 - Launch of the Screaming Narwhal",
          "playtime_forever": 0,
          "img_icon_url": "580fc2d11bc532376b6754e7dd9cae1de80ba81b",
          "img_logo_url": "bcb86eb71db655f852eb6fa91b2a14b4c6150ab8",
          "has_community_visible_stats": true
        },
        {
          "appid": 31180,
          "name": "Tales of Monkey Island: Chapter 2 - The Siege of Spinner Cay ",
          "playtime_forever": 0,
          "img_icon_url": "cfa8c2b340a17ec0e91498ff1fc334033b9d7861",
          "img_logo_url": "b5675ccdc820cdf9457cee24959cf24e2a5d06ee",
          "has_community_visible_stats": true
        },
        {
          "appid": 31190,
          "name": "Tales of Monkey Island: Chapter 3 - Lair of the Leviathan ",
          "playtime_forever": 0,
          "img_icon_url": "b093a6b3d6af137f94c76f252be7196a0cdcae5b",
          "img_logo_url": "4a41727a08fe61271cb24e86ab125ecf03db6072",
          "has_community_visible_stats": true
        },
        {
          "appid": 31200,
          "name": "Tales of Monkey Island: Chapter 4 - The Trial and Execution of Guybrush Threepwood ",
          "playtime_forever": 0,
          "img_icon_url": "56084f9b28ac7134f369c95e34d1692cb610d410",
          "img_logo_url": "04b4fda138b232a780469269c3b42a5ee4a467d7",
          "has_community_visible_stats": true
        },
        {
          "appid": 31210,
          "name": "Tales of Monkey Island: Chapter 5 - Rise of the Pirate God",
          "playtime_forever": 0,
          "img_icon_url": "2606f9f68f07f85dc208ded4576deda018f823a2",
          "img_logo_url": "3f9c6bec6d6c155a0061a99f359d766d61c6fce1",
          "has_community_visible_stats": true
        },
        {
          "appid": 32430,
          "name": "STAR WARS™: The Force Unleashed™ Ultimate Sith Edition",
          "playtime_forever": 0,
          "img_icon_url": "5a4a943e2b397aaffc6ec5f17e00c9f59e0ba88c",
          "img_logo_url": "5a545bd2a5df4e8ff82072243ebf7beff46eb977",
          "has_community_visible_stats": true
        },
        {
          "appid": 22370,
          "name": "Fallout 3 - Game of the Year Edition",
          "playtime_forever": 0,
          "img_icon_url": "21d7090bdea8f6685ca730850b7b55acfdb92732",
          "img_logo_url": "ddccc41c513694e7a5542aa115e9e091d6495420"
        },
        {
          "appid": 41700,
          "name": "S.T.A.L.K.E.R.: Call of Pripyat",
          "playtime_forever": 0,
          "img_icon_url": "60b32d190ebcaac8d2b2aaf16db2b07015616eb2",
          "img_logo_url": "93b0cfaad1110993eff5f2017d37511c3e5258a5"
        },
        {
          "appid": 24980,
          "name": "Mass Effect 2",
          "playtime_forever": 0,
          "img_icon_url": "e6f3b9b0762fd4d42a732abfc41887f6c5903a52",
          "img_logo_url": "d446fe6d77c9f434cd7fd871400b978fc01fb4e7"
        },
        {
          "appid": 33230,
          "name": "Assassin's Creed II",
          "playtime_forever": 0,
          "img_icon_url": "0492d8ee860ac99168e46efeb003029c3224cb38",
          "img_logo_url": "6d29461ee9303967cb32c2142afaf9bbdb911b6f"
        },
        {
          "appid": 35140,
          "name": "Batman: Arkham Asylum GOTY Edition",
          "playtime_forever": 0,
          "img_icon_url": "e52f91ecb0d3f20263e96fe188de1bcc8c91643e",
          "img_logo_url": "172e0928b845c18491f1a8fee0dafe7a146ac129",
          "has_community_visible_stats": true
        },
        {
          "appid": 21090,
          "name": "F.E.A.R.",
          "playtime_forever": 0,
          "img_icon_url": "71f118282be5aaa34eb82506593130ecfcc6a90b",
          "img_logo_url": "df122e0ee9eb2a5371910ffda0f8a3382e09232e"
        },
        {
          "appid": 21110,
          "name": "F.E.A.R.: Extraction Point",
          "playtime_forever": 0,
          "img_icon_url": "153d4f89ef0bd59a0039c396ff963a31d4d5e71b",
          "img_logo_url": "df122e0ee9eb2a5371910ffda0f8a3382e09232e"
        },
        {
          "appid": 21120,
          "name": "F.E.A.R.: Perseus Mandate",
          "playtime_forever": 0,
          "img_icon_url": "7b1d0271f2735ca66e1cb681eb4da1a7c985d53f",
          "img_logo_url": "df122e0ee9eb2a5371910ffda0f8a3382e09232e"
        },
        {
          "appid": 42960,
          "name": "Victoria II",
          "playtime_forever": 0,
          "img_icon_url": "1b7b3f6c7b29460acdfd8e0c79b2f55102321c8e",
          "img_logo_url": "c6da70d1455f21b993341ecca3c33976bfa67f1d"
        },
        {
          "appid": 50130,
          "name": "Mafia II",
          "playtime_forever": 0,
          "img_icon_url": "62f1f7324e520be067aec1ab06d1ec6fa56f25bd",
          "img_logo_url": "31c60ba32d7ff5b53b5d2a5795b42077279f499a",
          "has_community_visible_stats": true
        },
        {
          "appid": 45740,
          "name": "Dead Rising 2",
          "playtime_forever": 0,
          "img_icon_url": "1a8ab50763c91798622cc43eaad21960bf4d4b1a",
          "img_logo_url": "0a981863c79af886690c9f9e05a7f3a75135b2d9",
          "has_community_visible_stats": true
        },
        {
          "appid": 19030,
          "name": "Rocket Knight",
          "playtime_forever": 0,
          "img_icon_url": "8550135163d18d85c3f384852e84b9f35872277a",
          "img_logo_url": "398e22028a6936ee3d8dcf8d0ed9a3e0c9b2825c",
          "has_community_visible_stats": true
        },
        {
          "appid": 43000,
          "name": "Front Mission Evolved",
          "playtime_forever": 0,
          "img_icon_url": "bf776314cff071231b10c8f87ab8f95307809b0b",
          "img_logo_url": "6fb7c7f5ca4d4820b47cd6906ce5ca370fd1b48d",
          "has_community_visible_stats": true
        },
        {
          "appid": 32360,
          "name": "The Secret of Monkey Island: Special Edition",
          "playtime_forever": 0,
          "img_icon_url": "d0c7dfee42d8ce36806e34f078e74ce82d21314d",
          "img_logo_url": "def80542d013e251b343bd5047e640672448063d",
          "has_community_visible_stats": true
        },
        {
          "appid": 32460,
          "name": "Monkey Island 2: Special Edition",
          "playtime_forever": 0,
          "img_icon_url": "3ff954e2286123d0d4cf91dfca57e62a0b7248a9",
          "img_logo_url": "520648565e99a6a8afa4f92a55dc4a3d39400768",
          "has_community_visible_stats": true
        },
        {
          "appid": 6120,
          "name": "Shank",
          "playtime_forever": 0,
          "img_icon_url": "936571702a9ed84a6c267003b3f5b26df9ae830d",
          "img_logo_url": "b5938715bdb8479a24b3fc6b5c74936d160a5878",
          "has_community_visible_stats": true
        },
        {
          "appid": 32500,
          "name": "STAR WARS™: The Force Unleashed™ II",
          "playtime_forever": 0,
          "img_icon_url": "88a3ef4f5cd4bbad37df743818bf379aaff014e8",
          "img_logo_url": "3256757659e8d38690d382324f9f31dc64845fc8",
          "has_community_visible_stats": true
        },
        {
          "appid": 47810,
          "name": "Dragon Age: Origins - Ultimate Edition",
          "playtime_forever": 0,
          "img_icon_url": "e5e0b9629b314f7338f493d26227eb2bd1172e77",
          "img_logo_url": "aca1a363cb76d4c74d4c891407ca06693a963699"
        },
        {
          "appid": 31280,
          "name": "Poker Night at the Inventory",
          "playtime_forever": 0,
          "img_icon_url": "7d50bd1f5e7cfe68397e9ca0041836ad18153dfb",
          "img_logo_url": "d962cde096bca06ee10d09880e9f3d6257941161",
          "has_community_visible_stats": true
        },
        {
          "appid": 78000,
          "name": "Bejeweled 3",
          "playtime_forever": 0,
          "img_icon_url": "53d0a60a240a6795f86a1815fc994e843df4b58f",
          "img_logo_url": "7c4e103ce278e3bc50d7e7b33c4ddb8196f88817",
          "has_community_visible_stats": true
        },
        {
          "appid": 31290,
          "name": "Back to the Future: Ep 1 - It's About Time",
          "playtime_forever": 262,
          "img_icon_url": "a9a9b1683209e3223779ad2315e4bf03e27619d7",
          "img_logo_url": "252a22c149c29a2d93fcb0080f3ee9d2dbbd9a2f",
          "has_community_visible_stats": true
        },
        {
          "appid": 94500,
          "name": "Back to the Future: Ep 2 - Get Tannen!",
          "playtime_forever": 215,
          "img_icon_url": "d5382aedd7594e088a078341034e4b369210ec9a",
          "img_logo_url": "ae0d863314d6111e6debaef7c9cdcf2940738b1e",
          "has_community_visible_stats": true
        },
        {
          "appid": 94510,
          "name": "Back to the Future: Ep 3 - Citizen Brown",
          "playtime_forever": 157,
          "img_icon_url": "a51c4795b44f689628bd76bc64cad310385ba1a2",
          "img_logo_url": "7fe46585ad7fbc8fedb38db7f8dd8608be5a46ee",
          "has_community_visible_stats": true
        },
        {
          "appid": 94520,
          "name": "Back to the Future: Ep 4 - Double Visions",
          "playtime_forever": 203,
          "img_icon_url": "5a7fd98c15742b9fca4e443b5eaadf953a6c83d3",
          "img_logo_url": "ac9fc7c15ae656d200299003ce22ab84185e54e4",
          "has_community_visible_stats": true
        },
        {
          "appid": 94530,
          "name": "Back to the Future: Ep 5 - OUTATIME",
          "playtime_forever": 221,
          "img_icon_url": "3d034831533383bc5a58787763925660684de8c4",
          "img_logo_url": "76361dfc1ef09dbb7f6572a6da7ab38a811f54af",
          "has_community_visible_stats": true
        },
        {
          "appid": 21100,
          "name": "F.E.A.R. 3",
          "playtime_forever": 0,
          "img_icon_url": "01b73115a3ff7315d14f0bcf7beff01ef76162b4",
          "img_logo_url": "d2fcf83ec76e845ed19f4ff8324304e2981af391",
          "has_community_visible_stats": true
        },
        {
          "appid": 42910,
          "name": "Magicka",
          "playtime_forever": 0,
          "img_icon_url": "0eb97d0cd644ee08b1339d2160c7a6adf2ea0a65",
          "img_logo_url": "8c59c674ef40f59c3bafde8ff0d59b7994c66477",
          "has_community_visible_stats": true
        },
        {
          "appid": 48190,
          "name": "Assassin's Creed Brotherhood",
          "playtime_forever": 0,
          "img_icon_url": "6a87d0b61e45e8075decd1f8ace549300b036a52",
          "img_logo_url": "00991d5d48183ce4148617ff0fe9db2fabda346b"
        },
        {
          "appid": 55100,
          "name": "Homefront",
          "playtime_forever": 0,
          "img_icon_url": "52d4f669d6aa00c92406457d430ed5aa6a035c54",
          "img_logo_url": "3ed92bed83fc529adf7b0293ce70a73b70ad34ff",
          "has_community_visible_stats": true
        },
        {
          "appid": 40800,
          "name": "Super Meat Boy",
          "playtime_forever": 0,
          "img_icon_url": "64eec20c9375e7473b964f0d0bc41d19f03add3b",
          "img_logo_url": "70f084857297d5fdd96d019db3a990d6d9ec64f1",
          "has_community_visible_stats": true
        },
        {
          "appid": 95300,
          "name": "Capsized",
          "playtime_forever": 0,
          "img_icon_url": "d89b6b69c1b57bae98710d574698ffbc4445e68d",
          "img_logo_url": "4ed97f9c05ae4e0351dd5191f907adf5ddb1d356",
          "has_community_visible_stats": true
        },
        {
          "appid": 44340,
          "name": "Operation Flashpoint: Red River",
          "playtime_forever": 0,
          "img_icon_url": "f66dd400734a6c0be674b6086059073db94af4f7",
          "img_logo_url": "c2321e427db6dc9f2d3c80c285cd7a8cc98272ac"
        },
        {
          "appid": 620,
          "name": "Portal 2",
          "playtime_forever": 0,
          "img_icon_url": "2e478fc6874d06ae5baf0d147f6f21203291aa02",
          "img_logo_url": "d2a1119ddc202fab81d9b87048f495cbd6377502",
          "has_community_visible_stats": true
        },
        {
          "appid": 20920,
          "name": "The Witcher 2: Assassins of Kings Enhanced Edition",
          "playtime_forever": 0,
          "img_icon_url": "62dd5c627664df1bcabc47727c7dcd7ccab353e9",
          "img_logo_url": "f0274a91931ed39f7c69dca9f907ceae6450785c",
          "has_community_visible_stats": true
        },
        {
          "appid": 57900,
          "name": "Duke Nukem Forever",
          "playtime_forever": 0,
          "img_icon_url": "abea248e21456fae958b16dae9f131a51921199d",
          "img_logo_url": "a8d1246ad6962f0bfe9f59b1a314fcf53e712fd3",
          "has_community_visible_stats": true
        },
        {
          "appid": 58560,
          "name": "Runaway: A Twist of Fate",
          "playtime_forever": 0,
          "img_icon_url": "90f212d78f2cd40811f21b3ecf4174b2f58401bf",
          "img_logo_url": "b365f3134d4f24c0099bd311ed9f528c3a7ffef6"
        },
        {
          "appid": 92000,
          "name": "Hydrophobia: Prophecy",
          "playtime_forever": 0,
          "img_icon_url": "28a3cf9181b37f1005421e7c478e98b7f16c5cdc",
          "img_logo_url": "4dd1b0dc99cbc1b0d9d0460f9b2ace5e6d68e47a",
          "has_community_visible_stats": true
        },
        {
          "appid": 31270,
          "name": "Puzzle Agent",
          "playtime_forever": 0,
          "img_icon_url": "d0cae0b07b2512302968bd7625a9bf12cebdfba8",
          "img_logo_url": "f1bd7dd0bae1026b17c61a605a567ed68e683fef",
          "has_community_visible_stats": true
        },
        {
          "appid": 22000,
          "name": "World of Goo",
          "playtime_forever": 0,
          "img_icon_url": "fce27346192c28e854b7381f160bd19ae645f62f",
          "img_logo_url": "d2a60cb23c6743862af40267817099b08602ee92",
          "has_community_visible_stats": true
        },
        {
          "appid": 42160,
          "name": "War of the Roses",
          "playtime_forever": 0,
          "img_icon_url": "a87b69ac8257540cf378fe17dc9b6beaf7c15eee",
          "img_logo_url": "78a33bc66b2d52371ee1dc7d210c95167bd0c895",
          "has_community_visible_stats": true
        },
        {
          "appid": 206980,
          "name": "War of the Roses Balance Beta",
          "playtime_forever": 0,
          "img_icon_url": "f1bf7dd2ad1010bcf6d3b58454a402c8c179fae6",
          "img_logo_url": "edbc12973ba245bdd19d27994494ccdecbcd5520"
        },
        {
          "appid": 19680,
          "name": "Alice: Madness Returns",
          "playtime_forever": 0,
          "img_icon_url": "a9dc16a5a8293cea1c1d934bdbc08721708b1399",
          "img_logo_url": "16eb0cc15cde07377c0cb3bffa6d92bbc6dd72b2"
        },
        {
          "appid": 46500,
          "name": "Syberia",
          "playtime_forever": 0,
          "img_icon_url": "bd19a0149639c539662f8bfc6086623dcd517214",
          "img_logo_url": "907505ac9678e8c543fd44c12dbaf74ad7a972a9"
        },
        {
          "appid": 8600,
          "name": "RACE 07",
          "playtime_forever": 0,
          "img_icon_url": "4934ef713f31d50f516e713fdeb38c2a87f7bfa4",
          "img_logo_url": "2b735e9b03a2b5e72ac727a8cac0bc1b95b6027a"
        },
        {
          "appid": 44620,
          "name": "STCC II",
          "playtime_forever": 0,
          "img_icon_url": "f69db2946f486b4dffe2e999b4619ac1957fc546",
          "img_logo_url": "0b950f4974aaca9b421773a6d5ab393e3638d04f"
        },
        {
          "appid": 44630,
          "name": "RACE 07 - Formula RaceRoom Add-On",
          "playtime_forever": 0,
          "img_icon_url": "a638f775e454c77b2cc06df20049ce0a09c6794f",
          "img_logo_url": "2b03526ffc33573d715a0e7a2f2b339c9fc25a25"
        },
        {
          "appid": 44650,
          "name": "GT Power Expansion",
          "playtime_forever": 0,
          "img_icon_url": "34b989a4782e744f9156bec9fbc9e23dbdbcc6df",
          "img_logo_url": "42de9027eb1de6e1f6de74c39da2bfea1ede4d53"
        },
        {
          "appid": 44660,
          "name": "The Retro Expansion",
          "playtime_forever": 0,
          "img_icon_url": "a51f56d7637029f4165b95834b848f68085d58ef",
          "img_logo_url": "316201daa88254521efbf2e40bd13072fb422163"
        },
        {
          "appid": 44670,
          "name": "The WTCC 2010 Pack",
          "playtime_forever": 0,
          "img_icon_url": "defbbfd48b740c3f8136ef852e91f83945320c3e",
          "img_logo_url": "16231ff44b47b3fb67650cdff6ac02bc4d198143"
        },
        {
          "appid": 44680,
          "name": "Race Injection",
          "playtime_forever": 0,
          "img_icon_url": "",
          "img_logo_url": ""
        },
        {
          "appid": 107300,
          "name": "Breath of Death VII ",
          "playtime_forever": 0,
          "img_icon_url": "26685ef54eb9d6220dfba280542917fc24f5ecab",
          "img_logo_url": "252cb9c5b020cf81df91f6fe472f29c017e863fb"
        },
        {
          "appid": 107310,
          "name": "Cthulhu Saves the World ",
          "playtime_forever": 0,
          "img_icon_url": "dd3f023ea459f8fedf927a5255523b7fc9ba6ad2",
          "img_logo_url": "c21721fb881d5548bd9e9693d9c35d41f2565715"
        },
        {
          "appid": 107100,
          "name": "Bastion",
          "playtime_forever": 0,
          "img_icon_url": "8377b4460f19465c261673f76f2656bdb3288273",
          "img_logo_url": "d113d66ef88069d7d35a74cfaf2e2ee917f61133",
          "has_community_visible_stats": true
        },
        {
          "appid": 35320,
          "name": "Insane 2",
          "playtime_forever": 0,
          "img_icon_url": "e59f642906fe20bd347cee4037b97d039a9f9aab",
          "img_logo_url": "5d71166730722581ce32fb41c10991d72c2f3068",
          "has_community_visible_stats": true
        },
        {
          "appid": 8140,
          "name": "Tomb Raider: Underworld",
          "playtime_forever": 0,
          "img_icon_url": "7182f90258cee15aed17cd0318a270276c21b3b3",
          "img_logo_url": "a08097780278c2c88bd7c46ce500a7a2baef87ec"
        },
        {
          "appid": 3830,
          "name": "Psychonauts",
          "playtime_forever": 0,
          "img_icon_url": "460b6471db7d83ee6943c1a87f7a9f2898634952",
          "img_logo_url": "b361ab26b2c47d4abd11be0ebd3d6b675512ec1b",
          "has_community_visible_stats": true
        },
        {
          "appid": 115100,
          "name": "Costume Quest",
          "playtime_forever": 0,
          "img_icon_url": "ba04f2c7aa5a5c8762d088d9a9fe10d67c92e59b",
          "img_logo_url": "125ad90dba2c6c3726837ae9c1889d8009f806a9",
          "has_community_visible_stats": true
        },
        {
          "appid": 201870,
          "name": "Assassin's Creed Revelations",
          "playtime_forever": 0,
          "img_icon_url": "08f7266496718dbd5d2f93e4776d3f5dc368ad54",
          "img_logo_url": "ac2a172c79ea93d98102b15dbf5dbac8d88aea70"
        },
        {
          "appid": 9200,
          "name": "RAGE",
          "playtime_forever": 0,
          "img_icon_url": "258791dd730f3009acc4c6c1846ed75b2c3911fa",
          "img_logo_url": "db0e1951e4359d76f32cf65b24ef1fa6a819afc9",
          "has_community_visible_stats": true
        },
        {
          "appid": 110800,
          "name": "L.A. Noire",
          "playtime_forever": 0,
          "img_icon_url": "ee09d4d2552e52c78d139a3b6dbe60173079d9f8",
          "img_logo_url": "4176ba6f679e28225a95447082c38db290bc2557",
          "has_community_visible_stats": true
        },
        {
          "appid": 39160,
          "name": "Dungeon Siege III",
          "playtime_forever": 0,
          "img_icon_url": "c9dba11084b0a049b273819dba062ea89ecf3c05",
          "img_logo_url": "b0832153c35729d61575ffc71bea98e001dee41e",
          "has_community_visible_stats": true
        },
        {
          "appid": 39190,
          "name": "Dungeon Siege",
          "playtime_forever": 0,
          "img_icon_url": "93bc26efb692cca3964ec8ba424572f5e1bf0e70",
          "img_logo_url": "a990f7078b44f094834a44fdb1cc16ba54ef8828"
        },
        {
          "appid": 39200,
          "name": "Dungeon Siege 2",
          "playtime_forever": 0,
          "img_icon_url": "f5902b5bb4057129ab67ad9f202a254e1f0cc0fb",
          "img_logo_url": "6db0c68f08e9332a99e83adc2152399c9cee196b"
        },
        {
          "appid": 35700,
          "name": "Trine",
          "playtime_forever": 0,
          "img_icon_url": "298185d8ef9d688fe96a5fa15ee8174892f1c000",
          "img_logo_url": "b4cc556db2d1101effb23e576aaeeb3b799157c6",
          "has_community_visible_stats": true
        },
        {
          "appid": 201830,
          "name": "Jurassic Park: The Game",
          "playtime_forever": 0,
          "img_icon_url": "a01c4cd4cd09c140ecfc41d360abea823b36b9b4",
          "img_logo_url": "0f8e8999e32f4eb75741412034f749be63d413a4",
          "has_community_visible_stats": true
        },
        {
          "appid": 25000,
          "name": "Overgrowth",
          "playtime_forever": 0,
          "img_icon_url": "b4fc4f8ab00a1fe366b97864db80dc889d6c32d0",
          "img_logo_url": "9704ca5d2fdd68cbbabe9cd3369430b4973e337a",
          "has_community_visible_stats": true
        },
        {
          "appid": 12200,
          "name": "Bully: Scholarship Edition",
          "playtime_forever": 0,
          "img_icon_url": "791f13dd4ea6c4cdf171670cc576682171c1eae5",
          "img_logo_url": "e2aad562be7e67c2477972fa738675e005cb73df",
          "has_community_visible_stats": true
        },
        {
          "appid": 202200,
          "name": "Galactic Civilizations II: Ultimate Edition",
          "playtime_forever": 0,
          "img_icon_url": "74ef4f8f1c2b4879a54bd93b95eec8f5d20f3727",
          "img_logo_url": "ad23e69a2e91b9ad7675c11766b179e24cf8a7e1",
          "has_community_visible_stats": true
        },
        {
          "appid": 94590,
          "name": "Puzzle Agent 2",
          "playtime_forever": 0,
          "img_icon_url": "d558eb6bddeb55f5f145822f3949ab50bc02aff9",
          "img_logo_url": "b7aac2e076fb1c5178681e2cab0f8bae4380c96d",
          "has_community_visible_stats": true
        },
        {
          "appid": 67370,
          "name": "The Darkness II",
          "playtime_forever": 0,
          "img_icon_url": "6ca6291fe216a8cc5c56f01b1e7f83e396a8fa81",
          "img_logo_url": "349189b0ac9570679227cb1bd2c742bec12bf97a",
          "has_community_visible_stats": true
        },
        {
          "appid": 22380,
          "name": "Fallout: New Vegas",
          "playtime_forever": 0,
          "img_icon_url": "1711fd8c46d739feec76bd4a64eaeeca5b87e3a7",
          "img_logo_url": "1a52975c043227184162627285e4bc0c83216e02",
          "has_community_visible_stats": true
        },
        {
          "appid": 65300,
          "name": "Dustforce",
          "playtime_forever": 0,
          "img_icon_url": "7823652dcb6b11c024003ec590c17f461637c66f",
          "img_logo_url": "6b3657f05813efb8061cb67d8989118ba5c7dde3",
          "has_community_visible_stats": true
        },
        {
          "appid": 64000,
          "name": "Men of War: Assault Squad",
          "playtime_forever": 0,
          "img_icon_url": "7c6cd9e61afc348bb95f2b5a54adcd488c2efbc4",
          "img_logo_url": "7e041d717dbcb7f9805955e1410d983e1415d009",
          "has_community_visible_stats": true
        },
        {
          "appid": 48000,
          "name": "LIMBO",
          "playtime_forever": 0,
          "img_icon_url": "463f57855017564301b17050fba73804b3bd86d6",
          "img_logo_url": "9f35c3d64649a5a03b69d6a9218b1f77caf15025",
          "has_community_visible_stats": true
        },
        {
          "appid": 102840,
          "name": "Shank 2",
          "playtime_forever": 0,
          "img_icon_url": "6f916b594b1056789013a3e210cdc89a5245cd6d",
          "img_logo_url": "eec11d103112f36876bd746854d6e8f130ea6078",
          "has_community_visible_stats": true
        },
        {
          "appid": 108500,
          "name": "Vessel",
          "playtime_forever": 0,
          "img_icon_url": "34deae874c052b949f4766630b86a9d7d274d7b9",
          "img_logo_url": "086f0d6b8d1e0df72a5699d50529b964bd26bc15",
          "has_community_visible_stats": true
        },
        {
          "appid": 115110,
          "name": "Stacking",
          "playtime_forever": 0,
          "img_icon_url": "c6f9f3ea7afced3ac93e08b43c8e64aa45488648",
          "img_logo_url": "5bbd716992eab33cc5bbcd54cf691da700372329",
          "has_community_visible_stats": true
        },
        {
          "appid": 35000,
          "name": "Mini Ninjas",
          "playtime_forever": 0,
          "img_icon_url": "02f702c604e0692b8185f1b0e94e013d0a242d3a",
          "img_logo_url": "5da10ac68dbbe1e253d9aca64c4a341ed080eeaa"
        },
        {
          "appid": 98400,
          "name": "Hard Reset",
          "playtime_forever": 0,
          "img_icon_url": "7b78a792e27095d927b7a7f0a1394813bcfba177",
          "img_logo_url": "1788f94f7a69c9ed50a165101e71a6d0d64ad12c",
          "has_community_visible_stats": true
        },
        {
          "appid": 201700,
          "name": "DiRT Showdown",
          "playtime_forever": 0,
          "img_icon_url": "cc23d301fb2dcd0fcb1c442ff26ccef79fb891da",
          "img_logo_url": "531aad068bd1950bfff9c3f92cf908b58c507d89",
          "has_community_visible_stats": true
        },
        {
          "appid": 207320,
          "name": "Ys: The Oath in Felghana",
          "playtime_forever": 0,
          "img_icon_url": "0333d2c557927f6c68c8d29f23f32bd6738f76de",
          "img_logo_url": "08991748908c58ca76354e81b0f6ec1fc4311e17",
          "has_community_visible_stats": true
        },
        {
          "appid": 203750,
          "name": "Binary Domain",
          "playtime_forever": 0,
          "img_icon_url": "14de291b4d113c03ba76111b22e697a4d220d840",
          "img_logo_url": "101cf86ad6b8d8ce6328c373d23b0b09318b5a1f",
          "has_community_visible_stats": true
        },
        {
          "appid": 108710,
          "name": "Alan Wake",
          "playtime_forever": 336,
          "img_icon_url": "ec7953511aaaf5a2c2093b872b5b43c6cab56462",
          "img_logo_url": "0f9b6613ac50bf42639ed6a2e16e9b78e846ef0a",
          "has_community_visible_stats": true
        },
        {
          "appid": 207490,
          "name": "Rayman Origins",
          "playtime_forever": 0,
          "img_icon_url": "1e155c2bc13e8793aed8bb61fdac798fe0d49de7",
          "img_logo_url": "ebfd3f8da2b0416d71724a2929740a72a6eaabf4"
        },
        {
          "appid": 7000,
          "name": "Tomb Raider: Legend",
          "playtime_forever": 0,
          "img_icon_url": "df2e4400b953ab62c43ddd590684ecafd339134d",
          "img_logo_url": "a7e20f47f52a72173ee28bd29c09a459b5689744"
        },
        {
          "appid": 102500,
          "name": "Kingdoms of Amalur: Reckoning™",
          "playtime_forever": 0,
          "img_icon_url": "41b9bfd1d6b5900daea157618a4b8458ff85ba2c",
          "img_logo_url": "6d4f2d6a95423ee4b43d217d1e089fd9112f7ae7",
          "has_community_visible_stats": true
        },
        {
          "appid": 209120,
          "name": "Street Fighter X Tekken",
          "playtime_forever": 0,
          "img_icon_url": "d1074c10767068d8fcac509999edeb395b4168c8",
          "img_logo_url": "2951e1c66ae83f55c71b1f03f8b80b72093d6465"
        },
        {
          "appid": 207610,
          "name": "The Walking Dead",
          "playtime_forever": 0,
          "img_icon_url": "87a13ae0a2a76488792924aa8bb0dd8a7760f931",
          "img_logo_url": "6d756726214dd97c54966814d58508b86d5eabcf",
          "has_community_visible_stats": true
        },
        {
          "appid": 6880,
          "name": "Just Cause",
          "playtime_forever": 0,
          "img_icon_url": "aeaef41cac61d12ef93cf5eddc86859b1f0c73fa",
          "img_logo_url": "60d726f5e078a6da168414cf45b8619973f6d69c"
        },
        {
          "appid": 212030,
          "name": "Kung Fu Strike: The Warrior's Rise",
          "playtime_forever": 0,
          "img_icon_url": "97a2dcc75d66a8a2c707ba7053a67aae2bbff264",
          "img_logo_url": "f4552c6c779a565160dab3e1541b2c669ec40eae",
          "has_community_visible_stats": true
        },
        {
          "appid": 7670,
          "name": "BioShock",
          "playtime_forever": 0,
          "img_icon_url": "9a7c9f640a76e6a32592277dbbc36a0f6da05372",
          "img_logo_url": "4c2a7f97e6556a95319eb346aed7beff9fe0535c",
          "has_community_visible_stats": true
        },
        {
          "appid": 409710,
          "name": "BioShock Remastered",
          "playtime_forever": 0,
          "img_icon_url": "eb72262cd3ccc3219dd76392be3b60a4b6cbfd38",
          "img_logo_url": "cb7318b68128f6aa90c9e4e2d14544281ac22da0",
          "has_community_visible_stats": true
        },
        {
          "appid": 94300,
          "name": "The Dream Machine",
          "playtime_forever": 0,
          "img_icon_url": "6c876b1ffd82d9aeb59cdde2520c6939610e0acc",
          "img_logo_url": "0c2c70ef5cb7b9c74c994e3cf0576b406e2140cf"
        },
        {
          "appid": 94303,
          "name": "The Dream Machine: Chapter 3",
          "playtime_forever": 0,
          "img_icon_url": "901e6e30d29ccaf8c34ae656a16567e276c0ab4a",
          "img_logo_url": "ea7cbcdf1624524824d5d0d295f9c1d6fb552b98",
          "has_community_visible_stats": true
        },
        {
          "appid": 202750,
          "name": "Alan Wake's American Nightmare",
          "playtime_forever": 0,
          "img_icon_url": "313aabf37ed0b521ad969d3fe21768d31300f1ca",
          "img_logo_url": "d3593fa14e4ea8685dc6b1f71dbaa980c013ff02",
          "has_community_visible_stats": true
        },
        {
          "appid": 23700,
          "name": "Puzzle Kingdoms",
          "playtime_forever": 0,
          "img_icon_url": "3066daa0b8f8d94c9c03172a58df5c0cbaddd63a",
          "img_logo_url": "c7b44d57c6965a4b9f7efbd1809cdcbcd7f269b8"
        },
        {
          "appid": 207350,
          "name": "Ys Origin",
          "playtime_forever": 0,
          "img_icon_url": "e43656400874f67f8219d959923e52457d754aa7",
          "img_logo_url": "bccccf0670a3b306b63b881420da8e0978f94380",
          "has_community_visible_stats": true
        },
        {
          "appid": 204100,
          "name": "Max Payne 3",
          "playtime_forever": 0,
          "img_icon_url": "96af86331719b56cefc55298b4fcb99c99f1cfee",
          "img_logo_url": "135cf60686c2bf570999b96d5c29eb31e962293a",
          "has_community_visible_stats": true
        },
        {
          "appid": 12750,
          "name": "GRID",
          "playtime_forever": 0,
          "img_icon_url": "c9367af908a9527040b41f6323aee4507bcba722",
          "img_logo_url": "4d703d465e6e83f272ff07b3b9636fe52f2e9a19"
        },
        {
          "appid": 11590,
          "name": "Hospital Tycoon",
          "playtime_forever": 0,
          "img_icon_url": "b1810df56eabad54637c937d079627fe140865db",
          "img_logo_url": "9cd2384a1fca7df258ed889d956ed4178984b24d"
        },
        {
          "appid": 12830,
          "name": "Operation Flashpoint: Dragon Rising",
          "playtime_forever": 0,
          "img_icon_url": "ee4443a4f7619cf1118ec5b4ca5c191370d662f6",
          "img_logo_url": "f3b280a927511a7ff71041cf99e494ae0dd5572b"
        },
        {
          "appid": 12810,
          "name": "Overlord II",
          "playtime_forever": 0,
          "img_icon_url": "cc38122745bd44454e7e122e86023fb35e652d9d",
          "img_logo_url": "7107ed1429c4be7637571fdf262f61af6bc7d4a2"
        },
        {
          "appid": 214830,
          "name": "Half Minute Hero: Super Mega Neo Climax Ultimate Boy",
          "playtime_forever": 0,
          "img_icon_url": "6308689dd86dbfb4f18e82ab246bcdd68b46e914",
          "img_logo_url": "8a2dab9d7c5550052db46e78ea6fab7c53f016f9",
          "has_community_visible_stats": true
        },
        {
          "appid": 50300,
          "name": "Spec Ops: The Line",
          "playtime_forever": 933,
          "img_icon_url": "55ae859a90d61c08f18ed3aa0ee2579169d2c2bf",
          "img_logo_url": "e449c7dc5a9861cf94476de1b394640f3866ddda",
          "has_community_visible_stats": true
        },
        {
          "appid": 215160,
          "name": "The Book of Unwritten Tales",
          "playtime_forever": 0,
          "img_icon_url": "7970941b9969f0ba36aa8382757c5c46bb00b248",
          "img_logo_url": "93f1f5dfa8843bec47eecf8a281fcb43cb953151"
        },
        {
          "appid": 215280,
          "name": "Secret World Legends",
          "playtime_forever": 0,
          "img_icon_url": "8a5e538dc4a0a2eb2e169120cb395a4aa018e9c7",
          "img_logo_url": "58018d1b0ee71525e0f229ab5628a45eb8633a29"
        },
        {
          "appid": 113200,
          "name": "The Binding of Isaac",
          "playtime_forever": 0,
          "img_icon_url": "383cf045ca20625db18f68ef5e95169012118b9e",
          "img_logo_url": "d9a7ee7e07dffed1700cb8b3b9482105b88cc5b5",
          "has_community_visible_stats": true
        },
        {
          "appid": 40330,
          "name": "Secret Files: Tunguska",
          "playtime_forever": 0,
          "img_icon_url": "ce468c2ea19a714a3c49d4569bac55ecfedf6787",
          "img_logo_url": "c3720c98aad4034027103d8ac31a8c5d8d588d43"
        },
        {
          "appid": 216910,
          "name": "Of Orcs And Men",
          "playtime_forever": 0,
          "img_icon_url": "2c86d840b6f7750e264cb79147831ac9f4c36e8e",
          "img_logo_url": "7ac5fb8c0c389b442f579f34756eb3c9041e7e79",
          "has_community_visible_stats": true
        },
        {
          "appid": 214340,
          "name": "Deponia",
          "playtime_forever": 497,
          "img_icon_url": "48a94d33cae6065b0bedf7eefd01bd5321c7c729",
          "img_logo_url": "12ea414ebaaed18950a4454fd770065bc1cef38e",
          "has_community_visible_stats": true
        },
        {
          "appid": 211050,
          "name": "Battle vs Chess",
          "playtime_forever": 0,
          "img_icon_url": "d92c3df0ee7ac6cace3500936e08ec54d4b6e0d3",
          "img_logo_url": "414f45296e2bada3bd7d5283a090883bed4a4a3f"
        },
        {
          "appid": 209080,
          "name": "Guns of Icarus Online",
          "playtime_forever": 0,
          "img_icon_url": "968e8c0b7a55f0229392278123dfd486140c9421",
          "img_logo_url": "925b7c2a6b698144622bed4bd2d0086114df2fa9",
          "has_community_visible_stats": true
        },
        {
          "appid": 8330,
          "name": "Telltale Texas Hold'Em",
          "playtime_forever": 0,
          "img_icon_url": "4d89e4c932d44e3d5c70d54618a99f3b7f844d0f",
          "img_logo_url": "fcf8e12a4ee0897ab1a4acd12c0441e33404b236",
          "has_community_visible_stats": true
        },
        {
          "appid": 94600,
          "name": "Hector: Ep 1",
          "playtime_forever": 0,
          "img_icon_url": "aa0a113e80b8cdeff47a523a87dd2fad1f43d73e",
          "img_logo_url": "fae3324a5c43647dda98be23bf1db4480c031273",
          "has_community_visible_stats": true
        },
        {
          "appid": 94610,
          "name": "Hector: Ep 2",
          "playtime_forever": 0,
          "img_icon_url": "0bfad503074efc1c46755d05d36be4755945f8fe",
          "img_logo_url": "52a3e61842a22bf75f6bf6d355400d5b3776f9b4",
          "has_community_visible_stats": true
        },
        {
          "appid": 94620,
          "name": "Hector: Ep 3",
          "playtime_forever": 0,
          "img_icon_url": "aa0a113e80b8cdeff47a523a87dd2fad1f43d73e",
          "img_logo_url": "97ec609986c7672a3928e7dca14d9aead4cbf65f",
          "has_community_visible_stats": true
        },
        {
          "appid": 8200,
          "name": "Sam & Max 101: Culture Shock",
          "playtime_forever": 0,
          "img_icon_url": "5a50b3323f27700cfc9819aaffce64163e7baf71",
          "img_logo_url": "cc27cb22a900a47776f7e8f2339445e56c4203de",
          "has_community_visible_stats": true
        },
        {
          "appid": 8210,
          "name": "Sam & Max 102: Situation: Comedy",
          "playtime_forever": 0,
          "img_icon_url": "3a147dba083ba6afef23b62a663ffc400fd02af4",
          "img_logo_url": "f056c93b54900791e615fa76f6715e02c6f1ec5a",
          "has_community_visible_stats": true
        },
        {
          "appid": 8220,
          "name": "Sam & Max 103: The Mole, the Mob and the Meatball",
          "playtime_forever": 0,
          "img_icon_url": "4597164a33a872bda12690f3381e341b5be3ed88",
          "img_logo_url": "7001e776773c44d4d58fd39bcbd11e0ae90f1707",
          "has_community_visible_stats": true
        },
        {
          "appid": 8240,
          "name": "Sam & Max 105: Reality 2.0",
          "playtime_forever": 0,
          "img_icon_url": "71f6aa0229ef7811891da0d329efc66a1ff50a57",
          "img_logo_url": "d58dd23de608345e82a25a4d68f30462de05bddb",
          "has_community_visible_stats": true
        },
        {
          "appid": 8250,
          "name": "Sam & Max 106: Bright Side of the Moon",
          "playtime_forever": 0,
          "img_icon_url": "b990384aaed21561b980f639e4304adfe94119c8",
          "img_logo_url": "29e3eb9508f45d1af8103fd7d5a0be84ab98090d",
          "has_community_visible_stats": true
        },
        {
          "appid": 8260,
          "name": "Sam & Max 201: Ice Station Santa",
          "playtime_forever": 0,
          "img_icon_url": "883cbc08a6b43e4700beaa5b45d38cd37e67287a",
          "img_logo_url": "72af33c394ef67586cd4344a8463df9628846f9d",
          "has_community_visible_stats": true
        },
        {
          "appid": 8270,
          "name": "Sam & Max 202: Moai Better Blues",
          "playtime_forever": 0,
          "img_icon_url": "e3cc48139012d31287b6fe7fe977a3f995cb1c3b",
          "img_logo_url": "64b7c8196c8c0ef7dc97692673a5b8d810a409b4",
          "has_community_visible_stats": true
        },
        {
          "appid": 8280,
          "name": "Sam & Max 203: Night of the Raving Dead",
          "playtime_forever": 0,
          "img_icon_url": "81d700cbfe6a6efa8d638f82c99e9f7468fa2250",
          "img_logo_url": "f654237d787fdabd4ce78704f1817ef05c58f75c",
          "has_community_visible_stats": true
        },
        {
          "appid": 8290,
          "name": "Sam & Max 204: Chariots of the Dogs",
          "playtime_forever": 0,
          "img_icon_url": "a953f143763395a867c4fec7abe03ec89120c1df",
          "img_logo_url": "083183d7b58c45688ccf49e91a7aa980fc079749",
          "has_community_visible_stats": true
        },
        {
          "appid": 8300,
          "name": "Sam & Max 205: What's New Beelzebub?",
          "playtime_forever": 0,
          "img_icon_url": "ff4df5dbfcc2ca9d6fbdb5e5d37d48bce74e943d",
          "img_logo_url": "4ee9cc1ff6b98471908579261933b62f70131e82",
          "has_community_visible_stats": true
        },
        {
          "appid": 31220,
          "name": "Sam & Max 301: The Penal Zone",
          "playtime_forever": 0,
          "img_icon_url": "2c9c4ac6dfa50c4c479b6b436f04974a372588f7",
          "img_logo_url": "517196c999fe6316134332e749782154bde9adf5",
          "has_community_visible_stats": true
        },
        {
          "appid": 31230,
          "name": "Sam & Max 302: The Tomb of Sammun-Mak",
          "playtime_forever": 0,
          "img_icon_url": "e83fbb799f46b349586ca55fcf612350cc88ffe7",
          "img_logo_url": "517196c999fe6316134332e749782154bde9adf5",
          "has_community_visible_stats": true
        },
        {
          "appid": 31240,
          "name": "Sam & Max 303: They Stole Max's Brain!",
          "playtime_forever": 0,
          "img_icon_url": "bf1ebfe347a80e2ac31577e0569de3aa201cd17f",
          "img_logo_url": "517196c999fe6316134332e749782154bde9adf5",
          "has_community_visible_stats": true
        },
        {
          "appid": 31250,
          "name": "Sam & Max 304: Beyond the Alley of the Dolls",
          "playtime_forever": 0,
          "img_icon_url": "d4f834ac9d48cd59645f453d0cb30655dee6f629",
          "img_logo_url": "62ef5af2ce55bb787ff490126c110c41131043bc",
          "has_community_visible_stats": true
        },
        {
          "appid": 31260,
          "name": "Sam & Max 305: The City that Dares not Sleep",
          "playtime_forever": 0,
          "img_icon_url": "9e41c6d0c777cd0a7fb4e96da4f20d2227841725",
          "img_logo_url": "62ef5af2ce55bb787ff490126c110c41131043bc",
          "has_community_visible_stats": true
        },
        {
          "appid": 8310,
          "name": "Bone: Out from Boneville",
          "playtime_forever": 0,
          "img_icon_url": "874f73d9ea6699ebc230288ee903f1ac5a27c1a1",
          "img_logo_url": "ed8b7228de22213b6d1798c7743e3468107107e6",
          "has_community_visible_stats": true
        },
        {
          "appid": 8320,
          "name": "Bone: The Great Cow Race",
          "playtime_forever": 0,
          "img_icon_url": "dc41c95dbe719e8a8b92235558bfb973c230191f",
          "img_logo_url": "0a74278075a33e8ffedbe27e1cf068c51a47a971",
          "has_community_visible_stats": true
        },
        {
          "appid": 208750,
          "name": "Apotheon",
          "playtime_forever": 327,
          "img_icon_url": "fbc158aabfc55bbfd24d3a7c2f6bc1a2aa63c838",
          "img_logo_url": "8002a4ffea96fa323246e0e68c9a1eb053bf91ab",
          "has_community_visible_stats": true
        },
        {
          "appid": 33610,
          "name": "Broken Sword 3 - the Sleeping Dragon",
          "playtime_forever": 0,
          "img_icon_url": "0bc866d53e83fe65fe1765af3af53899ec9876fd",
          "img_logo_url": "beb03adaadfef9e5356e9fc2032b4c27594fc264"
        },
        {
          "appid": 33600,
          "name": "Broken Sword 2 - the Smoking Mirror: Remastered",
          "playtime_forever": 0,
          "img_icon_url": "e7a984c090029be3f0ce0ac70de5d280882cc055",
          "img_logo_url": "31ea8ee398fb2a53ca51bde0ffa882e8096d329a"
        },
        {
          "appid": 11450,
          "name": "Overlord",
          "playtime_forever": 0,
          "img_icon_url": "8fc55092acac2cb5f72dbf8f644fcd430c461dd6",
          "img_logo_url": "0700d43ef52f5c83b5794a8984ce0ecf34169465"
        },
        {
          "appid": 200260,
          "name": "Batman: Arkham City GOTY",
          "playtime_forever": 0,
          "img_icon_url": "746ecf3ce44b2525eb7ad643e76a3b60913d2662",
          "img_logo_url": "9b229e12fd5ce27bd101d5862c19b1a6e3d01239",
          "has_community_visible_stats": true
        },
        {
          "appid": 218640,
          "name": "Lucius",
          "playtime_forever": 0,
          "img_icon_url": "467db011e0b4c1159777829746688eed7cf80f1d",
          "img_logo_url": "53557469e2a6ef3617debf9180f27283e331c702",
          "has_community_visible_stats": true
        },
        {
          "appid": 220200,
          "name": "Kerbal Space Program",
          "playtime_forever": 0,
          "img_icon_url": "6dc8c1377c6b0ffedaeaec59c253f8c33fb3e62b",
          "img_logo_url": "52c3e31754351fd276206aa7232972961fae0295"
        },
        {
          "appid": 200710,
          "name": "Torchlight II",
          "playtime_forever": 0,
          "img_icon_url": "40776762bb63c4eded37d1a2b4431a90aa57ea84",
          "img_logo_url": "fd37abb86628ff54ed304f75c2fb7cf75a4f6902",
          "has_community_visible_stats": true
        },
        {
          "appid": 206440,
          "name": "To the Moon",
          "playtime_forever": 316,
          "img_icon_url": "6e29eb4076a6253fdbccb987a2a21746d2df54d7",
          "img_logo_url": "f0e5a7037facd7bff7656ebe2396a23735c608c2",
          "has_community_visible_stats": true
        },
        {
          "appid": 204360,
          "name": "Castle Crashers",
          "playtime_forever": 0,
          "img_icon_url": "9b7625f9b70f103397fd0416fd92abb583db8659",
          "img_logo_url": "793216ec14c639bdaf2f0119c8cc408b8e9ad7b1",
          "has_community_visible_stats": true
        },
        {
          "appid": 219910,
          "name": "Edna & Harvey: Harvey's New Eyes",
          "playtime_forever": 0,
          "img_icon_url": "df160221c5be01797610ac2e452404c01e968b59",
          "img_logo_url": "6c9e9d249c6b516b2e6faa3a50a074b7ea47d1c3",
          "has_community_visible_stats": true
        },
        {
          "appid": 7510,
          "name": "X-Blades",
          "playtime_forever": 0,
          "img_icon_url": "311b4363fd29a55fbdd23f515e409785b520b6e8",
          "img_logo_url": "85bb604055343baeae32c8afe9bc51107bb31318"
        },
        {
          "appid": 7220,
          "name": "Runaway: The Dream of the Turtle",
          "playtime_forever": 0,
          "img_icon_url": "efb43b65ab9e2cf6e5514e2e88267a9ee491c26a",
          "img_logo_url": "d164b973bc9525770c24e5474aaf55d4ec755ff8"
        },
        {
          "appid": 223470,
          "name": "POSTAL 2",
          "playtime_forever": 0,
          "img_icon_url": "9f491f765476222da4ef508a5dd38db4787da5b8",
          "img_logo_url": "ec19963d4431918601eee5198bafcd021259136d",
          "has_community_visible_stats": true
        },
        {
          "appid": 220740,
          "name": "Chaos on Deponia",
          "playtime_forever": 638,
          "img_icon_url": "6366cdcc4c5cf0fdecea4d1906ac14854274328a",
          "img_logo_url": "7c03c2c98458e168cb7359940fb064d26639c4e4",
          "has_community_visible_stats": true
        },
        {
          "appid": 219150,
          "name": "Hotline Miami",
          "playtime_forever": 0,
          "img_icon_url": "d5bf432ec0975cbd26282389575858df9730364f",
          "img_logo_url": "540a1457099f072ced7153239861e42f14febd56",
          "has_community_visible_stats": true
        },
        {
          "appid": 223220,
          "name": "Giana Sisters: Twisted Dreams",
          "playtime_forever": 0,
          "img_icon_url": "ac543243f2541e7a7728bf2165c5f3ebc57679fb",
          "img_logo_url": "9c8baddbab7938b5b995843d36526a30bd12bb1d",
          "has_community_visible_stats": true
        },
        {
          "appid": 57690,
          "name": "Tropico 4",
          "playtime_forever": 0,
          "img_icon_url": "8e58ca48b978439ba4986938247fdb21a645a673",
          "img_logo_url": "9123d91a47399baf116c2949dcebcd42b1962fbe",
          "has_community_visible_stats": true
        },
        {
          "appid": 214170,
          "name": "Divine Divinity",
          "playtime_forever": 0,
          "img_icon_url": "3c998935a3614a463b4af6f02b856ea80a7550f1",
          "img_logo_url": "1b8e78e613544e01b2df4756fdd62266c2fb306e"
        },
        {
          "appid": 219760,
          "name": "Beyond Divinity",
          "playtime_forever": 0,
          "img_icon_url": "1b292705395f5d11cba06cedbba1a3083e1381d3",
          "img_logo_url": "fc8d6cacb76a5a9b5ca5cc8482531e57790a246e"
        },
        {
          "appid": 219780,
          "name": "Divinity II: Developer's Cut",
          "playtime_forever": 0,
          "img_icon_url": "e0dae7e1b381620b7265023f350b169990da1abf",
          "img_logo_url": "4f27f30ba5b89a80f162072e824d2255752945ad"
        },
        {
          "appid": 218740,
          "name": "Pid ",
          "playtime_forever": 0,
          "img_icon_url": "3591b01899e540f5996d826ede66dc0ede3d5937",
          "img_logo_url": "5fe67ade3a6f7ca843dd68691a0c61846bc65656",
          "has_community_visible_stats": true
        },
        {
          "appid": 1900,
          "name": "Earth 2160",
          "playtime_forever": 0,
          "img_icon_url": "0a721907de90582dd4a53b55a2f260df19e2c72b",
          "img_logo_url": "15b0300cc7774ce4c08f7f98bdfd4f2eeb142513"
        },
        {
          "appid": 23300,
          "name": "Yosumin!",
          "playtime_forever": 0,
          "img_icon_url": "bb8d4fa0fa3d81550e348569966557bd470840ec",
          "img_logo_url": "61abf68e5c605f0ddace0f086ad410a1ce951f98"
        },
        {
          "appid": 208480,
          "name": "Assassin's Creed® III",
          "playtime_forever": 0,
          "img_icon_url": "5d13ffd781fa7658903fbd762b51a76f8c734d2b",
          "img_logo_url": "81f8b16b6f248f18e808967ce3ea19fdfed668f1",
          "has_community_visible_stats": true
        },
        {
          "appid": 220240,
          "name": "Far Cry® 3",
          "playtime_forever": 0,
          "img_icon_url": "8231f43d3dd47fb4b4111fd6bd4a849e73eb0520",
          "img_logo_url": "47811cc7821070306a24622b5d33c04c85264279"
        },
        {
          "appid": 4500,
          "name": "S.T.A.L.K.E.R.: Shadow of Chernobyl",
          "playtime_forever": 0,
          "img_icon_url": "c57f5fdde74464aed0a09c2e5dd41f8973cbee8d",
          "img_logo_url": "a4f810cb3cbfa8562493e6d9b4fa0afb9706aeb7"
        },
        {
          "appid": 224960,
          "name": "Tomb Raider I",
          "playtime_forever": 0,
          "img_icon_url": "ba7d3a70fa6300f72bef961595addd1ff6b17a36",
          "img_logo_url": "8d6c3b57af6cd63228aef85b98dab3a5e1470b76"
        },
        {
          "appid": 225300,
          "name": "Tomb Raider II",
          "playtime_forever": 0,
          "img_icon_url": "",
          "img_logo_url": "16ade9bf6146dcdb35a1e88372001f7f1d7b5b51"
        },
        {
          "appid": 225320,
          "name": "Tomb Raider III: Adventures of Lara Croft",
          "playtime_forever": 0,
          "img_icon_url": "",
          "img_logo_url": "abea79007994d6150d8e5589e2731f469ff1250f"
        },
        {
          "appid": 1930,
          "name": "Two Worlds: Epic Edition",
          "playtime_forever": 0,
          "img_icon_url": "f20291118f8abb2c4d50579801b908720b7d2651",
          "img_logo_url": "57094b687bdeaef65791bf2b710c8c6a11189359"
        },
        {
          "appid": 7520,
          "name": "Two Worlds II",
          "playtime_forever": 0,
          "img_icon_url": "998eb03dc48a266da0f5e4fd3a1eb735a2baf7fc",
          "img_logo_url": "a579b50f29893c4b490715fa15e493e22b30f148",
          "has_community_visible_stats": true
        },
        {
          "appid": 7530,
          "name": "Two Worlds II Castle Defense",
          "playtime_forever": 0,
          "img_icon_url": "377469c518dbb8f889a544c5adf7a1066371cded",
          "img_logo_url": "3f78d240f16be86abce7d883dc9b986def0769e3"
        },
        {
          "appid": 18470,
          "name": "Helldorado",
          "playtime_forever": 0,
          "img_icon_url": "1d40393811f96e9aeabb671786d6633b016eb575",
          "img_logo_url": "52fd3c477b22430c0eeab44141cb27daa90cff3c"
        },
        {
          "appid": 105000,
          "name": "A New Beginning - Final Cut",
          "playtime_forever": 0,
          "img_icon_url": "d25d09485324fe08eba3be64539ce3007e0cb683",
          "img_logo_url": "7ff3892e29ac321921927ec58b9b554ea79d4385",
          "has_community_visible_stats": true
        },
        {
          "appid": 113020,
          "name": "Monaco",
          "playtime_forever": 0,
          "img_icon_url": "169e693854d9f70984987fa3b6b5f1f9eb27122a",
          "img_logo_url": "60642017342ab7f3c924ee9d13b99702a50e5d9e",
          "has_community_visible_stats": true
        },
        {
          "appid": 34010,
          "name": "Alpha Protocol",
          "playtime_forever": 0,
          "img_icon_url": "2ac0dd4f55d09d6d20cdb21b03116dac6fe48164",
          "img_logo_url": "3ea9dae28f71c9535229dc23a594734497307fa9"
        },
        {
          "appid": 220440,
          "name": "DmC Devil May Cry",
          "playtime_forever": 0,
          "img_icon_url": "1fcf4c7d96acb1cdf2b30fb40a02030ba6991e2d",
          "img_logo_url": "2ab620349a9efef60523e04f9d0fd9f8bb807e0e",
          "has_community_visible_stats": true
        },
        {
          "appid": 65800,
          "name": "Dungeon Defenders",
          "playtime_forever": 0,
          "img_icon_url": "8de7e7e9af523591c34b713b0b21910058ab4169",
          "img_logo_url": "71af270cff61ab197f9932212012134a436d9682",
          "has_community_visible_stats": true
        },
        {
          "appid": 200900,
          "name": "Cave Story+",
          "playtime_forever": 366,
          "img_icon_url": "076431cdb43f9a04364076bf13f7010d8d2e0670",
          "img_logo_url": "a242e0465a65ffafbf75eeb521812fb575990a33",
          "has_community_visible_stats": true
        },
        {
          "appid": 50620,
          "name": "Darksiders",
          "playtime_forever": 0,
          "img_icon_url": "e429cee10d864faf2aae2ea9cd75e8e1942fbe08",
          "img_logo_url": "14bd29bc9b291081b63258e3bfbbf5bb655c2347",
          "has_community_visible_stats": true
        },
        {
          "appid": 50650,
          "name": "Darksiders II",
          "playtime_forever": 0,
          "img_icon_url": "a2d5549090144f1bfd9e00f1b460c1ad0aa9c366",
          "img_logo_url": "b0b8edfa57f332dc529c04b4dd2f5475227e71ac",
          "has_community_visible_stats": true
        },
        {
          "appid": 388410,
          "name": "Darksiders II Deathinitive Edition",
          "playtime_forever": 0,
          "img_icon_url": "a2d5549090144f1bfd9e00f1b460c1ad0aa9c366",
          "img_logo_url": "56b6070f07938b73cbf8b80c5b53b7245a832bce",
          "has_community_visible_stats": true
        },
        {
          "appid": 462780,
          "name": "Darksiders Warmastered Edition",
          "playtime_forever": 0,
          "img_icon_url": "4616a0d94eb5864f2933fd0157bb60a27b14d5fe",
          "img_logo_url": "f188a32787983ef5346157473a4d8ba0ed024d98",
          "has_community_visible_stats": true
        },
        {
          "appid": 7210,
          "name": "Runaway: A Road Adventure",
          "playtime_forever": 0,
          "img_icon_url": "d13fcf2099c5c7b671cfcfa27726ccdc0207fd03",
          "img_logo_url": "0d3940ab7008e93972340536385d99d66fe2e1bd"
        },
        {
          "appid": 228260,
          "name": "Fallen Enchantress: Legendary Heroes",
          "playtime_forever": 0,
          "img_icon_url": "240d87e806b31707ad649d01ae509018abc112ec",
          "img_logo_url": "98e69c7b9cba21a9f744c298fd996e3d3bb2e4bc",
          "has_community_visible_stats": true
        },
        {
          "appid": 35720,
          "name": "Trine 2",
          "playtime_forever": 0,
          "img_icon_url": "061ecbbd7c70ae1c052377bad136c7759cbb708d",
          "img_logo_url": "7d7c3b93bd85ad1db2a07f6cca01a767069c6407",
          "has_community_visible_stats": true
        },
        {
          "appid": 221810,
          "name": "The Cave",
          "playtime_forever": 0,
          "img_icon_url": "5f4b8ad75a3da9759359149b762008cf290da6ad",
          "img_logo_url": "acc11ba3303f4c706adccc75a1ee4fe28dc52727",
          "has_community_visible_stats": true
        },
        {
          "appid": 219200,
          "name": "Droid Assault",
          "playtime_forever": 0,
          "img_icon_url": "e88dc9ebde8728b95ba07dd55c3f5389dee39d5b",
          "img_logo_url": "20cabef5c38c8e4e075ee002894c9ef4920a5580",
          "has_community_visible_stats": true
        },
        {
          "appid": 220090,
          "name": "The Journey Down: Chapter One",
          "playtime_forever": 0,
          "img_icon_url": "e657a06c3c4e903a9d5bc1975ce7aa70379d5fe2",
          "img_logo_url": "4a1e3a25e7c1c0c51f0c72e3f0c7fe030b84dd5c",
          "has_community_visible_stats": true
        },
        {
          "appid": 229520,
          "name": "Dungeon Hearts",
          "playtime_forever": 0,
          "img_icon_url": "9572afb6138be87eecdb63c5aa67a8b43f015247",
          "img_logo_url": "627a0f06e20c656ef36d2e9fcdf7b236881aa260",
          "has_community_visible_stats": true
        },
        {
          "appid": 38210,
          "name": "Roogoo",
          "playtime_forever": 0,
          "img_icon_url": "e32d836ab4760287bc8cb7982068fe6065eb406b",
          "img_logo_url": "97627b5c80c8e230fbb0c7111fc7cec32e0ba702"
        },
        {
          "appid": 203830,
          "name": "The Dark Eye: Chains of Satinav",
          "playtime_forever": 0,
          "img_icon_url": "d5a6da1a5d7101ca86bbb45e25de6d0463a2b62c",
          "img_logo_url": "2c255278bbd5c10771a8760cfd8f3ce08a595300",
          "has_community_visible_stats": true
        },
        {
          "appid": 219890,
          "name": "Antichamber",
          "playtime_forever": 325,
          "img_icon_url": "1c3afe41623c51172ef1dc96427bf23bb940748b",
          "img_logo_url": "23966982a5795854342af3522706c7f9c6a83cb5"
        },
        {
          "appid": 223810,
          "name": "Ys I",
          "playtime_forever": 0,
          "img_icon_url": "d749d1b847cd218b2f013dabf5587d7f1a16356d",
          "img_logo_url": "f35de56358c405b85bee757d0ff0990aed8714a3",
          "has_community_visible_stats": true
        },
        {
          "appid": 223870,
          "name": "Ys II",
          "playtime_forever": 0,
          "img_icon_url": "0b625753d0ea120c5e2c3ecf52d57151df3b58e4",
          "img_logo_url": "1ad3b3fa7a4842a83c042e92bd9e4f5f468908de",
          "has_community_visible_stats": true
        },
        {
          "appid": 238070,
          "name": "Shadow Warrior Classic (1997)",
          "playtime_forever": 0,
          "img_icon_url": "c14baa163827ff7fd44a71bb64d81b3e8085d602",
          "img_logo_url": "20d631b0a235642d31965fe395c6abef6e14d1b0",
          "has_community_visible_stats": true
        },
        {
          "appid": 286100,
          "name": "You Have to Win the Game",
          "playtime_forever": 0,
          "img_icon_url": "26628aeaa64d8827918cd2e8abefa2a27bb29d28",
          "img_logo_url": "bb0e5d132bbfa7864e2fd94beb8f8a862db6d88e",
          "has_community_visible_stats": true
        },
        {
          "appid": 231200,
          "name": "Kentucky Route Zero",
          "playtime_forever": 0,
          "img_icon_url": "b20e61960b1b1fdab3073026336719bfa28a8cdd",
          "img_logo_url": "47db5498046a3e6774bbc0c443c9f42eaa3f39f6"
        },
        {
          "appid": 221040,
          "name": "Resident Evil 6 / Biohazard 6",
          "playtime_forever": 0,
          "img_icon_url": "0a0832a59b6c0a4ffa995b0343892729fe61ec1b",
          "img_logo_url": "2398457399f20b979f0e5d0641262560a72408d5",
          "has_community_visible_stats": true
        },
        {
          "appid": 225260,
          "name": "Brütal Legend",
          "playtime_forever": 0,
          "img_icon_url": "e3f25fba8538e5fb1ead751e767c2774df4fb0b4",
          "img_logo_url": "cc8b60ac1fa649c950ff7a9881b98709b8372f94",
          "has_community_visible_stats": true
        },
        {
          "appid": 231910,
          "name": "Leisure Suit Larry in the Land of the Lounge Lizards: Reloaded",
          "playtime_forever": 0,
          "img_icon_url": "49675bfb000c18dd88216f22ed081f35437ee004",
          "img_logo_url": "35e8af8a0f5d7a8c402642d2d0c9e156d2dc7122",
          "has_community_visible_stats": true
        },
        {
          "appid": 203250,
          "name": "Star Trek",
          "playtime_forever": 0,
          "img_icon_url": "d057d217c23766dcdd8e333a4706d4a63ec00643",
          "img_logo_url": "fb78be16107d75645264e7dfabeb371812e3ce24",
          "has_community_visible_stats": true
        },
        {
          "appid": 217920,
          "name": "Alien Rage - Unlimited",
          "playtime_forever": 0,
          "img_icon_url": "aa52645539c5ff66296303f65310bb89eaf15f44",
          "img_logo_url": "474952ecf2d3f0fd178ba446253dc22fba23598c",
          "has_community_visible_stats": true
        },
        {
          "appid": 9870,
          "name": "Ghostbusters: The Video Game",
          "playtime_forever": 0,
          "img_icon_url": "bf97b63302c33ab80787095e60b49bfd415253a2",
          "img_logo_url": "d38866acc5d1beb22e4763695e51fa3d22e454f1"
        },
        {
          "appid": 21690,
          "name": "Resident Evil 5 / Biohazard 5",
          "playtime_forever": 0,
          "img_icon_url": "26108f5caff3638c9f522dd79ee84a12761f373a",
          "img_logo_url": "e277ab70fff98bb2300a39bf8e2371a746fe50b1",
          "has_community_visible_stats": true
        },
        {
          "appid": 8870,
          "name": "BioShock Infinite",
          "playtime_forever": 0,
          "img_icon_url": "4ebaf5f9ee74f50152f7ff361debef7553fa0e4e",
          "img_logo_url": "870bb889e192cf8d31876ed04d329a5d51c6fc2c",
          "has_community_visible_stats": true
        },
        {
          "appid": 234900,
          "name": "Anodyne",
          "playtime_forever": 331,
          "img_icon_url": "b1af088db2d3ce10061dd8b448ff47bf39cfe74c",
          "img_logo_url": "ce8d28352178a7c4911f22000bce4cc76f58c80d",
          "has_community_visible_stats": true
        },
        {
          "appid": 207930,
          "name": "Sacred Citadel",
          "playtime_forever": 0,
          "img_icon_url": "86ba4a65a016575fec95f10717e0e37cc4771401",
          "img_logo_url": "ac6b326412f4000453e789a8c3a104145728836c",
          "has_community_visible_stats": true
        },
        {
          "appid": 233470,
          "name": "Evoland",
          "playtime_forever": 0,
          "img_icon_url": "b43008e5cf72a1a3a239b63d4112e304c5bdb8fd",
          "img_logo_url": "3432d5d501ca6cf01eebb98a43d927537d42134e",
          "has_community_visible_stats": true
        },
        {
          "appid": 44350,
          "name": "GRID 2",
          "playtime_forever": 0,
          "img_icon_url": "78dfd2533e0ee4cdf981cf13fbdea9596f1ed433",
          "img_logo_url": "059c8f0e1995b134f78deaf93b0a352db58ba0e1",
          "has_community_visible_stats": true
        },
        {
          "appid": 230700,
          "name": "La-Mulana",
          "playtime_forever": 0,
          "img_icon_url": "4c10ebd5c3071b06920b464bfca327de6a55f845",
          "img_logo_url": "75d5231594f6c50b9118abfe6b7d067026459b0d",
          "has_community_visible_stats": true
        },
        {
          "appid": 210770,
          "name": "Sanctum 2",
          "playtime_forever": 0,
          "img_icon_url": "4cdfa1d19be460374a111b718ce3a204f21ea1dc",
          "img_logo_url": "333a8c65480bb85148bb3a185843a8520ae5d90f",
          "has_community_visible_stats": true
        },
        {
          "appid": 222480,
          "name": "Resident Evil Revelations / Biohazard Revelations",
          "playtime_forever": 0,
          "img_icon_url": "5725845fe83f846a04135034c5be55aef008c725",
          "img_logo_url": "cf242207e0e06251f55baf2f89c37ded12c79329",
          "has_community_visible_stats": true
        },
        {
          "appid": 224760,
          "name": "FEZ",
          "playtime_forever": 0,
          "img_icon_url": "900590f739d69da4f50112669f5d949a2e6b9261",
          "img_logo_url": "d2789dc5fb6bfee4d07cd3ec06985593fffd606c",
          "has_community_visible_stats": true
        },
        {
          "appid": 34920,
          "name": "Razor2: Hidden Skies",
          "playtime_forever": 0,
          "img_icon_url": "0754285eb191877740c10c3cef0401de3fe71df9",
          "img_logo_url": "f197a0a02c22812f2658d654bafe86c29c58d093",
          "has_community_visible_stats": true
        },
        {
          "appid": 234650,
          "name": "Shadowrun Returns",
          "playtime_forever": 0,
          "img_icon_url": "13c8e62b9b079fba385a2d959ed4e8ce2c378c8c",
          "img_logo_url": "d2ada23f204adf831dc89be7c6edbbbc38c99ef6",
          "has_community_visible_stats": true
        },
        {
          "appid": 204450,
          "name": "Call of Juarez Gunslinger",
          "playtime_forever": 0,
          "img_icon_url": "d1183204f15b5d86f1f7bfcaaf358a017723c34d",
          "img_logo_url": "643f4290bce8f5bd18d24c8c1e85d4265767b3c9",
          "has_community_visible_stats": true
        },
        {
          "appid": 12640,
          "name": "Drakensang",
          "playtime_forever": 0,
          "img_icon_url": "bdd45bb2d71372f5f9498987067e8c2d09c8e28a",
          "img_logo_url": "6919132af7de0a9db35a68fd37db835958ae2188"
        },
        {
          "appid": 65930,
          "name": "The Bureau: XCOM Declassified",
          "playtime_forever": 0,
          "img_icon_url": "854f133d072a65e5577c2a21bf25ba863e4a6a23",
          "img_logo_url": "52da3125ac55d5d1406c610f7e8c78755f0752ff",
          "has_community_visible_stats": true
        },
        {
          "appid": 228300,
          "name": "Remember Me",
          "playtime_forever": 0,
          "img_icon_url": "3a8acb47f7aa7381f08588be7f2a5267c52baa7f",
          "img_logo_url": "9589ec01438852ed7e9dc2581ee63884ad22836a",
          "has_community_visible_stats": true
        },
        {
          "appid": 224060,
          "name": "Deadpool",
          "playtime_forever": 0,
          "img_icon_url": "2470cbfaab7949c06b2a1355347400e596a03a60",
          "img_logo_url": "53d5d045b96b52126ef3515e088cdeb331c88897",
          "has_community_visible_stats": true
        },
        {
          "appid": 57640,
          "name": "Broken Sword 1 - Shadow of the Templars: Director's Cut",
          "playtime_forever": 0,
          "img_icon_url": "1a42af15fc34f52a4df0a804552867e5f20f7f43",
          "img_logo_url": "ab603d7a68ebf18d1ae3d30696021f1ce401696e"
        },
        {
          "appid": 233980,
          "name": "Unepic",
          "playtime_forever": 1723,
          "img_icon_url": "4183d791378e064d24251dc00261dcf048f54ad9",
          "img_logo_url": "f73ba613d68dbbe3fca617c5cec14786f74b80ed",
          "has_community_visible_stats": true
        },
        {
          "appid": 236090,
          "name": "Dust: An Elysian Tail",
          "playtime_forever": 0,
          "img_icon_url": "3779535aba1ad565d504a7d52c6dd5c9eeb47fb2",
          "img_logo_url": "544fd60b00696d8c3402828da7055fea64d619ca",
          "has_community_visible_stats": true
        },
        {
          "appid": 230820,
          "name": "The Night of the Rabbit",
          "playtime_forever": 0,
          "img_icon_url": "dd544720b32ab6e4239bcd75fe4569611ae99d76",
          "img_logo_url": "f71541e76b90f7c3d52fce7bf7f717d8fd2f1b0f",
          "has_community_visible_stats": true
        },
        {
          "appid": 231740,
          "name": "Knights of Pen and Paper +1",
          "playtime_forever": 0,
          "img_icon_url": "c8920c13bacc61396e2d6d323d0301c2005b274d",
          "img_logo_url": "988728706a0259a446141134a2788d3892a11a0f",
          "has_community_visible_stats": true
        },
        {
          "appid": 72850,
          "name": "The Elder Scrolls V: Skyrim",
          "playtime_forever": 0,
          "img_icon_url": "b9aca8a189abd8d6aaf09047dbb0f57582683e1c",
          "img_logo_url": "c5af3cde13610fca25cd17634a96d72487d21e74",
          "has_community_visible_stats": true
        },
        {
          "appid": 213030,
          "name": "Penny Arcade's On the Rain-Slick Precipice of Darkness 3",
          "playtime_forever": 0,
          "img_icon_url": "cb74eb2f715f706f868e60045746711dda28afe9",
          "img_logo_url": "39cd6d3c7215fee336740655bf0e4fd74c5070ec",
          "has_community_visible_stats": true
        },
        {
          "appid": 237570,
          "name": "Penny Arcade's On the Rain-Slick Precipice of Darkness 4",
          "playtime_forever": 0,
          "img_icon_url": "38dbcdd9a7d77d93be3ac0f8e5b0c1aca0534cd7",
          "img_logo_url": "c66da3f8285197c2e222c10064d2d9c434882b58"
        },
        {
          "appid": 242780,
          "name": "Cognition: An Erica Reed Thriller",
          "playtime_forever": 0,
          "img_icon_url": "fe5e99dbb463088f8ba04de39d0bc1a2736f95d6",
          "img_logo_url": "39d1daf7576ab93edd710f933b771fe4146948a0",
          "has_community_visible_stats": true
        },
        {
          "appid": 242820,
          "name": "140",
          "playtime_forever": 0,
          "img_icon_url": "b4c2f57912256aef7cf649c42b768b8f162a78c2",
          "img_logo_url": "1f08bea3bc30abc955bb3cb95a8f8d1a4659f4cb",
          "has_community_visible_stats": true
        },
        {
          "appid": 233250,
          "name": "Planetary Annihilation",
          "playtime_forever": 0,
          "img_icon_url": "cc7e967f96e5fb35cf45c742f2c73b2d4f348bd0",
          "img_logo_url": "c9fcec848203470a2ff487fdb42e0fc9f6500e8d",
          "has_community_visible_stats": true
        },
        {
          "appid": 243120,
          "name": "Betrayer",
          "playtime_forever": 0,
          "img_icon_url": "d3f4fef822c0ebd279b55f655953715a890ab587",
          "img_logo_url": "cd708131ce1b4b5e6348c4016f2d228280f06840",
          "has_community_visible_stats": true
        },
        {
          "appid": 243200,
          "name": "Memoria",
          "playtime_forever": 0,
          "img_icon_url": "d9490b1cd51cb802f8650472cf47ee8dbfec2777",
          "img_logo_url": "6ab3319f46d4823d59c042dbd75676891084859c",
          "has_community_visible_stats": true
        },
        {
          "appid": 95000,
          "name": "Super Splatters",
          "playtime_forever": 0,
          "img_icon_url": "5cd9940728999df0afb677ce73baa16e3e8a3995",
          "img_logo_url": "2a8d9a529dfeb28cce833e76aaa2d799100c0607",
          "has_community_visible_stats": true
        },
        {
          "appid": 39140,
          "name": "FINAL FANTASY VII",
          "playtime_forever": 0,
          "img_icon_url": "dc89ce8f07d5ec36a5617a66fe16e3541d2cc172",
          "img_logo_url": "a54e96c44d2b2c65a75702655ef47df336e0492a",
          "has_community_visible_stats": true
        },
        {
          "appid": 233370,
          "name": "The Raven - Legacy of a Master Thief",
          "playtime_forever": 0,
          "img_icon_url": "0f8297d566dcbad980b0200bd5ad45a01aa93154",
          "img_logo_url": "69bcd50011ed6c84d0dc58049f8fad044c34a000",
          "has_community_visible_stats": true
        },
        {
          "appid": 736810,
          "name": "The Raven Remastered",
          "playtime_forever": 0,
          "img_icon_url": "f73d27652b6535ebf4e214c1cf0747796eca733d",
          "img_logo_url": "f080118d4a3b6f850f82fc0034fa33e93acddf7c",
          "has_community_visible_stats": true
        },
        {
          "appid": 244810,
          "name": "Foul Play",
          "playtime_forever": 0,
          "img_icon_url": "9bca50d36a65aee72930ffbfb8445749a66fb134",
          "img_logo_url": "833f5fa1b2b8a395b68a5e2757d2818734f53faa",
          "has_community_visible_stats": true
        },
        {
          "appid": 244950,
          "name": "Where is my Heart?",
          "playtime_forever": 0,
          "img_icon_url": "a2078b3fa9732381ec284c2fbbcc08b956886342",
          "img_logo_url": "ca2a91f441581f99065c7eb69f4dd74d25448f91",
          "has_community_visible_stats": true
        },
        {
          "appid": 245150,
          "name": "The Novelist",
          "playtime_forever": 0,
          "img_icon_url": "f9db6d0bf2bf53dd014b90e1958d602189833afd",
          "img_logo_url": "5d9625113609396e2b2914d253174db6a7c228f3"
        },
        {
          "appid": 240200,
          "name": "Duke Nukem: Manhattan Project",
          "playtime_forever": 0,
          "img_icon_url": "12c7c1ed6f22409f509ed2195bda63c8076a01bc",
          "img_logo_url": "0d514b0ffb50926da5a9db3285d567134e978468"
        },
        {
          "appid": 237110,
          "name": "Mortal Kombat Komplete Edition",
          "playtime_forever": 0,
          "img_icon_url": "3b9c627b90f42cf650d5848e2fdd779fa4e6eb19",
          "img_logo_url": "307dc1eacffd54e5a7a02b663cec1c5105059811",
          "has_community_visible_stats": true
        },
        {
          "appid": 239250,
          "name": "Castlevania: Lords of Shadow 2",
          "playtime_forever": 831,
          "img_icon_url": "f7dce02872c56b564d70e0440fc7e0b893ce01d6",
          "img_logo_url": "788026250ab01c40deabb1b257702cd0f1013758",
          "has_community_visible_stats": true
        },
        {
          "appid": 91310,
          "name": "Dead Island",
          "playtime_forever": 0,
          "img_icon_url": "1df9ab123c2180d8933038be7578a21e2442befb",
          "img_logo_url": "62632a275a4cc08f0238ed3d589ce1d8627fde91",
          "has_community_visible_stats": true
        },
        {
          "appid": 216250,
          "name": "Dead Island Riptide",
          "playtime_forever": 0,
          "img_icon_url": "26c210f3d5241879e8ee9c24f23ac75dd62b118f",
          "img_logo_url": "3b62996c558973a9debf26afee64fe3eadf09ef5",
          "has_community_visible_stats": true
        },
        {
          "appid": 218820,
          "name": "Mercenary Kings",
          "playtime_forever": 0,
          "img_icon_url": "4544b2993465c51a977c83111b0c6f818823cb6b",
          "img_logo_url": "636cde6662517cb617f474a41fd706caa6674b1c",
          "has_community_visible_stats": true
        },
        {
          "appid": 207140,
          "name": "SpeedRunners",
          "playtime_forever": 0,
          "img_icon_url": "65cfa21149c415430fe36d83c23e079e6db476cc",
          "img_logo_url": "9cea1681e07aa5578508b27ca9e6ceca42dc792e",
          "has_community_visible_stats": true
        },
        {
          "appid": 368730,
          "name": "No Time To Explain Remastered",
          "playtime_forever": 0,
          "img_icon_url": "4cea102f506d192056197b347544bfe77489e847",
          "img_logo_url": "ee4a606dba7848e97d5bf4fe1e4b0ff8e67a889f",
          "has_community_visible_stats": true
        },
        {
          "appid": 246420,
          "name": "Kingdom Rush",
          "playtime_forever": 0,
          "img_icon_url": "2dccbb4dbb08987e38d7efe916fd303bea8dc39a",
          "img_logo_url": "4a30b9d0a8b6e7287f2b617dd28dce6910aeafe8",
          "has_community_visible_stats": true
        },
        {
          "appid": 247370,
          "name": "Mutant Mudds Deluxe",
          "playtime_forever": 0,
          "img_icon_url": "fd920cdc016710f5a142ddb488919f11a011dccf",
          "img_logo_url": "38b18142083a48c59041480b0ee86888423146f8",
          "has_community_visible_stats": true
        },
        {
          "appid": 225080,
          "name": "Brothers - A Tale of Two Sons",
          "playtime_forever": 0,
          "img_icon_url": "9d6e8bcae597582734b382d8da93e5fa7b1c640b",
          "img_logo_url": "12f5c3aaa2b1d69d0786e1d09e0b77e2da6f8980",
          "has_community_visible_stats": true
        },
        {
          "appid": 234080,
          "name": "Castlevania: Lords of Shadow - Ultimate Edition",
          "playtime_forever": 1520,
          "img_icon_url": "67b53957e72935b506c91cfc4dc0e1075471aac5",
          "img_logo_url": "3110df525bb5be2afcf92d50b11b31b2eb0f7f23",
          "has_community_visible_stats": true
        },
        {
          "appid": 242550,
          "name": "Rayman Legends",
          "playtime_forever": 0,
          "img_icon_url": "3525d947218233e1c2fbcc85faec66fcc1847ab3",
          "img_logo_url": "b163657cc450f59ce5ba002b8cd961f54fbde6ba"
        },
        {
          "appid": 244730,
          "name": "Divekick",
          "playtime_forever": 0,
          "img_icon_url": "5f0d9633e9347b6a34e83344d8fd90ebf9d337a6",
          "img_logo_url": "58bd4620a59d646f8385239aaf3aeff226b25139",
          "has_community_visible_stats": true
        },
        {
          "appid": 237630,
          "name": "DuckTales Remastered",
          "playtime_forever": 23,
          "img_icon_url": "65047061a0122f4a97f7e1aad43375e9d6374e66",
          "img_logo_url": "690e3fa20c78d99ccacfdefdf2e7f1b26a5a7f91",
          "has_community_visible_stats": true
        },
        {
          "appid": 214560,
          "name": "Mark of the Ninja",
          "playtime_forever": 0,
          "img_icon_url": "220f33169c93c2f6381cd785399fb52bfc79309f",
          "img_logo_url": "c20501309696e5bcda98e9e4f2649abc5720a1d1",
          "has_community_visible_stats": true
        },
        {
          "appid": 231040,
          "name": "Beatbuddy: Tale of the Guardians",
          "playtime_forever": 0,
          "img_icon_url": "1da60d64393a63533d348d0de309adb9c825402c",
          "img_logo_url": "630a0fe52c40facdbdf8be2070a5dc0fa1a83159",
          "has_community_visible_stats": true
        },
        {
          "appid": 94304,
          "name": "The Dream Machine: Chapter 4",
          "playtime_forever": 0,
          "img_icon_url": "d5c7ef70c479eb6d001b9593b3698f08b387ca4b",
          "img_logo_url": "613212fc595708f901aeea61597407e624deb9e8",
          "has_community_visible_stats": true
        },
        {
          "appid": 248820,
          "name": "Risk of Rain",
          "playtime_forever": 0,
          "img_icon_url": "27c63bdc153de52355c8e6983baa4bc0aedc3699",
          "img_logo_url": "62869ec060e6dfdd3ca53e23c85e30a16de9c291",
          "has_community_visible_stats": true
        },
        {
          "appid": 239350,
          "name": "Spelunky",
          "playtime_forever": 0,
          "img_icon_url": "f11cc6800f5139e79643331be28c348cd7ccbba2",
          "img_logo_url": "1a3ce75a075da21471d4d376681b6a8dd2c4a8bd",
          "has_community_visible_stats": true
        },
        {
          "appid": 249590,
          "name": "Teslagrad",
          "playtime_forever": 0,
          "img_icon_url": "5125c2952184a27097abd13bc5c97b6f07685b2e",
          "img_logo_url": "9f5866823d854ecfb37e5fd6718bfa2a65b871bb",
          "has_community_visible_stats": true
        },
        {
          "appid": 249680,
          "name": "Marlow Briggs",
          "playtime_forever": 22,
          "img_icon_url": "2c4ce684975d08b09125c6c31d4df05354760fc8",
          "img_logo_url": "5e309d3c883d3fcd2295e8d22298a87261614526",
          "has_community_visible_stats": true
        },
        {
          "appid": 209000,
          "name": "Batman™: Arkham Origins",
          "playtime_forever": 0,
          "img_icon_url": "76dac70a2206de1a80da4950da43e1b05ea302a8",
          "img_logo_url": "21a0c80630abe3a9dceddcbc224ed6b5b763c5b9",
          "has_community_visible_stats": true
        },
        {
          "appid": 245280,
          "name": "ENSLAVED™: Odyssey to the West™ Premium Edition",
          "playtime_forever": 0,
          "img_icon_url": "a453e521f28f768edd91855a5c37c99731f85cee",
          "img_logo_url": "77ec5f0a5542d439e118845060ead9fe16b4a14e",
          "has_community_visible_stats": true
        },
        {
          "appid": 241240,
          "name": "Contraption Maker",
          "playtime_forever": 0,
          "img_icon_url": "28a188e595676a06573ac01fae9fafda2457bf4c",
          "img_logo_url": "edbe8f53e518ad9effb167f01c5ce2ae3bc51788",
          "has_community_visible_stats": true
        },
        {
          "appid": 238320,
          "name": "Outlast",
          "playtime_forever": 0,
          "img_icon_url": "46bd3df92d4693d3b2af113bf8f31c2cbdadb20a",
          "img_logo_url": "7d02a2286fd5d4ce42cfc1ecc78e08c89e542f87",
          "has_community_visible_stats": true
        },
        {
          "appid": 250030,
          "name": "Lilly Looking Through",
          "playtime_forever": 0,
          "img_icon_url": "0957807158f258fb50a8d0e2e1ae8f70438e2c1f",
          "img_logo_url": "4251473555a6ca7df35c3555fc18c54c8f877963",
          "has_community_visible_stats": true
        },
        {
          "appid": 226720,
          "name": "Lost Planet 3",
          "playtime_forever": 0,
          "img_icon_url": "cd269b26372a01a73c98c584c3ca64df54e19237",
          "img_logo_url": "a0daa93969db024b78595f3f00ff00bbf0a0ee89",
          "has_community_visible_stats": true
        },
        {
          "appid": 250180,
          "name": "METAL SLUG 3",
          "playtime_forever": 0,
          "img_icon_url": "102c6efe9fc37ecdc929258c52dcaf3d445b873f",
          "img_logo_url": "2f38a8238712afa7cacdd41031a114b35cf9b9db",
          "has_community_visible_stats": true
        },
        {
          "appid": 250260,
          "name": "Jazzpunk: Director's Cut",
          "playtime_forever": 0,
          "img_icon_url": "6c89bf1c426df1d53dc7b03986977c1693694679",
          "img_logo_url": "4d0aa51a082934215453d77f10b8a985151c37bc",
          "has_community_visible_stats": true
        },
        {
          "appid": 250320,
          "name": "The Wolf Among Us",
          "playtime_forever": 0,
          "img_icon_url": "dee5f1476ddd237ed9faab86b59cb9ca9b275faa",
          "img_logo_url": "78ca4e28c36df80deac8f3c6499ce4e0c0d5a821",
          "has_community_visible_stats": true
        },
        {
          "appid": 250380,
          "name": "Knock-knock",
          "playtime_forever": 0,
          "img_icon_url": "9c9f2d96ca243d33c99699362729e7e7cc10a891",
          "img_logo_url": "2a26a47a9c1c228306fbf8eb398bea87404fb601",
          "has_community_visible_stats": true
        },
        {
          "appid": 250660,
          "name": "Bunny Must Die! Chelsea and the 7 Devils",
          "playtime_forever": 0,
          "img_icon_url": "96745884c3f3c67eb10b23145f4352551724cdcb",
          "img_logo_url": "4cfc75345a90a6cd638a956e5913a4b48dec065c",
          "has_community_visible_stats": true
        },
        {
          "appid": 251150,
          "name": "The Legend of Heroes: Trails in the Sky",
          "playtime_forever": 5646,
          "img_icon_url": "1dff8800665a241285116d4a90a5d291bac10942",
          "img_logo_url": "2712920255cf50e9c2d39c60ad49ce082eefed4e",
          "has_community_visible_stats": true
        },
        {
          "appid": 251290,
          "name": "The Legend of Heroes: Trails in the Sky SC",
          "playtime_forever": 0,
          "img_icon_url": "c0a3778f947e731d3a55e2efc13198fbd57a73e8",
          "img_logo_url": "1a0c88f624580ea5991f970d7e7a4617861ce0c3",
          "has_community_visible_stats": true
        },
        {
          "appid": 251630,
          "name": "The Impossible Game",
          "playtime_forever": 0,
          "img_icon_url": "4bc1a2cfe2af2cb28cdb83699c39886d3fecfe03",
          "img_logo_url": "6dbf9665fe910b7eb373dc9ffa0784d59acf5078",
          "has_community_visible_stats": true
        },
        {
          "appid": 251730,
          "name": "Legend of Grimrock 2",
          "playtime_forever": 0,
          "img_icon_url": "409a44fb3da66a0732e91418e1f952d3f032de5f",
          "img_logo_url": "5b91b2338838a8b9c81e0f0b3c995bd6b0434622",
          "has_community_visible_stats": true
        },
        {
          "appid": 251870,
          "name": "Go! Go! Nippon! ~My First Trip to Japan~",
          "playtime_forever": 0,
          "img_icon_url": "381e4ce3d30948efa3ed8d71c4735d03aeda750e",
          "img_logo_url": "50ec6fcc1ce5b28d6c16d964bdda3597519b5eb6",
          "has_community_visible_stats": true
        },
        {
          "appid": 252030,
          "name": "Valdis Story: Abyssal City",
          "playtime_forever": 23,
          "img_icon_url": "ac1d5be42d9ab19622df244c24edcf14857fdfba",
          "img_logo_url": "e747759692999a93f3eb267f48f4ef229b605595",
          "has_community_visible_stats": true
        },
        {
          "appid": 252230,
          "name": "YAIBA: NINJA GAIDEN Z",
          "playtime_forever": 0,
          "img_icon_url": "f118482b0a98977f229355db0342210c8ecf7af5",
          "img_logo_url": "09a3288b59f61381ad4902fbaee0364d0d54124e",
          "has_community_visible_stats": true
        },
        {
          "appid": 252330,
          "name": "Slender: The Arrival",
          "playtime_forever": 0,
          "img_icon_url": "11b78d079a277b24783b52967da73457ae2cfb57",
          "img_logo_url": "f235b034fc7d2753477f4250c9e505f0b0d2b956",
          "has_community_visible_stats": true
        },
        {
          "appid": 252350,
          "name": "Double Dragon Neon",
          "playtime_forever": 0,
          "img_icon_url": "83d16936ada513369648a2ee79ce375867f951b0",
          "img_logo_url": "43989029476dcdfcfc4ec6af3094e74dbab3dcb0",
          "has_community_visible_stats": true
        },
        {
          "appid": 252410,
          "name": "SteamWorld Dig",
          "playtime_forever": 0,
          "img_icon_url": "7757c90bbf65e312ea4871106250077e78007bc6",
          "img_logo_url": "99e9e265feb90662e9d8cacc6c8e285e8fc00d2d",
          "has_community_visible_stats": true
        },
        {
          "appid": 252430,
          "name": "Dusty Revenge",
          "playtime_forever": 0,
          "img_icon_url": "52e6e1d88ed1bc4036383344879d6e2ff0887fe4",
          "img_logo_url": "e272d611520b5e20b55f18dbf238c9b716800156",
          "has_community_visible_stats": true
        },
        {
          "appid": 252550,
          "name": "Qbeh-1: The Atlas Cube",
          "playtime_forever": 0,
          "img_icon_url": "699bdc47d38d1cab91631a61dba7123804849ee3",
          "img_logo_url": "de22310692890e9532cbf7be2b27cf419dbdbe76",
          "has_community_visible_stats": true
        },
        {
          "appid": 253290,
          "name": "FOTONICA",
          "playtime_forever": 0,
          "img_icon_url": "60af96880b2e48f185b41d76142507488aff0086",
          "img_logo_url": "885110ac95d82e7d3f09f5a0622e73b989c481d7",
          "has_community_visible_stats": true
        },
        {
          "appid": 253330,
          "name": "Neverending Nightmares",
          "playtime_forever": 0,
          "img_icon_url": "d6dcfb21c8e11eeefc83a5e0357f296ded8fb76e",
          "img_logo_url": "1cc142bcc2c286a8c2d9dd1af89440b8b266e4d3",
          "has_community_visible_stats": true
        },
        {
          "appid": 208610,
          "name": "Skullgirls ∞Endless Beta∞",
          "playtime_forever": 0,
          "img_icon_url": "7bf859db736b8825045b0cc79acc4bb7be8cd7b9",
          "img_logo_url": "33974b81779c888b3b4d9c4b91d86ef0907b11f3",
          "has_community_visible_stats": true
        },
        {
          "appid": 245170,
          "name": "Skullgirls",
          "playtime_forever": 0,
          "img_icon_url": "7bf859db736b8825045b0cc79acc4bb7be8cd7b9",
          "img_logo_url": "ca3f7bd4fbb3cf73855ebce91b6dafc2104d651b",
          "has_community_visible_stats": true
        },
        {
          "appid": 253840,
          "name": "Shantae: Half-Genie Hero",
          "playtime_forever": 0,
          "img_icon_url": "963d47c189ccb3592526a8acfc298511bb3c9b95",
          "img_logo_url": "795076b09cd82e8c17dedd5246910359ccab272c",
          "has_community_visible_stats": true
        },
        {
          "appid": 254200,
          "name": "FortressCraft Evolved",
          "playtime_forever": 0,
          "img_icon_url": "d0b946ea6ffd26802d922a7715a4c996dfe5e910",
          "img_logo_url": "a3ed3993e8b5f185682764ac58801e1d66f64131",
          "has_community_visible_stats": true
        },
        {
          "appid": 231310,
          "name": "MirrorMoon EP",
          "playtime_forever": 0,
          "img_icon_url": "918600aa6792efd4d32bdd9582201a05b71c9e49",
          "img_logo_url": "d6303c0d12115cab088def4da3eba65737618789",
          "has_community_visible_stats": true
        },
        {
          "appid": 224460,
          "name": "Contrast",
          "playtime_forever": 0,
          "img_icon_url": "7b0631a6110c7e8c648d88054de904a8d11cd62c",
          "img_logo_url": "337e115f6d0a973d517af5f0bc0f0764ca624eb3",
          "has_community_visible_stats": true
        },
        {
          "appid": 227600,
          "name": "Castle of Illusion",
          "playtime_forever": 0,
          "img_icon_url": "669f91e8f234174195a3c2c1a682ca1379705205",
          "img_logo_url": "cdbc2581c46fa378baa8f48e28513a1668b30941",
          "has_community_visible_stats": true
        },
        {
          "appid": 205100,
          "name": "Dishonored",
          "playtime_forever": 1081,
          "img_icon_url": "74f8ee1ba536e0759e64a9bf801fc013e16c8dd1",
          "img_logo_url": "b94f5ff693304b7f70f88403d444686c4af3b940",
          "has_community_visible_stats": true
        },
        {
          "appid": 247240,
          "name": "Volgarr the Viking",
          "playtime_forever": 166,
          "img_icon_url": "c6e7168b2be1a7fb1876a2e688eea8b69260c601",
          "img_logo_url": "94be1374c439b473e5d201bba71a05c9b90e1f8c",
          "has_community_visible_stats": true
        },
        {
          "appid": 255280,
          "name": "1954 Alcatraz",
          "playtime_forever": 365,
          "img_icon_url": "6bc32323432b79032d254626c17fd456b3a0e3e5",
          "img_logo_url": "108d6040f71bb245e1dcdf0b2bddc43a670ee53b",
          "has_community_visible_stats": true
        },
        {
          "appid": 255300,
          "name": "Journey of a Roach",
          "playtime_forever": 0,
          "img_icon_url": "ce14586c7854f51e3482bcfa3e73f56e2c2e14ac",
          "img_logo_url": "9d67050896757567eb1d66b4a3708a6d72149ed3",
          "has_community_visible_stats": true
        },
        {
          "appid": 255320,
          "name": "Edna & Harvey: The Breakout",
          "playtime_forever": 0,
          "img_icon_url": "e5e4f0163497a9f2f1d311730297660c70bf0b7b",
          "img_logo_url": "cc0b7bf270ed3808e29cb6c03221a5090161090c",
          "has_community_visible_stats": true
        },
        {
          "appid": 243000,
          "name": "Omikron - The Nomad Soul",
          "playtime_forever": 0,
          "img_icon_url": "579f3f0aca7f57df18145eac9073837f2bbe0891",
          "img_logo_url": "0e6780c70e1d73a7dbcd7a1e804b39c5a3a76a77"
        },
        {
          "appid": 234670,
          "name": "NARUTO SHIPPUDEN: Ultimate Ninja STORM 3 Full Burst",
          "playtime_forever": 0,
          "img_icon_url": "ddd956cc6ec3370449f96298653d4119c5666fff",
          "img_logo_url": "12895039c12ec92be72faa2d13acb88886b8cb97",
          "has_community_visible_stats": true
        },
        {
          "appid": 247660,
          "name": "Deadly Premonition: The Director's Cut",
          "playtime_forever": 0,
          "img_icon_url": "d0cd660d0f176461ea17a503516ce3eff770969d",
          "img_logo_url": "5abd9c719b79e0d8a79295761b374b61be918f1d",
          "has_community_visible_stats": true
        },
        {
          "appid": 242050,
          "name": "Assassin's Creed IV Black Flag",
          "playtime_forever": 0,
          "img_icon_url": "a03c06290af00eed66814815663b68f5fd063882",
          "img_logo_url": "917bd16fd8a6707d7f2a93fd6473da1f5b22e922",
          "has_community_visible_stats": true
        },
        {
          "appid": 233130,
          "name": "Shadow Warrior",
          "playtime_forever": 0,
          "img_icon_url": "5d7c205ce18f17a99c95ff81499c2f496e0570f2",
          "img_logo_url": "30d29d6ec47f0b586349106f8265e69ee091ce95",
          "has_community_visible_stats": true
        },
        {
          "appid": 255520,
          "name": "Viscera Cleanup Detail: Shadow Warrior",
          "playtime_forever": 0,
          "img_icon_url": "56c3840d52a0da4f397d9991d368c75ed53d99fa",
          "img_logo_url": "09a86e49413f57dc99f2e7f54b029868ffb5cbc8",
          "has_community_visible_stats": true
        },
        {
          "appid": 249330,
          "name": "Unholy Heights",
          "playtime_forever": 0,
          "img_icon_url": "60e36f6436486e0bfd129ab55c9a495814b51f85",
          "img_logo_url": "eb33f77c417e0644d23382f458ff675e89e710dc",
          "has_community_visible_stats": true
        },
        {
          "appid": 258030,
          "name": "Earthlock: Festival of Magic",
          "playtime_forever": 0,
          "img_icon_url": "15bc5a7ff1df39d31c8dcfc9a30ab0e50329379c",
          "img_logo_url": "25de374e0143de019c744900bf51bac172b8c73c",
          "has_community_visible_stats": true
        },
        {
          "appid": 761030,
          "name": "EARTHLOCK",
          "playtime_forever": 0,
          "img_icon_url": "75ed33b58a140b546347f6933dac172d1eccc1fb",
          "img_logo_url": "e0baddb24a4fdbaf2193491c1010bdb4d4b9b128",
          "has_community_visible_stats": true
        },
        {
          "appid": 238010,
          "name": "Deus Ex: Human Revolution - Director's Cut",
          "playtime_forever": 0,
          "img_icon_url": "6c7ccd62c124ae63820e06ed9b24d4559c5b0b1f",
          "img_logo_url": "4bcb3e48bcb2ebb52403b1fc3f43dbd43607ba2e",
          "has_community_visible_stats": true
        },
        {
          "appid": 241910,
          "name": "Goodbye Deponia",
          "playtime_forever": 746,
          "img_icon_url": "591793052b14fe6f1233e28056fdab998329f54c",
          "img_logo_url": "d045962f0cd5801b2bd530180f2c1f6388f8c67f",
          "has_community_visible_stats": true
        },
        {
          "appid": 238930,
          "name": "7 Grand Steps, Step 1: What Ancients Begat",
          "playtime_forever": 353,
          "img_icon_url": "055393418480c99e1f19c2bd0507a1a5a26ca093",
          "img_logo_url": "3fc5b14bb2006a98c79dfb9aef89e1e3fc39fe49"
        },
        {
          "appid": 258890,
          "name": "Type:Rider",
          "playtime_forever": 0,
          "img_icon_url": "747c0e8001b3262358af6a1abf51ec0f27eea698",
          "img_logo_url": "9932ad807beac236c3af3ff151cb782c6b9ba60b",
          "has_community_visible_stats": true
        },
        {
          "appid": 258910,
          "name": "Citizens of Earth",
          "playtime_forever": 0,
          "img_icon_url": "1f342433fc6240cffe142f0914ab3319b7c478ce",
          "img_logo_url": "79cca0525f80f81af2a10ba08bbad3930dcaf20b",
          "has_community_visible_stats": true
        },
        {
          "appid": 25910,
          "name": "Supreme Ruler 2020: Gold",
          "playtime_forever": 0,
          "img_icon_url": "8a5f4527fbbcd4fb0f95c3c178c27faba1868dfd",
          "img_logo_url": "5da8a1569b1b06657e815558e60203b2f355e853"
        },
        {
          "appid": 258950,
          "name": "Montague's Mount",
          "playtime_forever": 0,
          "img_icon_url": "4196cff1737ff47bc60aad46ed3704fd393f99a9",
          "img_logo_url": "a09672aacb1c5cc6fac0eca0d2c0d92816ca4387",
          "has_community_visible_stats": true
        },
        {
          "appid": 259600,
          "name": "Finding Teddy",
          "playtime_forever": 6,
          "img_icon_url": "16a9f2317257621a44dc2f881f0fe0d324ccd6db",
          "img_logo_url": "8af36a2ecf5a7bfe2c5e921b9a938759ab964f95",
          "has_community_visible_stats": true
        },
        {
          "appid": 260130,
          "name": "Agarest Zero",
          "playtime_forever": 342,
          "img_icon_url": "33f926a6ed2668945800ff74d1d7a28e2830e578",
          "img_logo_url": "085bd28322a23855d16e58b14011742b01c94d21",
          "has_community_visible_stats": true
        },
        {
          "appid": 260160,
          "name": "The Last Tinker: City of Colors",
          "playtime_forever": 0,
          "img_icon_url": "311534e966c73979270e92d99c85487f164300c1",
          "img_logo_url": "b99d7f2de3d1e24f68951097c7d2095841054bbd",
          "has_community_visible_stats": true
        },
        {
          "appid": 260230,
          "name": "Valiant Hearts: The Great War™ / Soldats Inconnus : Mémoires de la Grande Guerre™",
          "playtime_forever": 0,
          "img_icon_url": "903f95e08ebcd45dfb78deb171b2f12909892be2",
          "img_logo_url": "f3269280b3379f701de6d308b6d3fb3868079741",
          "has_community_visible_stats": true
        },
        {
          "appid": 254440,
          "name": "Pool Nation",
          "playtime_forever": 0,
          "img_icon_url": "811b1863d4130637839c75f9cc779e0595c9a855",
          "img_logo_url": "2644832fbb1a74fc3149a543b3ec0ae19dfcf941",
          "has_community_visible_stats": true
        },
        {
          "appid": 260570,
          "name": "Gray Matter",
          "playtime_forever": 0,
          "img_icon_url": "e4ca6a52d7e36c7e840462d4bf140a26dcbeb856",
          "img_logo_url": "8df49a9017b99494e6eb1a18d9d40b9224efb3a0",
          "has_community_visible_stats": true
        },
        {
          "appid": 238430,
          "name": "Contagion",
          "playtime_forever": 0,
          "img_icon_url": "5b4f22ca04d338866b0c904af3166d9e939027f2",
          "img_logo_url": "207217ab75cce8e73642b286d8ebb66a86bd776a",
          "has_community_visible_stats": true
        },
        {
          "appid": 261030,
          "name": "The Walking Dead: Season Two",
          "playtime_forever": 0,
          "img_icon_url": "8dd081faf73fb7ff57151a1498b7025e8fc85dc4",
          "img_logo_url": "fcfbf375cb9249601eca22c1847447b92e15d82d",
          "has_community_visible_stats": true
        },
        {
          "appid": 261110,
          "name": "Killer is Dead",
          "playtime_forever": 0,
          "img_icon_url": "4bb1ca7ca4ffa4be186bdf0d01d0f07c71283fd3",
          "img_logo_url": "187ab8f22a578692b0ec5dcbe1c07e30828b566f",
          "has_community_visible_stats": true
        },
        {
          "appid": 217100,
          "name": "Dementium II HD",
          "playtime_forever": 0,
          "img_icon_url": "8b52ad2671613ffd9fadfd5f5112a0c15601b5e3",
          "img_logo_url": "c77f7234be78d03c33d71ec8b629bdbb08b8df49",
          "has_community_visible_stats": true
        },
        {
          "appid": 261570,
          "name": "Ori and the Blind Forest",
          "playtime_forever": 0,
          "img_icon_url": "a8b0c1ca89fa421e5061d4ac5c17e2fbf4f08845",
          "img_logo_url": "e8b71ff338698994bca662ec8e6cf930f2ddc98b",
          "has_community_visible_stats": true
        },
        {
          "appid": 242700,
          "name": "Injustice: Gods Among Us Ultimate Edition",
          "playtime_forever": 0,
          "img_icon_url": "4ea9f28360ca77b2ced5c8ac5545fcfc6ea8ef15",
          "img_logo_url": "405bc57977d5e6fb112765e63fffcb110dbcd751",
          "has_community_visible_stats": true
        },
        {
          "appid": 261640,
          "name": "Borderlands: The Pre-Sequel",
          "playtime_forever": 0,
          "img_icon_url": "af5ef05eac8b1eb618e4f57354ac7b3e918ab1bd",
          "img_logo_url": "df64c72fd335a03dbcc0a19b1f81acc8db1b94ba",
          "has_community_visible_stats": true
        },
        {
          "appid": 261760,
          "name": "Lichdom: Battlemage",
          "playtime_forever": 0,
          "img_icon_url": "515ce37935d28a04ac47e8887ba8302563f72733",
          "img_logo_url": "faaab51297ea82a0f7aab34979840d4495bf9f9f",
          "has_community_visible_stats": true
        },
        {
          "appid": 262150,
          "name": "Vanguard Princess",
          "playtime_forever": 0,
          "img_icon_url": "afd5895ec03d562a24001f37a91307189742e10c",
          "img_logo_url": "3f01516a44624ad2c0ac67d3b183ba47903a0914",
          "has_community_visible_stats": true
        },
        {
          "appid": 281050,
          "name": "Vanguard Princess Director's Cut",
          "playtime_forever": 0,
          "img_icon_url": "8fb457a2688ff5e15907a23da58f78196f49846f",
          "img_logo_url": "c4b7afb85cc1164388982fe3c3b994dd6308da21",
          "has_community_visible_stats": true
        },
        {
          "appid": 262280,
          "name": "Dungeons 2",
          "playtime_forever": 0,
          "img_icon_url": "429544f4c7680d781477059d59e5314974574db3",
          "img_logo_url": "afb1940ff93142e6f3197ab2de68a4209617e747",
          "has_community_visible_stats": true
        },
        {
          "appid": 258090,
          "name": "99 Spirits",
          "playtime_forever": 348,
          "img_icon_url": "70672d0e93969ab7439c3629087ae6b3affb9af5",
          "img_logo_url": "4f354eaf40b000748b33768d22bcefa18c932d8a",
          "has_community_visible_stats": true
        },
        {
          "appid": 262300,
          "name": "Tsukumogami",
          "playtime_forever": 0,
          "img_icon_url": "673b7fda589fbae0d7ab0f9e2fa09603415f84a7",
          "img_logo_url": "19d83fc481f0046c542c5eaf1256c2bb3203b0b6",
          "has_community_visible_stats": true
        },
        {
          "appid": 262470,
          "name": "Rollers of the Realm",
          "playtime_forever": 0,
          "img_icon_url": "ea53bff4833f0d6f8a177a41e9d74640e9916562",
          "img_logo_url": "46e0f063802da5e165f26e6b994f49e707cfe307",
          "has_community_visible_stats": true
        },
        {
          "appid": 262830,
          "name": "Crimsonland",
          "playtime_forever": 0,
          "img_icon_url": "1e16c0ea05ee0247fb10ae0bc4953068c205460f",
          "img_logo_url": "952e3d2819acc0206ec2462b49aa1cc0ddfe3b60",
          "has_community_visible_stats": true
        },
        {
          "appid": 262850,
          "name": "The Journey Down: Chapter Two",
          "playtime_forever": 0,
          "img_icon_url": "1cf06abd7b867ea9efa49471577dac715b13fb3d",
          "img_logo_url": "739e3f9c0eead30ecd4e90801ba765148a801609",
          "has_community_visible_stats": true
        },
        {
          "appid": 262920,
          "name": "Super Chain Crusher Horizon",
          "playtime_forever": 0,
          "img_icon_url": "955d0090b1df5166912dbde1a5ac5fdc98706c43",
          "img_logo_url": "114cb08d27e74318372a7207c2caf63b088c0272",
          "has_community_visible_stats": true
        },
        {
          "appid": 262960,
          "name": "Castle In The Darkness",
          "playtime_forever": 0,
          "img_icon_url": "60cb0be7250ce21f88e4c180a2ab0721ad25575a",
          "img_logo_url": "df2b67f04a9b3065eae7f7081fc7c74875afc326",
          "has_community_visible_stats": true
        },
        {
          "appid": 263320,
          "name": "Saturday Morning RPG",
          "playtime_forever": 0,
          "img_icon_url": "ff7f93ce673c5e02ef30cbd747593a15fa4a457f",
          "img_logo_url": "0ff04121c441a23d74ac22dc6be79313393e14be",
          "has_community_visible_stats": true
        },
        {
          "appid": 263560,
          "name": "Paper Sorcerer",
          "playtime_forever": 0,
          "img_icon_url": "f0ec7578778f97583208eb0049aab8c65e5ac37e",
          "img_logo_url": "46706b3d283d1dd87de88f68705b688a3160fde7",
          "has_community_visible_stats": true
        },
        {
          "appid": 263620,
          "name": "Mitsurugi Kamui Hikae",
          "playtime_forever": 0,
          "img_icon_url": "6222d43d969905805e1f926686f7d4b750a7d7b9",
          "img_logo_url": "e91cd3ad13190d95f5cc7d6a84bbfb7161397b69",
          "has_community_visible_stats": true
        },
        {
          "appid": 263980,
          "name": "Out There Somewhere",
          "playtime_forever": 0,
          "img_icon_url": "9ef5b78baca02bd93df4050525de055b4b5c0e3a",
          "img_logo_url": "cc2393b0264e69356d1861811e423a4b76fdfc6b",
          "has_community_visible_stats": true
        },
        {
          "appid": 264060,
          "name": "Full Bore",
          "playtime_forever": 0,
          "img_icon_url": "7d6d0095ddda23aee1ebfa324f95a8611fd23631",
          "img_logo_url": "cdda4719d04e4beb7eae76b5fde681612aced3ec",
          "has_community_visible_stats": true
        },
        {
          "appid": 264140,
          "name": "Pixel Piracy",
          "playtime_forever": 0,
          "img_icon_url": "c0f25a14c80aa3d9e5fe4b6d3af2a803cbf15b69",
          "img_logo_url": "574f4b847a8a212b1fc28f23b5e7a4a32716c23a",
          "has_community_visible_stats": true
        },
        {
          "appid": 264300,
          "name": "Guns'N'Zombies",
          "playtime_forever": 0,
          "img_icon_url": "3a8dc56bee10bc7b742488d1d63c11e18e0c9bd4",
          "img_logo_url": "ad331f63c7ec2f74f20546ab3137f0524cb26e85",
          "has_community_visible_stats": true
        },
        {
          "appid": 264540,
          "name": "Platformines",
          "playtime_forever": 0,
          "img_icon_url": "201088f745d9866d1460e6fd44e4c0f793d00135",
          "img_logo_url": "6f6e5bf0b6cd8ce38772f5c5803d777bed4a87d0",
          "has_community_visible_stats": true
        },
        {
          "appid": 227300,
          "name": "Euro Truck Simulator 2",
          "playtime_forever": 0,
          "img_icon_url": "adc18a4fc9adc0330144b76d61cbda68bb2394a0",
          "img_logo_url": "719b87f11209f9f20aeb237b3c6a42992bcf7c57",
          "has_community_visible_stats": true
        },
        {
          "appid": 265120,
          "name": "Meridian: New World",
          "playtime_forever": 0,
          "img_icon_url": "44f23be8ffe246a2a8d5d7d36ee0b32160227ebd",
          "img_logo_url": "33db68eabe196b89b4d8805825efde4abaf2baa5",
          "has_community_visible_stats": true
        },
        {
          "appid": 265210,
          "name": "Viscera Cleanup Detail: Santa's Rampage",
          "playtime_forever": 0,
          "img_icon_url": "3ddf9cd4df46fda7ed35cd3e79f04bc0dfd28ee9",
          "img_logo_url": "977246a38ba71702437f9f13c9565b6b84053e39",
          "has_community_visible_stats": true
        },
        {
          "appid": 265300,
          "name": "Lords Of The Fallen",
          "playtime_forever": 0,
          "img_icon_url": "b01d7d060da64430dbbc5195f26225f2e75c38b0",
          "img_logo_url": "fdfdbc63bd9135077be18a47e8c74f293a9aab85",
          "has_community_visible_stats": true
        },
        {
          "appid": 265610,
          "name": "Epic Battle Fantasy 4",
          "playtime_forever": 0,
          "img_icon_url": "a6556081949eaea3eb8525dd8a6a798e6915b2b3",
          "img_logo_url": "39a2a3122d188f7aeba0e65b21b23a54af2899f9",
          "has_community_visible_stats": true
        },
        {
          "appid": 265690,
          "name": "NaissanceE",
          "playtime_forever": 0,
          "img_icon_url": "54258e735824e583e2d8aebe3f72774f887b330f",
          "img_logo_url": "ecb5f1c34ae21f4b4d4f798df93ad211a0c0107d"
        },
        {
          "appid": 266130,
          "name": "Breach & Clear",
          "playtime_forever": 0,
          "img_icon_url": "26dc1c75df69df7e3d1d6cd1d86b2b7238c187ac",
          "img_logo_url": "d32361035e143a77f30581c5fc9417ab4ec89035",
          "has_community_visible_stats": true
        },
        {
          "appid": 266210,
          "name": "One Way Heroics",
          "playtime_forever": 0,
          "img_icon_url": "5d816b0e129681a22343ec34550016f1ad2d2caf",
          "img_logo_url": "da51cd7a26789ecea262b662d0cd699548c14593",
          "has_community_visible_stats": true
        },
        {
          "appid": 266550,
          "name": "Spark Rising",
          "playtime_forever": 0,
          "img_icon_url": "2c0b262d80481eb6035f49b08fd0f619a32d02b8",
          "img_logo_url": "105c57a0db27018f533ef3f9bece3bee7b2a4e7c",
          "has_community_visible_stats": true
        },
        {
          "appid": 39150,
          "name": "FINAL FANTASY VIII",
          "playtime_forever": 0,
          "img_icon_url": "e2b0371cd72160603e7ecaaf95b238a46ba254e6",
          "img_logo_url": "0c912769e975586cdcfe4a6b008d538f1f96a032",
          "has_community_visible_stats": true
        },
        {
          "appid": 267340,
          "name": "Beware Planet Earth",
          "playtime_forever": 0,
          "img_icon_url": "9458f3c1de2fc55a52641bfa24adbbd77a95a55a",
          "img_logo_url": "8eba0d0619522d9c8f33731217d433b4b04d0005",
          "has_community_visible_stats": true
        },
        {
          "appid": 267980,
          "name": "Hostile Waters: Antaeus Rising",
          "playtime_forever": 0,
          "img_icon_url": "16e39ac1fdd63f50b9d1dca5ff8f69c00360a051",
          "img_logo_url": "87954c8183ec79c5009e82a28e724824ef78f2db"
        },
        {
          "appid": 262940,
          "name": "Broken Sword 5 - the Serpent's Curse",
          "playtime_forever": 0,
          "img_icon_url": "2b9d572f1f7fb816d1b5162c6edbdfb2761ac071",
          "img_logo_url": "b96897135307ab1c3e264ad354795ba13b9102f7",
          "has_community_visible_stats": true
        },
        {
          "appid": 268360,
          "name": "Krautscape",
          "playtime_forever": 0,
          "img_icon_url": "2ce99ae58fb9bbb97b262ad39dfa5c65f9b6afb6",
          "img_logo_url": "a24b9499691f47ecb3d00a4b932118f4c8669a07",
          "has_community_visible_stats": true
        },
        {
          "appid": 268870,
          "name": "Satellite Reign",
          "playtime_forever": 0,
          "img_icon_url": "b609806b16b6581548c7d2908f0f75936ea86f48",
          "img_logo_url": "ab0b0cb4aa329c420417988cc0a23f1e4663f820",
          "has_community_visible_stats": true
        },
        {
          "appid": 269210,
          "name": "Hero Siege",
          "playtime_forever": 0,
          "img_icon_url": "a2c00f23b023458392d57401e50ba7be60a17c66",
          "img_logo_url": "a6eac837537abc6be26390d36782dcd1e138ceb4",
          "has_community_visible_stats": true
        },
        {
          "appid": 269490,
          "name": "Bardbarian",
          "playtime_forever": 0,
          "img_icon_url": "6b0ae27accb8129ddc1b30fab250cc12ba756b52",
          "img_logo_url": "2a1b18274d1eb251b87b29cbed12c69ad9559ed6",
          "has_community_visible_stats": true
        },
        {
          "appid": 269650,
          "name": "Dex",
          "playtime_forever": 0,
          "img_icon_url": "c6b56ed02ec7f1cad3ce20506da00a6d8bbf854e",
          "img_logo_url": "aae39a965b0aafc63079bfb61c2c728223ae9f36",
          "has_community_visible_stats": true
        },
        {
          "appid": 269790,
          "name": "DreadOut",
          "playtime_forever": 0,
          "img_icon_url": "a5fccac81bf4c4fe8be3378d5acfb9c8ed4e6d17",
          "img_logo_url": "6eeb1de656c9abc00d0ecec124187a7ee306b86e",
          "has_community_visible_stats": true
        },
        {
          "appid": 269810,
          "name": "Spate",
          "playtime_forever": 0,
          "img_icon_url": "f5a9ec1d524f47122b4c25c4df442bb5395f9839",
          "img_logo_url": "89f05eca4f349b906d3ae6be530d1e52369e4784",
          "has_community_visible_stats": true
        },
        {
          "appid": 269890,
          "name": "AR-K",
          "playtime_forever": 332,
          "img_icon_url": "420663b350afe528ca26ac6b255bb2a49478179d",
          "img_logo_url": "ec2e5377e11a4b9c7501a61b3e40f16fdd669d9b",
          "has_community_visible_stats": true
        },
        {
          "appid": 351070,
          "name": "AR-K Chapter 1&2 OST",
          "playtime_forever": 0,
          "img_icon_url": "9881f350de4b8f39c1c656c094951001f9679cf5",
          "img_logo_url": "98f5a45c4e159e2af4810443476a7cd68fd87d18",
          "has_community_visible_stats": true
        },
        {
          "appid": 270150,
          "name": "RUNNING WITH RIFLES",
          "playtime_forever": 0,
          "img_icon_url": "feacad90f64b2db6c3f7ac6dafe62d21a1b3df55",
          "img_logo_url": "606ee56251263439b33135fba87e6eb98e14e10e",
          "has_community_visible_stats": true
        },
        {
          "appid": 270190,
          "name": "1Heart",
          "playtime_forever": 361,
          "img_icon_url": "19646ea1005ee3fd33e69e1defb1fd5ff404cda5",
          "img_logo_url": "ab4d51dbfcb2fc8ebceeeed5ec9c8af6a181f14b",
          "has_community_visible_stats": true
        },
        {
          "appid": 235460,
          "name": "METAL GEAR RISING: REVENGEANCE",
          "playtime_forever": 0,
          "img_icon_url": "ad0f84fe48b57f3861b6c6d743f26b98d670c21f",
          "img_logo_url": "a21384421dafa03783e3672a5f4754f70e63235e",
          "has_community_visible_stats": true
        },
        {
          "appid": 271590,
          "name": "Grand Theft Auto V",
          "playtime_forever": 0,
          "img_icon_url": "1e72f87eb927fa1485e68aefaff23c7fd7178251",
          "img_logo_url": "e447e82f8b0c67f9e001498503c62f2a187bc609",
          "has_community_visible_stats": true
        },
        {
          "appid": 254460,
          "name": "Obscure",
          "playtime_forever": 0,
          "img_icon_url": "9b5c4088b170ff431682f8378017ddd6253ab822",
          "img_logo_url": "e26df0bc7cdcaec5692954545bee9928f54eb7f9",
          "has_community_visible_stats": true
        },
        {
          "appid": 254480,
          "name": "Obscure 2",
          "playtime_forever": 0,
          "img_icon_url": "c3db88a36e38a4049b5b4cebe742af34373c3994",
          "img_logo_url": "4b4b4697084e2e720a80157a7422757925bba838",
          "has_community_visible_stats": true
        },
        {
          "appid": 232790,
          "name": "Broken Age",
          "playtime_forever": 0,
          "img_icon_url": "4b859c6d3dbb58c102276516b4cff175e8dd4ea0",
          "img_logo_url": "41b5954cafec81314d6f223e38faca0a23501252",
          "has_community_visible_stats": true
        },
        {
          "appid": 271990,
          "name": "Dreamscape",
          "playtime_forever": 0,
          "img_icon_url": "4dd7cf63c7b0c326c5aeb23f499b52d7e88d741f",
          "img_logo_url": "2772069448e8ce47db9bbae60bbc116005001201"
        },
        {
          "appid": 272010,
          "name": "Aveyond 3-1: Lord of Twilight",
          "playtime_forever": 0,
          "img_icon_url": "a29d7dff27861db2f3438424d3146aac2c795b5b",
          "img_logo_url": "609b28802914397b4f8c329e9a03b8ec71f46a53",
          "has_community_visible_stats": true
        },
        {
          "appid": 237990,
          "name": "The Banner Saga",
          "playtime_forever": 0,
          "img_icon_url": "e64d55d8d84214d216a51ee25de58299c5d16f59",
          "img_logo_url": "337592064585a148a69e685df93680d957d4928a",
          "has_community_visible_stats": true
        },
        {
          "appid": 57300,
          "name": "Amnesia: The Dark Descent",
          "playtime_forever": 0,
          "img_icon_url": "2c08de657a8b273eeb55bb5bf674605ca023e381",
          "img_logo_url": "75b8a82acfb05abda97977ac4eb5af20e0dcf01e",
          "has_community_visible_stats": true
        },
        {
          "appid": 239200,
          "name": "Amnesia: A Machine for Pigs",
          "playtime_forever": 0,
          "img_icon_url": "3bf781ae80c6eeb8827819fb2ed92aa2353d3007",
          "img_logo_url": "5df7be324aa92b538c4ef24cbdec85dbbcf950fc",
          "has_community_visible_stats": true
        },
        {
          "appid": 260210,
          "name": "Assassin's Creed Liberation",
          "playtime_forever": 0,
          "img_icon_url": "d2078b609c69bdbae54023d88b1ec1667037f248",
          "img_logo_url": "75699258ac3f300ad200c5ae5a21e91a6b4be068"
        },
        {
          "appid": 208670,
          "name": "Blades of Time",
          "playtime_forever": 0,
          "img_icon_url": "3995fc3484966919fee62b53d1c66b11914798e9",
          "img_logo_url": "1d3d3d42f41e9a0c6aa7c43970a4b4b30f2dcc11",
          "has_community_visible_stats": true
        },
        {
          "appid": 274290,
          "name": "Gods Will Be Watching",
          "playtime_forever": 0,
          "img_icon_url": "51f6574d3b860cd1b6acb5018cd8b124cf375de0",
          "img_logo_url": "eb114da252f5d217daa0fea760d253a405fe44c3",
          "has_community_visible_stats": true
        },
        {
          "appid": 274310,
          "name": "Always Sometimes Monsters",
          "playtime_forever": 330,
          "img_icon_url": "b8d6a2bc557097acd2f3bdfe98a78710911b9279",
          "img_logo_url": "ff89d3b12d9bb81c2d1f418f837ab11eeecb31b3",
          "has_community_visible_stats": true
        },
        {
          "appid": 222880,
          "name": "Insurgency",
          "playtime_forever": 0,
          "img_icon_url": "b072fc4239951c1952f4877edde340419438b528",
          "img_logo_url": "b620a8286682299e2a1ff272b04fb09d1b4edd38",
          "has_community_visible_stats": true
        },
        {
          "appid": 274900,
          "name": "Murder Miners",
          "playtime_forever": 0,
          "img_icon_url": "0ba9a54e98ca963a8cabee36325f873f0b164c62",
          "img_logo_url": "922d51756e33529ada722cf1c5b618445d93177c",
          "has_community_visible_stats": true
        },
        {
          "appid": 275180,
          "name": "Costume Quest 2",
          "playtime_forever": 0,
          "img_icon_url": "6d745d162bf282b475850a8eed6602cfb9ae33ad",
          "img_logo_url": "424c83b6825582c439696a47718cfb0dd8defcec",
          "has_community_visible_stats": true
        },
        {
          "appid": 257970,
          "name": "Loren The Amazon Princess",
          "playtime_forever": 0,
          "img_icon_url": "8808bdf5d01f50ad9c5fba80dd6da7a12ce7d712",
          "img_logo_url": "e1d53f06b8d767f181fba185f00bf2e8814c1e59",
          "has_community_visible_stats": true
        },
        {
          "appid": 275390,
          "name": "Guacamelee! Super Turbo Championship Edition",
          "playtime_forever": 0,
          "img_icon_url": "933dbbe576dca66cc46757e58871b2ae597a16bf",
          "img_logo_url": "308ffe62dde83000f2ded073dae8feee16b187fa",
          "has_community_visible_stats": true
        },
        {
          "appid": 275470,
          "name": "Chip",
          "playtime_forever": 0,
          "img_icon_url": "1870d035c666093214bc70b164c5fcef8506c56f",
          "img_logo_url": "0a70f5973a983e7444d1cf3fb69ec55d894ca28e",
          "has_community_visible_stats": true
        },
        {
          "appid": 237890,
          "name": "Agarest: Generations of War",
          "playtime_forever": 333,
          "img_icon_url": "cf4c1c2c0268938159de975a8241f2908097c850",
          "img_logo_url": "8d73aab62f83dc498329f81605662c7097161c6b",
          "has_community_visible_stats": true
        },
        {
          "appid": 277470,
          "name": "The Book of Legends",
          "playtime_forever": 0,
          "img_icon_url": "1c89a5d6421bfeac4582119074195fcff5ba1680",
          "img_logo_url": "e7eb0bf8e3c290dc01a53ec7e20ac8f2bb9e39cf",
          "has_community_visible_stats": true
        },
        {
          "appid": 277520,
          "name": "Albedo: Eyes from Outer Space",
          "playtime_forever": 330,
          "img_icon_url": "64ca32148587bc6c0afd61f1a7d546e5e1474f20",
          "img_logo_url": "9d34c93c53b561593eb1b91edfdd9ae23b21654e",
          "has_community_visible_stats": true
        },
        {
          "appid": 277680,
          "name": "About Love, Hate and the other ones",
          "playtime_forever": 349,
          "img_icon_url": "1332cafb6ef4695ecafe8e6de61bd7da763fec80",
          "img_logo_url": "93976bd329cdc0347629790cebec84ecb4cc6d78",
          "has_community_visible_stats": true
        },
        {
          "appid": 277890,
          "name": "Shantae: Risky's Revenge - Director's Cut",
          "playtime_forever": 578,
          "img_icon_url": "8277b9535db820adf146e2975a55d578c683b6ce",
          "img_logo_url": "f4e1636363b77c9b1237318db69ed63119e16fb5",
          "has_community_visible_stats": true
        },
        {
          "appid": 278360,
          "name": "A Story About My Uncle",
          "playtime_forever": 347,
          "img_icon_url": "feb33a53334222c82aaf0545e00913bd92bcd3d9",
          "img_logo_url": "c09b785aaf65100b28703e661223230db6e3232d",
          "has_community_visible_stats": true
        },
        {
          "appid": 22100,
          "name": "Mount & Blade",
          "playtime_forever": 0,
          "img_icon_url": "86d2f78aaff6629cded326df094c933b50302f0f",
          "img_logo_url": "473636784ed6eba8d20a0ed105c1a1ed106fee35"
        },
        {
          "appid": 278460,
          "name": "Skyborn",
          "playtime_forever": 0,
          "img_icon_url": "c9ffdb61ac610c337e437006c6f2da78c673f769",
          "img_logo_url": "4e719b701c309d5224a70856862cff9051d93413",
          "has_community_visible_stats": true
        },
        {
          "appid": 278490,
          "name": "Aveyond 3-2: Gates of Night",
          "playtime_forever": 0,
          "img_icon_url": "a78fad9e786a4ba0ff4f36e862e33b3781da9dec",
          "img_logo_url": "2bde1568d85722520b6a6b1a204f14ac83e1a78a"
        },
        {
          "appid": 278530,
          "name": "3 Stars of Destiny",
          "playtime_forever": 356,
          "img_icon_url": "b3e45b503ff29f9495d2878e0b21ee9ddf3aa24e",
          "img_logo_url": "183483a5ea531fc2e0d872d2e281f88f9d21defe",
          "has_community_visible_stats": true
        },
        {
          "appid": 278640,
          "name": "Terrian Saga: KR-17",
          "playtime_forever": 0,
          "img_icon_url": "1f40068714f7d3f04e21b43a819446ce39a82beb",
          "img_logo_url": "21f78e9a4bcc7badf22c908fb4934966848fe7da",
          "has_community_visible_stats": true
        },
        {
          "appid": 239160,
          "name": "Thief",
          "playtime_forever": 0,
          "img_icon_url": "d7688a71380a10c1e6113cee1a25ec8c7ae85aed",
          "img_logo_url": "8b3fe89d5099893ba9f39bde79259fcf92964f6b",
          "has_community_visible_stats": true
        },
        {
          "appid": 213670,
          "name": "South Park™: The Stick of Truth™",
          "playtime_forever": 79,
          "img_icon_url": "afad8295902080fb2aedd9aaabb3e21c10eecc85",
          "img_logo_url": "659c39e8727258477a5fab408ed0cf0c79b67fbc",
          "has_community_visible_stats": true
        },
        {
          "appid": 279500,
          "name": "Nicolas Eymerich The Inquisitor",
          "playtime_forever": 0,
          "img_icon_url": "77cdd59e6ab1cffc64fa714a51bacb8bfdf1a6b3",
          "img_logo_url": "027477c5042863c3c67d03405d39dd977b1da893",
          "has_community_visible_stats": true
        },
        {
          "appid": 279580,
          "name": "Devil's Dare",
          "playtime_forever": 0,
          "img_icon_url": "d83cafd6afa0a6d8ddd94f9411662dd8f7b214ab",
          "img_logo_url": "649c5484d50b0903a57697d65c57b81fdef40724",
          "has_community_visible_stats": true
        },
        {
          "appid": 280140,
          "name": "Millennium - A New Hope",
          "playtime_forever": 0,
          "img_icon_url": "4067d5e759780fa3323922ccd97732d2ea51aece",
          "img_logo_url": "986547b3477fb3f0db7f39df52d6d1106eeb0ce0",
          "has_community_visible_stats": true
        },
        {
          "appid": 281200,
          "name": "A Boy and His Blob",
          "playtime_forever": 349,
          "img_icon_url": "0520b5276c59c1dda534581ea336e9d9b46a5037",
          "img_logo_url": "d543c38e3256efcf0c2b05a8c6b28695a68b076b",
          "has_community_visible_stats": true
        },
        {
          "appid": 235210,
          "name": "Strider",
          "playtime_forever": 0,
          "img_icon_url": "363d62e4943638e09dfbd991ffffb6d401859749",
          "img_logo_url": "dd5f5ed41ebc12d5c0b20ce8073dfd163bf83c90",
          "has_community_visible_stats": true
        },
        {
          "appid": 254700,
          "name": "resident evil 4 / biohazard 4",
          "playtime_forever": 0,
          "img_icon_url": "535bfea3332662271f1e3a972832bc0b4aba5a38",
          "img_logo_url": "532d72710af44f29cc123c5796e95e0382461ee5",
          "has_community_visible_stats": true
        },
        {
          "appid": 281940,
          "name": "Woolfe - The Red Hood Diaries",
          "playtime_forever": 0,
          "img_icon_url": "a5612264d78f0ca653aab70e0d5ad9a137a3e0a4",
          "img_logo_url": "7fb46465741440ca91edeee4acfd66cd3ca14293",
          "has_community_visible_stats": true
        },
        {
          "appid": 282140,
          "name": "SOMA",
          "playtime_forever": 0,
          "img_icon_url": "018a23ae2cd618b302731b91fcef5954c22d615c",
          "img_logo_url": "ae099d49d4353982e81043704ef239d1074cc67d",
          "has_community_visible_stats": true
        },
        {
          "appid": 282530,
          "name": "Castlevania: Lords of Shadow – Mirror of Fate HD",
          "playtime_forever": 848,
          "img_icon_url": "361a38b8c0a2f032bcb82c07d0c72d5553a4e1ff",
          "img_logo_url": "7b0e58ad9558a280cda8e49654132f301ebac79e",
          "has_community_visible_stats": true
        },
        {
          "appid": 282620,
          "name": "The Battle of Sol",
          "playtime_forever": 0,
          "img_icon_url": "a34dd1e3f63edecef859bb79d9fba7acf9d80556",
          "img_logo_url": "1d5f3dcb396fc7c49620badf59b252c628a0b5f2",
          "has_community_visible_stats": true
        },
        {
          "appid": 282760,
          "name": "Circuits",
          "playtime_forever": 0,
          "img_icon_url": "4f5d542a05347b80734ef12981af9005809c2ee3",
          "img_logo_url": "7a108f4a205f48460723966b9ac482206a05cb21",
          "has_community_visible_stats": true
        },
        {
          "appid": 282900,
          "name": "Hyperdimension Neptunia Re;Birth1",
          "playtime_forever": 0,
          "img_icon_url": "0cd228210df4f44ef8e77f62b18d1a9af81a72d4",
          "img_logo_url": "c2835cfa39dbaf8e7e1f668e4bf491855a274947",
          "has_community_visible_stats": true
        },
        {
          "appid": 283180,
          "name": "The Samaritan Paradox",
          "playtime_forever": 0,
          "img_icon_url": "bdbba74a6af9ae9af6016d5c293504181ae86259",
          "img_logo_url": "e4c4b74b7078afeb5b1b7e025292c3b078a9ed4d",
          "has_community_visible_stats": true
        },
        {
          "appid": 283230,
          "name": "Spoiler Alert",
          "playtime_forever": 0,
          "img_icon_url": "86c7b69bfb7f998fe972d5723bbfb1c9b27588bd",
          "img_logo_url": "9c9a2b13cf9e2062d682693e02aae386c5285dc0",
          "has_community_visible_stats": true
        },
        {
          "appid": 283330,
          "name": "Desert Thunder",
          "playtime_forever": 0,
          "img_icon_url": "1cb57a8a38fd6634262904559fa846ee1b4d71fe",
          "img_logo_url": "8e4707d6444f78ee3073fefa05dade2ce21c9d1e"
        },
        {
          "appid": 283350,
          "name": "Eurofighter Typhoon",
          "playtime_forever": 0,
          "img_icon_url": "f9a2ef79928818894f29e0f7a74ef96cd50a2025",
          "img_logo_url": "7e32a43f9aea79bd11f503e945ca3be454adc211"
        },
        {
          "appid": 283430,
          "name": "Litil Divil",
          "playtime_forever": 0,
          "img_icon_url": "904a09969e0c53718bb1f5e498c8d35ef30ed50a",
          "img_logo_url": "ef6cf012c60375b43746b4d011bf748ebd93706e"
        },
        {
          "appid": 283680,
          "name": "Astebreed: Definitive Edition",
          "playtime_forever": 0,
          "img_icon_url": "6e03146f2a90c23ec68421d50275b1f3cee6c0e3",
          "img_logo_url": "8aca9983f3727d846bf92d5794d03a37b1833293",
          "has_community_visible_stats": true
        },
        {
          "appid": 284460,
          "name": "DeadCore",
          "playtime_forever": 0,
          "img_icon_url": "09c71ba4bd8e9c3289e5fbc1cbe63ef8c95441f8",
          "img_logo_url": "cfc9a6d858f1cdd1c6774be5514c3922d3b6caeb",
          "has_community_visible_stats": true
        },
        {
          "appid": 284970,
          "name": "Project Root",
          "playtime_forever": 0,
          "img_icon_url": "57cfeb1da622e439a772110b7ffa5b72f58f8f9e",
          "img_logo_url": "dbd841fa523c4726a993134164510feadb5048a4",
          "has_community_visible_stats": true
        },
        {
          "appid": 285070,
          "name": "Between Me and The Night",
          "playtime_forever": 0,
          "img_icon_url": "ef6d76d49b8a9ff07881bf420d9fecd099714d59",
          "img_logo_url": "e4b2b6edc8539dcdd8cca080d0547398a315c906",
          "has_community_visible_stats": true
        },
        {
          "appid": 285160,
          "name": "LEGO® The Hobbit™",
          "playtime_forever": 0,
          "img_icon_url": "b36006499181631396618d8c32dad8c13c6263ae",
          "img_logo_url": "4e0add73085939dff818bdc506437560feacfd91",
          "has_community_visible_stats": true
        },
        {
          "appid": 285820,
          "name": "Action Henk",
          "playtime_forever": 0,
          "img_icon_url": "e3087d28373bf8b95d697d6ee8f53dbc35eb9e6d",
          "img_logo_url": "d88e95493a4d2e2e2704463da04e63e12af3dc56",
          "has_community_visible_stats": true
        },
        {
          "appid": 286140,
          "name": "Eidolon",
          "playtime_forever": 0,
          "img_icon_url": "6b5b0ff59c47eb7fbb3bec90bdf1d9b637c73b23",
          "img_logo_url": "4338dcdf7a21bfbf0d59b43bdfa7d8cd33c8ef82"
        },
        {
          "appid": 286260,
          "name": "fault - milestone one",
          "playtime_forever": 0,
          "img_icon_url": "ccbf24b117f4df1f706d7d883891fd108c5b9912",
          "img_logo_url": "ef1898a69875b56d1dba9e4e873f624aa831c763",
          "has_community_visible_stats": true
        },
        {
          "appid": 286570,
          "name": "F1 2015",
          "playtime_forever": 0,
          "img_icon_url": "efba76526ca5cd18fabc9878c840ac583f08ead5",
          "img_logo_url": "7f31abdf6d6a1ce630cb96f3ac34a8a6be6243c8",
          "has_community_visible_stats": true
        },
        {
          "appid": 287340,
          "name": "Colin McRae Rally",
          "playtime_forever": 0,
          "img_icon_url": "68f63fecae9d9648bc461433036ec3fa33074ff5",
          "img_logo_url": "db0986dbbb106cd5be74caa89a6b345ce8820755",
          "has_community_visible_stats": true
        },
        {
          "appid": 236930,
          "name": "Blackwell Epiphany",
          "playtime_forever": 0,
          "img_icon_url": "6141a4b964bf09f0c1873015cec56dc96b5d801a",
          "img_logo_url": "1beb836f58937d007dcbdfeccb657ead65d60e55",
          "has_community_visible_stats": true
        },
        {
          "appid": 287700,
          "name": "METAL GEAR SOLID V: THE PHANTOM PAIN",
          "playtime_forever": 0,
          "img_icon_url": "7a1737163c96ea641143db45709a4ac444ba8f7b",
          "img_logo_url": "6af3d5aae37f4d5e6add4f49d5edd58f7bdd420b",
          "has_community_visible_stats": true
        },
        {
          "appid": 287980,
          "name": "Mini Metro",
          "playtime_forever": 0,
          "img_icon_url": "1b389229474bb7112d0f0b8189b1cbe4ff8f14b5",
          "img_logo_url": "4234f411b730d42f76721274c806e80beefd5bd1",
          "has_community_visible_stats": true
        },
        {
          "appid": 288060,
          "name": "Whispering Willows",
          "playtime_forever": 0,
          "img_icon_url": "e23e8166821f223e489e2c1db6b0bcdd5ea0044a",
          "img_logo_url": "910ac4bc31a7422cd9705cdc4d022929e4c9b6aa",
          "has_community_visible_stats": true
        },
        {
          "appid": 288880,
          "name": "Pineview Drive",
          "playtime_forever": 0,
          "img_icon_url": "41691a6c21b4c33bfa082c0c4a03eed5d79864db",
          "img_logo_url": "4e0f6d77bff739a43910455fdb23afcf7d8032ef",
          "has_community_visible_stats": true
        },
        {
          "appid": 233290,
          "name": "MURDERED: SOUL SUSPECT™",
          "playtime_forever": 0,
          "img_icon_url": "b0507554c8d39f2c4ebd45a446660e6b0d93ff52",
          "img_logo_url": "affa1c1b6d38bfcc2a870ad6c3bd4ceed5641966",
          "has_community_visible_stats": true
        },
        {
          "appid": 228400,
          "name": "ACE COMBAT™ ASSAULT HORIZON Enhanced Edition",
          "playtime_forever": 0,
          "img_icon_url": "13de66d70c73e745734f924af94e3957d3852fbc",
          "img_logo_url": "83fa0368512c32d0838ae8bb2a1e2069e3d958ec",
          "has_community_visible_stats": true
        },
        {
          "appid": 290770,
          "name": "The Fall",
          "playtime_forever": 0,
          "img_icon_url": "ba47c2f507c83a267c2182805d747120e8117050",
          "img_logo_url": "3842fbcad476bfc442df26181feebe428676af24",
          "has_community_visible_stats": true
        },
        {
          "appid": 291010,
          "name": "The Hat Man: Shadow Ward",
          "playtime_forever": 0,
          "img_icon_url": "8a9fd7a24e99762464daa999cde440cca5373d2c",
          "img_logo_url": "eb73de1295eb89e671c196df13de78200ac10db3",
          "has_community_visible_stats": true
        },
        {
          "appid": 291130,
          "name": "Akane the Kunoichi",
          "playtime_forever": 335,
          "img_icon_url": "f4fb81df6e1368b012160f344bf69fa2cdbdb447",
          "img_logo_url": "5c2d6e151ca2766a71ddc162c2b37a93adb67109",
          "has_community_visible_stats": true
        },
        {
          "appid": 251430,
          "name": "The Inner World",
          "playtime_forever": 0,
          "img_icon_url": "1d80cce2b884a54f983700b4e1b4e98fc4b8cf0f",
          "img_logo_url": "947dec2942d74ec42deaec8a343adb98c97c8a57",
          "has_community_visible_stats": true
        },
        {
          "appid": 202310,
          "name": "Ridge Racer™ Unbounded",
          "playtime_forever": 0,
          "img_icon_url": "360a4850060cc3579611e1992f93006ebef72c78",
          "img_logo_url": "559703d4c20e19cc9bf03be521debcc3d9b5c925",
          "has_community_visible_stats": true
        },
        {
          "appid": 292380,
          "name": "Racer 8",
          "playtime_forever": 0,
          "img_icon_url": "dc95e7d7c2a247a42e226733ebb229963b065454",
          "img_logo_url": "4c0577f37d5eaba1cc76c9607ddd6a983fc0e78c",
          "has_community_visible_stats": true
        },
        {
          "appid": 292390,
          "name": "Realms of the Haunting",
          "playtime_forever": 0,
          "img_icon_url": "13abd6b022bb179fe9fdc41411ac0c525c320639",
          "img_logo_url": "8a739324621874fa3e31b440f1712194c7674a7f"
        },
        {
          "appid": 293440,
          "name": "Elliot Quest",
          "playtime_forever": 19,
          "img_icon_url": "69320d78870d443def86155038e78fd5ca749c76",
          "img_logo_url": "d22b1bb8a5236de469fae79d45cc7b9c27e6fd26",
          "has_community_visible_stats": true
        },
        {
          "appid": 294020,
          "name": "Merchants of Kaidan",
          "playtime_forever": 0,
          "img_icon_url": "78b1d0ea5b99a63d458ff8c6b907644c4df8871c",
          "img_logo_url": "8f082e52ad83ce34300b52d8971dc4794d59b8fe",
          "has_community_visible_stats": true
        },
        {
          "appid": 294040,
          "name": "Loot Hero DX",
          "playtime_forever": 0,
          "img_icon_url": "e7afe980c69c967599c5d827f6d50b72f500692d",
          "img_logo_url": "dc0b52c58f2253b685b811386a029ccc2f76c748",
          "has_community_visible_stats": true
        },
        {
          "appid": 201810,
          "name": "Wolfenstein: The New Order",
          "playtime_forever": 1012,
          "img_icon_url": "965b8943ad172d8cf0c74e86068b4aa670aba92c",
          "img_logo_url": "e945e10daf94f6fe80d629a1c3381125df27f4cb",
          "has_community_visible_stats": true
        },
        {
          "appid": 256290,
          "name": "Child of Light",
          "playtime_forever": 0,
          "img_icon_url": "abd72e23f4d18e943f98169e4a3a4a9243e069ea",
          "img_logo_url": "1b2d7aa9019a5b6f75070c75a8d54745d576b0cc",
          "has_community_visible_stats": true
        },
        {
          "appid": 294810,
          "name": "BlazBlue: Continuum Shift Extend",
          "playtime_forever": 0,
          "img_icon_url": "244644a6721480f40e4de61e4bb7a337d999e435",
          "img_logo_url": "b7105e9443e1f3bed7bb3fd1599a140e75dcf2c6",
          "has_community_visible_stats": true
        },
        {
          "appid": 294860,
          "name": "Valkyria Chronicles™",
          "playtime_forever": 0,
          "img_icon_url": "176c6dcafc9bf0fbb87f9adeb224df88c8248a66",
          "img_logo_url": "08316e1d6a45d8e13de7f5e1a1480cf4efff15cf",
          "has_community_visible_stats": true
        },
        {
          "appid": 246070,
          "name": "Hack 'n' Slash",
          "playtime_forever": 0,
          "img_icon_url": "57587c74e58cfff46972f0cd3867ff7ad4f3a30c",
          "img_logo_url": "47a902e7589ee213d82159a22cddb2cb8428aa5a",
          "has_community_visible_stats": true
        },
        {
          "appid": 296490,
          "name": "GemCraft - Chasing Shadows",
          "playtime_forever": 0,
          "img_icon_url": "d9dee57d707fc52432ad5dd48f196948c32b767c",
          "img_logo_url": "ced2f850435f5e56aeba054e6df8a1a17bdd8a28",
          "has_community_visible_stats": true
        },
        {
          "appid": 296630,
          "name": "Kraven Manor",
          "playtime_forever": 0,
          "img_icon_url": "23f6c628a2b514aa0fed30c32713e40f9c553532",
          "img_logo_url": "37ee9552df393ce872535118140469b9052a3473",
          "has_community_visible_stats": true
        },
        {
          "appid": 296710,
          "name": "Monstrum ",
          "playtime_forever": 0,
          "img_icon_url": "76b297603afd400703606320c97faa90b0c2d03f",
          "img_logo_url": "43c616cbe0ae3ee702446bd98c4486a8b6b54b74",
          "has_community_visible_stats": true
        },
        {
          "appid": 296830,
          "name": "Lucius II",
          "playtime_forever": 0,
          "img_icon_url": "ad52a3fdf6c9ec72f432b0a4570e55c672cbd57f",
          "img_logo_url": "de99858f47f4a9170a7f67f080e4793d67ec9b11",
          "has_community_visible_stats": true
        },
        {
          "appid": 297090,
          "name": "World War 1 Centennial Edition",
          "playtime_forever": 0,
          "img_icon_url": "26a72fa1b1a4d58650aca8c5765777480489a29a",
          "img_logo_url": "aa65960132429dd4b23a22bdf0923738a55da166"
        },
        {
          "appid": 297370,
          "name": "Hero of Many",
          "playtime_forever": 0,
          "img_icon_url": "17c5b7287f98e205ec64c8218ef756b25620a7b6",
          "img_logo_url": "86106f7f8d16ba04c064d4436fc6da8bd592e243",
          "has_community_visible_stats": true
        },
        {
          "appid": 297490,
          "name": "Spy Chameleon - RGB Agent",
          "playtime_forever": 0,
          "img_icon_url": "2b65742cc18d62f3d170bdbcc344e9e8a2306d91",
          "img_logo_url": "282f3dae377c6ab0694541875ed637c64d7700fe",
          "has_community_visible_stats": true
        },
        {
          "appid": 298890,
          "name": "Adventure Time: The Secret Of The Nameless Kingdom",
          "playtime_forever": 0,
          "img_icon_url": "18d2991fa7b2e76cbc71e549829d8dd444a3852c",
          "img_logo_url": "c43055e5a18a79353bf8a7be6a6d4633b79634dd",
          "has_community_visible_stats": true
        },
        {
          "appid": 237930,
          "name": "Transistor",
          "playtime_forever": 0,
          "img_icon_url": "b45dc369d45afe945d379e75545959f845094f92",
          "img_logo_url": "705c88fb22dc45a6e886f5afd96ba3c2e258aa06",
          "has_community_visible_stats": true
        },
        {
          "appid": 299440,
          "name": "Rooks Keep",
          "playtime_forever": 0,
          "img_icon_url": "6be1b2794c3c9fc05c2018cdbbc8873a21ea2c6e",
          "img_logo_url": "d2d40d9badfa206d9efc4b6756c8741361c5a7db",
          "has_community_visible_stats": true
        },
        {
          "appid": 299680,
          "name": "Tengami",
          "playtime_forever": 0,
          "img_icon_url": "c83ba9e7134d9c3c2acf3e7ed130b6357f9d4f9f",
          "img_logo_url": "86098e79a2719aebb0930ba2afaf30966c7b5719",
          "has_community_visible_stats": true
        },
        {
          "appid": 300550,
          "name": "Shadowrun: Dragonfall - Director's Cut",
          "playtime_forever": 0,
          "img_icon_url": "1fe5384c9e513285fd2eaf920b2615f9f0dab4b6",
          "img_logo_url": "e0c29af1992441227caacb463b2b46b9f41b2309",
          "has_community_visible_stats": true
        },
        {
          "appid": 300570,
          "name": "Infinifactory",
          "playtime_forever": 0,
          "img_icon_url": "7566ab0b4fa8cad3c5c15b4f9a23498441a04281",
          "img_logo_url": "5b41f9020fa38615e7a40e3db2bf2b50bd2dbb64",
          "has_community_visible_stats": true
        },
        {
          "appid": 239120,
          "name": "FINAL FANTASY III",
          "playtime_forever": 0,
          "img_icon_url": "618600b4e0706858a1ea49ac539de931ea394eae",
          "img_logo_url": "6eb865019ee1a42555fa9303c8c7386cc5323b89",
          "has_community_visible_stats": true
        },
        {
          "appid": 292120,
          "name": "FINAL FANTASY XIII",
          "playtime_forever": 2886,
          "img_icon_url": "83c929d4965963f6e0bc17969a2599e7829ac23d",
          "img_logo_url": "e9650bfd8e5af872bb7972ec2da9b9bd95ef123b",
          "has_community_visible_stats": true
        },
        {
          "appid": 292140,
          "name": "FINAL FANTASY XIII-2",
          "playtime_forever": 1816,
          "img_icon_url": "7ce525f8676dd0c8e5827db329c69e5f6ee82366",
          "img_logo_url": "7ac75033c9590f543a6e0018cd599e0ac2f8e207",
          "has_community_visible_stats": true
        },
        {
          "appid": 301220,
          "name": "Legends of Persia",
          "playtime_forever": 0,
          "img_icon_url": "4866eb4fc0047d9217095c13cefd7c97ee5e9103",
          "img_logo_url": "e399de982c8c14b5d500fcc5825ee48f5f51b45c",
          "has_community_visible_stats": true
        },
        {
          "appid": 286690,
          "name": "Metro 2033 Redux",
          "playtime_forever": 104,
          "img_icon_url": "353ea01a045c084215bca95519808eaa7319ce0c",
          "img_logo_url": "ee15b7b90827e5450caf4ec5b021c7753153514d",
          "has_community_visible_stats": true
        },
        {
          "appid": 287390,
          "name": "Metro: Last Light Redux",
          "playtime_forever": 0,
          "img_icon_url": "7139b7af2ad9d248d21e98302b964cd61474318c",
          "img_logo_url": "fe86857d70887fab98ae989d09771ee4bcbce861",
          "has_community_visible_stats": true
        },
        {
          "appid": 301560,
          "name": "White Night",
          "playtime_forever": 0,
          "img_icon_url": "cfc1a861c6cef3bbfc9052fc110f91b3297908e1",
          "img_logo_url": "5b026defaee7645f5b112c07327a788a8f1759d2",
          "has_community_visible_stats": true
        },
        {
          "appid": 302790,
          "name": "Momodora III",
          "playtime_forever": 0,
          "img_icon_url": "2ddfb45c1c1c06ff89c093eecc61ee3bcaa5621c",
          "img_logo_url": "dc60a71443f9a76cdd01e96423c4f9e533d8a542",
          "has_community_visible_stats": true
        },
        {
          "appid": 303390,
          "name": "Dead Bits",
          "playtime_forever": 0,
          "img_icon_url": "23c393739364bf2fb75577bdf03c8cafc418b8e4",
          "img_logo_url": "1d139a1ea3a0b537218e703b39f4160e1a90e0aa",
          "has_community_visible_stats": true
        },
        {
          "appid": 303860,
          "name": "Machines At War 3",
          "playtime_forever": 0,
          "img_icon_url": "1d5b0237ff6d3bd19e2e50a4872c2e78a8c1929a",
          "img_logo_url": "589216f2667a09423b698d47334bbbcc392c89f2",
          "has_community_visible_stats": true
        },
        {
          "appid": 243470,
          "name": "Watch_Dogs",
          "playtime_forever": 0,
          "img_icon_url": "53368e59a196dfa9af66ecd32135939da97fa72e",
          "img_logo_url": "8957636e0668d8913539aa535a8a508e3e54b1f4",
          "has_community_visible_stats": true
        },
        {
          "appid": 305920,
          "name": "Another Perspective",
          "playtime_forever": 0,
          "img_icon_url": "e1d2af3f891ff69905cea125e557f4fd8f6bf10c",
          "img_logo_url": "d904637bed13557c7e3d05b0f11c254911404b55",
          "has_community_visible_stats": true
        },
        {
          "appid": 250400,
          "name": "How to Survive",
          "playtime_forever": 0,
          "img_icon_url": "2c493ce76970c4d70183c17b2e44b0099f0c56d5",
          "img_logo_url": "2f8ca72af2f828d0480aae0998d1d79532599366",
          "has_community_visible_stats": true
        },
        {
          "appid": 246840,
          "name": "FATE",
          "playtime_forever": 0,
          "img_icon_url": "33b277e8ece514cb371ed36b601627885551087c",
          "img_logo_url": "6129712560fee9bb516f70e52c46abd93008b8ad",
          "has_community_visible_stats": true
        },
        {
          "appid": 276890,
          "name": "FATE: Undiscovered Realms",
          "playtime_forever": 0,
          "img_icon_url": "3483b80736999ce9a553e0e6d0637ef86c3ea2ff",
          "img_logo_url": "ac655ed229924729a62b6f29f6969fdb2d7c5d48",
          "has_community_visible_stats": true
        },
        {
          "appid": 307210,
          "name": "Will Fight for Food: Super Actual Sellout: Game of the Hour",
          "playtime_forever": 0,
          "img_icon_url": "2835dc1c33c415f1ab276f0354de762f5b8cfe1e",
          "img_logo_url": "a3105a13de24d646fd8d93bbec9f09d5c32b1752",
          "has_community_visible_stats": true
        },
        {
          "appid": 308040,
          "name": "Back to Bed",
          "playtime_forever": 0,
          "img_icon_url": "030732feca4ee5746b201e4e8e06c003f4df7850",
          "img_logo_url": "92db8bb5a5f230d7dad9e8624221beba3797edb1",
          "has_community_visible_stats": true
        },
        {
          "appid": 308420,
          "name": "Ziggurat",
          "playtime_forever": 0,
          "img_icon_url": "f71ca22e208f97aed120a2d16889e4b2ee47cd9c",
          "img_logo_url": "b07c41cc37ed95263c03e567bd659aa391d7b82d",
          "has_community_visible_stats": true
        },
        {
          "appid": 310080,
          "name": "Hatoful Boyfriend",
          "playtime_forever": 0,
          "img_icon_url": "1fec8ef53bcbd40149e47820abeaf969f364e1da",
          "img_logo_url": "2bdfe5a272a3c12e2d12add904c92bcce1e1118e",
          "has_community_visible_stats": true
        },
        {
          "appid": 9480,
          "name": "Saints Row 2",
          "playtime_forever": 0,
          "img_icon_url": "1a0aef912300d396a67f9eda9d8b4e66c41c9891",
          "img_logo_url": "2b9558ad2e909957d66b4d8aa5e8b51eda951f69"
        },
        {
          "appid": 55230,
          "name": "Saints Row: The Third",
          "playtime_forever": 0,
          "img_icon_url": "ec83645f13643999e7c91da75d418053d6b56529",
          "img_logo_url": "1129528455a8b297fb6404cbb90e802a62881b11",
          "has_community_visible_stats": true
        },
        {
          "appid": 206420,
          "name": "Saints Row IV",
          "playtime_forever": 0,
          "img_icon_url": "b5e8448a3e2ea31ddf3595addae4e1eee2375c0d",
          "img_logo_url": "6f7e659c6b58971ebf9710b7f0048c20c68aadd7",
          "has_community_visible_stats": true
        },
        {
          "appid": 310360,
          "name": "Higurashi When They Cry Hou - Ch.1 Onikakushi",
          "playtime_forever": 0,
          "img_icon_url": "8347887b669eb9459f5359516bb7bb99cc0dc8e3",
          "img_logo_url": "a5584c5b8d35f87bc105a585098517098180782c",
          "has_community_visible_stats": true
        },
        {
          "appid": 310700,
          "name": "Super Win the Game",
          "playtime_forever": 0,
          "img_icon_url": "4a7752d2e865d15a77117d554d404a8542ff5d1e",
          "img_logo_url": "88f985e50ae047e74c362932260e8d84d22c074b",
          "has_community_visible_stats": true
        },
        {
          "appid": 237850,
          "name": "Dreamfall Chapters",
          "playtime_forever": 0,
          "img_icon_url": "e1238d11e33f11cb516d460dea2cd1d42d84ec2e",
          "img_logo_url": "06507734d920cae6fcd5275cf97069bcd0e365c4",
          "has_community_visible_stats": true
        },
        {
          "appid": 311340,
          "name": "METAL GEAR SOLID V: GROUND ZEROES",
          "playtime_forever": 122,
          "img_icon_url": "60ea6c7b08077263bac018bcd32435ec858e867c",
          "img_logo_url": "a1dc6285cba9b17f37c8e1e341a747576f1e121c",
          "has_community_visible_stats": true
        },
        {
          "appid": 311480,
          "name": "Vertical Drop Heroes HD",
          "playtime_forever": 0,
          "img_icon_url": "3a100354ad12a11707e458404428faf5ca844442",
          "img_logo_url": "694a7e47def8d55aed9adeec60514853b8e4cf38",
          "has_community_visible_stats": true
        },
        {
          "appid": 312540,
          "name": "Ys VI: The Ark of Napishtim",
          "playtime_forever": 0,
          "img_icon_url": "b7c5eec48a8a8a3133f290c9e302cacf4285c359",
          "img_logo_url": "bddba547308ec03a329fff798589f248a6f502f7",
          "has_community_visible_stats": true
        },
        {
          "appid": 312600,
          "name": "Rime Berta",
          "playtime_forever": 0,
          "img_icon_url": "b3f65d9fdf80b90358e0ad25ea83d84ac8a52756",
          "img_logo_url": "aae5e8e5c086409940c45c0a2bb60b49aad68d55",
          "has_community_visible_stats": true
        },
        {
          "appid": 312610,
          "name": "METAL SLUG X",
          "playtime_forever": 101,
          "img_icon_url": "9ada8e531324056d8b31e175bc30fb7dcd582a65",
          "img_logo_url": "0aefb6b3c12d129be74b9efdcefd46a8f919dba2",
          "has_community_visible_stats": true
        },
        {
          "appid": 312840,
          "name": "Fahrenheit: Indigo Prophecy Remastered",
          "playtime_forever": 0,
          "img_icon_url": "d232a53093cd49a04df465bdc72c36332da7d18f",
          "img_logo_url": "e176c7506da0a20e339a5b64d2c19debf39fd837",
          "has_community_visible_stats": true
        },
        {
          "appid": 268420,
          "name": "Aura Kingdom",
          "playtime_forever": 0,
          "img_icon_url": "934ca855013bed0e09446708e38b1f2a0b31779f",
          "img_logo_url": "bb09c5d38267fe31d6fc49ffddbb5b611d110710"
        },
        {
          "appid": 313400,
          "name": "REVOLVER360 RE:ACTOR",
          "playtime_forever": 0,
          "img_icon_url": "1c1e9fcd382fec011512d7d000d9861d4d042e8d",
          "img_logo_url": "2f500e024e77f1fe174cf46f027f86704a64e685",
          "has_community_visible_stats": true
        },
        {
          "appid": 313470,
          "name": "Boot Hill Heroes",
          "playtime_forever": 1071,
          "img_icon_url": "1730700dee69b5b4d1797677d3cee2b07432ad08",
          "img_logo_url": "b09ac2630c145dfaea2edf7379151985cabe84d7",
          "has_community_visible_stats": true
        },
        {
          "appid": 313740,
          "name": "Sakura Spirit",
          "playtime_forever": 0,
          "img_icon_url": "a726cf15cd314542c12a2e157b30f8ae5e3ec776",
          "img_logo_url": "a011e605797b95cd94fb6109d80bc9ee836544e0"
        },
        {
          "appid": 314150,
          "name": "Double Dragon Trilogy",
          "playtime_forever": 0,
          "img_icon_url": "6c621f65c5f5fb700f88ae966220e54bbcba76e9",
          "img_logo_url": "bc197f21e81ac8b055aa8c2ab1610ec4ea4f3fa7",
          "has_community_visible_stats": true
        },
        {
          "appid": 314790,
          "name": "Silence",
          "playtime_forever": 0,
          "img_icon_url": "b8ef68689361eb486147b2f3c4fe264e58812a27",
          "img_logo_url": "ae0fe7ead1433571eb5f00d8bd1081ec6e7d78ba",
          "has_community_visible_stats": true
        },
        {
          "appid": 314810,
          "name": "Randal's Monday",
          "playtime_forever": 0,
          "img_icon_url": "c778726bf49cea5dc7a5495528391c578eb92726",
          "img_logo_url": "7ed9805d63a639481a5665988154f9a204c260bc",
          "has_community_visible_stats": true
        },
        {
          "appid": 10150,
          "name": "Prototype",
          "playtime_forever": 0,
          "img_icon_url": "c09d5f7b718a963b1f24162184c28d3293e8fd8c",
          "img_logo_url": "5bfed786b34d71ee47edf9a40cb8d5e4413abb28",
          "has_community_visible_stats": true
        },
        {
          "appid": 115320,
          "name": "PROTOTYPE 2",
          "playtime_forever": 0,
          "img_icon_url": "157062f0a8a18c0ae1550feb327b21e2407f528a",
          "img_logo_url": "e2410507cd791d9e9088fe6a2445b3de886f1f01",
          "has_community_visible_stats": true
        },
        {
          "appid": 255070,
          "name": "Abyss Odyssey",
          "playtime_forever": 346,
          "img_icon_url": "f4d556775211ef09c6cc482c99411ee41c732325",
          "img_logo_url": "00d3f7815a6f2efe46788c6e8b17bd15870ff13c",
          "has_community_visible_stats": true
        },
        {
          "appid": 315650,
          "name": "Deep Under the Sky",
          "playtime_forever": 0,
          "img_icon_url": "2672091154581f2a2d81139d97208cfe7ba76bb0",
          "img_logo_url": "068bf4e19f5ed2db1173fa221aefe927b641e112",
          "has_community_visible_stats": true
        },
        {
          "appid": 315810,
          "name": "eden*",
          "playtime_forever": 0,
          "img_icon_url": "32288e5d3079604742a0da4b01cb67843ceb7be4",
          "img_logo_url": "2a660a08d542b6accc45d38e779aff522b2d1282"
        },
        {
          "appid": 12100,
          "name": "Grand Theft Auto III",
          "playtime_forever": 0,
          "img_icon_url": "646c4b3bc16b6726e190b7a29717c4b71f9abc7d",
          "img_logo_url": "0e73825e3abd7bfe43b55a49bbcb862aee7c2e71",
          "has_community_visible_stats": true
        },
        {
          "appid": 12110,
          "name": "Grand Theft Auto: Vice City",
          "playtime_forever": 0,
          "img_icon_url": "5f7423da14152cfbe201103ceba3112988624a98",
          "img_logo_url": "6ba37ecba052f89c72272dd28b2daa89087a7eb3",
          "has_community_visible_stats": true
        },
        {
          "appid": 12120,
          "name": "Grand Theft Auto: San Andreas",
          "playtime_forever": 0,
          "img_icon_url": "895e9391da2b8155989022d9dd95b12e09766375",
          "img_logo_url": "32e2d1d2054295603724f30c81c3cf46dc6392c0",
          "has_community_visible_stats": true
        },
        {
          "appid": 12210,
          "name": "Grand Theft Auto IV",
          "playtime_forever": 0,
          "img_icon_url": "a3cf6a64c73f991898a9e34681d0db8226eaa191",
          "img_logo_url": "47fb2a3e0763be24e49662591d6e076c58b2178d",
          "has_community_visible_stats": true
        },
        {
          "appid": 12220,
          "name": "Grand Theft Auto: Episodes from Liberty City",
          "playtime_forever": 0,
          "img_icon_url": "603a444d9092d007f2d9640d12ebba62b1efc377",
          "img_logo_url": "40d1512d5922313878298c3731ffc066091a113a",
          "has_community_visible_stats": true
        },
        {
          "appid": 12230,
          "name": "Grand Theft Auto III",
          "playtime_forever": 0,
          "img_icon_url": "",
          "img_logo_url": "0e73825e3abd7bfe43b55a49bbcb862aee7c2e71",
          "has_community_visible_stats": true
        },
        {
          "appid": 12240,
          "name": "Grand Theft Auto: Vice City",
          "playtime_forever": 0,
          "img_icon_url": "",
          "img_logo_url": "6ba37ecba052f89c72272dd28b2daa89087a7eb3",
          "has_community_visible_stats": true
        },
        {
          "appid": 12250,
          "name": "Grand Theft Auto: San Andreas",
          "playtime_forever": 0,
          "img_icon_url": "",
          "img_logo_url": "32e2d1d2054295603724f30c81c3cf46dc6392c0",
          "has_community_visible_stats": true
        },
        {
          "appid": 315850,
          "name": "Amazing Princess Sarah",
          "playtime_forever": 334,
          "img_icon_url": "6df980eaec7eee1dfb1beba83e5ca4f9d4009ec7",
          "img_logo_url": "ef72b1cb196a32c0db445316d137bd07d4b2c245",
          "has_community_visible_stats": true
        },
        {
          "appid": 315860,
          "name": "Celestian Tales: Old North",
          "playtime_forever": 0,
          "img_icon_url": "e529d22161931172f0631d24be50c314ce187374",
          "img_logo_url": "a1bfc39dd0780851af12d7e4dc54a340e7871687",
          "has_community_visible_stats": true
        },
        {
          "appid": 316160,
          "name": "Broken Sword 4 - the Angel of Death",
          "playtime_forever": 0,
          "img_icon_url": "cd6d3cc6d8ed1db0bb62c8e3476f5d07910b6a44",
          "img_logo_url": "ef3d46da34e99fd9b0a256bd896d45bb55d0b4b2"
        },
        {
          "appid": 313360,
          "name": "The Maker's Eden",
          "playtime_forever": 0,
          "img_icon_url": "f777eebeb8be7a4b1878d55212ad88db2fc66cbe",
          "img_logo_url": "ad4fafb2793938cc2bc4246478332ccca0a31c31"
        },
        {
          "appid": 316790,
          "name": "Grim Fandango Remastered",
          "playtime_forever": 0,
          "img_icon_url": "4e9da606cb52dfebf296dc8d3b0bc6ba81065a52",
          "img_logo_url": "1cd953b329e53555cb1acba0b615edd75ad72ad4",
          "has_community_visible_stats": true
        },
        {
          "appid": 316840,
          "name": "The Sacred Tears TRUE",
          "playtime_forever": 0,
          "img_icon_url": "59bc6c0f807300160304f1909bfc18c059acf072",
          "img_logo_url": "253236faaab517c59573176be165cf8369d1a5d1"
        },
        {
          "appid": 317040,
          "name": "Strife: Veteran Edition",
          "playtime_forever": 0,
          "img_icon_url": "493eec0345ac61367722d22336b09ad09a187689",
          "img_logo_url": "a9d21d8806ab014f69836f1422f48c91503a6193",
          "has_community_visible_stats": true
        },
        {
          "appid": 205020,
          "name": "Lumino City",
          "playtime_forever": 0,
          "img_icon_url": "d6854b4b2fa69b562fef7df4178434e9bca958e3",
          "img_logo_url": "4ff0e840f4e49d3ecb8eb60e7fb595d213ae85ae"
        },
        {
          "appid": 253510,
          "name": "Warmachine Tactics",
          "playtime_forever": 0,
          "img_icon_url": "73e0e95503656718273a9596b0e83d3ce44e344a",
          "img_logo_url": "7fcc17724770975ff44c6a6445c80c1b97c89557",
          "has_community_visible_stats": true
        },
        {
          "appid": 318600,
          "name": "The Flame in the Flood",
          "playtime_forever": 0,
          "img_icon_url": "c02a40cc55fcb8f37d02680b1ee0da53290da5bf",
          "img_logo_url": "f4b4f192dfbfb2a3cba25e179710436950699531",
          "has_community_visible_stats": true
        },
        {
          "appid": 319140,
          "name": "Xeodrifter™",
          "playtime_forever": 31,
          "img_icon_url": "6309c2a443de4bac541e74932f565d9e905f85a1",
          "img_logo_url": "96715effc0bf10b0de15c7a6986eaa91df32bc9d",
          "has_community_visible_stats": true
        },
        {
          "appid": 319510,
          "name": "Five Nights at Freddy's",
          "playtime_forever": 0,
          "img_icon_url": "5da39a273efa65643cf4c12025898da0076dad69",
          "img_logo_url": "93881986f2f6f5a1dd7206eda9f9f3970d67a0cb"
        },
        {
          "appid": 319910,
          "name": "Trine 3: The Artifacts of Power",
          "playtime_forever": 0,
          "img_icon_url": "15853f24d7576a83cd4d54532a2a93c2d8d6024d",
          "img_logo_url": "0add332eadf0fff659f29f665048691cdeafd3f4",
          "has_community_visible_stats": true
        },
        {
          "appid": 320040,
          "name": "Moon Hunters",
          "playtime_forever": 0,
          "img_icon_url": "fc61b16cfd617414a286e338968dadf06a74c512",
          "img_logo_url": "e08118354c490c7319e84e7695ce1e16b7fad751",
          "has_community_visible_stats": true
        },
        {
          "appid": 8190,
          "name": "Just Cause 2",
          "playtime_forever": 0,
          "img_icon_url": "73582e392a2b9413fe93b011665a5b9cf26ff175",
          "img_logo_url": "26c2c027f835e968d78b212b4cc6438f692b027a",
          "has_community_visible_stats": true
        },
        {
          "appid": 203160,
          "name": "Tomb Raider",
          "playtime_forever": 0,
          "img_icon_url": "3ee640a8aba6992678e36f4e40cba9c71c02348b",
          "img_logo_url": "495f7d723659add6ea476b3699be5424282ac4b8",
          "has_community_visible_stats": true
        },
        {
          "appid": 317300,
          "name": "Roommates",
          "playtime_forever": 0,
          "img_icon_url": "d7019e1a97810fe8b101969b8220f1a406054105",
          "img_logo_url": "ddabd4d4d65c0597adc57cac99ac0a5351bf095b",
          "has_community_visible_stats": true
        },
        {
          "appid": 6850,
          "name": "Hitman 2: Silent Assassin",
          "playtime_forever": 0,
          "img_icon_url": "4c6b98ac952877ebfb890721624ae79276cf8358",
          "img_logo_url": "7ea9c76a7f5bb7da7a5e1b47ca4c7c664c0a7cd4"
        },
        {
          "appid": 6860,
          "name": "Hitman: Blood Money",
          "playtime_forever": 0,
          "img_icon_url": "c8f46248ca288cf332f170a954ffafff2c122131",
          "img_logo_url": "e67a1e905379399095d5dd509e1e58be3e4889b3"
        },
        {
          "appid": 6900,
          "name": "Hitman: Codename 47",
          "playtime_forever": 0,
          "img_icon_url": "e70bdfd4711b79b67d7e7f7cb0fda9bef539254a",
          "img_logo_url": "093e9f48d3f11c6fee729cfc50a13fc89488fd11"
        },
        {
          "appid": 203140,
          "name": "Hitman: Absolution",
          "playtime_forever": 0,
          "img_icon_url": "fe5e36ac1548793eb48b6b25b701b37d86fb94a3",
          "img_logo_url": "b86fc98f1d9d4339deff8a1cf117cc3c7080ff55",
          "has_community_visible_stats": true
        },
        {
          "appid": 205930,
          "name": "Hitman: Sniper Challenge",
          "playtime_forever": 0,
          "img_icon_url": "756d0ca797b4c8a1340dd73bf49b0bb0656a1802",
          "img_logo_url": "16c46e3680624cb22aa0a2c9d113f6e07f11bcbf",
          "has_community_visible_stats": true
        },
        {
          "appid": 247430,
          "name": "Hitman: Contracts",
          "playtime_forever": 0,
          "img_icon_url": "d8babb26255685ba5de55aa78eb6c7a267b33bac",
          "img_logo_url": "4a5352f1cf8471abe600cd289cdf8ef96d13958b"
        },
        {
          "appid": 200110,
          "name": "Nosgoth",
          "playtime_forever": 0,
          "img_icon_url": "5059f6d2b9c87f10a631fcdcbed1020f7aae0ef9",
          "img_logo_url": "9894c6c94dca1b2c6f1836a3c780d8504efdd4ef",
          "has_community_visible_stats": true
        },
        {
          "appid": 224300,
          "name": "Legacy of Kain: Defiance",
          "playtime_forever": 0,
          "img_icon_url": "ca46370c6c55bed7a3bcbd4eb2c684a2fd1b50e5",
          "img_logo_url": "e4680b81b8922e6ec53d85be87c6c538158fce67"
        },
        {
          "appid": 224920,
          "name": "Legacy of Kain: Soul Reaver",
          "playtime_forever": 0,
          "img_icon_url": "6c32e0d044dde141a46297b486e08c0fa9ccf3b8",
          "img_logo_url": "b6f5280ce118723a2abc6cff05d902f0cbce22d2"
        },
        {
          "appid": 224940,
          "name": "Legacy of Kain: Soul Reaver 2",
          "playtime_forever": 0,
          "img_icon_url": "ae768d1809857e57d67eb3238e01e9ac31a60187",
          "img_logo_url": "acc06bf19ed988180aee974780a4973236a6e284"
        },
        {
          "appid": 242960,
          "name": "Blood Omen 2: Legacy of Kain",
          "playtime_forever": 0,
          "img_icon_url": "305467c0b4cf5c54cc0b7fe5d17bec42778c2c46",
          "img_logo_url": "86d8745356a9379c628ffe8ae98b1d1c621937a4"
        },
        {
          "appid": 321040,
          "name": "DiRT 3 Complete Edition",
          "playtime_forever": 0,
          "img_icon_url": "fdfb3aa153b57d6ae6cd8099cb4456b3d5b182b6",
          "img_logo_url": "bb89ef19777c30c8551b0f4eea296d6ca33007cc",
          "has_community_visible_stats": true
        },
        {
          "appid": 321880,
          "name": "Aveyond 3-3: The Lost Orb",
          "playtime_forever": 0,
          "img_icon_url": "3bfa44cc625465067c13cc5d0a99083434001102",
          "img_logo_url": "585e6117a5352c91bda40e27a4ae85549717e650"
        },
        {
          "appid": 321890,
          "name": "Aveyond 3-4: The Darkthrop Prophecy",
          "playtime_forever": 0,
          "img_icon_url": "a67e9614af8f48bf7edfb4aa459fcd69a109ff5e",
          "img_logo_url": "dc11f2204f60f4a729342389d4d7d46509a7a41e"
        },
        {
          "appid": 322190,
          "name": "SteamWorld Heist",
          "playtime_forever": 0,
          "img_icon_url": "5838d342266d56bdf4416d59d35939df5d52b2f0",
          "img_logo_url": "848d24321b2f126c2fe2b94cf618898fc8bfd6b6",
          "has_community_visible_stats": true
        },
        {
          "appid": 322290,
          "name": "Gurumin: A Monstrous Adventure",
          "playtime_forever": 0,
          "img_icon_url": "1fefeee2d3a6b4abd3ca52071cda810a918715c3",
          "img_logo_url": "26a2626fc8dc58cf071a78f9e23fa1b9305e80ec",
          "has_community_visible_stats": true
        },
        {
          "appid": 322410,
          "name": "Bin Weevils Arty Arcade",
          "playtime_forever": 0,
          "img_icon_url": "434de643a956eb5678d4fb535f35b93f72c6c9c7",
          "img_logo_url": "d6084d22930fe4b977d69dc8ec6883deb88aa7d6"
        },
        {
          "appid": 289930,
          "name": "TransOcean: The Shipping Company",
          "playtime_forever": 0,
          "img_icon_url": "9dbd0f96515db78e22414401a2fef5884429bd84",
          "img_logo_url": "17eda0c8eecb35aab9ea0c2991cf0a4c7345c0c2",
          "has_community_visible_stats": true
        },
        {
          "appid": 258520,
          "name": "The Vanishing of Ethan Carter",
          "playtime_forever": 0,
          "img_icon_url": "d94c31f17ff27a733fd01ecf929106264b89b8b5",
          "img_logo_url": "bdcdde76010c917f2c63f99c093aaac86583e0ad",
          "has_community_visible_stats": true
        },
        {
          "appid": 400430,
          "name": "The Vanishing of Ethan Carter Redux",
          "playtime_forever": 0,
          "img_icon_url": "1385cf04f3625f1281c964f070cc6eaeb5642cd3",
          "img_logo_url": "1d5dcfed693a2b01a9fcdddff3c9bff9ccaaf4d9",
          "has_community_visible_stats": true
        },
        {
          "appid": 265550,
          "name": "Dead Rising 3",
          "playtime_forever": 0,
          "img_icon_url": "f5cdf9af7b3ab97308c13a326fe22bad9445419c",
          "img_logo_url": "5ceb6870a685e3ec406d76d2a0bd783fa768823c",
          "has_community_visible_stats": true
        },
        {
          "appid": 45760,
          "name": "Ultra Street Fighter IV",
          "playtime_forever": 301,
          "img_icon_url": "473fcea2eb516528608dff7f9e3e61009d76a282",
          "img_logo_url": "bdd481249e579f852056b51db32a6279444d4f47",
          "has_community_visible_stats": true
        },
        {
          "appid": 322900,
          "name": "War, the Game",
          "playtime_forever": 0,
          "img_icon_url": "50b80e5ccde84c728b3d145271325d7c227857f3",
          "img_logo_url": "100cfa1ae13e1cfbbd09bedef33468ba7dadad44",
          "has_community_visible_stats": true
        },
        {
          "appid": 323320,
          "name": "Grow Home",
          "playtime_forever": 0,
          "img_icon_url": "acd05186b5f70b1007745f38cb30c7be5488cb5d",
          "img_logo_url": "f4b4226f4715ca8e68c8ed4ddbbe3adb0cc5f8ab",
          "has_community_visible_stats": true
        },
        {
          "appid": 45770,
          "name": "Dead Rising 2: Off the Record",
          "playtime_forever": 0,
          "img_icon_url": "390b241cf36d54a3cb59be36a72c35ed41f6172b",
          "img_logo_url": "305469a745b2eb470daea8a30a282c79fa52d940",
          "has_community_visible_stats": true
        },
        {
          "appid": 323580,
          "name": "Jotun: Valhalla Edition",
          "playtime_forever": 0,
          "img_icon_url": "69b336a11a1e8acee4034b970a89c1df7cf53b32",
          "img_logo_url": "09e4f191cbafba7d3cde386e73abe1e0504146b4",
          "has_community_visible_stats": true
        },
        {
          "appid": 226560,
          "name": "Escape Dead Island",
          "playtime_forever": 0,
          "img_icon_url": "a1238454cc3c3b9b536146fd742a85b9ea60fe6f",
          "img_logo_url": "47655472d7239fd74d6bf7739ae1c8f2bbfb29b1",
          "has_community_visible_stats": true
        },
        {
          "appid": 323680,
          "name": "Caffeine",
          "playtime_forever": 0,
          "img_icon_url": "a5c1b2822890ee50ad7a5c46048419f9c6725692",
          "img_logo_url": "3f675c39a7f32130ec4bf4cb9ebaa875f17f2764",
          "has_community_visible_stats": true
        },
        {
          "appid": 323720,
          "name": "Decay - The Mare",
          "playtime_forever": 0,
          "img_icon_url": "9ec8e9f834898ffce37cdd13356ced14e25effe0",
          "img_logo_url": "27ce91e73183339c1906da310e63fccfedb031ed",
          "has_community_visible_stats": true
        },
        {
          "appid": 302510,
          "name": "Ryse: Son of Rome",
          "playtime_forever": 0,
          "img_icon_url": "46114151ac11d6984ec280405d65c15a4ed9438e",
          "img_logo_url": "70e45a9429e0e188817b813147a417bb82dc3bf0",
          "has_community_visible_stats": true
        },
        {
          "appid": 214490,
          "name": "Alien: Isolation",
          "playtime_forever": 27,
          "img_icon_url": "7bf964858835da75630a43ac0ddbf0f66a40902f",
          "img_logo_url": "6cdbe0113548dad8cee3d24fbace9abe298ac9af",
          "has_community_visible_stats": true
        },
        {
          "appid": 241930,
          "name": "Middle-earth™: Shadow of Mordor™",
          "playtime_forever": 1338,
          "img_icon_url": "161afab3c9f725f593635723cc64a7fbeb324ead",
          "img_logo_url": "00631b918ee8799ff48b3d91d9eb0d3278aafa83",
          "has_community_visible_stats": true
        },
        {
          "appid": 307690,
          "name": "Sleeping Dogs: Definitive Edition",
          "playtime_forever": 0,
          "img_icon_url": "8ef5e84ffbd9a4b3fb28d308073bccb415fc25ab",
          "img_logo_url": "a94b6a1f734fc5687cac302cd026f30d0ee6295f",
          "has_community_visible_stats": true
        },
        {
          "appid": 232750,
          "name": "Mars: War Logs",
          "playtime_forever": 0,
          "img_icon_url": "d026dc3bf85a48ecfd3731e7f942799736837771",
          "img_logo_url": "cd53becb4551a1564d24b48a92bc20bf515a1c48",
          "has_community_visible_stats": true
        },
        {
          "appid": 243930,
          "name": "Bound By Flame",
          "playtime_forever": 0,
          "img_icon_url": "ae47c402e4fa7dc59befa901cf9b09533e30e081",
          "img_logo_url": "048d308cb7145752c60316c717c0960889e34a6c",
          "has_community_visible_stats": true
        },
        {
          "appid": 303790,
          "name": "Faery - Legends of Avalon",
          "playtime_forever": 0,
          "img_icon_url": "0014ba0d99d1eae2400876ee44e6d90fd68a6f23",
          "img_logo_url": "9892716ae5c98f01f14d547b51b96eda63525488",
          "has_community_visible_stats": true
        },
        {
          "appid": 325520,
          "name": "Fire",
          "playtime_forever": 0,
          "img_icon_url": "13b8f049e074abbd9770507d83b763e377ea1209",
          "img_logo_url": "7aa37606d122da746cf972d8cd14bc5bdbebccd4",
          "has_community_visible_stats": true
        },
        {
          "appid": 305050,
          "name": "Outland",
          "playtime_forever": 0,
          "img_icon_url": "bdfa511693bd167835fb1d32331f0a579dcdb9a5",
          "img_logo_url": "26c5b8f3b69f028073168ced552f87e40a9e4b58",
          "has_community_visible_stats": true
        },
        {
          "appid": 325880,
          "name": "Crystal Catacombs",
          "playtime_forever": 0,
          "img_icon_url": "370db8c7087454860799a7b1d6747e3ea4370b2f",
          "img_logo_url": "04e5ef08863e14a1c0e8b0518d01ada7bf74fa3e",
          "has_community_visible_stats": true
        },
        {
          "appid": 326120,
          "name": "Horizon Shift",
          "playtime_forever": 0,
          "img_icon_url": "d4a475cb910c0eaef87c4e9e883873aab26b7fc6",
          "img_logo_url": "4a024c12358e6d52ee109594c75eb469bd1b7bd6",
          "has_community_visible_stats": true
        },
        {
          "appid": 326410,
          "name": "Windward",
          "playtime_forever": 0,
          "img_icon_url": "d2fe4af6899d61abc09521f6924012c5149b0a1c",
          "img_logo_url": "64f8e9b19cf298cb3217a266abb99a618c69f385",
          "has_community_visible_stats": true
        },
        {
          "appid": 327220,
          "name": "Anna's Quest",
          "playtime_forever": 330,
          "img_icon_url": "e77eff79409dd1440ab7fb8101c0ad88bf4b3bfa",
          "img_logo_url": "c77142e8e1eec8680f0ab8f7177d6ba479a833e1",
          "has_community_visible_stats": true
        },
        {
          "appid": 327410,
          "name": "A Bird Story",
          "playtime_forever": 353,
          "img_icon_url": "cef549201cdfcf82ae56d9975cdcf96ea5515760",
          "img_logo_url": "86490bbde1c9817ec5e67b1b77234ba5c9559e03",
          "has_community_visible_stats": true
        },
        {
          "appid": 328270,
          "name": "Leviathan: The Last Day of the Decade",
          "playtime_forever": 0,
          "img_icon_url": "d80d6ed7b7e29f53759c029785f47ebeb9cb6534",
          "img_logo_url": "1019927ef443196a2dab7cf0d5b7ac51faf02280",
          "has_community_visible_stats": true
        },
        {
          "appid": 328730,
          "name": "Letter Quest: Grimm's Journey",
          "playtime_forever": 0,
          "img_icon_url": "c42e14f02ee0ce8b94ad408c2a07d30929335b10",
          "img_logo_url": "80f240921122dbfc45df5c607c08c1b36aa45e48",
          "has_community_visible_stats": true
        },
        {
          "appid": 373970,
          "name": "Letter Quest: Grimm's Journey Remastered",
          "playtime_forever": 0,
          "img_icon_url": "8b015fde87fba1b87cf2b03ffddfb645ee54be75",
          "img_logo_url": "55ab26ef516f0d072b3634858ba724d90caac5f3",
          "has_community_visible_stats": true
        },
        {
          "appid": 328760,
          "name": "SanctuaryRPG: Black Edition",
          "playtime_forever": 0,
          "img_icon_url": "ed07f2436cc71f42847469e504368bdda3493ca7",
          "img_logo_url": "e806a50635d8d5db8100ccbaa44ae054ff355540",
          "has_community_visible_stats": true
        },
        {
          "appid": 286220,
          "name": "Pier Solar and the Great Architects",
          "playtime_forever": 2388,
          "img_icon_url": "df367bd453b1f672ff40ea2b99ea08edd2acfff7",
          "img_logo_url": "1954e60db38f39adec4e2a0cd9c2c852e2ce883d",
          "has_community_visible_stats": true
        },
        {
          "appid": 329380,
          "name": "Stealth Inc 2",
          "playtime_forever": 0,
          "img_icon_url": "7092cf3b2009fb9cd61d6d1a19542feaeb112e07",
          "img_logo_url": "a33c7e1f295648db3136fd498c5971f73473de57",
          "has_community_visible_stats": true
        },
        {
          "appid": 329490,
          "name": "Phantom Breaker: Battle Grounds",
          "playtime_forever": 0,
          "img_icon_url": "2b4e53770acd55c645d14dfa90241641ff423d23",
          "img_logo_url": "35a068a0739cd22330f2ee488b040b6c25f85b33",
          "has_community_visible_stats": true
        },
        {
          "appid": 329830,
          "name": "The Moon Sliver",
          "playtime_forever": 0,
          "img_icon_url": "0b49e728f2e136687771d2f1352b2a553d6f1f31",
          "img_logo_url": "4822c5454fcf3bf5a172b0b4a66758134c61ba87"
        },
        {
          "appid": 211420,
          "name": "DARK SOULS™: Prepare To Die Edition",
          "playtime_forever": 0,
          "img_icon_url": "a24804c6c8412c8cd9d50efd06bf03fa58ff80a9",
          "img_logo_url": "d293c8e38f56de2c7097b2c7a975caca49029a8b",
          "has_community_visible_stats": true
        },
        {
          "appid": 262690,
          "name": "Little Racers STREET",
          "playtime_forever": 0,
          "img_icon_url": "82071f5500f247e6a48497627cee95c5c7f9622c",
          "img_logo_url": "793d2d1aac3058878b33f97eb1dd013249e6b1c9",
          "has_community_visible_stats": true
        },
        {
          "appid": 21000,
          "name": "LEGO Batman: The Videogame",
          "playtime_forever": 0,
          "img_icon_url": "51fee43ed4f78494e23bd3eaeec5be43635dbd4c",
          "img_logo_url": "63cde4cbc3ab537bfe53c85b7d0b8ecdffc7d9c8"
        },
        {
          "appid": 213330,
          "name": "LEGO Batman 2",
          "playtime_forever": 0,
          "img_icon_url": "92446ccf0abc6ab3e716251c0004acda5c3b13fd",
          "img_logo_url": "0c4b05ceb9ee10b4eb744e663c22385ccca16c07"
        },
        {
          "appid": 313690,
          "name": "LEGO® Batman™ 3: Beyond Gotham",
          "playtime_forever": 0,
          "img_icon_url": "4bd5b11c55070ddb17b14f05293eac8a464f71ac",
          "img_logo_url": "802a54c3e6bf7209cbf8b0b9359966393db16705",
          "has_community_visible_stats": true
        },
        {
          "appid": 330270,
          "name": "Warlocks vs Shadows",
          "playtime_forever": 0,
          "img_icon_url": "fd4af6e173b7a04182b35a5ee02be24aaefb4500",
          "img_logo_url": "7177b0ae3a3961e9ea8fbad7f5c6ed4d5f4984e9",
          "has_community_visible_stats": true
        },
        {
          "appid": 246090,
          "name": "Spacebase DF-9",
          "playtime_forever": 0,
          "img_icon_url": "934cf3be95251c9c06db60529f3555ceda4cb631",
          "img_logo_url": "2fafa0c2e577ee4ee3f0af4fa648725f7a9550f0"
        },
        {
          "appid": 330390,
          "name": "Grandia II Anniversary Edition",
          "playtime_forever": 0,
          "img_icon_url": "76e2cdd9d78dc42eaf85c472aff50a650958bbd2",
          "img_logo_url": "996cb27251fa6edffe7afbe99933566389f105ef",
          "has_community_visible_stats": true
        },
        {
          "appid": 330420,
          "name": "The Interactive Adventures of Dog Mendonça and Pizzaboy",
          "playtime_forever": 0,
          "img_icon_url": "0571bc14f0c5d0d18676e439764cdef4883e9f0c",
          "img_logo_url": "7537892b48af95bdce857e3185c1476224f57e7e"
        },
        {
          "appid": 289650,
          "name": "Assassin's Creed Unity",
          "playtime_forever": 0,
          "img_icon_url": "0629ffe7fc7d88011c5b9705625bfe3791fe0afe",
          "img_logo_url": "83f480dbfb5da90a58d9b9766571c9ef4c2a55ac",
          "has_community_visible_stats": true
        },
        {
          "appid": 330820,
          "name": "2064: Read Only Memories",
          "playtime_forever": 0,
          "img_icon_url": "f28ddc7333b9d1bcbfbb8213f1bc7b410664c224",
          "img_logo_url": "4defebbb2015cd9ed4612b57dbecf377e7becd34",
          "has_community_visible_stats": true
        },
        {
          "appid": 330830,
          "name": "Tales from the Borderlands",
          "playtime_forever": 0,
          "img_icon_url": "620dbf286a2ef353c9c93e5b7c047027b18f1475",
          "img_logo_url": "7e7183b717e05a2972e796d56526c260e7238f58",
          "has_community_visible_stats": true
        },
        {
          "appid": 330840,
          "name": "Game of Thrones - A Telltale Games Series",
          "playtime_forever": 5,
          "img_icon_url": "b981849ef37118e0cef6388f0c893d381323e4b3",
          "img_logo_url": "93beb587402d54d1226cda1c3ba81947eae6253f",
          "has_community_visible_stats": true
        },
        {
          "appid": 268050,
          "name": "The Evil Within",
          "playtime_forever": 0,
          "img_icon_url": "3ec11bf61b679fe85a529fc139ed90f39964a7c1",
          "img_logo_url": "a652a995aaa6acb77b99ffac1c699898e79be926",
          "has_community_visible_stats": true
        },
        {
          "appid": 331220,
          "name": "Thief Town",
          "playtime_forever": 0,
          "img_icon_url": "f7b562e0b8980561013cb7af9700704995918d35",
          "img_logo_url": "531fdd2daf80c27d1fe7d535649f71da38cf6eeb",
          "has_community_visible_stats": true
        },
        {
          "appid": 331440,
          "name": "bit Dungeon II",
          "playtime_forever": 0,
          "img_icon_url": "aabe254f5ca4294f4a7b3ad9234bfffa6ee7ccca",
          "img_logo_url": "8db3bbb4b99cd0f8ae7d4e5f1f36c1f4096dc2f4",
          "has_community_visible_stats": true
        },
        {
          "appid": 331870,
          "name": "AER Memories of Old",
          "playtime_forever": 0,
          "img_icon_url": "a08489f03db66c46ec657411d0e25e1ae2006b2b",
          "img_logo_url": "9fb11c38c8e5a6aa4a9b3e41cba14487b07ee5ce",
          "has_community_visible_stats": true
        },
        {
          "appid": 298110,
          "name": "Far Cry 4",
          "playtime_forever": 0,
          "img_icon_url": "be1366a047a515ed2275aa500a36a50de587b7a7",
          "img_logo_url": "eff3e45aca29c0242f663a48db0883e6da5d1d6d",
          "has_community_visible_stats": true
        },
        {
          "appid": 332200,
          "name": "Axiom Verge",
          "playtime_forever": 0,
          "img_icon_url": "daa500b0c841a769a8012376e17648dd88813438",
          "img_logo_url": "97fb318cd65ab72a7fe8f6fcf9cf6e4ab8f36204",
          "has_community_visible_stats": true
        },
        {
          "appid": 332390,
          "name": "Undefeated",
          "playtime_forever": 0,
          "img_icon_url": "83aba6fd2abfb82bfc7145ae07dceca3e2c20c79",
          "img_logo_url": "fee2d5c497d5f24dc623e2853c5d917dba01a1a9",
          "has_community_visible_stats": true
        },
        {
          "appid": 332800,
          "name": "Five Nights at Freddy's 2",
          "playtime_forever": 0,
          "img_icon_url": "94d87ac302559e7742905416bab1cf1af530a974",
          "img_logo_url": "7e61a298e1db26778316c9a48ed27a514e198a39"
        },
        {
          "appid": 332970,
          "name": "Storm United",
          "playtime_forever": 0,
          "img_icon_url": "5f1ac4b1462d07dff9cc9b370d1b34caa1cfea50",
          "img_logo_url": "eec0a97f9d1b639b2386b46c0fb8d5250cbe1d11",
          "has_community_visible_stats": true
        },
        {
          "appid": 333250,
          "name": "Forward to the Sky",
          "playtime_forever": 0,
          "img_icon_url": "9fa49247457798d52780cd7c3e47069b939ef7ea",
          "img_logo_url": "3c14f70db8c5c8132c7a2bfc5cc458faab3573c6",
          "has_community_visible_stats": true
        },
        {
          "appid": 333650,
          "name": "Pahelika: Revelations HD",
          "playtime_forever": 0,
          "img_icon_url": "5c92f774c54088163bf6770b271454f7e80f4e74",
          "img_logo_url": "3fbe5484930ea055a817c9a9a773563b666fdca5",
          "has_community_visible_stats": true
        },
        {
          "appid": 333730,
          "name": "Dark Gates",
          "playtime_forever": 0,
          "img_icon_url": "3f61e1dbdaba3e1873071bcca239a87cd0fe097a",
          "img_logo_url": "ef8b787a55b0d13568ea08102ee10f3ec9298356",
          "has_community_visible_stats": true
        },
        {
          "appid": 333540,
          "name": "Neocolonialism",
          "playtime_forever": 0,
          "img_icon_url": "f7d8c8457a0fbf50252d6f07a1044abe34fc4b00",
          "img_logo_url": "7085f3fcd1bf34884325ce0877a94895d284e30c"
        },
        {
          "appid": 333980,
          "name": "AKIBA'S TRIP: Undead & Undressed",
          "playtime_forever": 337,
          "img_icon_url": "db02955f6ac396b0d700b03bb9d38d6f5a7c7798",
          "img_logo_url": "63c0dd6867efdbb3bf72e28af39b024f630af88b",
          "has_community_visible_stats": true
        },
        {
          "appid": 334190,
          "name": "Insanity's Blade",
          "playtime_forever": 0,
          "img_icon_url": "35f20afda6d186724318d5082c7d2fc613df66f6",
          "img_logo_url": "8b96ea50bb80d1b702700e4d4bc73d2080dd39d2",
          "has_community_visible_stats": true
        },
        {
          "appid": 265330,
          "name": "Gomo",
          "playtime_forever": 0,
          "img_icon_url": "16aef88d0f34e2991dbb722465cb4dcc5bb4a112",
          "img_logo_url": "a54feb3e78cb678eee3c6b4892d3de93d8471a15"
        },
        {
          "appid": 268540,
          "name": "The Whispered World Special Edition",
          "playtime_forever": 0,
          "img_icon_url": "de0e41988e68c7e8dfd8ab69f548323f63cf2d9c",
          "img_logo_url": "ad40f2e4e7960906562b37753716376ce8abe8dc",
          "has_community_visible_stats": true
        },
        {
          "appid": 335300,
          "name": "DARK SOULS™ II: Scholar of the First Sin",
          "playtime_forever": 0,
          "img_icon_url": "6e665503e93d1383b774a68dfea23ac7afc0e3aa",
          "img_logo_url": "6ca16aa1b83311e5e3e8f0f0c2b8046f5d9e95a8",
          "has_community_visible_stats": true
        },
        {
          "appid": 222420,
          "name": "THE KING OF FIGHTERS '98 ULTIMATE MATCH FINAL EDITION",
          "playtime_forever": 0,
          "img_icon_url": "c991e64d6b5547a94ba541a59717a861a013bf9c",
          "img_logo_url": "4c865e50d1d33045f4b0bb2b57dbcbf7b74408be",
          "has_community_visible_stats": true
        },
        {
          "appid": 335560,
          "name": "Light Bound",
          "playtime_forever": 0,
          "img_icon_url": "6ae8629f01d87b603eb7db0f46c2711a3fc06df0",
          "img_logo_url": "38c174ca5075a0c0a62176e6a4c7104f231022f3",
          "has_community_visible_stats": true
        },
        {
          "appid": 335670,
          "name": "LISA",
          "playtime_forever": 0,
          "img_icon_url": "4db7f8a68772c4b4ce484fc2db896e8000c32f7a",
          "img_logo_url": "eae97830175ff475185bbd26efa907f776912c22",
          "has_community_visible_stats": true
        },
        {
          "appid": 336020,
          "name": "Pahelika: Secret Legends",
          "playtime_forever": 0,
          "img_icon_url": "4dc5db6bf9b7146db55424523368400f5bbe4062",
          "img_logo_url": "e00b5f2516b1392377dd183bdb3e3339ae71167d",
          "has_community_visible_stats": true
        },
        {
          "appid": 337360,
          "name": "Dead Island Retro Revenge",
          "playtime_forever": 0,
          "img_icon_url": "3693712646c888407c9bf14dba6229d8fdb26517",
          "img_logo_url": "544fd27b36834cdb248f711a2851471f02b6c127",
          "has_community_visible_stats": true
        },
        {
          "appid": 337940,
          "name": "X-note",
          "playtime_forever": 0,
          "img_icon_url": "21efa9728a9374e743682894d740e1c66bcb21e4",
          "img_logo_url": "ea639603ae87a7d2232161d68c5dbd8b95b4b996"
        },
        {
          "appid": 337980,
          "name": "Vagrant Hearts",
          "playtime_forever": 0,
          "img_icon_url": "3907febfc993846bc99ef94e9b87ef43f1e98ae9",
          "img_logo_url": "f70eaa8ef0c2235fb08755d19640306f3bfdbf3a",
          "has_community_visible_stats": true
        },
        {
          "appid": 338050,
          "name": "Anoxemia",
          "playtime_forever": 328,
          "img_icon_url": "08d02cf0c67f21c6aa108798380b6747e9845819",
          "img_logo_url": "5e3f587825e046337b59357c61507d273c387d00",
          "has_community_visible_stats": true
        },
        {
          "appid": 338140,
          "name": "Venetica",
          "playtime_forever": 0,
          "img_icon_url": "3bb0d4dcc328625b4a35b88233c545424559d9ff",
          "img_logo_url": "86c86dd822dafcfdb8aa3398a652a6122654c788"
        },
        {
          "appid": 338320,
          "name": "Pixel Heroes: Byte & Magic",
          "playtime_forever": 0,
          "img_icon_url": "73c001ae9e62b9922ac658791cf8c381be455a71",
          "img_logo_url": "c416197c7299800a95013ea469cb64fe231cf0ea",
          "has_community_visible_stats": true
        },
        {
          "appid": 339190,
          "name": "Dead Synchronicity: Tomorrow Comes Today",
          "playtime_forever": 0,
          "img_icon_url": "310a84ab3f614abc7f434ccf47df355e764fa62b",
          "img_logo_url": "7faeba2d0bdb89ae74b327744f0840aeb837bd7f",
          "has_community_visible_stats": true
        },
        {
          "appid": 339200,
          "name": "Oceanhorn: Monster of Uncharted Seas",
          "playtime_forever": 0,
          "img_icon_url": "702021450ce232e55a829d7ed9f029f41ae11c73",
          "img_logo_url": "17c0e81e3632959c897d53e576bc46d306bdb46c",
          "has_community_visible_stats": true
        },
        {
          "appid": 319630,
          "name": "Life is Strange™",
          "playtime_forever": 4,
          "img_icon_url": "e54b13da0710a35e7088664a77632e79157d8332",
          "img_logo_url": "e92d2b3a0c01be3bdd77ec665a3014028efaa105",
          "has_community_visible_stats": true
        },
        {
          "appid": 222180,
          "name": "Mushroom Men: Truffle Trouble ",
          "playtime_forever": 0,
          "img_icon_url": "418b5f7b6f045af1fa036f31b320a803331029f6",
          "img_logo_url": "16a27cd1815adb4d52b7ebbf1ec382ce71614284",
          "has_community_visible_stats": true
        },
        {
          "appid": 340050,
          "name": "Survivalist",
          "playtime_forever": 0,
          "img_icon_url": "a368c38490da6d21930374d91506c3982b0c1323",
          "img_logo_url": "5f4d0e03db71f2445126c9522793aa36f968669e",
          "has_community_visible_stats": true
        },
        {
          "appid": 340170,
          "name": "FINAL FANTASY TYPE-0 HD",
          "playtime_forever": 0,
          "img_icon_url": "f118decd2b35d1fc568bd5517db7d2d27dfc6b2f",
          "img_logo_url": "748e1a2bdfae60edc984f60c368bd665ead0f394",
          "has_community_visible_stats": true
        },
        {
          "appid": 340200,
          "name": "Bloop",
          "playtime_forever": 0,
          "img_icon_url": "8672944966d57a030660099122a4648b598b7e3c",
          "img_logo_url": "970be632b90e7db2022388eaeb48573c9ea64770"
        },
        {
          "appid": 340490,
          "name": "Subterrain",
          "playtime_forever": 0,
          "img_icon_url": "e015057878d7bb0ad6904fc9925bca3372a1ce7b",
          "img_logo_url": "01edad96381bc0c8a2572a79cf86e2991cb8f9b7",
          "has_community_visible_stats": true
        },
        {
          "appid": 340730,
          "name": "Sunrider Academy",
          "playtime_forever": 0,
          "img_icon_url": "c8e2dd573569d7f8e701f84e815272c7c3e4a1e5",
          "img_logo_url": "c9ea4c928c7926901723cc426f6dbd212ec07b7a",
          "has_community_visible_stats": true
        },
        {
          "appid": 340860,
          "name": "RaidersSphere4th",
          "playtime_forever": 0,
          "img_icon_url": "baba8b42a0c621778adc711a55b72456c9f74cdc",
          "img_logo_url": "6d67dfd0f2fa13bd18771e44ab19b0511007586f"
        },
        {
          "appid": 341020,
          "name": "Chronicles of Teddy",
          "playtime_forever": 0,
          "img_icon_url": "0e432c38e369d6b5ecee922fe5170c264efa445f",
          "img_logo_url": "9d128180c1bcd131b4bbe777d34cadbdc061a5fc",
          "has_community_visible_stats": true
        },
        {
          "appid": 341060,
          "name": "The Lady",
          "playtime_forever": 0,
          "img_icon_url": "e7a85bed03c018ecb2fe2a3cba3f429e39721dcf",
          "img_logo_url": "3d1e451bd2187f4a0bb696d06bd98951a854001d"
        },
        {
          "appid": 341070,
          "name": "Stay Dead Evolution",
          "playtime_forever": 0,
          "img_icon_url": "6bfe066a6f4ddf6fbed6d687e99b2ab70b641992",
          "img_logo_url": "cabd0547051930f4c84f1ee8c76897fa2b3246b8"
        },
        {
          "appid": 341310,
          "name": "Greyfox",
          "playtime_forever": 0,
          "img_icon_url": "e29fffe6ce2e981f7645553af7216d5f414a1060",
          "img_logo_url": "4e4b20a2cd0ea05ef193afe5771dc93eb5bc27f0",
          "has_community_visible_stats": true
        },
        {
          "appid": 341530,
          "name": "Stardust Vanguards",
          "playtime_forever": 0,
          "img_icon_url": "92a6a1985ad07525b72ff18aad79dfab7b6b9f01",
          "img_logo_url": "6d4dfaf1be97a3d9c368413398ba3c63c3a36981",
          "has_community_visible_stats": true
        },
        {
          "appid": 304240,
          "name": "Resident Evil / biohazard HD REMASTER",
          "playtime_forever": 0,
          "img_icon_url": "a60a960c76f26ab81773d4f2d372836db37a0012",
          "img_logo_url": "3bb107ed29b854c5c5a5db1da709ca9950830c19",
          "has_community_visible_stats": true
        },
        {
          "appid": 342300,
          "name": "Zombie Kill of the Week - Reborn",
          "playtime_forever": 0,
          "img_icon_url": "756023505088c14cea9ea8f11edaff825bf10565",
          "img_logo_url": "0111170c34f2fdf8067371e00df653dd67461600",
          "has_community_visible_stats": true
        },
        {
          "appid": 342380,
          "name": "Sakura Angels",
          "playtime_forever": 0,
          "img_icon_url": "152302fc713a77006d405db54b0eaf31f462ee0a",
          "img_logo_url": "731bea1473407465e6fb8107eba5997332503706"
        },
        {
          "appid": 342580,
          "name": "12 Labours of Hercules",
          "playtime_forever": 368,
          "img_icon_url": "31abb0b9320a397bde1ca97f0992881389a952b0",
          "img_logo_url": "e77bb63eb787efa59d7349ddeceffd9ae2575b62",
          "has_community_visible_stats": true
        },
        {
          "appid": 342620,
          "name": "It came from space, and ate our brains",
          "playtime_forever": 0,
          "img_icon_url": "c8a6651ade4e5fedc287de7eaf268777131ce4a7",
          "img_logo_url": "2ed737c6808b1bf399578cecae09cc480e05b789",
          "has_community_visible_stats": true
        },
        {
          "appid": 342740,
          "name": "Samudai",
          "playtime_forever": 0,
          "img_icon_url": "366dd2a30dab87df7e905ab40a13bc02035b505f",
          "img_logo_url": "c18bbbc5c66b616713b311ab8b2923c09b23a3f6",
          "has_community_visible_stats": true
        },
        {
          "appid": 342880,
          "name": "NekoChan Hero - Collection",
          "playtime_forever": 0,
          "img_icon_url": "8b662dd0677d8aac725797052e1b9a64fdbd6598",
          "img_logo_url": "020947a5e28aa29bc71a9b3dab57668a32d9d812",
          "has_community_visible_stats": true
        },
        {
          "appid": 343800,
          "name": "Shadowgate: MacVenture Series",
          "playtime_forever": 0,
          "img_icon_url": "9da39d39127d0d0b3e6f6b1d78018aa3a3f1caa0",
          "img_logo_url": "81730d8ffa3a1e9326082805d75fd80190ab6635"
        },
        {
          "appid": 343840,
          "name": "Trash TV",
          "playtime_forever": 0,
          "img_icon_url": "464c424c0b44821b16004c1c9d68a6b10c88c3e9",
          "img_logo_url": "6457f79ddf1720a3460bf1ca4faff457fa201789"
        },
        {
          "appid": 344770,
          "name": "fault - milestone two side:above",
          "playtime_forever": 0,
          "img_icon_url": "a3707196aafb29c42e4c4c5035e3886136dbb3c8",
          "img_logo_url": "0aa4cb3eda8b098c95de80ab844584b368334cc4",
          "has_community_visible_stats": true
        },
        {
          "appid": 345290,
          "name": "The Quivering",
          "playtime_forever": 0,
          "img_icon_url": "35b08a96f42339f101c0833ead768ec1007408d4",
          "img_logo_url": "b62e9a4f2ff0cee05554b836dbc47207f9f530e7",
          "has_community_visible_stats": true
        },
        {
          "appid": 345300,
          "name": "Wildlife Creative Studio",
          "playtime_forever": 0,
          "img_icon_url": "8569efbfe96eae918f4843772a6b40eb9c08cc07",
          "img_logo_url": "90363f578ed2c1230019655cebf79b2d560cc994",
          "has_community_visible_stats": true
        },
        {
          "appid": 345310,
          "name": "My Family Creative Studio",
          "playtime_forever": 0,
          "img_icon_url": "02d0180a2825481e4151802fde4092bc7043d788",
          "img_logo_url": "dc61eb83a40fb6fe582524ff5cf290de4a0f5a4a",
          "has_community_visible_stats": true
        },
        {
          "appid": 301910,
          "name": "Saints Row: Gat out of Hell",
          "playtime_forever": 0,
          "img_icon_url": "7e817d02a9e0a52cdc171a006a8f0a48225456ea",
          "img_logo_url": "9193816dc18df31ee0cd45c74429fb12541ad5e6",
          "has_community_visible_stats": true
        },
        {
          "appid": 222440,
          "name": "THE KING OF FIGHTERS 2002 UNLIMITED MATCH",
          "playtime_forever": 0,
          "img_icon_url": "c9235efbfa66ad95a6ece092a7860ca8b92cfa6f",
          "img_logo_url": "787a8b76a5c9348784f50798403b2672e488c557",
          "has_community_visible_stats": true
        },
        {
          "appid": 345820,
          "name": "Shantae and the Pirate's Curse",
          "playtime_forever": 907,
          "img_icon_url": "e98df0098ddb330479fadb0cdd2d8707a0f8a6b5",
          "img_logo_url": "f750e634ebc89709b3995e870c0f54b8dfb76486",
          "has_community_visible_stats": true
        },
        {
          "appid": 346040,
          "name": "Joe's Diner",
          "playtime_forever": 0,
          "img_icon_url": "8dc33ae5fe67ed69bc627d9c66db1a744af9b734",
          "img_logo_url": "c1c22ffb951715fcb077b3d8f564d3b555bd263d",
          "has_community_visible_stats": true
        },
        {
          "appid": 294440,
          "name": "Shadowgate",
          "playtime_forever": 0,
          "img_icon_url": "56380c4cc0778b4876ec758726a87adae93a1de2",
          "img_logo_url": "c2d534f81c7d5481d3f4d54e1770a694595cf827",
          "has_community_visible_stats": true
        },
        {
          "appid": 347430,
          "name": "Frankenstein: Master of Death",
          "playtime_forever": 0,
          "img_icon_url": "d9b4a87f0d931bdae70caacc82b18fb2f892fe09",
          "img_logo_url": "343847a092d706666c732ce2c39ee93c970c0482",
          "has_community_visible_stats": true
        },
        {
          "appid": 347830,
          "name": "Fairy Fencer F",
          "playtime_forever": 0,
          "img_icon_url": "1da23c4abde426dcf9e0eeef4aebd46fd494c8c5",
          "img_logo_url": "2f42e4b3523a96ff33eba6a14a8416371beb063c",
          "has_community_visible_stats": true
        },
        {
          "appid": 348550,
          "name": "GUILTY GEAR XX ACCENT CORE PLUS R",
          "playtime_forever": 0,
          "img_icon_url": "32a39eb9c65606d49e4687695ae7e9055039fb2d",
          "img_logo_url": "964cbd449e465185ec7a41ce6c9eed6f7790f734",
          "has_community_visible_stats": true
        },
        {
          "appid": 342090,
          "name": "The Clans - Saga of the Twins",
          "playtime_forever": 0,
          "img_icon_url": "5640ecc30c9fe326295f9edcc62dffb214dfcf4f",
          "img_logo_url": "861ba96a2e3907fb7c827445fcf10a757651a55f",
          "has_community_visible_stats": true
        },
        {
          "appid": 349500,
          "name": "Cubicle Quest",
          "playtime_forever": 0,
          "img_icon_url": "52f85bd0c81443f8344612a6e93c5b2460af105e",
          "img_logo_url": "66ddcea1c1560bf67116a118191bafe3cfb8f293"
        },
        {
          "appid": 349680,
          "name": "Supercharged Robot VULKAISER",
          "playtime_forever": 0,
          "img_icon_url": "4cd9aa2420b0c5f561e96db7401c35ebcda3ef34",
          "img_logo_url": "bbb18f8d5d798ba8b25a76fb83c33150a136ba72",
          "has_community_visible_stats": true
        },
        {
          "appid": 287290,
          "name": "Resident Evil Revelations 2 / Biohazard Revelations 2",
          "playtime_forever": 0,
          "img_icon_url": "0ec692bbc91bbff0b455faef6cb2b8a37bbbca93",
          "img_logo_url": "dbd99a34dde062952221c88b3a649ea9c18038a2",
          "has_community_visible_stats": true
        },
        {
          "appid": 350510,
          "name": "HassleHeart",
          "playtime_forever": 0,
          "img_icon_url": "ec13811a85a0629e4345604bad906933f9b2e103",
          "img_logo_url": "927df793bd0fef87b14ead15f97b9f67415921fe"
        },
        {
          "appid": 312790,
          "name": "Agarest: Generations of War 2",
          "playtime_forever": 340,
          "img_icon_url": "e363f8c76f2b6915baf070d74c170e93987e2176",
          "img_logo_url": "408de3b619eeacd22c113bd2cc91ccbf9c8f802e",
          "has_community_visible_stats": true
        },
        {
          "appid": 350740,
          "name": "The Bug Butcher",
          "playtime_forever": 63,
          "img_icon_url": "6689177c796a4de03ec6d32e0b420bfe49656e5e",
          "img_logo_url": "7158a2967b6fcd7eb61f6050b0215db4be1d8631",
          "has_community_visible_stats": true
        },
        {
          "appid": 351920,
          "name": "Crazy Machines 3",
          "playtime_forever": 0,
          "img_icon_url": "6cf6da3763360bbc86b1b55b10ddbb7e8f363d81",
          "img_logo_url": "e732663b90ea52f0ca11e498163833ede860ca39",
          "has_community_visible_stats": true
        },
        {
          "appid": 352520,
          "name": "The Silent Age",
          "playtime_forever": 0,
          "img_icon_url": "0339cd35d349c79dc8c8fe062666e33f1c1c46f1",
          "img_logo_url": "32618d93fef9f091c76a539be5769f130c56b6d4",
          "has_community_visible_stats": true
        },
        {
          "appid": 353270,
          "name": "Hyperdimension Neptunia Re;Birth3 V Generation",
          "playtime_forever": 0,
          "img_icon_url": "161670f3123daa059c05efe67888b055b71a5125",
          "img_logo_url": "67dba637e798d5a53b6f66bd30fa7a45d671a7b2",
          "has_community_visible_stats": true
        },
        {
          "appid": 353560,
          "name": "Plug & Play",
          "playtime_forever": 0,
          "img_icon_url": "880292e9840a12d2491e4686c9b3388afacb671e",
          "img_logo_url": "8118ff1e24f43231cb83e8b61e0b158f9c63ff59"
        },
        {
          "appid": 240160,
          "name": "Duke Nukem",
          "playtime_forever": 0,
          "img_icon_url": "1642330d816183a1e1ba37a1ac29698f90e8bb88",
          "img_logo_url": "26fc43d96de98211588509c8f5c5005ac9a310c9"
        },
        {
          "appid": 240180,
          "name": "Duke Nukem 2",
          "playtime_forever": 0,
          "img_icon_url": "a50daeabd996d9703a76181519687517678d4407",
          "img_logo_url": "e7bf4474700bdb8754e04507a42a83f6437ec6e6"
        },
        {
          "appid": 358170,
          "name": "Arctic Adventure",
          "playtime_forever": 0,
          "img_icon_url": "2e0c86cfca59900e6a2d40e902a1a2c58e0819c9",
          "img_logo_url": "773470b14519d87359168e18a108a787e5a4aa72"
        },
        {
          "appid": 358180,
          "name": "Bio Menace",
          "playtime_forever": 0,
          "img_icon_url": "8a54c346da1a2328081c2230b6e0ddbd6c55067d",
          "img_logo_url": "9e3ee1f81a5c8260139ac5c1cd3f21293953ddf4"
        },
        {
          "appid": 358190,
          "name": "Blake Stone: Aliens of Gold",
          "playtime_forever": 0,
          "img_icon_url": "45fce15e40cf48cc9a99d39198d4be2015c1cd0f",
          "img_logo_url": "b95c8b05a56e81787c822abfec9fa76841bc6e2a"
        },
        {
          "appid": 358200,
          "name": "Math Rescue",
          "playtime_forever": 0,
          "img_icon_url": "9fa3b26a82089575c16652fe47b82580c8650086",
          "img_logo_url": "c1640631c15ffb3a1ab1d87b326db8ecfb9373f4"
        },
        {
          "appid": 358210,
          "name": "Monster Bash",
          "playtime_forever": 0,
          "img_icon_url": "1e51124f5c767df98b06dfe307caafcf400f733c",
          "img_logo_url": "dad3841de003c7eb658a594032fe8e376d6be953"
        },
        {
          "appid": 358220,
          "name": "Mystic Towers",
          "playtime_forever": 0,
          "img_icon_url": "c1ab25e699e1094ad6ecd306647c09a8b1238ca4",
          "img_logo_url": "7dabe8b721c2c4de4211663061fe8d42b1663132"
        },
        {
          "appid": 358230,
          "name": "Paganitzu",
          "playtime_forever": 0,
          "img_icon_url": "af38227450729cc66bf70f38bac12042d1cad383",
          "img_logo_url": "c34bddf6be952fe08144518560eacf8b0e077239"
        },
        {
          "appid": 358240,
          "name": "Monuments of Mars",
          "playtime_forever": 0,
          "img_icon_url": "650da8a901471b4a59863b8192fd246ebca180bc",
          "img_logo_url": "5fe223904fae255c4c9faec097a22783aea29dc7"
        },
        {
          "appid": 358250,
          "name": "Cosmo's Cosmic Adventure",
          "playtime_forever": 0,
          "img_icon_url": "b583d526d606641215ce80bfd65b3206c23fca82",
          "img_logo_url": "ab7750813769b39e322910d3a5e840ba3991ba2e"
        },
        {
          "appid": 358260,
          "name": "Crystal Caves",
          "playtime_forever": 0,
          "img_icon_url": "8bfccae17680f5a2357a0015b81335c25fe21760",
          "img_logo_url": "6bf7dc45f55486cd3cf2fae473b313614b2f76f5"
        },
        {
          "appid": 358270,
          "name": "Death Rally (Classic)",
          "playtime_forever": 0,
          "img_icon_url": "bee288de9618f208b5da80e2f2a837537a7d3b0f",
          "img_logo_url": "71822239d2a8d3a92fbf4f52a09dd08b02fcd567"
        },
        {
          "appid": 358280,
          "name": "Alien Carnage / Halloween Harry",
          "playtime_forever": 0,
          "img_icon_url": "dd75e13543b629d7ed886d2f17a5a5a22fd979e9",
          "img_logo_url": "a636fd67fc0c726f5c3e5b29ca7b0ad2fb9b245b"
        },
        {
          "appid": 358290,
          "name": "Hocus Pocus",
          "playtime_forever": 0,
          "img_icon_url": "c2ff6c900feef47c16de123d9c04783a73b8c86c",
          "img_logo_url": "100991cf63683f5876569d56e667884623f2838b"
        },
        {
          "appid": 358300,
          "name": "Major Stryker",
          "playtime_forever": 0,
          "img_icon_url": "7d0d6e2a063659c1e6b1d2ac80143fde943e4c78",
          "img_logo_url": "b5bfc7305bea72690ce40b7dc2bbadb22c3c9c00"
        },
        {
          "appid": 358310,
          "name": "Blake Stone: Planet Strike",
          "playtime_forever": 0,
          "img_icon_url": "001ce204c3efbd9917ef1bccc73d07c651ea5b0c",
          "img_logo_url": "d46a3788eb051b2554d736e10d2c59fbe393bd45"
        },
        {
          "appid": 358320,
          "name": "Realms of Chaos",
          "playtime_forever": 0,
          "img_icon_url": "088a9b9693fc8fc3316b6dd889b6de6eaefcc7d3",
          "img_logo_url": "515868b748cc073b4cb6b4de6a1726dd08f0170e"
        },
        {
          "appid": 358330,
          "name": "Pharaoh's Tomb",
          "playtime_forever": 0,
          "img_icon_url": "67fdb0bb83105a6aeb477f4e256bdd62b3557f16",
          "img_logo_url": "1cf9e181dabfd18373392a7062e0dec50db5f7ee"
        },
        {
          "appid": 358340,
          "name": "Word Rescue",
          "playtime_forever": 0,
          "img_icon_url": "8ae20fb757e5603623b38d25547b501f27ea204d",
          "img_logo_url": "36d9c1f460f343305ed2ea160d6ccb3fc0f50cef"
        },
        {
          "appid": 358350,
          "name": "Secret Agent",
          "playtime_forever": 0,
          "img_icon_url": "ce240abf1a7780dff81dc3f09eba9686e158ad9e",
          "img_logo_url": "a65bbbcaf7fbfd2bf2e5970312a428fe3b586fcd"
        },
        {
          "appid": 358360,
          "name": "Raptor: Call of the Shadows (1994 Classic Edition)",
          "playtime_forever": 0,
          "img_icon_url": "96b0ddd5c04dab7e9419221e51654d8430f509a7",
          "img_logo_url": "f981d3af7b097f78c2d1a8f7181a992616d18325"
        },
        {
          "appid": 358370,
          "name": "Terminal Velocity",
          "playtime_forever": 0,
          "img_icon_url": "b6031dd928901825b4c9fefd2e19675f6e52e49e",
          "img_logo_url": "7d08d000f12f3d1ed0036d68a2e49389628ccdea"
        },
        {
          "appid": 358380,
          "name": "Wacky Wheels",
          "playtime_forever": 0,
          "img_icon_url": "2a96cd2c5d127ec715d7c65f2e2b6f3e5360fc33",
          "img_logo_url": "bfcfcaa23cf772372cb7acdc8cffbe0834511e0a"
        },
        {
          "appid": 358390,
          "name": "Stargunner",
          "playtime_forever": 0,
          "img_icon_url": "e1abc21461a0158185cae01bf685317cd76a52c6",
          "img_logo_url": "491368e2cf4dcf725a0873122056cd7d2bddac5b"
        },
        {
          "appid": 358400,
          "name": "Shadow Warrior (Classic)",
          "playtime_forever": 0,
          "img_icon_url": "eb9defb48725909195f639b3f17f245dd7e53a73",
          "img_logo_url": "fc8d6980141a3840b07ca85e618ab1d168806639"
        },
        {
          "appid": 358410,
          "name": "Rise of the Triad: Dark War",
          "playtime_forever": 0,
          "img_icon_url": "10753f72753a023cbbe09943496a15c47add77a6",
          "img_logo_url": "5a1eec703ccf8d1218bbaa5fe2dde69774684bfb"
        },
        {
          "appid": 358420,
          "name": "Xenophage",
          "playtime_forever": 0,
          "img_icon_url": "a84be36e920d572c09194cb27a052435bf74c0d1",
          "img_logo_url": "c96bb452cb344008e4ba2703f1d61388b9e1aba9"
        },
        {
          "appid": 358430,
          "name": "Balls of Steel",
          "playtime_forever": 0,
          "img_icon_url": "72a26b2f7f50df105fe03ff889f05be1a6a2806c",
          "img_logo_url": "b051514ea39e2f02443b998066a896988ab94536"
        },
        {
          "appid": 358440,
          "name": "Dark Ages",
          "playtime_forever": 0,
          "img_icon_url": "46d2645ccacf31ff129de8b7de1d5ca09de237de",
          "img_logo_url": "2337ee2089117e9a66c259f2a34a67a8792e3201"
        },
        {
          "appid": 359850,
          "name": "Duke Nukem 3D",
          "playtime_forever": 0,
          "img_icon_url": "6e9cb4179b64a39e440694c3e3add0280c5d076c",
          "img_logo_url": "ad5da422a5769f7f4d8f646ba6645bf94ba47575"
        },
        {
          "appid": 311560,
          "name": "Assassin's Creed Rogue",
          "playtime_forever": 26,
          "img_icon_url": "422e9d527f4a509a179cfbd2832b47db793d8000",
          "img_logo_url": "286043867f5791b30def36449d7eb0fb393e23ca",
          "has_community_visible_stats": true
        },
        {
          "appid": 354140,
          "name": "Five Nights at Freddy's 3",
          "playtime_forever": 0,
          "img_icon_url": "981fe46812ed997cd3b358e20f6d8cc89bf32b5d",
          "img_logo_url": "fd078f9982d7dfa44517055ab3b2350b071029ad"
        },
        {
          "appid": 354680,
          "name": "Adventures of Bertram Fiddle: Episode 1: A Dreadly Business",
          "playtime_forever": 0,
          "img_icon_url": "54e3961a7a6e17a050b0185f6b1bdf3ed1e41823",
          "img_logo_url": "e28f43e28654d8a546cfb754ff87c0419702a1e2"
        },
        {
          "appid": 354860,
          "name": "The Adventures of Tree",
          "playtime_forever": 0,
          "img_icon_url": "a1f0da62d9d8aa360106dde0740efb015137f918",
          "img_logo_url": "d06b046190b2716ddb151377b561724b07447f97",
          "has_community_visible_stats": true
        },
        {
          "appid": 354960,
          "name": "An Imp? A Fiend!",
          "playtime_forever": 329,
          "img_icon_url": "b5822f850248928627548a65d53ac8ef0e30ed8f",
          "img_logo_url": "6105ec3efbafc68b5ec0340db4b16c94ffddb0cd"
        },
        {
          "appid": 350080,
          "name": "Wolfenstein: The Old Blood ",
          "playtime_forever": 379,
          "img_icon_url": "e45bc4d7a8233546c74f1cbba4e7d7a02bfc53a0",
          "img_logo_url": "91b96f8e81a82dabe53c95fd8d93b5dcdbb60fec",
          "has_community_visible_stats": true
        },
        {
          "appid": 355240,
          "name": "NEO AQUARIUM - The King of Crustaceans -",
          "playtime_forever": 0,
          "img_icon_url": "33ed4d9e30cc93037934efa416956f562fac38b3",
          "img_logo_url": "178a60cd1bf60ae53f672370fbe0248195fcaac3",
          "has_community_visible_stats": true
        },
        {
          "appid": 355270,
          "name": "199X",
          "playtime_forever": 0,
          "img_icon_url": "885dee9bd49ac3d16c1251bcd4fba1b42c5a32c1",
          "img_logo_url": "df1886e3fb626b789bb60000eea08382ea16ad08"
        },
        {
          "appid": 355520,
          "name": "The Albino Hunter",
          "playtime_forever": 0,
          "img_icon_url": "c49100b2e3b473390a207eb90d6f8211f478ba02",
          "img_logo_url": "6f7d7fba1d154a4c7ec1024d5b69099e3a0af685",
          "has_community_visible_stats": true
        },
        {
          "appid": 351640,
          "name": "Eternal Senia",
          "playtime_forever": 0,
          "img_icon_url": "8c968dc9409118250b7a855c640f698e7f33c6c0",
          "img_logo_url": "c19b5e298b958fcedb53514b01ae0599b9664701"
        },
        {
          "appid": 8850,
          "name": "BioShock 2",
          "playtime_forever": 0,
          "img_icon_url": "f5eda925c0e57373aaea4cae17b6f175115a8d54",
          "img_logo_url": "fde6fa1b15e4eb409c9d592197024571fded77e7",
          "has_community_visible_stats": true
        },
        {
          "appid": 409720,
          "name": "BioShock 2 Remastered",
          "playtime_forever": 0,
          "img_icon_url": "97527a02b36f8ac4aba21005c2d953cc908a08e1",
          "img_logo_url": "34c3f591cf4cb28711d7f33f73dee0f09cdb94a4",
          "has_community_visible_stats": true
        },
        {
          "appid": 357370,
          "name": "Return NULL - Episode 1",
          "playtime_forever": 0,
          "img_icon_url": "bba0350dc86c74fe3b406d75489fd69ebfd8786a",
          "img_logo_url": "e1f3ffe94349d5648b938d38a3cc5827947d27c2"
        },
        {
          "appid": 297130,
          "name": "Titan Souls",
          "playtime_forever": 0,
          "img_icon_url": "37ab14d4261626c8850487467ff38b2fc3d71f5b",
          "img_logo_url": "51395e1b870852f9a34935fe31a8304db3e8acc2",
          "has_community_visible_stats": true
        },
        {
          "appid": 357770,
          "name": "The District",
          "playtime_forever": 0,
          "img_icon_url": "3169c70643a5659cfdbdee3c8af48deb400f6e92",
          "img_logo_url": "5ccae51a20bb8e22d06dbc6e71b5a979fadcf5cc",
          "has_community_visible_stats": true
        },
        {
          "appid": 357780,
          "name": "Defend Your Life",
          "playtime_forever": 0,
          "img_icon_url": "483466e1db160318f5eee965a8eb603c3aa6c306",
          "img_logo_url": "eb3dc6ee8b4a030810929cfc80bf3200251e78a3",
          "has_community_visible_stats": true
        },
        {
          "appid": 345350,
          "name": "LIGHTNING RETURNS: FINAL FANTASY XIII",
          "playtime_forever": 1453,
          "img_icon_url": "0d325bd7fd995fdbe80b5146e032e1a011b12cf1",
          "img_logo_url": "15f2e89bb0547f27a670358047bdc67d3b5741c9",
          "has_community_visible_stats": true
        },
        {
          "appid": 234920,
          "name": "Dyscourse",
          "playtime_forever": 0,
          "img_icon_url": "9b71b2d6ebb6eed6424b71a98fd1c2bfc179560d",
          "img_logo_url": "1c76590a7ada5969f050936ac9c3bc8da7319d92",
          "has_community_visible_stats": true
        },
        {
          "appid": 358090,
          "name": "D4: Dark Dreams Don't Die",
          "playtime_forever": 0,
          "img_icon_url": "52958e20a966d1ca8423a967029aa68f67079d2c",
          "img_logo_url": "9543df086dcf5cdc667fdff73892bd7d9c42ec26",
          "has_community_visible_stats": true
        },
        {
          "appid": 359040,
          "name": "The Music Machine",
          "playtime_forever": 0,
          "img_icon_url": "81f2f22e5d08a7247c79bec1474f8ff0860aaf52",
          "img_logo_url": "c95c532e5ffc9cd7af3fdb9e9f38eabaaf43ac85"
        },
        {
          "appid": 359310,
          "name": "Evoland 2",
          "playtime_forever": 2008,
          "img_icon_url": "d2f9687a00ccfa481db07eb17d0f927fb2d90ce7",
          "img_logo_url": "064471571ca8c2cf77d2924ba94596eb95fe81af",
          "has_community_visible_stats": true
        },
        {
          "appid": 249650,
          "name": "Blackguards",
          "playtime_forever": 0,
          "img_icon_url": "2d63bd30fb43c757712705315aadcbfe6e6853ac",
          "img_logo_url": "b0feef47057c23586278f4a0136dcb0fcb74fed2",
          "has_community_visible_stats": true
        },
        {
          "appid": 314830,
          "name": "Blackguards 2",
          "playtime_forever": 0,
          "img_icon_url": "908201ee09e9dd3c20161eddaf417ec06044bc82",
          "img_logo_url": "5842806b4f3e96c792dd2a11c8ffbe378a0fa0e2",
          "has_community_visible_stats": true
        },
        {
          "appid": 46510,
          "name": "Syberia 2",
          "playtime_forever": 0,
          "img_icon_url": "7648a84341ef713722ca5a33a962defe39f3e5b8",
          "img_logo_url": "d88791c90403795d4a67fb9712b206085ab93c08"
        },
        {
          "appid": 359670,
          "name": "Vagrant Hearts 2",
          "playtime_forever": 0,
          "img_icon_url": "fe742a733ef236418b048580cefa3bbd29596870",
          "img_logo_url": "f353ff6c03370223e0e5598beb4ea5b65f1a01e1",
          "has_community_visible_stats": true
        },
        {
          "appid": 359870,
          "name": "FINAL FANTASY X/X-2 HD Remaster",
          "playtime_forever": 0,
          "img_icon_url": "86616d56239e83099e90940c8c9424489aee2ceb",
          "img_logo_url": "cf40f994b2656012147098b160bc44361d53b07c",
          "has_community_visible_stats": true
        },
        {
          "appid": 236450,
          "name": "PAC-MAN Championship Edition DX+",
          "playtime_forever": 0,
          "img_icon_url": "e5769d28986cf02b7e8cd21a7f505fca28444779",
          "img_logo_url": "099c05a258bbdb9c6ddf327c14fee4a13d0d86df",
          "has_community_visible_stats": true
        },
        {
          "appid": 360150,
          "name": "How To Survive Third Person",
          "playtime_forever": 0,
          "img_icon_url": "097c1711cd3e94de7796c42a70d5eb12efce9fe1",
          "img_logo_url": "729eae5d3140ad03bb2a8c19eb11a4eb40c0c7bf",
          "has_community_visible_stats": true
        },
        {
          "appid": 336420,
          "name": "Bloodsports.TV",
          "playtime_forever": 0,
          "img_icon_url": "d7f7df2103217cf2fe6a2c29f3a64f19c63ffce2",
          "img_logo_url": "844b8bb021696f188590b024a8840294cd8cb048",
          "has_community_visible_stats": true
        },
        {
          "appid": 360950,
          "name": "Descent: Underground",
          "playtime_forever": 0,
          "img_icon_url": "53d879c48dbba795fb01ebb5d5aac252051a6fbe",
          "img_logo_url": "2a440ff4c1477a249a928036555eaf29369e8f8c",
          "has_community_visible_stats": true
        },
        {
          "appid": 361990,
          "name": "Lakeview Cabin Collection",
          "playtime_forever": 0,
          "img_icon_url": "b6492e12b9db8929221b4c2a89f9ebf9deffc1a3",
          "img_logo_url": "e9bdb1b85d847b724a6c8ca2c3f7cecf69a0cb08",
          "has_community_visible_stats": true
        },
        {
          "appid": 47780,
          "name": "Dead Space 2",
          "playtime_forever": 0,
          "img_icon_url": "6393351676edc4fdc65937a599780818fd2f18b7",
          "img_logo_url": "38cb9890ddd639a066bf480e3f098e1b92af2376"
        },
        {
          "appid": 362130,
          "name": "Ashes of Immortality",
          "playtime_forever": 0,
          "img_icon_url": "659a33fd26a34d4e7c82919fcbcd1420ae9ab9b3",
          "img_logo_url": "010c8977fe4974e4c41eaa698d280d2529ffb200"
        },
        {
          "appid": 362140,
          "name": "Ashes of Immortality II",
          "playtime_forever": 0,
          "img_icon_url": "8d45be7bfefe9a7afa96970ffd35873c1c8971b3",
          "img_logo_url": "f0ea8ba228552ef45c8842c5bc0612cdc783ed14"
        },
        {
          "appid": 362150,
          "name": "Ashes of Immortality II - Bad Blood",
          "playtime_forever": 0,
          "img_icon_url": "ae72a3d73b9ab8ffdab3923bb24b0617bf47c236",
          "img_logo_url": "90623bc3a7234c365ec24f09831106804fa19fb0"
        },
        {
          "appid": 332250,
          "name": "The Next Penelope",
          "playtime_forever": 0,
          "img_icon_url": "dbde8b667e7a35ec8bf92d58be306a4c824d40d6",
          "img_logo_url": "9725e618cb16210782b36d183080c807e5370cd4",
          "has_community_visible_stats": true
        },
        {
          "appid": 362660,
          "name": "Blue Rose",
          "playtime_forever": 0,
          "img_icon_url": "7bcbb34325cb1193392988f1e41a883659b33099",
          "img_logo_url": "e5bccb809b2288ca80e93e4a69fa59be1a1ba673",
          "has_community_visible_stats": true
        },
        {
          "appid": 363440,
          "name": "Mega Man Legacy Collection",
          "playtime_forever": 145,
          "img_icon_url": "effb78715008287e9bc5eb68d145676ed38bf1ac",
          "img_logo_url": "0069f6651ce5099e1895e2dbe7ea31f047403092",
          "has_community_visible_stats": true
        },
        {
          "appid": 363600,
          "name": "Holy Potatoes! A Weapon Shop?!",
          "playtime_forever": 0,
          "img_icon_url": "f5ae92e94c30bf72a045dd3b4a71b1b8bf35b24f",
          "img_logo_url": "3d40214468cff0154fa066ab8967a3937b3a2b56",
          "has_community_visible_stats": true
        },
        {
          "appid": 363930,
          "name": "Dead Age",
          "playtime_forever": 0,
          "img_icon_url": "09fb18283cf6d2c51998f1a1e7117fe1b906aeb0",
          "img_logo_url": "4b7a207e6e70005da85a4e45789a609019e69ed1",
          "has_community_visible_stats": true
        },
        {
          "appid": 364270,
          "name": "Lilly and Sasha: Curse of the Immortals",
          "playtime_forever": 0,
          "img_icon_url": "bea99423af2e0b338d28d7047164cb6f0387c99e",
          "img_logo_url": "3f43725f19e236151e0e8bb95eaabe497f2f354c"
        },
        {
          "appid": 365450,
          "name": "Hacknet",
          "playtime_forever": 0,
          "img_icon_url": "2445c4ead6ee47784a326922271ec1c0b41d64e2",
          "img_logo_url": "50479f0bfe5a552132a0fc2668640b2e6a737398",
          "has_community_visible_stats": true
        },
        {
          "appid": 274270,
          "name": "NOT A HERO",
          "playtime_forever": 0,
          "img_icon_url": "f67cfd982237b07179a3c0fb0ca35077411e87ab",
          "img_logo_url": "36afb8a17a5cf3936cdbfffc4bd32bde482e1df2",
          "has_community_visible_stats": true
        },
        {
          "appid": 365770,
          "name": "Volume",
          "playtime_forever": 0,
          "img_icon_url": "a8247ebcf226cce9ffff799a5703e0e17a85b1c4",
          "img_logo_url": "136b4d6780efc93b2d907e453517674027484746",
          "has_community_visible_stats": true
        },
        {
          "appid": 365810,
          "name": "Flying Tigers: Shadows Over China",
          "playtime_forever": 0,
          "img_icon_url": "bb824e93bf995fb890ae589013e76f495f4ef70f",
          "img_logo_url": "8522fcd00427698c6d83b41b74ffc7d1081d476c",
          "has_community_visible_stats": true
        },
        {
          "appid": 312750,
          "name": "FINAL FANTASY IV",
          "playtime_forever": 0,
          "img_icon_url": "78ece41b1068f291ef61664813993558863d922d",
          "img_logo_url": "a72f0f20e028d7e919db8abcf3c3ad4576afa45d",
          "has_community_visible_stats": true
        },
        {
          "appid": 346830,
          "name": "FINAL FANTASY IV: THE AFTER YEARS",
          "playtime_forever": 0,
          "img_icon_url": "52199fee3e7d4bd5ab0fed85ff97f575e438cc58",
          "img_logo_url": "297d464a85ad3ad8c017f65a069ea4fd0a19de90",
          "has_community_visible_stats": true
        },
        {
          "appid": 366230,
          "name": "BASEBALL STARS 2",
          "playtime_forever": 0,
          "img_icon_url": "2b419f659582805e8f2b13bcedf391795d4df546",
          "img_logo_url": "5d8f5a1ab41576016b3a6dd9d6ed400924923fce",
          "has_community_visible_stats": true
        },
        {
          "appid": 366250,
          "name": "METAL SLUG",
          "playtime_forever": 0,
          "img_icon_url": "3169de7e7d03769312cdb12fbb418ad224613317",
          "img_logo_url": "28d378e21177c472f48789c72e8c1a4567002bc5",
          "has_community_visible_stats": true
        },
        {
          "appid": 366260,
          "name": "METAL SLUG 2",
          "playtime_forever": 0,
          "img_icon_url": "3335e8070277902ea30bc33f011a0b30b2183b3e",
          "img_logo_url": "cdb1fd917b7f3261c3dfdaa220dcef002abfd5a6",
          "has_community_visible_stats": true
        },
        {
          "appid": 366270,
          "name": "SHOCK TROOPERS",
          "playtime_forever": 0,
          "img_icon_url": "6147ce09f3fc9d08c65e878f599d947a9c81895f",
          "img_logo_url": "b531a38332a0025581040661cb9a777b767eeefd",
          "has_community_visible_stats": true
        },
        {
          "appid": 366280,
          "name": "TWINKLE STAR SPRITES",
          "playtime_forever": 0,
          "img_icon_url": "57718ebab091a994f31da74f5902d6594b0e1ef4",
          "img_logo_url": "67f775b54a5eb482e56cc6ad9e5ee8230ac1fc4a",
          "has_community_visible_stats": true
        },
        {
          "appid": 364900,
          "name": "Red Bit Ninja",
          "playtime_forever": 0,
          "img_icon_url": "ec157866ac5d7e33113b2803858d3ee3b13a1a1f",
          "img_logo_url": "f66d7625653033c34c64832ed986c4b9f70983fd",
          "has_community_visible_stats": true
        },
        {
          "appid": 367450,
          "name": "Poly Bridge",
          "playtime_forever": 0,
          "img_icon_url": "7bc695b845364099d87469c2d9074a3782c68638",
          "img_logo_url": "d5fd2146d109140289052b3584bf632d3643fc04",
          "has_community_visible_stats": true
        },
        {
          "appid": 367520,
          "name": "Hollow Knight",
          "playtime_forever": 0,
          "img_icon_url": "7b87aecda896ae747a6e40e3eb63498cb8b84df2",
          "img_logo_url": "6f48839fe61a9041c578419d72ed47e6b3f08141",
          "has_community_visible_stats": true
        },
        {
          "appid": 238370,
          "name": "Magicka 2",
          "playtime_forever": 0,
          "img_icon_url": "6c1ca2ff2c0f87c4d0a5378a9dbc7b7277bd239b",
          "img_logo_url": "c541f56b343320cccfdab1f9b9a87230e6e0b341",
          "has_community_visible_stats": true
        },
        {
          "appid": 397080,
          "name": "Magicka 2: Spell Balance Beta",
          "playtime_forever": 0,
          "img_icon_url": "",
          "img_logo_url": "",
          "has_community_visible_stats": true
        },
        {
          "appid": 368250,
          "name": "Valiant: Resurrection",
          "playtime_forever": 0,
          "img_icon_url": "1aa60396f02e640c2c571fd38a87c80fefe2985b",
          "img_logo_url": "91e599088bad08b8bb3300974e5c198ebae191e4",
          "has_community_visible_stats": true
        },
        {
          "appid": 368340,
          "name": "CrossCode",
          "playtime_forever": 0,
          "img_icon_url": "b7dd0b1a62b60be9f7e3c2639b1c0afcbc62f8c6",
          "img_logo_url": "0f004f4104be1fe06d1c474c6f177fda01288e23"
        },
        {
          "appid": 225640,
          "name": "Sacred 2 Gold",
          "playtime_forever": 0,
          "img_icon_url": "28844816d8aeca6c71b50f8b4f599c59f0d91afd",
          "img_logo_url": "1df208627b74f8d7f45e4504ba4fdfb55011f056"
        },
        {
          "appid": 247950,
          "name": "Sacred 3",
          "playtime_forever": 0,
          "img_icon_url": "bc5df33b4e9b4fabe56523632bcd52e149181d2a",
          "img_logo_url": "e4765d09081e6e34c7735a06988294ab0bf312ce",
          "has_community_visible_stats": true
        },
        {
          "appid": 40300,
          "name": "Risen",
          "playtime_forever": 0,
          "img_icon_url": "d5dbefdcd2e6e5def6518b3e66bf4ecf7f8aedcf",
          "img_logo_url": "e65177f1ab3fbbde7286c1233724eca7f5b9148c"
        },
        {
          "appid": 40390,
          "name": "Risen 2 - Dark Waters",
          "playtime_forever": 0,
          "img_icon_url": "874236eebecff1fe070032d137722ef3cdb47383",
          "img_logo_url": "e1b72e0af10d743a3d51800bd208bbe2890b9953",
          "has_community_visible_stats": true
        },
        {
          "appid": 249230,
          "name": "Risen 3 - Titan Lords",
          "playtime_forever": 0,
          "img_icon_url": "5d980c5528886a10e276942a680b6ff75975a36f",
          "img_logo_url": "d94a700a7cc6a601e151a5243ced16ca5b84e1c3",
          "has_community_visible_stats": true
        },
        {
          "appid": 329050,
          "name": "Devil May Cry® 4 Special Edition",
          "playtime_forever": 0,
          "img_icon_url": "e365480a8257204a0f8148bbf97251d39432522a",
          "img_logo_url": "a02a8e146a4380accc8a76c57db5d58d75c7c324",
          "has_community_visible_stats": true
        },
        {
          "appid": 370100,
          "name": "Uncrowded",
          "playtime_forever": 0,
          "img_icon_url": "165d36ffe1248c98e0898856c77b20385e464756",
          "img_logo_url": "1f563371a6578af5937f02edfcf4b89bcd4a9886"
        },
        {
          "appid": 370360,
          "name": "TIS-100",
          "playtime_forever": 0,
          "img_icon_url": "42fd5647ea90f7df3a2a0a8991d2eda676c8efa7",
          "img_logo_url": "5cafbe7be54aaf54999915159839949a5841d591",
          "has_community_visible_stats": true
        },
        {
          "appid": 370460,
          "name": "Arcana Heart 3 LOVE MAX!!!!!",
          "playtime_forever": 0,
          "img_icon_url": "74e61eb279c274ac754a2cd510780be778141dae",
          "img_logo_url": "92624e141fdc3d22f6cfebcdd3e3a4158c89e834",
          "has_community_visible_stats": true
        },
        {
          "appid": 370640,
          "name": "Midnight's Blessing",
          "playtime_forever": 0,
          "img_icon_url": "daffa7a59017492d6465645bbfd8bc4ccaffae89",
          "img_logo_url": "1b3e8e40d0dfec0a903ad113aff127faae0561c1",
          "has_community_visible_stats": true
        },
        {
          "appid": 370780,
          "name": "Fated Souls",
          "playtime_forever": 0,
          "img_icon_url": "d93d0b1a1a02fd2ec7ce61cd92e85bf3fbb4d22e",
          "img_logo_url": "0955dffa9a7135d4a2c4cbab95be8810e3561322",
          "has_community_visible_stats": true
        },
        {
          "appid": 351710,
          "name": "Hyperdimension Neptunia Re;Birth2 Sisters Generation",
          "playtime_forever": 0,
          "img_icon_url": "4408956509788bec91f53e3cd7bdc0f8bdc3bfcf",
          "img_logo_url": "f6f3b34fca9d67f8309afae55bce8fe0fad4a3d0",
          "has_community_visible_stats": true
        },
        {
          "appid": 371180,
          "name": "Super 3-D Noah's Ark",
          "playtime_forever": 0,
          "img_icon_url": "8b6c56f973c1750951dffa36ded3bdce72df145e",
          "img_logo_url": "c61f55b04b8f6281910a479f1437c3250a4fa307",
          "has_community_visible_stats": true
        },
        {
          "appid": 372000,
          "name": "Tree of Savior (English Ver.)",
          "playtime_forever": 632,
          "img_icon_url": "e9cc4c5a7366eda4d695583493aa56220eea5de4",
          "img_logo_url": "9817a0cbc3e917a98df329e8c8f5571981e00721",
          "has_community_visible_stats": true
        },
        {
          "appid": 332410,
          "name": "Moonchild",
          "playtime_forever": 0,
          "img_icon_url": "f96f8170eb7d5a7b0c0cbd8282dc59230d098df1",
          "img_logo_url": "e34cc5c6c5a7713d8dfde348cf453109395ae79d",
          "has_community_visible_stats": true
        },
        {
          "appid": 372360,
          "name": "Tales of Symphonia",
          "playtime_forever": 2850,
          "img_icon_url": "2a896e6b8a193b055e119c2da8fdd75e15263669",
          "img_logo_url": "f43f81e53236e948b9fa0a4ec5dfee4a67969f32",
          "has_community_visible_stats": true
        },
        {
          "appid": 373360,
          "name": "Cast of the Seven Godsends - Redux",
          "playtime_forever": 0,
          "img_icon_url": "a53bacf241d38c9fd0011bf52c9a61dcb3b50053",
          "img_logo_url": "6aba11ecf271fc96d694c203e6a282554577f2e6",
          "has_community_visible_stats": true
        },
        {
          "appid": 373440,
          "name": "Stargazer",
          "playtime_forever": 0,
          "img_icon_url": "4d6854118df44e065c62ad2219db0a8b0cf7615b",
          "img_logo_url": "abb182efa2f206cda4589d6a0d724b560b367700"
        },
        {
          "appid": 332380,
          "name": "Asguaard",
          "playtime_forever": 0,
          "img_icon_url": "553147f6bc333cd83f060b3e0f63e1ce3e1a943e",
          "img_logo_url": "aad6d6faf2cae8a6b304cd89a5dc27a96a869c34",
          "has_community_visible_stats": true
        },
        {
          "appid": 374320,
          "name": "DARK SOULS™ III",
          "playtime_forever": 0,
          "img_icon_url": "7abe1a33129c20cf10d2c74128bbd657a2a2c806",
          "img_logo_url": "54f76fd3abdd1446260f28ccc0cbc76034b32de9",
          "has_community_visible_stats": true
        },
        {
          "appid": 374570,
          "name": "Kung Fury",
          "playtime_forever": 0,
          "img_icon_url": "1344416065615ebc9468512017768b1fde6514a9",
          "img_logo_url": "215b9addb7ce94f5522f745491cabe203ea43afe"
        },
        {
          "appid": 298850,
          "name": "Millennium 5 - The Battle of the Millennium",
          "playtime_forever": 0,
          "img_icon_url": "e9fff3b34f0a559683e4175fec9163415fab498f",
          "img_logo_url": "727b7b64932b9b81d22a810120a5005d5fee2742",
          "has_community_visible_stats": true
        },
        {
          "appid": 298840,
          "name": "Millennium 4 - Beyond Sunset",
          "playtime_forever": 0,
          "img_icon_url": "8f52739d39a1b113dba5ec50925a5b9c1599b803",
          "img_logo_url": "e9612da2759d92aa4148b0fe8eac323f794a568a",
          "has_community_visible_stats": true
        },
        {
          "appid": 298830,
          "name": "Millennium 3 - Cry Wolf",
          "playtime_forever": 0,
          "img_icon_url": "d9d5375148b7a0c238c62600db7c9ebb9303770b",
          "img_logo_url": "1a033a7401bab78ed7ddad6e30d4c668d977ba8b",
          "has_community_visible_stats": true
        },
        {
          "appid": 298820,
          "name": "Millennium 2 - Take Me Higher",
          "playtime_forever": 0,
          "img_icon_url": "cc9ba24ac720a762bf545127e659f503c112d7b5",
          "img_logo_url": "a740b58bbfe830c96ff8ef3ac052d11ec8b9cb53",
          "has_community_visible_stats": true
        },
        {
          "appid": 375200,
          "name": "Sakura Fantasy Chapter 1",
          "playtime_forever": 0,
          "img_icon_url": "3fc430a6c6096d609041d684675030e468e77380",
          "img_logo_url": "a37542d43f4e9bbc6bb13dacd033288d16a0ae1e"
        },
        {
          "appid": 375520,
          "name": "Taimumari",
          "playtime_forever": 443,
          "img_icon_url": "9e7c5763017dca67f9c37028a21f67a4b8742695",
          "img_logo_url": "44e31016e998a9a4093852df645da85a1a88a25e",
          "has_community_visible_stats": true
        },
        {
          "appid": 375820,
          "name": "Human Resource Machine",
          "playtime_forever": 0,
          "img_icon_url": "16097a08c881abe08531e7d31dcf29e962581149",
          "img_logo_url": "fadb456336044de72d33a9e72683033951c41dea",
          "has_community_visible_stats": true
        },
        {
          "appid": 267750,
          "name": "Shadowrun Chronicles - Boston Lockdown",
          "playtime_forever": 0,
          "img_icon_url": "323110f304d992546a5f33d05af9c937e5d9e9e0",
          "img_logo_url": "0155712c7b4fcdbca90dbceb09ff2b6eab19e584",
          "has_community_visible_stats": true
        },
        {
          "appid": 377680,
          "name": "Sakura Beach",
          "playtime_forever": 0,
          "img_icon_url": "45ae73240006e5f26f793c16265f63cb573d71f1",
          "img_logo_url": "e75b5318ffd82972a739b1f88b54d07183bc74fb"
        },
        {
          "appid": 380750,
          "name": "Anima Gate of Memories",
          "playtime_forever": 326,
          "img_icon_url": "6beed2816773510791dbf71987c54cda3b9601e6",
          "img_logo_url": "45b467a2e0afcc687317cd5f16f576a2f757d2e3",
          "has_community_visible_stats": true
        },
        {
          "appid": 8980,
          "name": "Borderlands",
          "playtime_forever": 0,
          "img_icon_url": "eb45fb32b7f4e75564a30a52aa3c35a191253ea0",
          "img_logo_url": "88ec298ff1ff0d748df39e084892aed6e919b146",
          "has_community_visible_stats": true
        },
        {
          "appid": 49520,
          "name": "Borderlands 2",
          "playtime_forever": 0,
          "img_icon_url": "a3f4945226e69b6196074df4c776e342d3e5a3be",
          "img_logo_url": "86b0fa5ddb41b4dfff7df194a017f3418130d668",
          "has_community_visible_stats": true
        },
        {
          "appid": 208650,
          "name": "Batman™: Arkham Knight",
          "playtime_forever": 2637,
          "img_icon_url": "f6c2ce13796844750dfbd01685fb009eeac4bf70",
          "img_logo_url": "8968f6628f8207ee0ea7222d10a2ecd09d582a62",
          "has_community_visible_stats": true
        },
        {
          "appid": 267490,
          "name": "Batman™: Arkham Origins Blackgate - Deluxe Edition",
          "playtime_forever": 0,
          "img_icon_url": "48f20fda3989cba5ca66331d0bacc25c06337ced",
          "img_logo_url": "166af0bf5a982e91a9b31d89a88c6c7b269ba2fc",
          "has_community_visible_stats": true
        },
        {
          "appid": 382890,
          "name": "FINAL FANTASY V",
          "playtime_forever": 0,
          "img_icon_url": "14385c9fc1e08408c9675c7ebe51836c4cb269b3",
          "img_logo_url": "be4b92fe951caf7aaf632487332de17d73071ebf",
          "has_community_visible_stats": true
        },
        {
          "appid": 382900,
          "name": "FINAL FANTASY VI",
          "playtime_forever": 13,
          "img_icon_url": "987e160ff0865aebbc1a4b2753cf9e8ab15ae917",
          "img_logo_url": "007c45561fb029a592d7d27f5d9073d4f5b76cdc",
          "has_community_visible_stats": true
        },
        {
          "appid": 383150,
          "name": "Dead Island Definitive Edition",
          "playtime_forever": 0,
          "img_icon_url": "80d0dabca21c70909e63682512a24ce66c4f5c3b",
          "img_logo_url": "5e73a678e40f1df959702fe3706e1fef7ef535d1",
          "has_community_visible_stats": true
        },
        {
          "appid": 383180,
          "name": "Dead Island Riptide Definitive Edition",
          "playtime_forever": 0,
          "img_icon_url": "68ea0c6d8ed1d089687da6318a62a7c3e3b5741b",
          "img_logo_url": "dc96ce92ec5cdb7b6aa66c8fc02ede8ca6f184b0",
          "has_community_visible_stats": true
        },
        {
          "appid": 383960,
          "name": "Machina of the Planet Tree -Planet Ruler-",
          "playtime_forever": 0,
          "img_icon_url": "15bf82591799eace853c6216af6fa4859ee1cc5f",
          "img_logo_url": "9a2bef12add2f5e7515151aeeaec19e093ad2f1c"
        },
        {
          "appid": 345180,
          "name": "Victor Vran",
          "playtime_forever": 0,
          "img_icon_url": "daf6abde5f6599264275244b46d38c6fad84d6e1",
          "img_logo_url": "2ea4d34ea1fdcc8cdf51ad48ad0ad45ff252780e",
          "has_community_visible_stats": true
        },
        {
          "appid": 384630,
          "name": "Aviary Attorney",
          "playtime_forever": 0,
          "img_icon_url": "b963dd67c35a3362eae10fddc357aaedb0b36e43",
          "img_logo_url": "0ea191e6e968de4fb9b4a34eceaf7506b9ffaad3"
        },
        {
          "appid": 385560,
          "name": "Shadow Complex Remastered",
          "playtime_forever": 0,
          "img_icon_url": "28d0e9c604c2df052897ff84bddab71723cf2c22",
          "img_logo_url": "4a58ff2435a60d39fa0df80a7a245429519df285",
          "has_community_visible_stats": true
        },
        {
          "appid": 386990,
          "name": "Asphyxia",
          "playtime_forever": 0,
          "img_icon_url": "9ba2203cd2efbe88e844d7175c568ed02b7e53d3",
          "img_logo_url": "52981b8c75698ffdaaa4157082bbc4f10ed120ef",
          "has_community_visible_stats": true
        },
        {
          "appid": 387070,
          "name": "One Final Breath",
          "playtime_forever": 0,
          "img_icon_url": "cc36b2b6eefd4f096f51cb49ebe41d72cdcf2d67",
          "img_logo_url": "8b6647cf35257c1ef3a9ef706c7018ec70617dc7",
          "has_community_visible_stats": true
        },
        {
          "appid": 387340,
          "name": "Hyperdimension Neptunia U: Action Unleashed",
          "playtime_forever": 0,
          "img_icon_url": "029a132b80d71c8867cea543a4dd1cd7c63bf9d4",
          "img_logo_url": "9ad044e2bcee72759ef76044d76b678e0c06bc56",
          "has_community_visible_stats": true
        },
        {
          "appid": 388090,
          "name": "Five Nights at Freddy's 4",
          "playtime_forever": 0,
          "img_icon_url": "5973643d5902043e1953b068cacd909693fa43bd",
          "img_logo_url": "6c0f3476fff016f0da5c430fffb943032def999d"
        },
        {
          "appid": 388210,
          "name": "Day of the Tentacle Remastered",
          "playtime_forever": 0,
          "img_icon_url": "6d8caff1e7eb9011b1c7c03e687c0d43753ac8bd",
          "img_logo_url": "d652df906f889c9eeb47c52eb9190cbade874493",
          "has_community_visible_stats": true
        },
        {
          "appid": 295790,
          "name": "Never Alone (Kisima Ingitchuna)",
          "playtime_forever": 0,
          "img_icon_url": "bc3e0ca91dc7118dc0b7e4b084aa8d320f10df09",
          "img_logo_url": "d7e4cec47a01b6e913e495450be669c4997d0a17",
          "has_community_visible_stats": true
        },
        {
          "appid": 388880,
          "name": "Oxenfree",
          "playtime_forever": 0,
          "img_icon_url": "ebd4e04badb440be4f2ceca1a9b25efd1054c70e",
          "img_logo_url": "d1b867d5ea8ca4fa232132c35dbfc41b66df5b99",
          "has_community_visible_stats": true
        },
        {
          "appid": 389870,
          "name": "Mugen Souls",
          "playtime_forever": 0,
          "img_icon_url": "90a392b911b9f0fb5cbfea378f0a362d0467c334",
          "img_logo_url": "6fc5935cf4a0c9e4bf4a7bdcee4c29d595c91b51",
          "has_community_visible_stats": true
        },
        {
          "appid": 234140,
          "name": "Mad Max",
          "playtime_forever": 0,
          "img_icon_url": "71a4dd52273b749f96913a48dd6f439fb5ddc23e",
          "img_logo_url": "f114c3cf063da97aac613bf408b465e50f43a644",
          "has_community_visible_stats": true
        },
        {
          "appid": 391120,
          "name": "The Archetype",
          "playtime_forever": 0,
          "img_icon_url": "d7e33f47c9f6a2a6c617b4cdce83e215f101da6e",
          "img_logo_url": "2610882cb605e2cfdcc779bf918a9fc2efcec1c3",
          "has_community_visible_stats": true
        },
        {
          "appid": 391260,
          "name": "Labyronia RPG",
          "playtime_forever": 0,
          "img_icon_url": "88f83bdb965b7952395f6ca5d1f087538c409d18",
          "img_logo_url": "745d03a3efac5cb1faf6cbbe5c7f796c00deacc4",
          "has_community_visible_stats": true
        },
        {
          "appid": 391540,
          "name": "Undertale",
          "playtime_forever": 0,
          "img_icon_url": "2ce672b89b63ec1e70d2f12862e72eb4a33e9268",
          "img_logo_url": "ae953fb87a0fd4958ca21995226c065f33290eba"
        },
        {
          "appid": 391720,
          "name": "Layers of Fear",
          "playtime_forever": 358,
          "img_icon_url": "0fc90b925c7bbdcc95df6599a01b80dff18e6bbb",
          "img_logo_url": "64c61e0166b1bb2a80c57f76f2ca620f2ba49536",
          "has_community_visible_stats": true
        },
        {
          "appid": 394310,
          "name": "Punch Club",
          "playtime_forever": 0,
          "img_icon_url": "d423d509dc7c722f7c11ded0d1b3c25956c99449",
          "img_logo_url": "d8d3e4cf43e540ac562977713afdbc0932d65957",
          "has_community_visible_stats": true
        },
        {
          "appid": 396160,
          "name": "Secret Of Magia",
          "playtime_forever": 0,
          "img_icon_url": "571355dea2362d1385871ead9b7ada65d4a96278",
          "img_logo_url": "e0ca889c15c709f4a70c7af428227d61d2867cf5",
          "has_community_visible_stats": true
        },
        {
          "appid": 225540,
          "name": "Just Cause 3",
          "playtime_forever": 0,
          "img_icon_url": "c82c69c8d616d9d273d749e3dd3bd7a0f9da594a",
          "img_logo_url": "afdb9bd71ccd65d3fcfc4735076d50cead701036",
          "has_community_visible_stats": true
        },
        {
          "appid": 397500,
          "name": "Labyronia RPG 2",
          "playtime_forever": 0,
          "img_icon_url": "88f83bdb965b7952395f6ca5d1f087538c409d18",
          "img_logo_url": "01f00e587d604dda7728ac31dbe92e6627517bea",
          "has_community_visible_stats": true
        },
        {
          "appid": 399790,
          "name": "Destiny Warriors",
          "playtime_forever": 0,
          "img_icon_url": "e4a0f521fe87ffdac2a882924a03918655705684",
          "img_logo_url": "1b0104ebdcde8b26a16d2202930770c230530550",
          "has_community_visible_stats": true
        },
        {
          "appid": 400110,
          "name": "Else Heart.Break()",
          "playtime_forever": 0,
          "img_icon_url": "f2c5753428af58898e7a15979a003d255b4e708c",
          "img_logo_url": "446dfb76fdbe32476ba23784d35df97097079c6e"
        },
        {
          "appid": 4700,
          "name": "Medieval II: Total War",
          "playtime_forever": 0,
          "img_icon_url": "cc9c44b2c3b3b245b82d13c316a9b238705ff877",
          "img_logo_url": "fcd1abd6380998e473b92690e28a9fe0a1a27b8d"
        },
        {
          "appid": 211160,
          "name": "Viking: Battle for Asgard",
          "playtime_forever": 0,
          "img_icon_url": "fa0cc33822d69907bf8e761a645541784f85294d",
          "img_logo_url": "bc838a2645c033337296d94235bdd4e6f87a6c79",
          "has_community_visible_stats": true
        },
        {
          "appid": 345240,
          "name": "SHOGUN: Total War™ - Gold Edition",
          "playtime_forever": 0,
          "img_icon_url": "2a746356c90129eec1e9eeb50172cd4cbddd61ca",
          "img_logo_url": "d40d3f6da5c9c46e8b71f6c3843559e735d786ca"
        },
        {
          "appid": 403670,
          "name": "Lumber Island - That Special Place",
          "playtime_forever": 0,
          "img_icon_url": "c7cba768d6392440bd150f9c2d2b443c474e964c",
          "img_logo_url": "fe97a76fe4c8f808480cd05a9363cca4f31ca159"
        },
        {
          "appid": 405180,
          "name": "123 Slaughter Me Street",
          "playtime_forever": 0,
          "img_icon_url": "7b0da5c48bf3ba7c8f1b7c802acea63d9a5f7118",
          "img_logo_url": "015cdb933bfa9b6e2c06f4413187638af5cc8f07"
        },
        {
          "appid": 40340,
          "name": "Secret Files: Puritas Cordis",
          "playtime_forever": 0,
          "img_icon_url": "5962b3364d4ae851d6f4c7c5fa860769622ac9bd",
          "img_logo_url": "b020df823ee761aa270c9cb0411c2c99a741f0a8"
        },
        {
          "appid": 40350,
          "name": "Lost Horizon",
          "playtime_forever": 0,
          "img_icon_url": "8cb31c5612c07da676a24677343c8c5a7b5d1d32",
          "img_logo_url": "1d4ea6cc8d3fb2a0b65b952d94efd8d12edebb55"
        },
        {
          "appid": 216210,
          "name": "Secret Files 3",
          "playtime_forever": 0,
          "img_icon_url": "52dc697e8b888a912582b3822527ad37c150c878",
          "img_logo_url": ""
        },
        {
          "appid": 257220,
          "name": "Secret Files: Sam Peters",
          "playtime_forever": 0,
          "img_icon_url": "788427264cc2b29eaa3afbed90270e608e477b4c",
          "img_logo_url": "578f581307f1d2269eb8a11b26b209ec983ea2cc"
        },
        {
          "appid": 395560,
          "name": "Lost Horizon 2",
          "playtime_forever": 0,
          "img_icon_url": "0391fe74967dd4ffcc411b67232529656b4595f0",
          "img_logo_url": "af240a8d94092905a41fefea277d9efd29d81a4d"
        },
        {
          "appid": 405500,
          "name": "Dangerous Golf",
          "playtime_forever": 0,
          "img_icon_url": "cbdeeccf45c2f28e01c2e3c3a924421e595b203a",
          "img_logo_url": "3a44658f057eed8b95adad2a152c3f47023de4a8",
          "has_community_visible_stats": true
        },
        {
          "appid": 405640,
          "name": "Pony Island",
          "playtime_forever": 0,
          "img_icon_url": "cbfb87666914abca2e7c06231eeb59ab2a1883e8",
          "img_logo_url": "0778ea69bef198a63c75b241a33e105d1a75778c",
          "has_community_visible_stats": true
        },
        {
          "appid": 15700,
          "name": "Oddworld: Abe's Oddysee",
          "playtime_forever": 0,
          "img_icon_url": "146cabd04a2e1a813085fbc4511c10ca01fac51b",
          "img_logo_url": "b36c09cedd607be8661c6ae9dea7966158281c3f"
        },
        {
          "appid": 351970,
          "name": "Tales of Zestiria",
          "playtime_forever": 4107,
          "img_icon_url": "898f35ca5e8c803df592779a723cc741d32814c9",
          "img_logo_url": "d5fd78a650d3d1a5a5bbfcc307ce9e4b7b4d36cc",
          "has_community_visible_stats": true
        },
        {
          "appid": 405900,
          "name": "Disgaea PC",
          "playtime_forever": 0,
          "img_icon_url": "fdd85d1f156cf7b5200c9988da20f154a34a2610",
          "img_logo_url": "2e8f870fd9f65c494ac5c9e0b2cbd1d582f7650f",
          "has_community_visible_stats": true
        },
        {
          "appid": 392940,
          "name": "Epic Quest of the 4 Crystals",
          "playtime_forever": 0,
          "img_icon_url": "da05dfe4592920ddee48168a8205c31904c19b0e",
          "img_logo_url": "04c098c3142997fd01572f99b2477c32f3561981",
          "has_community_visible_stats": true
        },
        {
          "appid": 406440,
          "name": "Vanguard Princess Kurumi",
          "playtime_forever": 0,
          "img_icon_url": "f3dc5af7142117995e4a4336f19184f5c1a13876",
          "img_logo_url": "e767117ba86f79496d4106196ff18dbc0fa89294",
          "has_community_visible_stats": true
        },
        {
          "appid": 407230,
          "name": "Legend of Mysteria",
          "playtime_forever": 0,
          "img_icon_url": "1f6c9020a2547743ce7769647c37656aa0cae98f",
          "img_logo_url": "bee514653969bbd70b0cb4b03f420c43f80d8c42",
          "has_community_visible_stats": true
        },
        {
          "appid": 407980,
          "name": "Sakura Beach 2",
          "playtime_forever": 0,
          "img_icon_url": "45ae73240006e5f26f793c16265f63cb573d71f1",
          "img_logo_url": "0e131b2f35628ae16f59215572ff0c9ea45fde24"
        },
        {
          "appid": 409870,
          "name": "Phantom Brave PC",
          "playtime_forever": 0,
          "img_icon_url": "8d22a98e46654741d7a1de92b209505c53c7bf8b",
          "img_logo_url": "fa6a5adaa2392472106457169bfbddf0ab8836ee",
          "has_community_visible_stats": true
        },
        {
          "appid": 414700,
          "name": "Outlast 2",
          "playtime_forever": 0,
          "img_icon_url": "f336155a1714d0d413dea897f63ef19ee1f7cf21",
          "img_logo_url": "9d64d9cf9825c3ad9833f8ec21b491519c5c99e5",
          "has_community_visible_stats": true
        },
        {
          "appid": 415150,
          "name": "Pang Adventures",
          "playtime_forever": 200,
          "img_icon_url": "619c366002ff3452824b8150b84d20cbfbeb5b2c",
          "img_logo_url": "373521101d587ebfa0647af44097696f64f8b55a",
          "has_community_visible_stats": true
        },
        {
          "appid": 415480,
          "name": "Hyperdevotion Noire: Goddess Black Heart",
          "playtime_forever": 0,
          "img_icon_url": "92ba4551684a1e24d890736e82159b1bd8ac38d5",
          "img_logo_url": "9c57ee4b11d5bf67f16849abf5a7cba37845e37e",
          "has_community_visible_stats": true
        },
        {
          "appid": 418950,
          "name": "DreadOut: Keepers of The Dark",
          "playtime_forever": 0,
          "img_icon_url": "65fe96c038b3fd7d6a3cca3faa174b60da4214c9",
          "img_logo_url": "befbcd04e09918488560b17e87e112ddb6a0dae1",
          "has_community_visible_stats": true
        },
        {
          "appid": 349040,
          "name": "NARUTO SHIPPUDEN: Ultimate Ninja STORM 4",
          "playtime_forever": 0,
          "img_icon_url": "bb5c2a74c919d172b4a40c85591aaffd101d26ef",
          "img_logo_url": "18a02c4589dcc5dce21a508bd2a8e28a3c1e064b",
          "has_community_visible_stats": true
        },
        {
          "appid": 420060,
          "name": "Candle",
          "playtime_forever": 0,
          "img_icon_url": "ebbc7803660896a3623d1d5fa6fc1dcc299f13c5",
          "img_logo_url": "105a5ba80ea2cde0ed02d4a99e00d41bbcf2553c",
          "has_community_visible_stats": true
        },
        {
          "appid": 368500,
          "name": "Assassin's Creed Syndicate",
          "playtime_forever": 0,
          "img_icon_url": "4f67f650e621319029fa884755dbd89a2d7076f6",
          "img_logo_url": "ebf64f161c59f7a9e732bbaef1435354ef804265",
          "has_community_visible_stats": true
        },
        {
          "appid": 421050,
          "name": "Deponia Doomsday",
          "playtime_forever": 0,
          "img_icon_url": "6a4fd120d416a803f295de210637e6bc665ad2b6",
          "img_logo_url": "4f64566297c6909f0b14f7184625f08ab5487f08",
          "has_community_visible_stats": true
        },
        {
          "appid": 421810,
          "name": "OH! RPG!",
          "playtime_forever": 0,
          "img_icon_url": "df768870453d2e5ab698af6b7c370d6dbfa653fa",
          "img_logo_url": "e3b57f1b5fcc9c506de1f683768e59f6d7f7d528",
          "has_community_visible_stats": true
        },
        {
          "appid": 423230,
          "name": "Furi",
          "playtime_forever": 0,
          "img_icon_url": "85ba3f445459796811d4ea9596914e77dcc0a082",
          "img_logo_url": "dd947e9f88f87387114c96fed79c3772974c6e53",
          "has_community_visible_stats": true
        },
        {
          "appid": 423950,
          "name": "Deadlight Director’s Cut",
          "playtime_forever": 0,
          "img_icon_url": "f969ef92e6636ab7200a60ce5e2b85cae60d1d84",
          "img_logo_url": "442638cbbd51693feb486fc5ee816a853502f12d",
          "has_community_visible_stats": true
        },
        {
          "appid": 424840,
          "name": "Little Nightmares",
          "playtime_forever": 0,
          "img_icon_url": "1904c041f963b5e2cb22b56c370444517cc4e288",
          "img_logo_url": "2681ea83b77b831431b95ad69a8ad6036d1ab981",
          "has_community_visible_stats": true
        },
        {
          "appid": 376300,
          "name": "GUILTY GEAR Xrd -SIGN-",
          "playtime_forever": 0,
          "img_icon_url": "2e31736d99d2a84bcfad55264f3af01722c7e7dc",
          "img_logo_url": "98e55536d5152a4b703bd8f4dd2a1f008aa04361",
          "has_community_visible_stats": true
        },
        {
          "appid": 426000,
          "name": "HunieCam Studio",
          "playtime_forever": 0,
          "img_icon_url": "a40fc165593d7bc2a97d1c86598876a25c0702ae",
          "img_logo_url": "58d552b98eb0722d89f4447e4f11dc78b8c44656",
          "has_community_visible_stats": true
        },
        {
          "appid": 339340,
          "name": "Resident Evil 0 / biohazard 0 HD REMASTER",
          "playtime_forever": 0,
          "img_icon_url": "db120ea03830d200f5ba41567b2dd4556aa33705",
          "img_logo_url": "1718a3f2dd34e4905d31b122a08e0bfff2daaa17",
          "has_community_visible_stats": true
        },
        {
          "appid": 427190,
          "name": "Dead Rising",
          "playtime_forever": 0,
          "img_icon_url": "74ffef15e4897d294a86e5e33e7b427ce37f2219",
          "img_logo_url": "f77418310c04d3b81500dff57b34026d8753cac3",
          "has_community_visible_stats": true
        },
        {
          "appid": 428550,
          "name": "Momodora: Reverie Under the Moonlight",
          "playtime_forever": 524,
          "img_icon_url": "17d2a60ead58d88259b7ff68c0bf698b0b5e4afc",
          "img_logo_url": "70bd99b8c3b8c7cb646327f202c8ea8356332d83",
          "has_community_visible_stats": true
        },
        {
          "appid": 429570,
          "name": "The Walking Dead: Michonne",
          "playtime_forever": 0,
          "img_icon_url": "f75fb34d85b69cf28c5e8283a52d40aa09a33b01",
          "img_logo_url": "3d0c79badd05d2c0853a362586d73604c0618f82",
          "has_community_visible_stats": true
        },
        {
          "appid": 429660,
          "name": "Tales of Berseria",
          "playtime_forever": 0,
          "img_icon_url": "bc14b91107afcf731e5c324e3f1ef83098d8a8fa",
          "img_logo_url": "9feea7fc5d27169fcfd78f7c1250faa40bbf81fd",
          "has_community_visible_stats": true
        },
        {
          "appid": 310950,
          "name": "Street Fighter V: Arcade Edition",
          "playtime_forever": 0,
          "img_icon_url": "ea0db9dc61e606b544fa30d21d31354998ca1f95",
          "img_logo_url": "dc8c23570a919e8f2ac14b87d21b169bf740b12d",
          "has_community_visible_stats": true
        },
        {
          "appid": 377840,
          "name": "FINAL FANTASY IX",
          "playtime_forever": 20,
          "img_icon_url": "5c5cb414b598d8b1a9056f12f625f92a601ad421",
          "img_logo_url": "b342b31d52424965c35b938b8d08ed729ef3cd78",
          "has_community_visible_stats": true
        },
        {
          "appid": 440430,
          "name": "Life Is Strange™ - Directors' Commentary - 1.Two directors",
          "playtime_forever": 0,
          "img_icon_url": "",
          "img_logo_url": ""
        },
        {
          "appid": 440431,
          "name": "Life Is Strange™ - Directors' Commentary - 2. Let's play Life is Strange",
          "playtime_forever": 0,
          "img_icon_url": "",
          "img_logo_url": ""
        },
        {
          "appid": 440432,
          "name": "Life Is Strange™ - Directors' Commentary - 3. Intentions",
          "playtime_forever": 0,
          "img_icon_url": "",
          "img_logo_url": ""
        },
        {
          "appid": 440433,
          "name": "Life Is Strange™ - Directors' Commentary - 4. A matter of choice",
          "playtime_forever": 0,
          "img_icon_url": "",
          "img_logo_url": ""
        },
        {
          "appid": 440434,
          "name": "Life Is Strange™ - Directors' Commentary - 5. A lively world",
          "playtime_forever": 0,
          "img_icon_url": "",
          "img_logo_url": ""
        },
        {
          "appid": 440435,
          "name": "Life Is Strange™ - Directors' Commentary - 6. Capturing the moment",
          "playtime_forever": 0,
          "img_icon_url": "",
          "img_logo_url": ""
        },
        {
          "appid": 440436,
          "name": "Life Is Strange™ - Directors' Commentary - 7. Social issues",
          "playtime_forever": 0,
          "img_icon_url": "",
          "img_logo_url": ""
        },
        {
          "appid": 440437,
          "name": "Life Is Strange™ - Directors' Commentary - 8. Getting things right",
          "playtime_forever": 0,
          "img_icon_url": "",
          "img_logo_url": ""
        },
        {
          "appid": 440438,
          "name": "Life Is Strange™ - Directors' Commentary - 9. Voices of Arcadia Bay",
          "playtime_forever": 0,
          "img_icon_url": "",
          "img_logo_url": ""
        },
        {
          "appid": 391220,
          "name": "Rise of the Tomb Raider",
          "playtime_forever": 0,
          "img_icon_url": "0b8a37f32ed2b7c934be8aa94d53f71e274c6497",
          "img_logo_url": "5270d12abfc683e6f1d39e7ac0b6f2db366e7209",
          "has_community_visible_stats": true
        },
        {
          "appid": 433910,
          "name": "Neon Drive",
          "playtime_forever": 0,
          "img_icon_url": "c41a172f4858d669c24ddb2caf9d1e6f9645f594",
          "img_logo_url": "e9baf4e3b861f871f4f38ddfa48c59e015ba7202",
          "has_community_visible_stats": true
        },
        {
          "appid": 434650,
          "name": "Lost Castle",
          "playtime_forever": 0,
          "img_icon_url": "8462ea7ace0444710f30a8681714b57a4a7643d6",
          "img_logo_url": "01ec38bd5b3c45b73d60870383c166b8ea3bb601",
          "has_community_visible_stats": true
        },
        {
          "appid": 436670,
          "name": "The Legend of Heroes: Trails in the Sky the 3rd",
          "playtime_forever": 0,
          "img_icon_url": "1fb9610ea9419c4f320b29ff27ea6cac4fbfd2cb",
          "img_logo_url": "b6d400b5547fc3d11ebef4d49a23a1e1c10f539c",
          "has_community_visible_stats": true
        },
        {
          "appid": 392920,
          "name": "Arvale",
          "playtime_forever": 0,
          "img_icon_url": "5b565394f29c3886731695b0ad8bbe7db22a60f5",
          "img_logo_url": "34b875d36c6c0bcc5c2bfcf71cbeaba0910d4eb9"
        },
        {
          "appid": 438490,
          "name": "GOD EATER 2 Rage Burst",
          "playtime_forever": 0,
          "img_icon_url": "c694868390c63d40956b78e61dc0df27ce493a8c",
          "img_logo_url": "388656ae75b147289e0da238fa2d98e213924609",
          "has_community_visible_stats": true
        },
        {
          "appid": 460870,
          "name": "GOD EATER RESURRECTION",
          "playtime_forever": 0,
          "img_icon_url": "0b3a9c5f24bbc389654d2f189093cafe3cad6e7a",
          "img_logo_url": "21326dd2e1bb95ec29c6314979a1c700b892df26",
          "has_community_visible_stats": true
        },
        {
          "appid": 310380,
          "name": "Fractured Space",
          "playtime_forever": 0,
          "img_icon_url": "ad6e742508e69c690d94136b3226de22edc42faa",
          "img_logo_url": "6fb7ce7701d0715e440b723b092afac35fb79024",
          "has_community_visible_stats": true
        },
        {
          "appid": 441280,
          "name": "Pharaoh Rebirth+",
          "playtime_forever": 383,
          "img_icon_url": "c949a3802039baf4aff6ef642ce2d94f204d8e22",
          "img_logo_url": "f8b73ddeba768df5bdbad6ad0d182b9bf3598323",
          "has_community_visible_stats": true
        },
        {
          "appid": 4720,
          "name": "Condemned: Criminal Origins",
          "playtime_forever": 0,
          "img_icon_url": "8ea7e69eb0f0f056c39f9c9d655d3885bbe4215a",
          "img_logo_url": "ef7f2aec381356c407801bad681f118fed8e3705",
          "has_community_visible_stats": true
        },
        {
          "appid": 34270,
          "name": "SEGA Mega Drive & Genesis Classics",
          "playtime_forever": 7,
          "img_icon_url": "48a187fa87c58b798646a430d446dd36eeabd1a4",
          "img_logo_url": "63f7807a9c6acbb8a9359d11f4ec689a1c7a2841",
          "has_community_visible_stats": true
        },
        {
          "appid": 99300,
          "name": "Renegade Ops",
          "playtime_forever": 0,
          "img_icon_url": "def6f08f177180ab15bbf7c90ba8d323159ecd7d",
          "img_logo_url": "6535ea9cf634e38142113b7c0ede148355f110df",
          "has_community_visible_stats": true
        },
        {
          "appid": 446020,
          "name": "Jalopy",
          "playtime_forever": 0,
          "img_icon_url": "ac803e42cf2ee1f3044688b6536e89443d42d729",
          "img_logo_url": "a39a326aa94b50d0fc1fd43dcb04c0f9c301cc24",
          "has_community_visible_stats": true
        },
        {
          "appid": 446810,
          "name": "Blossom Tales: The Sleeping King",
          "playtime_forever": 135,
          "img_icon_url": "c0ce49f3fba27f7716f14e52fa83bcadea99c238",
          "img_logo_url": "24f7957e5813b20deac37d0dbeb1d84712e95fbe",
          "has_community_visible_stats": true
        },
        {
          "appid": 446840,
          "name": "Splasher",
          "playtime_forever": 0,
          "img_icon_url": "36f67028d518ba832a41276d3229e55b4beda103",
          "img_logo_url": "9743931b01a7a64d1088727f32adc073f734ded9",
          "has_community_visible_stats": true
        },
        {
          "appid": 4560,
          "name": "Company of Heroes - Legacy Edition",
          "playtime_forever": 0,
          "img_icon_url": "64946619217da497c9b29bc817bb40dd7d28c912",
          "img_logo_url": "e12e8695c6766b47a089351dd9c4531e669c2a7b"
        },
        {
          "appid": 4570,
          "name": "Warhammer 40,000: Dawn of War - Game of the Year Edition",
          "playtime_forever": 0,
          "img_icon_url": "a4c7a8cce43d797c275aaf601d6855b90ba87769",
          "img_logo_url": "2068980dca52521b069abc109f976d72ba0b1651",
          "has_community_visible_stats": true
        },
        {
          "appid": 4580,
          "name": "Warhammer 40,000: Dawn of War - Dark Crusade",
          "playtime_forever": 0,
          "img_icon_url": "584fcb6284ffd2fcf378fb3c199a9275a0a01b2d",
          "img_logo_url": "ed04f5f4ca5bd374fa09da62f583a7dbbf1d8558",
          "has_community_visible_stats": true
        },
        {
          "appid": 4760,
          "name": "Rome: Total War",
          "playtime_forever": 0,
          "img_icon_url": "5dc68565149dc971af6428157bcb600d80690080",
          "img_logo_url": "134817933edf4f8d0665d456889c0315c416fff2"
        },
        {
          "appid": 4770,
          "name": "Rome: Total War - Alexander",
          "playtime_forever": 0,
          "img_icon_url": "609a824446a556878febbc3aa0be7d1f9b92a4fb",
          "img_logo_url": "de46cc6041545762ad8b45a1bebd2320ba8cb356"
        },
        {
          "appid": 9310,
          "name": "Warhammer 40,000: Dawn of War - Winter Assault",
          "playtime_forever": 0,
          "img_icon_url": "60d48bf5f8a11fc4aeacb546a20cb40f537290bf",
          "img_logo_url": "88ca73c99d1a64b81a3306c690f0354a1643c1c1",
          "has_community_visible_stats": true
        },
        {
          "appid": 9340,
          "name": "Company of Heroes: Opposing Fronts",
          "playtime_forever": 0,
          "img_icon_url": "29725d719946c3e1aa4eea15d262c9fd789c1392",
          "img_logo_url": "830c99099ea2cfecfe74c41f376fc892a09dd181"
        },
        {
          "appid": 9450,
          "name": "Warhammer 40,000: Dawn of War - Soulstorm",
          "playtime_forever": 0,
          "img_icon_url": "d13abadb0f456659a634d2cd8286665a0c76a2c6",
          "img_logo_url": "3b300beec0113261da00267121eccd8702059a88",
          "has_community_visible_stats": true
        },
        {
          "appid": 20540,
          "name": "Company of Heroes: Tales of Valor",
          "playtime_forever": 0,
          "img_icon_url": "64946619217da497c9b29bc817bb40dd7d28c912",
          "img_logo_url": "ed0c55412acea558d025a3e238e2b7341edc5c41"
        },
        {
          "appid": 228200,
          "name": "Company of Heroes ",
          "playtime_forever": 0,
          "img_icon_url": "df92dc239acb3cf5d3e3eba645f3df2aaf7f91ad",
          "img_logo_url": "87aa009e93d5aa56a55d0e9056708d018ddd6483",
          "has_community_visible_stats": true
        },
        {
          "appid": 460120,
          "name": "Megadimension Neptunia VII",
          "playtime_forever": 0,
          "img_icon_url": "3b6915aaf3acfdcbeeefa5586cb4f5a1c1ff8745",
          "img_logo_url": "6d6917c99e988dfcc2657aaf4c54eabd0a6febfb",
          "has_community_visible_stats": true
        },
        {
          "appid": 460790,
          "name": "Bayonetta",
          "playtime_forever": 0,
          "img_icon_url": "9c591c4d3d3d5e5164ba08465aaf4407d86301b9",
          "img_logo_url": "5d49ade3a66eec6432f5a00ed29da02860f515b8",
          "has_community_visible_stats": true
        },
        {
          "appid": 461640,
          "name": "Sins Of The Demon RPG",
          "playtime_forever": 0,
          "img_icon_url": "8f4c063ec245b0fc93dfd182abbfa1da591ee350",
          "img_logo_url": "bf77caa67a2431ee2084231255f6a09424a5d231",
          "has_community_visible_stats": true
        },
        {
          "appid": 463270,
          "name": "Ghost 1.0",
          "playtime_forever": 1150,
          "img_icon_url": "99866a4004f1de3697dcd2e584dcbd2d13a1c9c4",
          "img_logo_url": "e49fc7774b40f2f72a9989226a720d5bbf77898b",
          "has_community_visible_stats": true
        },
        {
          "appid": 464060,
          "name": "RUINER",
          "playtime_forever": 0,
          "img_icon_url": "9b4e418e4261f7c6e3e594a52de8c6ac9cea636d",
          "img_logo_url": "b3a9d499b4e2315420190ed02721fa486f9d684f",
          "has_community_visible_stats": true
        },
        {
          "appid": 465840,
          "name": "THE LAST BLADE",
          "playtime_forever": 0,
          "img_icon_url": "0b9e8e145a5df63f74814e1fc20791e1d7bcf731",
          "img_logo_url": "e1c6c40eb03bf30b7f2cb378cd94a4335b039c3a",
          "has_community_visible_stats": true
        },
        {
          "appid": 465870,
          "name": "SHOCK TROOPERS 2nd Squad",
          "playtime_forever": 0,
          "img_icon_url": "96508b80304652b2c0f570f5727c185d20a29a6c",
          "img_logo_url": "20979df44dde9e811dcbec8a134fdac4ff2543ec",
          "has_community_visible_stats": true
        },
        {
          "appid": 379720,
          "name": "DOOM",
          "playtime_forever": 0,
          "img_icon_url": "b6e72ff47d1990cb644700751eeeff14e0aba6dc",
          "img_logo_url": "2e6aa9a14a0da798fe9f1c0a4d0bacecf241c0a2",
          "has_community_visible_stats": true
        },
        {
          "appid": 474960,
          "name": "Quantum Break",
          "playtime_forever": 0,
          "img_icon_url": "68c50addc599af219e4e135a81733a867c2986c2",
          "img_logo_url": "62135d6741356377895cde13c2b2d9049071beb6",
          "has_community_visible_stats": true
        },
        {
          "appid": 475550,
          "name": "Beholder",
          "playtime_forever": 0,
          "img_icon_url": "1025e7d1606b1e7a7f17048532039d78241e9f5d",
          "img_logo_url": "d0cb7d93e0bcb745182c5f5b91c5967eacae4d52",
          "has_community_visible_stats": true
        },
        {
          "appid": 223100,
          "name": "Homefront: The Revolution",
          "playtime_forever": 757,
          "img_icon_url": "95ace6b22d9d5b27362a5026dc894abcbcfab43f",
          "img_logo_url": "7eedc51aa6ea64b19f5e4175df285c4e073ef1a2",
          "has_community_visible_stats": true
        },
        {
          "appid": 390340,
          "name": "Umbrella Corps™ / Biohazard Umbrella Corps™",
          "playtime_forever": 0,
          "img_icon_url": "f3cae61c635739aa5e4ae3a721fdb66d2cf957d4",
          "img_logo_url": "d14f44c2f4300fb3e3d92e749fb01b0dadcc35c1",
          "has_community_visible_stats": true
        },
        {
          "appid": 441830,
          "name": "I am Setsuna",
          "playtime_forever": 0,
          "img_icon_url": "4930ac751f3e7dcd4747439cca34f8c9316473c1",
          "img_logo_url": "2c7df9c701b0fe9c0c8515126f144e386c89ad88",
          "has_community_visible_stats": true
        },
        {
          "appid": 487430,
          "name": "KARAKARA",
          "playtime_forever": 0,
          "img_icon_url": "c42c012b79058d38a7f60b2bf644671b379e76e7",
          "img_logo_url": "a317b54e55e2ba7a7f910a864cfeea395c845b4d",
          "has_community_visible_stats": true
        },
        {
          "appid": 34190,
          "name": "Sonic and SEGA All Stars Racing",
          "playtime_forever": 0,
          "img_icon_url": "4d1e6978b6ca1add7e4ab63e966129bef19598f0",
          "img_logo_url": "9c8d20af4ff2655632b371d0cb39b47f14823a28"
        },
        {
          "appid": 202530,
          "name": "SONIC THE HEDGEHOG 4 Episode I",
          "playtime_forever": 0,
          "img_icon_url": "c41474017b2da75cd9cf422d045a91af55ed92a6",
          "img_logo_url": "21b47f8223df647006671630e6f0348642681bab",
          "has_community_visible_stats": true
        },
        {
          "appid": 213610,
          "name": "Sonic Adventure™ 2 ",
          "playtime_forever": 0,
          "img_icon_url": "0ff2b133493b0bf7f1c16a38a83e7053f0b90f2d",
          "img_logo_url": "b59ee5bd744212a79db6fd8f71aec6729671da2b",
          "has_community_visible_stats": true
        },
        {
          "appid": 71250,
          "name": "Sonic Adventure DX",
          "playtime_forever": 0,
          "img_icon_url": "6568d25b43e2d1d07fc16cbe3ac9278ca51c2fb3",
          "img_logo_url": "e8374c63e76724af4648cdea5331f4ae39af4d06",
          "has_community_visible_stats": true
        },
        {
          "appid": 200940,
          "name": "Sonic CD",
          "playtime_forever": 0,
          "img_icon_url": "c13c103a631685935bafb047ff5c3a4f9d44d697",
          "img_logo_url": "07f30507b25a239911bd4cf8dd8667dd294fd5b9",
          "has_community_visible_stats": true
        },
        {
          "appid": 71340,
          "name": "Sonic Generations",
          "playtime_forever": 0,
          "img_icon_url": "efda039147f0968bc726c547ff3809f98b69964a",
          "img_logo_url": "21ec1e24c31a50500bbddb8c8c8add451e6dcbe1",
          "has_community_visible_stats": true
        },
        {
          "appid": 203650,
          "name": "SONIC THE HEDGEHOG 4 Episode II",
          "playtime_forever": 0,
          "img_icon_url": "d7a8313da6d6f5d5ce1f8a7efe4d0cbcf22c557c",
          "img_logo_url": "f1a12805277866d2664c99c80a09b04d7767af23",
          "has_community_visible_stats": true
        },
        {
          "appid": 212480,
          "name": "Sonic & All-Stars Racing Transformed Collection",
          "playtime_forever": 0,
          "img_icon_url": "95767af7b08d7ecebf1e9cb1ed1c92c98e4c084f",
          "img_logo_url": "351603b89e1863831c84aacab7bf3a315f03443b",
          "has_community_visible_stats": true
        },
        {
          "appid": 229660,
          "name": "Sonic and All-Stars Racing Transformed Metal Sonic DLC Pack",
          "playtime_forever": 0,
          "img_icon_url": "",
          "img_logo_url": "",
          "has_community_visible_stats": true
        },
        {
          "appid": 329440,
          "name": "Sonic Lost World",
          "playtime_forever": 0,
          "img_icon_url": "db03f24839f0c846601799dc78c9994fff4dc3c7",
          "img_logo_url": "0da4d847bf4852478329054dd08f2fa3319ec6a8",
          "has_community_visible_stats": true
        },
        {
          "appid": 314710,
          "name": "Mighty No. 9",
          "playtime_forever": 0,
          "img_icon_url": "33ee646666b111edf9501b8ff6acf4f33808ed90",
          "img_logo_url": "20e9f6588bba840d88c7262c8ba1f0f98aae81e3",
          "has_community_visible_stats": true
        },
        {
          "appid": 495050,
          "name": "Mega Man Legacy Collection 2",
          "playtime_forever": 1,
          "img_icon_url": "f0161c040ed0c9b65d164dfb5ed1d7587555c003",
          "img_logo_url": "1819e45d290481ff7ad79e27b86c633b9ea4c096",
          "has_community_visible_stats": true
        },
        {
          "appid": 495140,
          "name": "NARUTO: Ultimate Ninja STORM",
          "playtime_forever": 0,
          "img_icon_url": "6dd83d0ceedcac7ee60c08a478db6fcd54d820d1",
          "img_logo_url": "a2f13b74b4fdbc0cf4458afaa8fccbdf45b6ab60",
          "has_community_visible_stats": true
        },
        {
          "appid": 495280,
          "name": "Disgaea 2 PC",
          "playtime_forever": 0,
          "img_icon_url": "f0109c12f41344939c15d62e5509b948eec2d8ce",
          "img_logo_url": "8598ddb4d2c0944fc7308b91792897a53b9a6b41",
          "has_community_visible_stats": true
        },
        {
          "appid": 496810,
          "name": "MegaTagmension Blanc + Neptune VS Zombies",
          "playtime_forever": 0,
          "img_icon_url": "59daa5df1084feac1b4e84d66c7ab1dac75c4a46",
          "img_logo_url": "bfcaa082a1d124470860d5387fbb09978b817bce",
          "has_community_visible_stats": true
        },
        {
          "appid": 498240,
          "name": "Batman - The Telltale Series",
          "playtime_forever": 0,
          "img_icon_url": "f7d48a2727c415d45412c50dcb228252f57f689b",
          "img_logo_url": "f65743589420350c12dfa8ba7029741e7312e2bc",
          "has_community_visible_stats": true
        },
        {
          "appid": 273350,
          "name": "Evolve Stage 2",
          "playtime_forever": 0,
          "img_icon_url": "6b9e205c389185a4b0e079a13090c751e47375d2",
          "img_logo_url": "df025d71b278e46eb9322230bbd19c0cb8004b0d"
        },
        {
          "appid": 505730,
          "name": "Holy Potatoes! We’re in Space?!",
          "playtime_forever": 0,
          "img_icon_url": "4408f40123e05cddd395c8a5465b774d81631c5a",
          "img_logo_url": "1dce7032708f723215b0d081b0ded62ca05bf7b1",
          "has_community_visible_stats": true
        },
        {
          "appid": 369070,
          "name": "Slain: Back from Hell",
          "playtime_forever": 0,
          "img_icon_url": "8f827b8168ef8edbe198b97bd8c54f68c32b2425",
          "img_logo_url": "1bdb486950b8b3b64a839d9a489d5ca28345ad31",
          "has_community_visible_stats": true
        },
        {
          "appid": 506610,
          "name": "Five Nights at Freddy's: Sister Location",
          "playtime_forever": 0,
          "img_icon_url": "3e4a286cebe2ce195718ad1a18903e7667251d65",
          "img_logo_url": "5833284352a1660c7d0e6cccb8382e3ca7d24f0f"
        },
        {
          "appid": 360430,
          "name": "Mafia III",
          "playtime_forever": 3497,
          "img_icon_url": "3df3ee0dfcb254bedbf15822c93a6983f67b0822",
          "img_logo_url": "d54aa6212dc7b1d3ac4e35f9697d03ce09fca9ec",
          "has_community_visible_stats": true
        },
        {
          "appid": 337000,
          "name": "Deus Ex: Mankind Divided™",
          "playtime_forever": 0,
          "img_icon_url": "90a596fcc9934c7fe20d1202bca2c6fc80375d54",
          "img_logo_url": "092d516fd9e9abd05ed0182a9d57695d9b9b1285",
          "has_community_visible_stats": true
        },
        {
          "appid": 523900,
          "name": "Fated Souls 2",
          "playtime_forever": 0,
          "img_icon_url": "4d428672b424206b1703fdfe071df5ed9cd197de",
          "img_logo_url": "b97585854d1534e73d442cc0311e4ae4868d9022",
          "has_community_visible_stats": true
        },
        {
          "appid": 292030,
          "name": "The Witcher 3: Wild Hunt",
          "playtime_forever": 0,
          "img_icon_url": "87118494c65a92e1ac4c9734ce91950c1d6fe9a5",
          "img_logo_url": "2f22c2e5528b78662988dfcb0fc9aad372f01686",
          "has_community_visible_stats": true
        },
        {
          "appid": 524580,
          "name": "Fairy Fencer F Advent Dark Force",
          "playtime_forever": 0,
          "img_icon_url": "cc41a898533cb54d384cc4a6cf59db488fa30afd",
          "img_logo_url": "ac18b8a075919e89105ad7dfd58e76bdecc2bcd3",
          "has_community_visible_stats": true
        },
        {
          "appid": 525240,
          "name": "LOST SPHEAR",
          "playtime_forever": 12,
          "img_icon_url": "b076e5001b0a7b7ec2d32b6e3f56cad4b6fd9fc1",
          "img_logo_url": "76104ebbf8eedda457a583e4adbd3d8a9853c90a",
          "has_community_visible_stats": true
        },
        {
          "appid": 525480,
          "name": ".hack//G.U. Last Recode",
          "playtime_forever": 0,
          "img_icon_url": "dd92e278dbabb5765a0b220d97228dc66d993f06",
          "img_logo_url": "68fd602aaecb0fe79de317d301e39beb52e59bcc",
          "has_community_visible_stats": true
        },
        {
          "appid": 530620,
          "name": "Resident Evil 7 / Biohazard 7 Teaser: Beginning Hour",
          "playtime_forever": 33,
          "img_icon_url": "337f8f94b02f17d2b123eec0ab289ec6e93b902d",
          "img_logo_url": "a1ced06f7d0f9c270c6ad60ce8b33e64ee128d7e"
        },
        {
          "appid": 534290,
          "name": "Cursed Castilla (Maldita Castilla EX)",
          "playtime_forever": 0,
          "img_icon_url": "3c79d9be5e93e565856b6b4de396e91575946f39",
          "img_logo_url": "60b37cf60a38b4b2b5f2922d62e342f883f2d738",
          "has_community_visible_stats": true
        },
        {
          "appid": 536220,
          "name": "The Walking Dead: A New Frontier",
          "playtime_forever": 0,
          "img_icon_url": "9d1a710175bdc5ecf10885100728e44cd4ad0942",
          "img_logo_url": "19c4335343e096df07b8604b260801ff9485f3f6",
          "has_community_visible_stats": true
        },
        {
          "appid": 536420,
          "name": "Shining Plume",
          "playtime_forever": 0,
          "img_icon_url": "65c64d9918e33f633c42514dea52662bfa31eb72",
          "img_logo_url": "8c09224b9af3baa04c81d58b8b4ca5e0592271db"
        },
        {
          "appid": 536930,
          "name": "MOBIUS FINAL FANTASY",
          "playtime_forever": 0,
          "img_icon_url": "ce6d53315d19e982fc1cfe572268118f4267ccaa",
          "img_logo_url": "c28d8b126795aea11457c9576db44ec54c33504f",
          "has_community_visible_stats": true
        },
        {
          "appid": 115800,
          "name": "Owlboy",
          "playtime_forever": 803,
          "img_icon_url": "f8e4091c990afda896ed3220a733cb9b1a7e4fba",
          "img_logo_url": "3e5eaaa3a11ab21fffc4427a3ac96071f5ca3fb5",
          "has_community_visible_stats": true
        },
        {
          "appid": 540840,
          "name": "Lara Croft GO",
          "playtime_forever": 0,
          "img_icon_url": "41f8d46129b1c25de05b9967c13f267d7ffedf65",
          "img_logo_url": "6a1111ea699ff95fb0c508df818214e2e8fb5be6",
          "has_community_visible_stats": true
        },
        {
          "appid": 543260,
          "name": "Wonder Boy: The Dragon's Trap",
          "playtime_forever": 17,
          "img_icon_url": "3d827477e304911205ec8c5dfb6837497c7f9736",
          "img_logo_url": "5194381acfc395e0e5db9780a7b9d078b6f37213",
          "has_community_visible_stats": true
        },
        {
          "appid": 543870,
          "name": "NARUTO SHIPPUDEN: Ultimate Ninja STORM 2",
          "playtime_forever": 0,
          "img_icon_url": "d53e85e2fd3a6ffab715b0ddc1f6f75dc3ec2fef",
          "img_logo_url": "268bbb969b901e69146975c76637378aa6ed2628",
          "has_community_visible_stats": true
        },
        {
          "appid": 545270,
          "name": "NBA Playgrounds",
          "playtime_forever": 0,
          "img_icon_url": "90b4faaca4ad5ff992a28d58c15b0b6e2334757f",
          "img_logo_url": "215447b3ca9c1e2e6f73060f945ba7b522f82676",
          "has_community_visible_stats": true
        },
        {
          "appid": 549260,
          "name": "Alwa's Awakening",
          "playtime_forever": 0,
          "img_icon_url": "4b9f7740edbf0199334175a0b9c75e3603f1cca2",
          "img_logo_url": "2d310a96d4c1b4f085e1cb84ef4bc94aea635c9c",
          "has_community_visible_stats": true
        },
        {
          "appid": 489830,
          "name": "The Elder Scrolls V: Skyrim Special Edition",
          "playtime_forever": 0,
          "img_icon_url": "0dfe3eed5658f9fbd8b62f8021038c0a4190f21d",
          "img_logo_url": "356b389ad5e6937f097b21b23acbb89608878ecf",
          "has_community_visible_stats": true
        },
        {
          "appid": 552580,
          "name": "Jade's Journey",
          "playtime_forever": 0,
          "img_icon_url": "408991f0a78bec789aff6136654386c9645cbe82",
          "img_logo_url": "d499329545da1a14ad45baf41bd3314e79f24818",
          "has_community_visible_stats": true
        },
        {
          "appid": 451780,
          "name": "Trillion",
          "playtime_forever": 0,
          "img_icon_url": "18d11cde7caba9248c66b191183f00127a9df185",
          "img_logo_url": "82365655d5dc9a2768f51bf2af1b1cc9c4c78bb8",
          "has_community_visible_stats": true
        },
        {
          "appid": 555610,
          "name": "Gunmetal Arcadia Zero",
          "playtime_forever": 0,
          "img_icon_url": "d68a5804e9f59b128bcc8139c89c70dc484b986d",
          "img_logo_url": "5dc5dd276ccb1f372bc8330f4d3ec5e7f34c598f",
          "has_community_visible_stats": true
        },
        {
          "appid": 559210,
          "name": "Rakuen",
          "playtime_forever": 0,
          "img_icon_url": "1e6a438f1d108b5aebb553f933ce5c6c64760758",
          "img_logo_url": "460ecaffc1ae3fd8c6df072a3983132463d6e39d",
          "has_community_visible_stats": true
        },
        {
          "appid": 447040,
          "name": "Watch_Dogs 2",
          "playtime_forever": 0,
          "img_icon_url": "49dc5c3e4053e40159ffa73d2121daadbc467fe2",
          "img_logo_url": "7b99c7302fdf12b5eb3ccc2a0592368986a89686"
        },
        {
          "appid": 567090,
          "name": "8-Bit Bayonetta",
          "playtime_forever": 4,
          "img_icon_url": "16542a77033538639297e00e11fa40bdbf07296e",
          "img_logo_url": "527cc76d57b7cae5b66329e85db9d372b6d73849",
          "has_community_visible_stats": true
        },
        {
          "appid": 570460,
          "name": "Laser League",
          "playtime_forever": 0,
          "img_icon_url": "f92e08552df6dafe20b0172a8cd7f58d4433c349",
          "img_logo_url": "62663e71a1f889c124854b0f3be2def70002a649"
        },
        {
          "appid": 575510,
          "name": "Sakura Agent",
          "playtime_forever": 0,
          "img_icon_url": "d5d234c5eff71c589b1f1751d4c349554f2192cf",
          "img_logo_url": "69840c24d6be25d8f608b0b9f79b7e4d2d4365e4",
          "has_community_visible_stats": true
        },
        {
          "appid": 579950,
          "name": "Marvel's Guardians of the Galaxy: The Telltale Series",
          "playtime_forever": 0,
          "img_icon_url": "da3ab60bd3c16cca11b04ed3d9067fce11cf4b2d",
          "img_logo_url": "318a001cad6624a0c9cf46c4738394c2adb08d57",
          "has_community_visible_stats": true
        },
        {
          "appid": 586880,
          "name": "Mini Ghost",
          "playtime_forever": 200,
          "img_icon_url": "99866a4004f1de3697dcd2e584dcbd2d13a1c9c4",
          "img_logo_url": "5e0605aa3ba7e9cd269962950aaae193fb418ef9",
          "has_community_visible_stats": true
        },
        {
          "appid": 587100,
          "name": "Ys SEVEN",
          "playtime_forever": 0,
          "img_icon_url": "398ea481ed1ed4fb945c40e909904dc7e3ebbc11",
          "img_logo_url": "8aa48c9d57b04177cd778b07317177d3d04ec553",
          "has_community_visible_stats": true
        },
        {
          "appid": 587620,
          "name": "OKAMI HD / 大神 絶景版",
          "playtime_forever": 11,
          "img_icon_url": "fb528ddda674892e2430fbf4a3b2e79cd16e35c9",
          "img_logo_url": "abae2d0dea0f77eed08d624743cd669c36efe67e",
          "has_community_visible_stats": true
        },
        {
          "appid": 588650,
          "name": "Dead Cells",
          "playtime_forever": 0,
          "img_icon_url": "a431361e5dae5fd6ac2433064b62b2a37abc38f1",
          "img_logo_url": "939ddb43222217ab820dcea4437526c0487d557c",
          "has_community_visible_stats": true
        },
        {
          "appid": 543460,
          "name": "Dead Rising 4",
          "playtime_forever": 0,
          "img_icon_url": "31b6fac04029642cb5af6cf2ca5acafe88619b4b",
          "img_logo_url": "2947497af1de6ca63cc565c9a271258264dd3ca9",
          "has_community_visible_stats": true
        },
        {
          "appid": 595520,
          "name": "FINAL FANTASY XII THE ZODIAC AGE",
          "playtime_forever": 12,
          "img_icon_url": "d66a567cbe07ef274f9aad2990bfeee431b686ec",
          "img_logo_url": "4eae053b2553a996fe3f594d7944336a5a3df007",
          "has_community_visible_stats": true
        },
        {
          "appid": 524220,
          "name": "NieR:Automata™",
          "playtime_forever": 0,
          "img_icon_url": "ec431ecb2a5178c5a01bb15550f112f93af029bb",
          "img_logo_url": "907a5a93209a68b9605513c9e5658c8cc8924bd3",
          "has_community_visible_stats": true
        },
        {
          "appid": 597810,
          "name": "Shining Plume 2",
          "playtime_forever": 0,
          "img_icon_url": "538d6773211975de8bb7b1506749484500bdd360",
          "img_logo_url": "960cdb97e58dd2a7d3f3ea1af986bd2f9c0f8f01"
        },
        {
          "appid": 601430,
          "name": "The Evil Within 2",
          "playtime_forever": 2693,
          "img_icon_url": "823f7bef429bc85a7a68dba32d6f47938fc3b689",
          "img_logo_url": "9da3dc84af7d22143e0fab88cd66d9cbdc8a358a",
          "has_community_visible_stats": true
        },
        {
          "appid": 602790,
          "name": "Fated Souls 3",
          "playtime_forever": 0,
          "img_icon_url": "6c2628e7398f15d8813c9f0b5bceddc4e9ba4682",
          "img_logo_url": "0d82bb9e8488d0c2915a015759373b9c37866900",
          "has_community_visible_stats": true
        },
        {
          "appid": 607030,
          "name": "Jade's Journey 2",
          "playtime_forever": 0,
          "img_icon_url": "5e86879e74eea4d3412ac2e9b4d8803e6fa40459",
          "img_logo_url": "0cb700ff1ee179c6939f0de90ace04f2f48bad7f",
          "has_community_visible_stats": true
        },
        {
          "appid": 383980,
          "name": "Rivals of Aether",
          "playtime_forever": 0,
          "img_icon_url": "1eaacd26ab3f35ba0432b63f6ddac66cf3a79afa",
          "img_logo_url": "fb01fec9b138354400927be217728b71e89704d0",
          "has_community_visible_stats": true
        },
        {
          "appid": 219640,
          "name": "Chivalry: Medieval Warfare",
          "playtime_forever": 0,
          "img_icon_url": "d4628be29b7e97d93a3404870dfe79642b90b907",
          "img_logo_url": "dd3488ae69593cedf5e73b818ae98e6737aa956c",
          "has_community_visible_stats": true
        },
        {
          "appid": 615680,
          "name": "Vagrant Hearts Zero",
          "playtime_forever": 0,
          "img_icon_url": "e5b7587a1e44bd218f09056f8a89ee5e2bf222d3",
          "img_logo_url": "282861e821c6babbbece218d9bf386060402ed3f"
        },
        {
          "appid": 617440,
          "name": "Juanito Arcade Mayhem",
          "playtime_forever": 26,
          "img_icon_url": "d5deea8f33f69263b75bfb66ea42ca9078294207",
          "img_logo_url": "13edf38f17126c5edc9d2a05cc37270753fe5fb2",
          "has_community_visible_stats": true
        },
        {
          "appid": 626600,
          "name": "Lost Dimension",
          "playtime_forever": 0,
          "img_icon_url": "4f6ea7cc64a71def9048c10ba2fd9c73d8c10804",
          "img_logo_url": "ba6317eb6d4699d6abc505824be3af5538b142ba",
          "has_community_visible_stats": true
        },
        {
          "appid": 460810,
          "name": "Vanquish",
          "playtime_forever": 0,
          "img_icon_url": "a5024e8135e1865f7df484f512ddf4e117fc0140",
          "img_logo_url": "0e4d23bc68cf9265945cc85df3114bbf4618e8fa",
          "has_community_visible_stats": true
        },
        {
          "appid": 265930,
          "name": "Goat Simulator",
          "playtime_forever": 0,
          "img_icon_url": "edf6c28b83d5fc221338ff71d01caa11d11d3b50",
          "img_logo_url": "f116260f6858dda8a2e4c0ccedf270c8a24a0add",
          "has_community_visible_stats": true
        },
        {
          "appid": 637670,
          "name": "Secret of Mana",
          "playtime_forever": 0,
          "img_icon_url": "4c46e1e68c28baed34216ccada78d1a8bc38560f",
          "img_logo_url": "da94a7952e350adec40a0ca6d71e7fa5cb422634",
          "has_community_visible_stats": true
        },
        {
          "appid": 218620,
          "name": "PAYDAY 2",
          "playtime_forever": 0,
          "img_icon_url": "a6abc0d0c1e79c0b5b0f5c8ab81ce9076a542414",
          "img_logo_url": "4467a70648f49a6b309b41b81b4531f9a20ed99d",
          "has_community_visible_stats": true
        },
        {
          "appid": 371200,
          "name": "Halcyon 6: Starbase Commander (CLASSIC)",
          "playtime_forever": 0,
          "img_icon_url": "b01c5de9751c59359da58279f2b3013dd6ce6a05",
          "img_logo_url": "69a1bea69ed4d5e083a4d31716c5484a23f268e9",
          "has_community_visible_stats": true
        },
        {
          "appid": 651660,
          "name": "Halcyon 6: Lightspeed Edition",
          "playtime_forever": 0,
          "img_icon_url": "30aa60bafd91424c652c7952da0420965700ca38",
          "img_logo_url": "4c9d5c042908be67cb6dafa966508fc55a88bc74",
          "has_community_visible_stats": true
        },
        {
          "appid": 654050,
          "name": "JYDGE",
          "playtime_forever": 0,
          "img_icon_url": "06b4dad2373e7911c2f5059776fd8a52a0d43886",
          "img_logo_url": "c12dea1351b6a0315952d927fdf90d826552a3a9",
          "has_community_visible_stats": true
        },
        {
          "appid": 683280,
          "name": "Omega Quintet",
          "playtime_forever": 0,
          "img_icon_url": "9ead01904d7428c9d15305f554ba8b8b03ce9485",
          "img_logo_url": "723505c24d896e02c1de238c090588d29c15e289",
          "has_community_visible_stats": true
        },
        {
          "appid": 538680,
          "name": "The Legend of Heroes: Trails of Cold Steel",
          "playtime_forever": 0,
          "img_icon_url": "f072e87a22665d24cd7e4557e3f308ade2879be1",
          "img_logo_url": "a84d1a221aa6dd8e87dc57729787bf19d111077a",
          "has_community_visible_stats": true
        },
        {
          "appid": 377160,
          "name": "Fallout 4",
          "playtime_forever": 0,
          "img_icon_url": "779c4356ebe32af2af7c9f0bbba595dfe872cd7f",
          "img_logo_url": "8977a8e98acbbdd3c0ff905afb7e0a6e2eb555ea",
          "has_community_visible_stats": true
        },
        {
          "appid": 22350,
          "name": "BRINK",
          "playtime_forever": 0,
          "img_icon_url": "a8287f6a2627c344116d8f81fc518e71aab2a647",
          "img_logo_url": "bf7bd2ebb60328ce51b356dcd5d53d86dd7bbba5",
          "has_community_visible_stats": true
        },
        {
          "appid": 554620,
          "name": "Life is Strange: Before the Storm",
          "playtime_forever": 0,
          "img_icon_url": "3bf0d4bc9563fd21d273123646e74669abb83947",
          "img_logo_url": "adf6ec95859a24be9e9806706485e3b003b18775",
          "has_community_visible_stats": true
        },
        {
          "appid": 635320,
          "name": "Last Day of June",
          "playtime_forever": 0,
          "img_icon_url": "6d11292ea70eaaae88c771f41dd4e8142d906a8c",
          "img_logo_url": "46fb77e0003dd9113f40ed30f759bdc5efeaa380",
          "has_community_visible_stats": true
        },
        {
          "appid": 609150,
          "name": "STAR OCEAN™ - THE LAST HOPE™ - 4K & Full HD Remaster",
          "playtime_forever": 0,
          "img_icon_url": "d7355a3772b6295c73425b67fbd1173680ba345c",
          "img_logo_url": "193b6c3aa3b593ca96221ade5c67091e819ffb79",
          "has_community_visible_stats": true
        },
        {
          "appid": 552700,
          "name": "WORLD OF FINAL FANTASY",
          "playtime_forever": 0,
          "img_icon_url": "d5bc9620ca394b922ffe9f4914468672971a3e87",
          "img_logo_url": "b2ef8ea0cfa12cc6e5533c713cc2b59a14b0c3ff",
          "has_community_visible_stats": true
        },
        {
          "appid": 40990,
          "name": "Mafia",
          "playtime_forever": 0,
          "img_icon_url": "3c86a0fd4f94a97b6f4ce31b5c31532e2098fe7b",
          "img_logo_url": "fc9e420d9c8120ea585d04735ead47757b5ddd76"
        },
        {
          "appid": 748490,
          "name": "The Legend of Heroes: Trails of Cold Steel II",
          "playtime_forever": 0,
          "img_icon_url": "c052953471afd6a5cce9051642c4afed5d666f5b",
          "img_logo_url": "526d7992b126c03df1a62c909a44d41c50e00f04",
          "has_community_visible_stats": true
        },
        {
          "appid": 589360,
          "name": "Ni no Kuni™ II: Revenant Kingdom",
          "playtime_forever": 17,
          "img_icon_url": "d8469c3ccf71774d558cd51a1b40ba5f521f74d9",
          "img_logo_url": "6b754bf3207c99d09d8f91efe5d73e4ede14671e",
          "has_community_visible_stats": true
        },
        {
          "appid": 637650,
          "name": "FINAL FANTASY XV WINDOWS EDITION",
          "playtime_2weeks": 10,
          "playtime_forever": 5339,
          "img_icon_url": "0a99fcc7b08c7240d9146390cf1be28451aeef73",
          "img_logo_url": "fb3a73c1b6a8305b5dc19110d14420d835956bc0",
          "has_community_visible_stats": true
        },
        {
          "appid": 265590,
          "name": "The Red Solstice",
          "playtime_forever": 0,
          "img_icon_url": "cc33fb26c140d04a20fb0fd5f549454458122322",
          "img_logo_url": "4ee27b5b596b0be43ef61c04017bd372081d7033",
          "has_community_visible_stats": true
        },
        {
          "appid": 203770,
          "name": "Crusader Kings II",
          "playtime_forever": 0,
          "img_icon_url": "56e9c15cbeb6c1f873f7f1dc757bae7618861484",
          "img_logo_url": "a3170e4c1889a7e2a360feef61e07a5aee51dc88",
          "has_community_visible_stats": true
        },
        {
          "appid": 439190,
          "name": "Stories: The Path of Destinies",
          "playtime_forever": 0,
          "img_icon_url": "ed3383f9deed7e41515d34e544a67258fcf91c23",
          "img_logo_url": "3b40ad5b4d5b3797bb3e4b7c5cf12f5b6de55cfc",
          "has_community_visible_stats": true
        }
        ]
      }
    }
    return todo.response;
  }
}