import { UserProvider } from './../../providers/user/user';
import { GamesProvider } from './../../providers/games/games';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ISteam } from '../../interfaces/i-steam';
import { SteamProfile } from './miperfil';
//610DF6B101CA1B4AF3741D741702A841
//const steamAPI = require('610DF6B101CA1B4AF3741D741702A841');
/**
 * Generated class for the SteamPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-steam',
  templateUrl: 'steam.html',
})
export class SteamPage {
  //mi clave de usuario
  id: string = '76561198222302320';
  
  steams: {};
  allSteam: any;
  steamGames;
  steamAmount;
  steamHours;
  steamArray = [];
  n = 0;
  search = '';

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private gamesProvider: GamesProvider,
    private userProvider: UserProvider
  ) {
  }

  ionViewDidLoad() {
    this.steamAmount = SteamProfile.getPerfil().game_count;
    this.steamGames = SteamProfile.getPerfil().games.map( item => {
      let horas = item.playtime_forever / 60;
      let round = Math.round(horas);
      item.playtime_forever = round;
      item.img_icon_url = `http://media.steampowered.com/steamcommunity/public/images/apps/${item.appid}/${item.img_icon_url}.jpg`;     
      return item;
    });

    this.userProvider.getMyProfile().subscribe(me => {
      this.gamesProvider.getSteamGames(me.id_steam).subscribe(st => {
        this.steams = st;    
      });
      this.steamArray = SteamProfile.getPerfil().games
    });  
  }

  allGames() {
    this.steamGames = SteamProfile.getPerfil().games.map(item => {
      let horas = item.playtime_forever / 60;
      let round = Math.round(horas);
      item.playtime_forever = round;
      item.img_icon_url = `http://media.steampowered.com/steamcommunity/public/images/apps/${item.appid}/${item.img_icon_url}.jpg`;
      return item;
    });
  }

  getItems(ev) {
    // Reset items back to all of the items
    this.allGames();

    // set val to the value of the ev target
    let val = ev.target.value;
    
    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.steamGames = SteamProfile.getPerfil().games.filter(item => {
        let horas = item.playtime_forever / 60;
        let round = Math.round(horas);
        item.playtime_forever = round;
        item.img_icon_url = `http://media.steampowered.com/steamcommunity/public/images/apps/${item.appid}/${item.img_icon_url}.jpg`;
        console.log('filter', item);
        return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }
}
