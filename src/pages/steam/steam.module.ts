import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SteamPage } from './steam';

@NgModule({
  declarations: [
    SteamPage
  ],
  imports: [
    IonicPageModule.forChild(SteamPage),
  ],
})
export class SteamPageModule {}
