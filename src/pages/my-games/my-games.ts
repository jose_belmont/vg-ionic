import { MygamesProvider } from './../../providers/mygames/mygames';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, ActionSheetController } from 'ionic-angular';
import { IConsole } from '../../interfaces/i-consoles';
import { IMyGame } from '../../interfaces/i-mygame';

/**
 * Generated class for the MyGamesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-my-games',
  templateUrl: 'my-games.html',
})
export class MyGamesPage {

  consola: IConsole = {
    id_consoles: -1,
    name_console: '',
    img_console: '',
    platform: 0,
    plataforma: ""
  }

  game: IMyGame = {
    id_game: -1,
    platform: "",
    name_console: "",
    genre: "",
    title: "",
    companyia: "Taito",
    year: 1990,
    description: "",
    my_cover: "",
    cover: "",
    region: "PAL-ES",
    local_ubication: "",
    gamerscore: 0,
    trophies: 0,
    box: "Sí",
    manual: "Sí",
    date_finish: null,
    annotations: ""
  }
  
  games: IMyGame [];

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private mygamesProvider: MygamesProvider,
    private platforma: Platform,
    private actionsheetCtrl: ActionSheetController
  ) {
    if (this.navParams.data.id_consoles)
      this.consola = this.navParams.data;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyGamesPage');
    this.getMyGames();
  }

  getMyGames() {
    this.mygamesProvider.getAllMyGamesByConsole(this.consola.id_consoles).subscribe(gam => {
      this.games = gam;
      console.log(this.games);
    });
  }

  goMyGamesDetail(gam) {
    this.navCtrl.push('MygameDetailPage', gam);
  }

  openASNewGame() {
    let actionSheet = this.actionsheetCtrl.create({
      title: 'Opciones',
      cssClass: 'page-my-games-detail',
      buttons: [
        {
          text: 'Codigo de Barras',
          icon: !this.platforma.is('ios') ? 'list-box' : null,
          handler: () => {
            this.navCtrl.push('BarcodePage')
          }
        },
        {
          text: 'Buscar en BD',
          icon: !this.platforma.is('ios') ? 'search' : null,
          handler: () => {
            this.navCtrl.setRoot('BdPlatformsPage');
          }
        },
        {
          text: 'Crear nuevo Juego',
          icon: !this.platforma.is('ios') ? 'add' : null,
          handler: () => {
            console.log('Nuevo juego clicked');
            this.navCtrl.push('AddGamePage');
          }
        },
        {
          text: 'Cancel',
          role: 'cancel', // will always sort to be on the bottom
          icon: !this.platforma.is('ios') ? 'close' : null,
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }
}
