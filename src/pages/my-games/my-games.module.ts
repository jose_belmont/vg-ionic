import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyGamesPage } from './my-games';

@NgModule({
  declarations: [
    MyGamesPage,
  ],
  imports: [
    IonicPageModule.forChild(MyGamesPage),
  ],
})
export class MyGamesPageModule {}
