import { UserProvider } from './../../providers/user/user';
import { MarketProvider } from './../../providers/market/market';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ISell } from '../../interfaces/i-sell';

/**
 * Generated class for the MarketPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-market',
  templateUrl: 'market.html',
})
export class MarketPage {

  market: ISell = {
    id_trans_sell: -1,
    id_game: -1,
    id_user: -1,
    date_sell: "",
    price: 1,
    description: "",
    selled: false,
    send: false,
    cover: "",
    title: "",
    consola: ""
  }
  
  markets: ISell [];
  mylat: number;
  mylng: number;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private marketProvider : MarketProvider,
    private userProvider: UserProvider
  ) {
    userProvider.getMyProfile().subscribe(
      us => {
        this.mylat = us.lat;
        this.mylng = us.lng;
      }
    );
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MarketPage');
    this.getSells();
  }
  
  getSells() {
    this.marketProvider.getAllProducts().subscribe(r => {
      this.markets = r;
      console.log(this.markets);
    });
  }

  goMarketDetails(mark) {
    this.navCtrl.push('MarketDetailsPage', mark);
  }

}
