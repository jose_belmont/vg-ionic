import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { IGame } from '../../interfaces/i-game';

/**
 * Generated class for the ModalRegionsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-modal-regions',
  templateUrl: 'modal-regions.html',
})
export class ModalRegionsPage {

  game: IGame = {
    id_game: -1,
    id_user: -1,
    id_console: -1,
    id_genre: -1,
    title: "",
    companyia: "",
    year: 1981,
    description: "",
    cover: "",
    region: ""
  }
  
  region = [
    "NTSC-JAP",
    "NTSC-USA",
    "PAL-ES",
    "PAL-FR",
    "PAL-UK",
    "PAL-IT",
    "PAL-NOE"
  ];

  constructor(
    public navCtrl: NavController,
    public viewCtrl: ViewController,
    public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalRegionsPage');
  }

  closeModal() {
    this.viewCtrl.dismiss(this.game.region);
  }

  goBack() {
    this.navCtrl.pop();
  }
}
