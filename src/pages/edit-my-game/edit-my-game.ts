import { MygamesProvider } from './../../providers/mygames/mygames';

import { UserProvider } from './../../providers/user/user';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { IMyGame } from '../../interfaces/i-mygame';
import { Camera, CameraOptions } from '@ionic-native/camera';

/**
 * Generated class for the EditMyGamePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-edit-my-game',
  templateUrl: 'edit-my-game.html',
})
export class EditMyGamePage {

  mygame: IMyGame = {
    id_user: -1,
    id_game: -1,
    local_ubication: "",
    date_finish: null,
    annotations: "",
    gamerscore: 0,
    trophies: 0,
    box: "Sí",
    manual: "Sí",
    my_cover: "",
    mybarcode: ""
  }
  
  actualImage: string;
  newImage: string = '';

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private userProvider: UserProvider,
    private alertCtrl: AlertController,
    private mygamesProvider: MygamesProvider,
    private camera: Camera,

  ) {
    userProvider.getMyProfile().subscribe(
      us => {
        this.mygame.id_user = us.id_user;
      }
    );
    if (this.navParams.data.id_game) {
      this.mygame = this.navParams.data;
    } 
    this.actualImage = this.mygame.my_cover;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EditMyGamePage');
  }

  updateGameInfo() {

    this.mygamesProvider.updateMyGameInfo(this.mygame).subscribe(
      (response) => {
        this.showUpdateGameOk(),
          this.navCtrl.setRoot('MyPlatformsPage')
      },
      (error) => this.showErrorAddGame(error));
  }

  updateGameInfoCover() {
    this.mygame.my_cover = this.newImage;
    console.log('edit mi juego', JSON.stringify(this.mygame));
    this.mygamesProvider.updateMyGameCover(this.mygame).subscribe(
      (response) => {
        this.showUpdateGameOk(),
          this.navCtrl.setRoot('MyPlatformsPage')
      },
      (error) => this.showErrorAddGame(error));
  }

  private showUpdateGameOk() {
    let alert = this.alertCtrl.create({
      title: 'Actualizacion OK!',
      subTitle: 'El juego ha sido actualizado con exito',
      buttons: ['OK']
    });
    alert.present();
  }

  private showErrorAddGame(error) {
    let alert = this.alertCtrl.create({
      title: 'Error al añadir el juego',
      subTitle: error,
      buttons: ['Ok']
    });
    alert.present();
  }

  takePhoto() {
    const options: CameraOptions = {
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
    }
    this.getPicture(options);
    this.mygame.my_cover = this.newImage;
  }

  pickFromGallery() {
    const options: CameraOptions = {
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      targetWidth: 640,
      targetHeight: 640,
      destinationType: this.camera.DestinationType.DATA_URL
    }
    this.getPicture(options);
    this.mygame.my_cover = this.newImage;
  }

  private getPicture(options: CameraOptions) {
    this.camera.getPicture(options).then((imageData) => {
      this.newImage = 'data:image/jpeg;base64,' + imageData;
      this.alertCtrl.create({
        title: 'Success',
        subTitle: 'Añadida foto con exito',
        buttons: ['Ok']
      });
    }).catch(error => {
      this.alertCtrl.create({
        title: 'Error',
        subTitle: error,
        buttons: ['Ok']
      });
    });
  }
}