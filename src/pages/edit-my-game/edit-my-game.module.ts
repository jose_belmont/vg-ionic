import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditMyGamePage } from './edit-my-game';

@NgModule({
  declarations: [
    EditMyGamePage,
  ],
  imports: [
    IonicPageModule.forChild(EditMyGamePage),
  ],
})
export class EditMyGamePageModule {}
