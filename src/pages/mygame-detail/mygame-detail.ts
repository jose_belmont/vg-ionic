import { CommentsProvider } from './../../providers/comments/comments';
import { UserProvider } from './../../providers/user/user';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, ActionSheetController, AlertController } from 'ionic-angular';
import { IMyGame } from '../../interfaces/i-mygame';
import { MygamesProvider } from '../../providers/mygames/mygames';
import { IComment } from '../../interfaces/i-comment';

/**
 * Generated class for the MygameDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-mygame-detail',
  templateUrl: 'mygame-detail.html',
})
export class MygameDetailPage {

  content: string;
  defaultDate= new Date('2000-01-01');
  lat: number;
  lng: number;
  game: IMyGame = {
    id_game: -1,
    platform: "",
    name_console: "",
    genre: "",
    title: "",
    companyia: "Taito",
    year: 1990,
    description: "",
    my_cover: "",
    cover: "",
    region: "PAL-ES",
    local_ubication: "",
    gamerscore: 0,
    date_finish: null,
    annotations: "",
    box: "No",
    trophies: 0,
    manual: "No",
    mybarcode: "",
    youtube: "",
    sellLat: 3,
    sellLng: 3,
  }

  comment: IComment = {
    id_comment: -1,
    id_game: this.game.id_game,
    id_user: 2,
    title: '',
    description: '',
    date: null,
    consola: '',
    juego: '',
    avatar: '',
    user: '',
  }

  comments: IComment [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private alertCtrl: AlertController,
    private mygamesProvider: MygamesProvider,
    private platform: Platform,
    private actionsheetCtrl: ActionSheetController,
    private userProvider: UserProvider,
    private commentsProvider: CommentsProvider
  ) {
    this.content = "game";
    if (this.navParams.data.id_game)
      this.game = this.navParams.data;
      userProvider.getMyProfile().subscribe(
        us => {
          this.comment.id_user = us.id_user;
          this.lat = us.lat;
          this.lng = us.lng;
          console.log(this.game);
        }
      );
  }

  ionViewDidLoad() {
    this.getMyGamesDetail();
    this.getComments();
    this.comment.id_game = this.game.id_game;
    console.log(this.comment);
  }

  getMyGamesDetail() {
    this.mygamesProvider.getMyGameById(this.game.id_game).subscribe(gam => {
      this.game = gam;
      this.game.sellLat = this.lat;
      this.game.sellLng = this.lng;
      console.log(this.game);
    });
  }

  getComments() {
    this.commentsProvider.getAllCommentsOneGame(this.game.id_game).subscribe(allComments => {
      this.comments = allComments;
      console.log('allmycomments: ', allComments);
    });
  }

  deleteGame(com) {
    let alert = this.alertCtrl.create({
      title: 'Borrar',
      message: 'Quieres borrar el juego de tu colección?',
      buttons: [{ text: 'Cancel', role: 'cancel' },
      {
        text: 'Borrar',
        handler: () => {
          console.log(com);
          this.mygamesProvider.deleteMyGame(com.id_game).subscribe(
            () => this.navCtrl.setRoot('WelcomePage'),
            (error) => {
              let alert = this.alertCtrl.create({
                title: 'Error al borrar!',
                subTitle: 'No se puede borrar, porque lo tienes a la venta!',
                buttons: ['Ok']
              });
              alert.present();
            }
          );
        }
      }]
    });
    alert.present();

  }

  openActionSheet(game) {
    let actionSheet = this.actionsheetCtrl.create({
      title: 'Opciones de mi juego',
      cssClass: 'page-mygame-detail',
      buttons: [
        {
          text: 'Borrar',
          role: 'destructive',
          icon: !this.platform.is('ios') ? 'trash' : null,
          handler: () => {
            console.log('Delete clicked');
            this.deleteGame(game);
          }
        },
        {
          text: 'Vender',
          icon: !this.platform.is('ios') ? 'basket' : null,
          handler: () => {
            console.log('Vender clicked');
            this.navCtrl.push('AddSellGamePage', game);
          }
        },
        {
          text: 'Editar mi información',
          icon: !this.platform.is('ios') ? 'create' : null,
          handler: () => {
            console.log('Editar clicked');
            this.navCtrl.push('EditMyGamePage', game);
          }
        },
        {
          text: 'Comentar',
          icon: !this.platform.is('ios') ? 'chatbubbles' : null,
          handler: () => {
            console.log('Comentar clicked');
            this.navCtrl.push('CreateCommPage', this.comment);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel', // will always sort to be on the bottom
          icon: !this.platform.is('ios') ? 'close' : null,
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }

}
