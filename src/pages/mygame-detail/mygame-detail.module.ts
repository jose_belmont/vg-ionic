import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MygameDetailPage } from './mygame-detail';

@NgModule({
  declarations: [
    MygameDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(MygameDetailPage),
  ],
})
export class MygameDetailPageModule {}
