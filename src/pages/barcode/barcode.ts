import { UserProvider } from './../../providers/user/user';
import { IMyGame } from './../../interfaces/i-mygame';
import { GamesProvider } from './../../providers/games/games';

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { IGame } from '../../interfaces/i-game';
import { MyPlatformsPage } from '../my-platforms/my-platforms';
import { MygamesProvider } from '../../providers/mygames/mygames';

/**
 * Generated class for the BarcodePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-barcode',
  templateUrl: 'barcode.html',
})
export class BarcodePage {

  code: any;
  results: any;

  game: IGame = {
    id_game: -1,
    id_user: -1,
    id_console: -1,
    id_genre: -1,
    title: "",
    name_console: "",
    genre: "",
    companyia: "",
    year: 1981,
    description: "",
    cover: "",
    region: "",
    barcode: ""
  };

  games: IGame [];

  mygame: IMyGame = {
    id_user: -1,
    id_game: -1,
    local_ubication: "",
    date_finish: null,
    annotations: "",
    gamerscore: 0,
    trophies: 0,
    box: "Sí",
    manual: "Sí",
    my_cover: "",
    mybarcode: ""
  }

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private barcodeScanner: BarcodeScanner,
    private myGamesProvider: MygamesProvider,
    private gamesProvider: GamesProvider,
    private alertCtrl: AlertController,
    private userProvider: UserProvider
  ) {
    userProvider.getMyProfile().subscribe(
      us => {
        this.mygame.id_user = us.id_user;
        this.mygame.id_game = this.game.id_game;
      }
    );
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BarcodePage');
    this.getAllGames();
    
    console.log(this.games);
  }

  finder() {
    this.games.filter( gam => {
      if(gam.barcode === this.code) {
        this.game = gam;
        this.mygame.id_game = this.game.id_game;
        this.mygame.my_cover = this.game.cover.slice(44);
        console.log('juego:', this.game);
      }
    });
  }

  getAllGames() {
    this.gamesProvider.getAllGames().subscribe( gam => {
      this.games = gam;
    });
  }

  scanBarcode() {
    this.barcodeScanner.scan().then(barcodeData => {
      this.results = barcodeData;
      this.code = this.results.text;
    }), (err) => {
      alert('Error : ${err}');
    }
  };

  addGame() {
    this.myGamesProvider.addGameInfo(this.mygame).subscribe( ok => {
      console.log(ok);
      if(ok)
      this.showAddGameOk();
      this.navCtrl.setRoot('MyPlatformsPage');
    }),
      (error) => this.showErrorAddGame(error);
  }

  showAddGameOk() {
    let alert = this.alertCtrl.create({
      title: 'Añadido!',
      subTitle: 'El juego ha sido añadido a tu coleccion',
      buttons: ['OK']
    });
    alert.present();
  }

  showErrorAddGame(error) {
    let alert = this.alertCtrl.create({
      title: 'Error al añadir el juego',
      subTitle: error,
      buttons: ['Ok']
    });
    alert.present();
  }

  reset() {
    this.results = null;
  }

  lookup() {
    window.open(`http://www.upcindex.com/${this.results.text}`, '_system');
  }
}

