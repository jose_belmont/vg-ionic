import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MarketDetailsPage } from './market-details';
import { AgmCoreModule } from '@agm/core';

@NgModule({
  declarations: [
    MarketDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(MarketDetailsPage),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBqOTpdm_dhZ59MZGvFHSN7e21XeYcS1sM'
    })
  ],
})
export class MarketDetailsPageModule {}
