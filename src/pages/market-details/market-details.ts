import { MygamesProvider } from './../../providers/mygames/mygames';
import { IBuy } from './../../interfaces/i-buy';
import { MarketProvider } from './../../providers/market/market';
import { UserProvider } from './../../providers/user/user';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { ISell } from '../../interfaces/i-sell';
import { PayPal, PayPalPayment, PayPalConfiguration } from '@ionic-native/paypal';
import { IMyGame } from '../../interfaces/i-mygame';

/**
 * Generated class for the MarketDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-market-details',
  templateUrl: 'market-details.html',
})
export class MarketDetailsPage {

  today: Date = new Date();
  leftDays: number;
  zoom = 17;
  payed: boolean;
  usuario: string;
  vendedor: string;
  myProduct: boolean;
  usuarioId: number;

  market: ISell = {
    id_trans_sell: -1,
    id_game: -1,
    id_user: -1,
    date_sell: null,
    price: 1,
    description: "",
    selled: false,
    send: false,
    cover: "",
    title: "",
    consola: "",
    sellLat: 2,
    sellLng: 2,
    user: "",
    avatar: "",
  }
  mygame: IMyGame = {
    id_user: -1,
    id_game: -1,
    local_ubication: "",
    date_finish: null,
    annotations: "",
    gamerscore: 0,
    trophies: 0,
    box: "Sí",
    manual: "Sí",
    my_cover: "",
    mybarcode: ""
  }
  
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private userProvider: UserProvider,
    private marketProvider: MarketProvider,
    private payPal: PayPal,
    private alertCtrl: AlertController,
    private mygamesProvider: MygamesProvider
  ) {
    if (this.navParams.data.id_trans_sell) {
      this.market = this.navParams.data;
    }
    userProvider.getMyProfile().subscribe(
      us => {
        this.usuarioId = us.id_user;
        this.usuario = us.user;

      }
    );
  }

  ionViewDidLoad() {
    this.getMarketDetail();
    console.log(this.market);
    console.log(this.myProduct);
    
  }

  getMarketDetail() {
    this.marketProvider.getOneProductById(this.market.id_trans_sell).subscribe(r => {
      this.market = r;
      this.market.cover = r.cover;
    });
  }

  addToMyCollection() {
    this.mygame.id_game = this.market.id_game;
    this.mygame.my_cover = this.market.cover.slice(44);
    this.mygamesProvider.addGameInfo(this.mygame).subscribe(ok => {
      if (ok) {
        this.showAddGameOk();
        this.navCtrl.setRoot('MyPlatformsPage');
      }
    });
  }

  private showAddGameOk() {
    let alert = this.alertCtrl.create({
      title: 'añadido!',
      subTitle: 'El juego ha sido añadido con exito',
      buttons: ['OK']
    });
    alert.present();
  }
  
  buyGame() {
    this.market.selled = true;
    this.marketProvider.changeToSellStatus(this.market).subscribe( ok => {
      this.addToMyCollection();
      this.showPaypalOk();
      this.navCtrl.setRoot('ProfilePage');
    }), (error) => console.log(error);
  }

  paypal() {
    this.payPal.init({
      PayPalEnvironmentProduction: '',
      PayPalEnvironmentSandbox: 'Acwd372rNIs9Hzo_sac6Y4sxKvtBG59DA-zmSbyvD9rRqsp8LePlTVKKY3AYp3mIUNjJjlhLgmSSzGq_'
    }).then(() => {
      // Environments: PayPalEnvironmentNoNetwork, PayPalEnvironmentSandbox, PayPalEnvironmentProduction
      this.payPal.prepareToRender('PayPalEnvironmentSandbox', new PayPalConfiguration({
        // Only needed if you get an "Internal Service Error" after PayPal login!
        //payPalShippingAddressOption: 2 // PayPalShippingAddressOptionPayPal
      })).then(() => {
        let payment = new PayPalPayment(this.market.price.toString(), 'EUR', this.market.title, 'Juego del mercadillo Vg-Collection');
        this.payPal.renderSinglePaymentUI(payment).then((result) => {
          // Successfully paid
          console.log(result);
          this.payed = true;
          this.buyGame();
          // Example sandbox response
          //
          // {
          //   "client": {
          //     "environment": "sandbox",
          //     "product_name": "PayPal iOS SDK",
          //     "paypal_sdk_version": "2.16.0",
          //     "platform": "iOS"
          //   },
          //   "response_type": "payment",
          //   "response": {
          //     "id": "PAY-1AB23456CD789012EF34GHIJ",
          //     "state": "approved",
          //     "create_time": "2016-10-03T13:33:33Z",
          //     "intent": "sale"
          //   }
          // }
        }, () => {
          // Error or render dialog closed without being successful
          console.log("Error or render dialog closed without being successful");
        });
      }, () => {
        // Error in configuration
        console.log("Error in configuration");
      });
    }, () => {
      // Error in initialization, maybe PayPal isn't supported or something else
      console.log("Error", " Error in initialization, maybe PayPal isn't supported or something else");
    });
  }

  private showPaypalOk() {
    let alert = this.alertCtrl.create({
      title: 'Pago Aceptado con exito!',
      subTitle: `${this.usuario} ha comprado ${this.market.title}`,
      buttons: ['OK']
    });
    alert.present();
  }
}
