import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { IGame } from '../../interfaces/i-game';
import { IGenre } from '../../interfaces/i-genre';

/**
 * Generated class for the ModalGenresPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-modal-genres',
  templateUrl: 'modal-genres.html',
})
export class ModalGenresPage {

  game: IGame = {
    id_game: -1,
    id_user: -1,
    id_console: -1,
    id_genre: -1,
    title: "",
    companyia: "",
    year: 1981,
    description: "",
    cover: "",
    region: ""
  }
  
  genre: IGenre = {
    id_genre: -1,
    type: ""
  }

  genres: IGenre[] = this.navParams.data;

  constructor(
    public navCtrl: NavController,
    public viewCtrl: ViewController,
    public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalConsolasPage');
  }

  closeModal() {
    this.viewCtrl.dismiss({ id: this.game.id_genre, type: this.genre.type });
  }

  goBack() {
    this.navCtrl.pop();
  }
}
