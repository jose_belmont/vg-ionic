import { MarketProvider } from './../../providers/market/market';
import { MygamesProvider } from './../../providers/mygames/mygames';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GamesProvider } from '../../providers/games/games';
import { IMenu } from '../../interfaces/i-menu';

/**
 * Generated class for the WelcomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-welcome',
  templateUrl: 'welcome.html',
})
export class WelcomePage {

  amountCol = { total: 0};
  amountSell = { total: 0};
  amountGam = { total: 0 };
  menu: IMenu;
  
  
  myGames: string;
  bdGames: string;
  market: string;
  

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private mygamesProvider: MygamesProvider,
    private marketProvider: MarketProvider,
    private gamesProvider: GamesProvider, 
  ) {
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad WelcomePage');
    this.getAmountGames();
    this.getAmountSell();
    this.getAmountMygames();
    this.getMyGamesImage();
    this.getBdGamesImage();
    this.getMarketImage();
    console.log(this.market, this.myGames, this.bdGames);
  }

  getAmountMygames() {
    this.mygamesProvider.getAmountGames().subscribe(
      total => {
        this.amountCol = total.amount.total;
      }
    )
  }
  
  // getImagesMenu() {
  //   this.gamesProvider.getAllMenuImages().subscribe( r=> {
  //     r.map( l => {
  //       this.menu = l;
  //       console.log(this.menu);
  //     })
  //     this.images = r;
      
  //   })
  // }

  getMyGamesImage() {
    this.gamesProvider.getMenuImageById(1).subscribe( r => {
      console.log(r);
      this.myGames = r;
    });
  }
  getBdGamesImage() {
    this.gamesProvider.getMenuImageById(2).subscribe( r => {
      this.bdGames = r;
    });
  }
  getMarketImage() {
    this.gamesProvider.getMenuImageById(3).subscribe( r => {
      this.market = r;
    });
  }

  getAmountGames() {
    this.gamesProvider.getAmountGames().subscribe(
      total => {
        this.amountGam = total.amount.total;
      }
    )
  }
  

  getAmountSell() {
    this.marketProvider.getAmountSell().subscribe(
      total => {
        this.amountSell = total.amount.total;
      }
    )
  }

  goMyCollection() {
    this.navCtrl.push("MyPlatformsPage");
  }

  goCollection() {
    this.navCtrl.push("BdPlatformsPage");
  }

  goMarket() {
    this.navCtrl.push("MarketPage");
  }
}
