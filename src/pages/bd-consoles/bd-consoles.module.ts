import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BdConsolesPage } from './bd-consoles';

@NgModule({
  declarations: [
    BdConsolesPage,
  ],
  imports: [
    IonicPageModule.forChild(BdConsolesPage),
  ],
})
export class BdConsolesPageModule {}
