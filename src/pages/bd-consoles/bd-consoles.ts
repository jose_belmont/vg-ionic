import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, ActionSheetController } from 'ionic-angular';
import { IPlatform } from '../../interfaces/i-platform';
import { IConsole } from '../../interfaces/i-consoles';
import { GamesProvider } from '../../providers/games/games';

/**
 * Generated class for the BdConsolesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-bd-consoles',
  templateUrl: 'bd-consoles.html',
})
export class BdConsolesPage {

  platform: IPlatform = {
    id_platform: -1,
    name_platform: "",
    img_platform: ""
  }

  consola: IConsole = {
    id_consoles: -1,
    name_console: '',
    img_console: '',
    platform: 0,
    plataforma: ""
  }
  consoles: IConsole[];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private gamesProvider: GamesProvider,
    private platforma: Platform,
    private actionSheetCtrl: ActionSheetController
    
  ) {
    if (this.navParams.data.id_platform)
      this.platform = this.navParams.data;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyConsolesPage');
    console.log(this.platform);
    this.getConsoles();
  }

  getConsoles() {
    this.gamesProvider.getAllConsolesByPlatform(this.platform.id_platform).subscribe(con => {
      this.consoles = con;
      console.log(this.consoles);
    });
  }

  goGames(con) {
    this.navCtrl.push('BdGamesPage', con);
  }

  openASNewGame() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Opciones',
      cssClass: 'page-my-games-detail',
      buttons: [
        {
          text: 'Codigo de Barras',
          icon: !this.platforma.is('ios') ? 'list-box' : null,
          handler: () => {
            this.navCtrl.setRoot('BarcodePage');
          }
        },
        {
          text: 'Ir a mi Colección',
          icon: !this.platforma.is('ios') ? 'albums' : null,
          handler: () => {
            console.log('Ir a mi coleccion clicked');
            this.navCtrl.setRoot('MyPlatformsPage');
          }
        },
        {
          text: 'Crear nuevo Juego',
          icon: !this.platforma.is('ios') ? 'add' : null,
          handler: () => {
            console.log('Nuevo juego clicked');
            this.navCtrl.push('AddGamePage');
          }
        },
        {
          text: 'Cancel',
          role: 'cancel', // will always sort to be on the bottom
          icon: !this.platforma.is('ios') ? 'close' : null,
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }
}
