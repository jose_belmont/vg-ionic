import { MygamesProvider } from './../../providers/mygames/mygames';
import { IPlatform } from './../../interfaces/i-platform';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, ActionSheetController } from 'ionic-angular';

/**
 * Generated class for the MyPlatformsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-my-platforms',
  templateUrl: 'my-platforms.html',
})
export class MyPlatformsPage {

  platform: IPlatform = {
    id_platform: -1,
    name_platform: "",
    img_platform: ""
  }
  platforms: IPlatform [];

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private mygamesProvider: MygamesProvider,
    private platforma: Platform,
    private actionsheetCtrl: ActionSheetController
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyPlatformsPage');
    this.getMyPlatforms();
  }

  getMyPlatforms() {
    this.mygamesProvider.getAllMyPlatforms().subscribe(pl => {
      this.platforms = pl;
      console.log(this.platforms);
    });
  }

  goMyConsoles(plat) {
    this.navCtrl.push('MyConsolesPage', plat);
  }

  openASNewGame() {
    let actionSheet = this.actionsheetCtrl.create({
      title: 'Opciones',
      cssClass: 'page-my-platforms-detail',
      buttons: [
        {
          text: 'Codigo de Barras',
          icon: !this.platforma.is('ios') ? 'list-box' : null,
          handler: () => {
            this.navCtrl.push('BarcodePage')
          }
        },
        {
          text: 'Buscar en BD',
          icon: !this.platforma.is('ios') ? 'search' : null,
          handler: () => {
            this.navCtrl.setRoot('BdPlatformsPage');
          }
        },
        {
          text: 'Crear nuevo Juego',
          icon: !this.platforma.is('ios') ? 'add' : null,
          handler: () => {
            console.log('Nuevo juego clicked');
            this.navCtrl.push('AddGamePage');
          }
        },
        {
          text: 'Cancel',
          role: 'cancel', // will always sort to be on the bottom
          icon: !this.platforma.is('ios') ? 'close' : null,
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }
}
