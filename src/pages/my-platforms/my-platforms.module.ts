import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyPlatformsPage } from './my-platforms';

@NgModule({
  declarations: [
    MyPlatformsPage,
  ],
  imports: [
    IonicPageModule.forChild(MyPlatformsPage),
  ],
})
export class MyPlatformsPageModule {}
