import { UserProvider} from './../../providers/user/user';
import { CommentsProvider } from './../../providers/comments/comments';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { IComment } from '../../interfaces/i-comment';
import { IMyGame } from '../../interfaces/i-mygame';

/**
 * Generated class for the CreateCommPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-create-comm',
  templateUrl: 'create-comm.html',
})
export class CreateCommPage {
  game: IMyGame = {
    id_game: -1,
    platform: "",
    name_console: "",
    genre: "",
    title: "",
    companyia: "Taito",
    year: 1990,
    description: "",
    my_cover: "",
    cover: "",
    region: "PAL-ES",
    local_ubication: "",
    gamerscore: 0,
    trophies: 0,
    box: "Sí",
    manual: "Sí",
    date_finish: null,
    annotations: ""
  }

  comment: IComment = {
    id_game: this.navParams.data.id_game,
    id_user: this.navParams.data.id_user,
    title: '',
    description: '',
    date: new Date(),
  }
  edit: boolean;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private commentProvider: CommentsProvider,
    private alertCtrl: AlertController,
    private userProvider: UserProvider
  ) {
    userProvider.getMyProfile().subscribe(
      us => {
        this.comment.id_user = us.id_user;
      }
    );
    this.edit = false;
    if (this.navParams.data.title) {
      this.comment = this.navParams.data;
      console.log(this.comment);
      this.edit = true;
    } else {
      this.edit = false;
    }
    console.log(this.comment);
  }
  
  ionViewDidLoad() {
    console.log('ionViewDidLoad CreateCommPage');
  }

  private showAddCommentOk() {
    let alert = this.alertCtrl.create({
      title: 'Creación OK!',
      subTitle: 'El comentario ha sido creado con exito',
      buttons: ['OK']
    });
    alert.present();
  }

  private showUpdateCommentOk() {
    let alert = this.alertCtrl.create({
      title: 'Actualizacion OK!',
      subTitle: 'El comentario ha sido actualizado con exito',
      buttons: ['OK']
    });
    alert.present();
  }

  private showErrorAddComment(error) {
    let alert = this.alertCtrl.create({
      title: 'Error al añadir el comentario',
      subTitle: error,
      buttons: ['Ok']
    });
    alert.present();
  }

  addComm(comment) {
    if (this.edit) {
      this.commentProvider.updateComment(this.comment).subscribe(
            (response) => {
              this.showUpdateCommentOk(),
                this.navCtrl.setRoot('ProfilePage')
            },
            (error) => this.showErrorAddComment(error));
    } else {     
        this.commentProvider.addComment(comment).subscribe((
          response) => {
          this.showAddCommentOk(),
            this.navCtrl.setRoot('ProfilePage')
        },
          (error) => this.showErrorAddComment(error));
    }
  }
}
