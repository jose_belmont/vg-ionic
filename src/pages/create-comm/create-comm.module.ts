import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CreateCommPage } from './create-comm';

@NgModule({
  declarations: [
    CreateCommPage,
  ],
  imports: [
    IonicPageModule.forChild(CreateCommPage),
  ],
})
export class CreateCommPageModule {}
