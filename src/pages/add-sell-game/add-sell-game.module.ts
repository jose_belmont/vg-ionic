import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddSellGamePage } from './add-sell-game';
import { PaypalButtonModule } from './../../providers/paypal-button/paypal-button.module';

@NgModule({
  declarations: [
    AddSellGamePage,
  ],
  imports: [
    IonicPageModule.forChild(AddSellGamePage),
    PaypalButtonModule.forRoot({
      sandbox: 'Acwd372rNIs9Hzo_sac6Y4sxKvtBG59DA-zmSbyvD9rRqsp8LePlTVKKY3AYp3mIUNjJjlhLgmSSzGq_',
      production: '',
      environment: 'sandbox'
    })
  ],
})
export class AddSellGamePageModule {}
