import { GamesProvider } from './../../providers/games/games';
import { MarketProvider } from './../../providers/market/market';
import { UserProvider } from './../../providers/user/user';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { IGame } from '../../interfaces/i-game';
import { ISell } from '../../interfaces/i-sell';

/**
 * Generated class for the AddSellGamePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-sell-game',
  templateUrl: 'add-sell-game.html',
})
export class AddSellGamePage {

  market: ISell = {
    id_trans_sell: -1,
    id_game: -1,
    id_user: -1,
    date_sell: "",
    price: 1,
    description: "",
    selled: false,
    send: false,
    cover: "",
    title: "",
    consola: "",
    sellLat: 30,
    sellLng: 3
  }
  
  game: IGame = {
    id_game: -1,
    id_user: -1,
    id_console: -1,
    id_genre: -1,
    title: "",
    name_console: "",
    genre: "",
    companyia: "",
    year: 1981,
    description: "",
    cover: "",
    region: "",
    barcode: ""
  }
  edit: boolean;
  payed: boolean = false;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private userProvider: UserProvider,
    private marketProvider: MarketProvider,
    private gamesProvider: GamesProvider,
    private alertCtrl: AlertController
  ) {

    if (this.navParams.data.id_game) {
      this.market = this.navParams.data;
      this.game = this.navParams.data;
    }
    userProvider.getMyProfile().subscribe(
      us => {
        this.game.id_user = us.id_user;
      }
    );
    this.edit = this.navParams.data.price > 0 ? true : false;
    if(this.edit) {
      console.log(this.edit);
      //marketProvider.getOneProductById()
    }
  }

  ionViewDidLoad() {
    console.log(this.market);
    //this.getGame();
  }

  getGame() {
    this.gamesProvider.getGameById(this.game.id_game).subscribe(gam => {
      this.game = gam;
    })
  }

  addSell() {
    console.log(this.market);
    if(this.edit) {
      // this.market.id_game = this.game.id_game;
      this.marketProvider.updateProduct(this.market).subscribe( r => {
        this.showUpdateGameOk();
        this.navCtrl.setRoot('MarketPage');
      });
    }
    else {
      const data = {
        id_game: this.market.id_game,
        id_user: this.market.id_user,
        price: this.market.price,
        description: this.market.description,
        send: this.market.send,
        sellLat: this.market.sellLat,
        sellLng: this.market.sellLng
      }
      console.log(data);
      this.marketProvider.sellProduct(data).subscribe( r => {
        if(r) {
          this.showAddGameOk();
          this.navCtrl.setRoot('ProfilePage');
        }
      });
    }
  }

  private showUpdateGameOk() {
    let alert = this.alertCtrl.create({
      title: 'añadido!',
      subTitle: 'El juego ha sido actualizado con exito',
      buttons: ['OK']
    });
    alert.present();
  }

  private showAddGameOk() {
    let alert = this.alertCtrl.create({
      title: 'añadido!',
      subTitle: 'El juego ha sido añadido con exito',
      buttons: ['OK']
    });
    alert.present();
  }

}
