import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ActionSheetController, Platform } from 'ionic-angular';
import { GamesProvider } from '../../providers/games/games';
import { IGame } from '../../interfaces/i-game';
import { IConsole } from '../../interfaces/i-consoles';

/**
 * Generated class for the BdGamesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-bd-games',
  templateUrl: 'bd-games.html',
})
export class BdGamesPage {

  consola: IConsole = {
    id_consoles: -1,
    name_console: '',
    img_console: '',
    platform: 0,
    plataforma: ""
  }

  game: IGame = {
    id_game: -1,
    id_user: -1,
    id_console: -1,
    id_genre: -1,
    title: "",
    companyia: "",
    year: 1981,
    description: "",
    cover: "",
    region: ""
  }

  games: IGame[];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private gamesProvider: GamesProvider,
    private platforma: Platform,
    private actionsheetCtrl: ActionSheetController
  ) {
    if (this.navParams.data.id_consoles)
      this.consola = this.navParams.data;
  }

  ionViewDidLoad() {
    this.getGames();
  }

  getGames() {
    this.gamesProvider.getAllGamesByConsole(this.consola.id_consoles).subscribe(gam => {
      this.games = gam;
      console.log(this.games);
    });
  }

  goGamesDetail(gam) {
    console.log('parametro:', gam);
    this.navCtrl.push('BdGameDetailPage', gam);
  }

  openASNewGame() {
    let actionSheet = this.actionsheetCtrl.create({
      title: 'Opciones',
      cssClass: 'page-my-games-detail',
      buttons: [
        {
          text: 'Codigo de Barras',
          icon: !this.platforma.is('ios') ? 'list-box' : null,
          handler: () => {
            this.navCtrl.setRoot('BarcodePage');
          }
        },
        {
          text: 'Ir a mi Colección',
          icon: !this.platforma.is('ios') ? 'albums' : null,
          handler: () => {
            console.log('Ir a mi coleccion clicked');
            this.navCtrl.setRoot('MyPlatformsPage');
          }
        },
        {
          text: 'Crear nuevo Juego',
          icon: !this.platforma.is('ios') ? 'add' : null,
          handler: () => {
            console.log('Nuevo juego clicked');
            this.navCtrl.push('AddGamePage');
          }
        },
        {
          text: 'Cancel',
          role: 'cancel', // will always sort to be on the bottom
          icon: !this.platforma.is('ios') ? 'close' : null,
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }

}
