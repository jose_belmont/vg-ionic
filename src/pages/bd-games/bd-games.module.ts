import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BdGamesPage } from './bd-games';

@NgModule({
  declarations: [
    BdGamesPage,
  ],
  imports: [
    IonicPageModule.forChild(BdGamesPage),
  ],
})
export class BdGamesPageModule {}
