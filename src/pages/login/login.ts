import { AuthProvider } from './../../providers/auth/auth';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { IUser } from '../../interfaces/i-user';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  user: IUser = {
    avatar : '',
    lat: 0,
    lng: 0,
    user: '',
    email: '',
    password: ''
  };
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public authProvider: AuthProvider,
    public alertCtrl: AlertController
    ) {}

  ionViewDidLoad() {
    this.authProvider.isLogged().subscribe((ok) => {
        if (ok) {
          this.navCtrl.setRoot('WelcomePage');
        }
      }
    );
  }

  ionViewWillLoad() {
    
  }
  login() {
    console.log(this.user);
    this.authProvider.login(this.user)
      .subscribe(
        () => this.navCtrl.setRoot('WelcomePage'),
        (error) => this.showErrorLogin(error)
      );
  }

  register() {
    this.navCtrl.push('RegisterPage');
  }

  private showErrorLogin(error) {
    let alert = this.alertCtrl.create({
      title: 'Login error',
      subTitle: error,
      buttons: ['Ok']
    });
    alert.present();
  }

}
