import { ModalGenresPage } from './../modal-genres/modal-genres';
import { ModalConsolasPage } from './../modal-consolas/modal-consolas';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddGamePage } from './add-game';

@NgModule({
  declarations: [
    AddGamePage,
  ],
  imports: [
    IonicPageModule.forChild(AddGamePage),
  ],
  entryComponents: [
  ]
})
export class AddGamePageModule {}
