import { MygamesProvider } from './../../providers/mygames/mygames';
import { ModalRegionsPage } from './../modal-regions/modal-regions';
import { ModalCompanyiasPage } from './../modal-companyias/modal-companyias';
import { UserProvider } from './../../providers/user/user';
import { GamesProvider } from './../../providers/games/games';
import { IGenre } from './../../interfaces/i-genre';
import { IConsole } from './../../interfaces/i-consoles';
import { IGame } from './../../interfaces/i-game';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ModalController, ToastController } from 'ionic-angular';
import { CameraOptions, Camera } from '@ionic-native/camera';
import { ModalConsolasPage } from '../modal-consolas/modal-consolas';
import { ModalGenresPage } from '../modal-genres/modal-genres';
import { IMyGame } from '../../interfaces/i-mygame';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';

/**
 * Generated class for the AddGamePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-game',
  templateUrl: 'add-game.html',
})

export class AddGamePage {

  consola: IConsole = {
    name_console: "",
    id_consoles: -1
  }

  consolas: IConsole[];

  genre: IGenre = {
    id_genre: -1,
    type: ""
  }

  genres: IGenre[]

  game: IGame = {
    id_game: -1,
    id_user: -1,
    id_console: -1,
    id_genre: -1,
    title: "",
    companyia: "",
    year: 1981,
    description: "",
    cover: "",
    region: "",
    barcode: "",
    add: true,
    youtube: ""
  }

  mygame: IMyGame = {
    id_user: -1,
    id_game: -1,
    local_ubication: "",
    date_finish: null,
    annotations: "",
    gamerscore: 0,
    trophies: 0,
    box: "Sí",
    manual: "Sí",
    my_cover: "",
    mybarcode: "",
    youtube: ""
  }

  edit: boolean;
  actualImage: any;
  add: boolean;
  actualYear: Date = new Date();
  temp: number;
  results:any;
  usuario:number;
  insertId: number;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private gamesProvider: GamesProvider,
    private camera: Camera,
    private alertCtrl: AlertController,
    private modalCtrl: ModalController,
    private toastCtrl: ToastController,
    private userProvider: UserProvider,
    private mygamesProvider: MygamesProvider,
    private barcodeScanner: BarcodeScanner
  ) {
    userProvider.getMyProfile().subscribe(
      us => {
        this.game.id_user = us.id_user;
        this.usuario = us.id_user;
      }
    );
    this.edit = false;
    if (this.navParams.data.id_game) {
      this.game = this.navParams.data;
      this.edit = true;
    } else {
      this.edit = false;
    }
    this.actualImage = this.game.cover;
    this.game.cover = '';
  }

  ionViewDidLoad() {
    this.getAllConsoles();
    this.getAllGenres();
  }

  scanBarcode() {
    this.barcodeScanner.scan().then((barcodeData) => {
      this.results = barcodeData;
      this.game.barcode = this.results.text;
    }), (err) => {
      alert('Error : ${err}');
    }
  };
  reset() {
    this.results = null;
  }

  OpenModalConsolas() {
    let modalCon = this.modalCtrl.create(ModalConsolasPage, this.consolas);

    modalCon.onDidDismiss(data => {
      this.consolas.filter(n => {
        if (n.id_consoles == data.id) {
          this.consola.name_console = n.name_console;
        }
      }
      );
      this.game.id_console = data.id;
    })
    modalCon.present();
  }

  OpenModalGenres() {
    let modalGen = this.modalCtrl.create(ModalGenresPage, this.genres);

    modalGen.onDidDismiss(data => {
      this.genres.filter(n => {
        if (n.id_genre == data.id) {
          this.genre.type = n.type;
        }
      }
      );
      this.game.id_genre = data.id;
    })
    modalGen.present();
  }

  OpenModalCompanyias() {
    let modalComp = this.modalCtrl.create(ModalCompanyiasPage);

    modalComp.onDidDismiss(data => {
      this.game.companyia = data;
    })
    modalComp.present();
  }

  OpenModalRegions() {
    let modalReg = this.modalCtrl.create(ModalRegionsPage);

    modalReg.onDidDismiss(data => {
      this.game.region = data;
    })
    modalReg.present();
  }

  getAllConsoles() {
    this.gamesProvider.getAllConsoles().subscribe(con => {
      this.consolas = con;
    });
  }

  getAllGenres() {
    this.gamesProvider.getAllGenres().subscribe(gen => {
      this.genres = gen;
    })
  }

  addGame() {
    if (this.edit) {
      this.gamesProvider.updateGame(this.game).subscribe(
        (response) => {
          this.showUpdateGameOk(),
            this.navCtrl.setRoot('WelcomePage')
        },
        (error) => this.showErrorAddGame(error));
    } else {
      
      if (this.game.add) {
        this.gamesProvider.addGame(this.game).subscribe(
          gam => {
            this.insertId = gam;
            this.mygame.id_game = this.insertId;
            this.mygame.mybarcode = this.game.barcode;
            this.mygame.id_user = this.usuario;
            this.mygame.my_cover = this.game.cover;
            this.mygamesProvider.addGameInfo(this.mygame).subscribe(r => {
            })
          },
          (error) => this.showErrorAddGame(console.log(error))
        );
        

        this.showAddGameOk();
        this.navCtrl.setRoot('WelcomePage')   
  }
      else {
        this.gamesProvider.addGame(this.game).subscribe((
          response) => {
          this.showAddGameOk(),
            this.navCtrl.setRoot('WelcomePage')
        },
          (error) => this.showErrorAddGame(error));
      }
    }
  }

  private showAddGameOk() {
    let alert = this.alertCtrl.create({
      title: 'Creación OK!',
      subTitle: 'El juego ha sido creado con exito',
      buttons: ['OK']
    });
    alert.present();
  }

  private showUpdateGameOk() {
    let alert = this.alertCtrl.create({
      title: 'Actualizacion OK!',
      subTitle: 'El juego ha sido actualizado con exito',
      buttons: ['OK']
    });
    alert.present();
  }

  private showErrorAddGame(error) {
    let alert = this.alertCtrl.create({
      title: 'Error al añadir el juego',
      subTitle: error,
      buttons: ['Ok']
    });
    alert.present();
  }

  takePhoto() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
    }
    this.getPicture(options);
  }

  pickFromGallery() {
    const options: CameraOptions = {
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      targetWidth: 640,
      targetHeight: 640,
      destinationType: this.camera.DestinationType.DATA_URL
    }
    this.getPicture(options);
  }

  private getPicture(options: CameraOptions) {
    this.camera.getPicture(options).then((imageData) => {
      this.game.cover = 'data:image/jpeg;base64,' + imageData;
      this.alertCtrl.create({
        title: 'Success',
        subTitle: 'Take picture ok',
        buttons: ['Ok']
      });
    }).catch(error => {
      this.alertCtrl.create({
        title: 'Error',
        subTitle: error,
        buttons: ['Ok']
      });
    });
  }
}
