import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { IConsole } from '../../interfaces/i-consoles';
import { IGame } from '../../interfaces/i-game';

/**
 * Generated class for the ModalConsolasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-modal-consolas',
  templateUrl: 'modal-consolas.html',
})
export class ModalConsolasPage {

  game: IGame = {
    id_game: -1,
    id_user: -1,
    id_console: -1,
    id_genre: -1,
    title: "",
    companyia: "",
    year: 1981,
    description: "",
    cover: "",
    region: ""
  }
  
  consola: IConsole = {
    name_console: "",
    id_consoles: -1
  }

  consolas: IConsole[] = this.navParams.data;
  
  constructor(
    public navCtrl: NavController, 
    public viewCtrl: ViewController, 
    public navParams: NavParams) {
  }
  ionViewDidLoad() {
    console.log(this.navParams.data)
  }
  
  ionViewDidEnter(){
   console.log(this.consolas);
  }
  closeModal() {
    this.viewCtrl.dismiss({id: this.game.id_console, name: this.consola.name_console});
  }

  goBack() {
    this.navCtrl.pop();
  }
}
