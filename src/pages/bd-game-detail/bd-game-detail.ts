import { YoutubeVideoPlayer } from '@ionic-native/youtube-video-player';
import { IMyGame } from './../../interfaces/i-mygame';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, Platform, ActionSheetController } from 'ionic-angular';
import { IGame } from '../../interfaces/i-game';
import { IComment } from '../../interfaces/i-comment';
import { GamesProvider } from '../../providers/games/games';
import { MygamesProvider } from '../../providers/mygames/mygames';
import { UserProvider } from '../../providers/user/user';
import { CommentsProvider } from '../../providers/comments/comments';

/**
 * Generated class for the BdGameDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-bd-game-detail',
  templateUrl: 'bd-game-detail.html',
})
export class BdGameDetailPage {

  content: string;
  defaultDate = new Date('2000-01-01');

  game: IGame = {
    id_game: -1,
    id_user: -1,
    id_console: -1,
    id_genre: -1,
    title: "",
    name_console: "",
    genre: "",
    companyia: "",
    year: 1981,
    description: "",
    cover: "",
    region: "",
    barcode: ""
  }

  comment: IComment = {
    id_comment: -1,
    id_game: this.game.id_game,
    id_user: 2,
    title: '',
    description: '',
    date: null,
    consola: '',
    juego: '',
    avatar: '',
    user: '',
  }

  comments: IComment[];

  mygame: IMyGame = {
    id_user: -1,
    id_game: -1,
    local_ubication: "",
    date_finish: null,
    annotations: "",
    gamerscore: 0,
    trophies: 0,
    box: "Sí",
    manual: "Sí",
    my_cover: "",
    mybarcode: ""
  }

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private alertCtrl: AlertController,
    private gamesProvider: GamesProvider,
    private mygamesProvider: MygamesProvider,
    private platform: Platform,
    private actionsheetCtrl: ActionSheetController,
    private userProvider: UserProvider,
    private commentsProvider: CommentsProvider,
    private youtube: YoutubeVideoPlayer
  ) {
    this.content = "game";
    if (this.navParams.data.id_game)
      this.game = this.navParams.data;
      console.log(this.game);
    userProvider.getMyProfile().subscribe(
      us => {
        this.comment.id_user = us.id_user;
        this.mygame.id_user = us.id_user;
      }
    );
  }

  ionViewDidLoad() {
    this.getGamesDetail();
    this.getComments();
    this.comment.id_game = this.game.id_game;
    console.log(this.comment);
  }

  getGamesDetail() {
    this.gamesProvider.getGameById(this.game.id_game).subscribe(gam => {
      this.game = gam;
      this.mygame.id_game = this.game.id_game;
      this.mygame.my_cover = this.game.cover.slice(44);
      console.log(this.game);
    });
  }

  getComments() {
    this.commentsProvider.getAllCommentsOneGame(this.game.id_game).subscribe(allComments => {
      this.comments = allComments;
      console.log('allmycomments: ', allComments);
    });
  }

  addToMyCollection() {
    this.mygamesProvider.addGameInfo(this.mygame).subscribe( ok => {
      if(ok) {
        this.showAddGameOk();
        this.navCtrl.setRoot('MyPlatformsPage');
      }
    });
  }

  private showAddGameOk() {
    let alert = this.alertCtrl.create({
      title: 'añadido!',
      subTitle: 'El juego ha sido añadido con exito',
      buttons: ['OK']
    });
    alert.present();
  }

  openActionSheet(game) {
    let actionSheet = this.actionsheetCtrl.create({
      title: 'Opciones del juego',
      cssClass: 'page-mygame-detail',
      buttons: [
        {
          text: 'Home',
          icon: !this.platform.is('ios') ? 'list' : null,
          handler: () => {         
            this.navCtrl.push('WelcomePage');
          }
        },
        {
          text: 'Añadir a mi coleccion',
          icon: !this.platform.is('ios') ? 'create' : null,
          handler: () => {
            this.addToMyCollection();
            this.navCtrl.push('MyPlatformsPage');
          }
        },
        {
          text: 'Comentar',
          icon: !this.platform.is('ios') ? 'chatbubbles' : null,
          handler: () => {
            console.log('Comentar clicked');
            this.navCtrl.push('CreateCommPage', this.comment);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel', // will always sort to be on the bottom
          icon: !this.platform.is('ios') ? 'close' : null,
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }

}
