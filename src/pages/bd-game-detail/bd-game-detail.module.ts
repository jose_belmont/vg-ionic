import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BdGameDetailPage } from './bd-game-detail';

@NgModule({
  declarations: [
    BdGameDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(BdGameDetailPage),
  ],
})
export class BdGameDetailPageModule {}
