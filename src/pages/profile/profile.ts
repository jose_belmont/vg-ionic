import { ISell } from './../../interfaces/i-sell';
import { RegisterPage } from './../register/register';
import { ListPage } from './../list/list';
import { HomePage } from './../home/home';
import { Component, NgModule } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { IUser } from '../../interfaces/i-user';
import { UserProvider } from '../../providers/user/user';
import { HttpClient } from '@angular/common/http';
import { Geolocation } from '@ionic-native/geolocation';
import { CommentsProvider } from '../../providers/comments/comments';
import { MarketProvider } from '../../providers/market/market';
import { IComment } from '../../interfaces/i-comment';
import { DatePipe } from '@angular/common';

/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  user: IUser = {
    avatar: '',
    lat: 38.4039418,
    lng: -0.5288701,
    user: '',
    name: '',
    surname: '',
    email: '',
    password: ''
  };
  lat: number;
  lng: number;
  updateMap: boolean = false;
  my: boolean;
  zoom = 17;
  id = this.user.id_user;
  content: string;

  comment: IComment = {
    id_comment: -1,
    id_game: 0,
    id_user: 0,
    title: '',
    description: '',
    date: new Date(),
    consola: '',
    juego: '',
    avatar: '',
    user: '',
  }

  market: ISell = {
    id_trans_sell: -1,
    id_game: -1,
    id_user: -1,
    date_sell: "",
    price: -1,
    description: "",
    selled: false,
    send: false,
    cover: "",
    title: "",
    consola: ""
  }

  itsMe: boolean;
  markets: ISell [];
  users: IUser[] = [];
  comments: IComment[] = [];
  commentsArray = [];

  constructor(public navCtrl: NavController,
    public alertCtrl: AlertController,
    public navParams: NavParams,
    public userProvider: UserProvider,
    public commentsProvider: CommentsProvider,
    public marketProvider: MarketProvider,
    public http: HttpClient,
    private geolocation: Geolocation) {
    this.getMyProfile();
    this.getMyComments();
    
    this.content = "info";
    //si existe el email es que el usuario existe
    if (this.navParams.data.email) {
      this.user = this.navParams.data;
      this.itsMe = false;
    } else {
      this.getMyProfile();
      this.itsMe = true;
    };
  }

  ionViewDidLoad() {
    this.getAllProfiles();
    this.getMyComments();
    this.getMySells();
    this.geolocation.getCurrentPosition().then((resp) => {
      this.lat = resp.coords.latitude;
      this.lng = resp.coords.longitude;
    }).catch((error) => {
      console.log('Error getting location', error);
    });
  }

  goEdit() {
    this.navCtrl.push('RegisterPage', this.user);
  }

  getMyProfile() {
    this.userProvider.getMyProfile().subscribe(result => { 
      console.log(this.user);
      this.user = result;
    });
  }

  getAllProfiles() {
    this.userProvider.getAllUsers().subscribe((allUsers) => {
      this.users = allUsers;  
      console.log('getallusers:', this.users);
    });
  }
  
  getMySells() {
    this.marketProvider.getAllMyProducts().subscribe ( se => {
      this.markets = se;
      console.log(this.markets);
    })
  }

  getMyComments() {
    this.commentsProvider.getMyComments().subscribe((allComments) => {
      this.comments = allComments;
      console.log('allmycomments: ', allComments);
    });
  }

  updateCoords() {
    this.userProvider.updateCoords(this.lat, this.lng).subscribe();
    this.updateMap = true;
  }

  editComm(com) {
    console.log(this.comment);
    this.navCtrl.push('CreateCommPage',com);
  }

  editProduct(mark) {
    this.navCtrl.push('AddSellGamePage', mark);
  }

  retireProduct(prod) {
    let alert = this.alertCtrl.create({
      title: 'Retira el producto',
      message: 'Quieres que se deje de vender tu juego?',
      buttons: [{ text: 'Cancel', role: 'cancel' },
      {
        text: 'Borrar',
        handler: () => {
          console.log(prod);
          this.marketProvider.deleteProduct(prod).subscribe(
            () => this.navCtrl.setRoot('ProfilePage'),
            (error) => {
              let alert = this.alertCtrl.create({
                title: 'Error borrando!',
                subTitle: error,
                buttons: ['Ok']
              });
              alert.present();
            }
          );
        }
      }]
    });
    alert.present();
  }


  deleteComment(com) {
    let alert = this.alertCtrl.create({
      title: 'Borrar este comentario',
      message: 'Quieres borrar este comentario?',
      buttons: [{ text: 'Cancel', role: 'cancel' },
      {
        text: 'Borrar',
        handler: () => {
          console.log(com);
          this.commentsProvider.deleteGameComment(com).subscribe(
            () => this.navCtrl.setRoot('ProfilePage'),
            (error) => {
              let alert = this.alertCtrl.create({
                title: 'Error deleted!',
                subTitle: error,
                buttons: ['Ok']
              });
              alert.present();
            }
          );
        }
      }]
    });
    alert.present();

  }
}
