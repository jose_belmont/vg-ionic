import { IConsole } from './../../interfaces/i-consoles';
import { MygamesProvider } from './../../providers/mygames/mygames';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ActionSheetController, Platform } from 'ionic-angular';
import { IPlatform } from '../../interfaces/i-platform';

/**
 * Generated class for the MyConsolesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-my-consoles',
  templateUrl: 'my-consoles.html',
})
export class MyConsolesPage {

  platform: IPlatform = {
    id_platform: -1,
    name_platform: "",
    img_platform: ""
  }
  
  consola: IConsole = {
    id_consoles: -1,
    name_console: '',
    img_console: '',
    platform: 0,
    plataforma: ""
  }
  consoles: IConsole [];

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private mygamesProvider: MygamesProvider,
    private actionsheetCtrl: ActionSheetController,
    private platforma: Platform
  ) {
      if(this.navParams.data.id_platform)
        this.platform = this.navParams.data;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyConsolesPage');
    console.log(this.platform);
    this.getMyConsoles();
  }

  getMyConsoles() {
    this.mygamesProvider.getAllMyConsolesByPlatform(this.platform.id_platform).subscribe(con => {
      this.consoles = con;
      console.log(this.consoles);
    });
  }

  goMyGames(con) {
    this.navCtrl.push('MyGamesPage', con);
  }

  openASNewGame() {
    let actionSheet = this.actionsheetCtrl.create({
      title: 'Opciones',
      cssClass: 'page-my-consoles-detail',
      buttons: [
        {
          text: 'Codigo de Barras',
          icon: !this.platforma.is('ios') ? 'list-box' : null,
          handler: () => {
            this.navCtrl.push('BarcodePage')
          }
        },
        {
          text: 'Buscar en BD',
          icon: !this.platforma.is('ios') ? 'search' : null,
          handler: () => {
            this.navCtrl.setRoot('BdPlatformsPage');
          }
        },
        {
          text: 'Crear nuevo Juego',
          icon: !this.platforma.is('ios') ? 'add' : null,
          handler: () => {
            console.log('Nuevo juego clicked');
            this.navCtrl.push('AddGamePage');
          }
        },
        {
          text: 'Cancel',
          role: 'cancel', // will always sort to be on the bottom
          icon: !this.platforma.is('ios') ? 'close' : null,
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }
}
