import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyConsolesPage } from './my-consoles';

@NgModule({
  declarations: [
    MyConsolesPage,
  ],
  imports: [
    IonicPageModule.forChild(MyConsolesPage),
  ],
})
export class MyConsolesPageModule {}
