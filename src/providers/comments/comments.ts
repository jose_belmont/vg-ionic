import { IComment } from './../../interfaces/i-comment';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { IUser } from '../../interfaces/i-user';
import { constants } from '../../constants';
import { IResponse } from '../../interfaces/i-response';

/*
  Generated class for the CommentsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CommentsProvider {

  constructor(public http: HttpClient) {
    console.log('Hello CommentsProvider Provider');
  }
  //Games
  getAllCommentsOneGame(id: number): Observable<IComment[]> {
    return this.http.get(`${constants.server}games/comments/${id}`)
      .map((response: { comments: IComment[], ok: boolean }) => {
        if (response.ok) {
          response.comments.map(c => {
            c.avatar = `${constants.imgUsers}${c.avatar}`;
          });
          console.log(response);
          return response.comments;
        } else {
          return [];
        }
      }).catch(error => Observable.throw(error));
  }
  //End Games

  //My Games
  getMyComments(): Observable<IComment[]> {
    return this.http.get(`${constants.server}mygames/mycomments`)
      .map((response: { comments: IComment[], ok: boolean }) => {
        if (response.ok) {
          response.comments.map(c => {
            c.avatar = `${constants.imgUsers}${c.avatar}`;
          });
          return response.comments
        }
        else return [];               
        }).catch(error => Observable.throw(error));
}

addComment(com: IComment): Observable < boolean > {
  return this.http
    .post(`${constants.server}games/comments/${com.id_game}`, com)
    .catch((resp: HttpErrorResponse) =>
      Observable.throw(
        `Error doing register!
            . Server returned code ${resp.status}, message was: ${resp.message}`
      )
    )
    .map((resp: IResponse) => {
      if (resp.error) {
        throw resp.errorMessage;
      } else {
        return true;
      }
    });
}

updateComment(com: IComment): Observable < IComment > {
  const data = {
    //ranking: com.ranking["5"],
    title: com.title,
    description: com.description
  };
  return this.http.put(`${constants.server}mygames/comment/${com.id_comment}`, data)
    .catch(error => Observable.throw("Error trying to update user. Detail: " + error))
    .map((user: IResponse) => {
      if (!user.error) {
        return user.result;
      }
      throw user.errorMessage;
    });
}

deleteGameComment(id: number): Observable < boolean > {
  return this.http.delete(`${constants.server}mygames/comment/${id}`)
    .catch((error: HttpErrorResponse) =>
      Observable.throw(
        `Error trying to get comments. Server returned ${error.message}`
      )
    )
    .map((resp: IResponse) => {
      if (!resp.error) {
        return true;
      }
      throw resp.errorMessage;
    });
}
  //End My Games  
}
