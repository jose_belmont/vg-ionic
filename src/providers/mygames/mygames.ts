
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { constants } from '../../constants';
import { IMyGame } from '../../interfaces/i-mygame';
import { IPlatform } from '../../interfaces/i-platform';
import { IConsole } from '../../interfaces/i-consoles';
import { IResponse } from '../../interfaces/i-response';

/*
  Generated class for the MygamesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MygamesProvider {

  constructor(public http: HttpClient) {
  }
  
  getAmountGames(): Observable<any> {
    return this.http.get(`${constants.server}mygames/amount`)
    .catch(error => Observable.throw(error));
  }

  getAllMyGames(): Observable<IMyGame[]> {
    return this.http.get(`${constants.server}mygames`)
      .map((response: { games: IMyGame[], ok: boolean }) => {
        if (response.ok) {
          response.games.map(game => {
            game.cover = `${constants.imgGames}${game.cover}`;
            game.my_cover = `${constants.imgGames}${game.my_cover}`;
          });
          return response.games;
        } else {
          return [];
        }
      }).catch(error => Observable.throw(error));
  }

  getAllMyPlatforms(): Observable<IPlatform[]> {
    return this.http.get(`${constants.server}mygames/platforms`)
      .map((response: { platforms: IPlatform[], ok: boolean }) => {
        if (response.ok) {
          response.platforms.map(plat => {
            plat.img_platform = `${constants.imgPlatfomrs}${plat.img_platform}`;
          });
          return response.platforms;
        } else {
          return [];
        }
      }).catch(error => Observable.throw(error));
  }

  getAllMyConsolesByPlatform(id: number): Observable<IConsole []> {
    return this.http.get(`${constants.server}mygames/consoles/${id}`)
      .map((response: { consoles: IConsole[], ok: boolean }) => {
        if (response.ok) {
          response.consoles.map(conso => {
            conso.img_console = `${constants.imgConsoles}${conso.img_console}`;
          });
          return response.consoles;
        } else {
          return [];
        }
      }).catch(error => Observable.throw(error));
  }

  getAllMyGamesByConsole(id: number): Observable<IMyGame []> {
    return this.http.get(`${constants.server}mygames/games/${id}`)
      .map((response: { games: IMyGame[], ok: boolean }) => {
        if (response.ok) {
          response.games.map(game => {
            game.cover = `${constants.imgGames}${game.cover}`;
            game.my_cover = `${constants.imgGames}${game.my_cover}`;
          });
          return response.games;
        } else {
          return [];
        }
      }).catch(error => Observable.throw(error));
  }

  getMyGameById(id: number): Observable<IMyGame> {
    return this.http.get(`${constants.server}mygames/${id}`)
      .map((response: { game: IMyGame, ok: boolean }) => {
        if (response.ok) {
            response.game.cover = `${constants.imgGames}${response.game.cover}`;
            response.game.my_cover = `${constants.imgGames}${response.game.my_cover}`;         
          return response.game;
        } else {
          return [];
        }
      }).catch(error => Observable.throw(error));
  }

  addGameInfo(game: IMyGame): Observable<boolean> {
    return this.http
      .post(`${constants.server}mygames/${game.id_game}`, game)
      .catch((resp: HttpErrorResponse) =>
        Observable.throw(
          `Error doing register!
            . Server returned code ${resp.status}, message was: ${resp.message}`
        )
      ).map(res => {
      return res.ok;
    }) 
  }

  addFromNewGame(game: IMyGame): Observable<boolean> {
    // const data = {
    //   local_ubication: game.local_ubication,
    //   date_finish: game.date_finish,
    //   annotations: game.annotations,
    //   gamerscore: game.gamerscore,
    //   trophies: game.trophies,
    //   box: game.box,
    //   manual: game.manual,
    //   my_cover: game.my_cover
    // }
    return this.http
      .post(`${constants.server}mygames`, game)
      .catch((resp: HttpErrorResponse) =>
        Observable.throw(
          `Error inserting game!
            . Server returned code ${resp.status}, message was: ${resp.message}`
        )
      ).map(res => {
        return res.ok;
      })
  }

  updateMyGameInfo(game: IMyGame): Observable<IMyGame> {
    const data = {
      gamerscore: game.gamerscore,
      trophies: game.trophies,
      date_finish: game.date_finish,
      local_ubication: game.local_ubication,
      box: game.box,
      manual: game.manual,
      annotations: game.annotations
    };
    return this.http.put(`${constants.server}mygames/${game.id_game}`, data)
      .catch(error => Observable.throw("Error trying to update user. Detail: " + error))
      .map((user: IResponse) => {
        if (!user.error) {
          return user.result;
        }
        throw user.errorMessage;
      });
  }

  updateMyGameCover(game: IMyGame): Observable<IMyGame> {
    const data = {
      my_cover: game.my_cover
    }
    return this.http.put(`${constants.server}mygames/cover/${game.id_game}`, data)
      .catch(error => Observable.throw("Error trying to update user. Detail: " + error))
      .map((user: IResponse) => {
        if (!user.error) {
          return user.result;
        }
        throw user.errorMessage;
      });
  }

  deleteMyGame(id: number): Observable<boolean> {
    return this.http.delete(`${constants.server}mygames/${id}`)
      .catch((error: HttpErrorResponse) =>
        Observable.throw(
          `Error trying to get events. Server returned ${error.message}`
        )
      )
      .map((resp: IResponse) => {
        if (!resp.error) {
          return true;
        }
        throw resp.errorMessage;
      });
  }
}
