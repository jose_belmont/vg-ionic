
import { IUser } from '../../interfaces/i-user';
import { constants } from '../../constants';
import { IResponse } from '../../interfaces/i-response';

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/observable/throw';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/fromPromise';

import { HttpErrorResponse } from '@angular/common/http/src/response';
import { Observable } from 'rxjs/Observable';

import { Storage } from '@ionic/storage';

/*
  Generated class for the AuthProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthProvider {

  logged: boolean;
  private currentUser: IUser;

  constructor(public http: HttpClient, private storage: Storage) {
  }

  login(user: IUser): Observable<boolean> {
    return this.http.post(`${constants.server}auth/login`, user)
      .catch((resp: HttpErrorResponse) =>
        Observable.throw(
          'Error doing login!' +
          `. Server returned code ${resp.status}, message was: ${resp.message}`
        )
      )
      .map((response: IResponse) => {
        if (response.error) {
          throw response.errorMessage;
        } else {
          console.log(response.token);
          localStorage.setItem('token', response.token);
          this.storage.set('token', response.token);
          this.logged = true;
          this.currentUser = user;
          return true;
        }
      });
  }

  logout(): boolean {
    this.storage.remove('token');
    this.logged = false;
    return false;
  }

  register(user: IUser): Observable<boolean> {
    return this.http
      .post(`${constants.server}auth/register`, user)
      .catch((resp: HttpErrorResponse) =>
        Observable.throw(
          `Error doing register!
          . Server returned code ${resp.status}, message was: ${resp.message}`
        )
      )
      .map((resp: IResponse) => {
        if (resp.error) {
          throw resp.errorMessage;
        } else {
          return true;
        }
      });
  }

  // isLogged(): Observable<boolean> {
  //   return Observable.create((observer: Observer<any>) => {

  //     this.storage.get('token').then(token => {
  //       this.http.get(`${constants.server}auth/token`).
  //         subscribe((resp: IResponse) => {
  //           if (!resp.error) {
  //             observer.next(true);
  //             observer.complete();
  //           } else {
  //             observer.next(false);
  //             observer.complete();
  //           }
  //         },
  //           error => {
  //             observer.next(false);
  //             observer.complete();
  //           }
  //         );

  //     });
  //   })
  // }
  isLogged(): Observable<boolean> {
    return Observable.fromPromise(this.storage.get('token'))
      .flatMap(token => {
        if (!token) return Observable.of(false);
        return this.http
          .get(constants.server + 'auth/token')
          .map((resp: IResponse) => (resp.error ? false : true))
          .catch(error => Observable.of(false));
      }).catch(error => {
        return Observable.of(false);
      });
  }
}
