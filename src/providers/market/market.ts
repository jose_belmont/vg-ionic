import { ISell } from './../../interfaces/i-sell';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { constants } from '../../constants';
import { IResponse } from '../../interfaces/i-response';
import { IBuy } from '../../interfaces/i-buy';

/*
  Generated class for the MarketProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MarketProvider {

  constructor(public http: HttpClient) {
    console.log('Hello MarketProvider Provider');
  }

  getAmountSell(): Observable<any> {
    return this.http.get(`${constants.server}markets/amount`)
      .catch(error => Observable.throw(error));
  }

  //Products se refiere a juegos en mercado (en venta)
  getAllProducts(): Observable<ISell[]> {
    return this.http.get(`${constants.server}markets`)
      .map((response: { products: ISell[], ok: boolean }) => {
        if (response.ok) {
          response.products.map(game => {
            game.cover = `${constants.imgGames}${game.cover}`;
          });
          return response.products;
        } else {
          return [];
        }
      }).catch(error => Observable.throw(error));
  }

  getAllMyProducts(): Observable<ISell[]> {
    return this.http.get(`${constants.server}markets/my`)
      .map((response: { products: ISell[], ok: boolean }) => {
        if (response.ok) {
          response.products.map(game => {
            game.cover = `${constants.imgGames}${game.cover}`;
          });
          return response.products;
        } else {
          return [];
        }
      }).catch(error => Observable.throw(error));
  }

  getAllProductsByUser(id: number): Observable<ISell[]> {
    return this.http.get(`${constants.server}markets/user/${id}`)
      .map((response: { products: ISell[], ok: boolean }) => {
        if (response.ok) {
          response.products.map(game => {
            game.cover = `${constants.imgGames}${game.cover}`;
          });
          return response.products;
        } else {
          return [];
        }
      }).catch(error => Observable.throw(error));
  }

  getAllProductsByPlatform(id: number): Observable<ISell[]> {
    return this.http.get(`${constants.server}markets/platform/${id}`)
      .map((response: { products: ISell[], ok: boolean }) => {
        if (response.ok) {
          response.products.map(game => {
            game.cover = `${constants.imgGames}${game.cover}`;
          });
          return response.products;
        } else {
          return [];
        }
      }).catch(error => Observable.throw(error));
  }

  getAllProductsByConsole(id: number): Observable<ISell[]> {
    return this.http.get(`${constants.server}markets/console/${id}`)
      .map((response: { products: ISell[], ok: boolean }) => {
        if (response.ok) {
          response.products.map(game => {
            game.cover = `${constants.imgGames}${game.cover}`;
          });
          return response.products;
        } else {
          return [];
        }
      }).catch(error => Observable.throw(error));
  }

  getOneProductById(id: number): Observable<ISell> {
    return this.http.get(`${constants.server}markets/${id}`)
      .map((response: { products: ISell, ok: boolean }) => {
        if (response.ok) {
          response.products.cover = `${constants.imgGames}${response.products.cover}`;
          response.products.avatar = `${constants.imgUsers}${response.products.avatar}`;
          console.log(response.products);
          return response.products;
        } else {
          return [];
        }
      }).catch(error => Observable.throw(error));
  }

  sellProduct(product: ISell): Observable<boolean> {
    const data = {
      price: product.price,
      description: product.description,
      send: product.send,
      sellLat: product.sellLat,
      sellLng: product.sellLng
    }
    return this.http
      .post(`${constants.server}markets/sell/${product.id_game}`, data)
      .catch((resp: HttpErrorResponse) =>
        Observable.throw(
          `Error doing register!
            . Server returned code ${resp.status}, message was: ${resp.message}`
        )
      )
      .map((resp: IResponse) => {
        if (resp.error) {
          throw resp.errorMessage;
        } else {
          return true;
        }
      });
  }

  // buyProduct(market): Observable<boolean> {
  //   const data = {
  //     id_trans_sell: market.id_trans_sell,
  //     id_user: market.id_user,
  //     date_buy: new Date(),
  //     payed: 0
  //   };
  //   return this.http
  //     .post(`${constants.server}markets/buy/${market.id}`, data)
  //     .catch((resp: HttpErrorResponse) =>
  //       Observable.throw(
  //         `Error doing register!
  //           . Server returned code ${resp.status}, message was: ${resp.message}`
  //       )
  //     )
  //     .map((resp: IResponse) => {
  //       if (resp.error) {
  //         throw resp.errorMessage;
  //       } else {
  //         return true;
  //       }
  //     });
  // }
  // PayProduct(buy: IBuy) : Observable<boolean> {
  //   let data = {

  //   };
  //   return this.http.patch(`${constants.server}markets/pay/${buy.id_trans_buy}`, data)
  //     .catch(error => Observable.throw("Error trying to update user. Detail: " + error))
  //     .map((user: IResponse) => {
  //       if (!user.error) {
  //         return user.result;
  //       }
  //       throw user.errorMessage;
  //     });
  // }

  updateProduct(product: ISell): Observable<ISell> {
    const data = {
      price: product.price,
      description: product.description,
      send: product.send
    }
    return this.http.put(`${constants.server}markets/${product.id_trans_sell}`, data)
      .catch(error => Observable.throw("Error trying to update user. Detail: " + error))
      .map((user: IResponse) => {
        if (!user.error) {
          return user.result;
        }
        throw user.errorMessage;
      });
  }

  changeToSellStatus(product: ISell): Observable <void>{
    const data = {
      selled: product.selled
    }
    return this.http.put(`${constants.server }markets/sell/${product.id_trans_sell}`, data)
      .catch(error => Observable.throw("Error trying to update user. Detail: " + error))
  }

  deleteProduct(id: number): Observable<boolean> {
    return this.http.delete(`${constants.server}markets/${id}`)
      .catch((error: HttpErrorResponse) =>
        Observable.throw(
          `Error trying to get events. Server returned ${error.message}`
        )
      )
      .map((resp: IResponse) => {
        if (!resp.error) {
          return true;
        }
        throw resp.errorMessage;
      });
  }
}

