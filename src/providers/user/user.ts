import { constants } from './../../constants';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { IUser } from '../../interfaces/i-user';
import { IResponse } from '../../interfaces/i-response';

/*
  Generated class for the UserProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UserProvider {

  constructor(public http: HttpClient) {
    console.log('Hello UserProvider Provider');
  }

  getAllUsers(): Observable<IUser[]> {
    return this.http.get(`${constants.server}users`)
    .map((response: { users: IUser[], ok: boolean }) => {
      if (response.ok) {
        response.users.map(us => {
          us.avatar = `${constants.imgUsers}${us.avatar}`;
        });
        return response.users;
      } else {
        return [];
      }
    }).catch(error => Observable.throw(error));
  }

  getMyProfile(): Observable<IUser> {
    return this.http.get(`${constants.server}users/me`)
      .catch(error => Observable.throw("Error trying to get user. Detail: " + error))
      .map((us: any) => {
        console.log(us);
        if (us.user) {
          us.user.avatar = constants.imgUsers + us.user.avatar;
          us.user.lat = +us.user.lat;
          us.user.lng = +us.user.lng;
          return us.user;
        }
        throw 'error al acceder a mi perfil';
      });
  }

  getProfile(id: number): Observable<IUser> {
    return this.http.get(`${constants.server}users/${id}`)
      .catch(error => Observable.throw("Error trying to get user. Detail: " + error))
      .map((us: any) => {
        console.log(us);
        if (us.user) {
          us.user.avatar = constants.imgUsers + us.user.avatar;
          us.lat = +us.user.lat;
          us.user.lng = +us.user.lng;
          return us.user;
        }
        throw 'error al acceder a mi perfil';
      });
  }

  updateProfile(user: IUser): Observable<IUser> {
    const data = {
      user: user.user,
      email: user.email,
      name: user.name,
      surname: user.surname,
      lat: user.lat,
      lng: user.lng
    };
    return this.http.put(`${constants.server}users/${user.id_user}`, data)
      .catch(error => Observable.throw("Error trying to update user. Detail: " + error))
      .map((user: IResponse) => {
        if (!user.error) {
          return user.result;
        }
        throw user.errorMessage;
      });
  }

  updateCoords(lat, lng){
    const data = {
      lat: lat,
      lng: lng
    };
    return this.http.put(`${constants.server}users/coords`,data)
      .catch(error => Observable.throw("Error trying to update user. Detail: " + error))
      .map((user: IResponse) => {
        if (!user.error) {
          return user.result;
        }
        throw user.errorMessage;
      });
  }

  changePassword(user: IUser): Observable<boolean> {
    const data = {
      password: user.password
    };
    return this.http.patch(`${constants.server}users/me/password`, data)
      .map((resp: { ok: boolean, error?: string }) => {
        if (resp.ok) {
          return resp.ok;
        }
        throw resp.error;
      });
  }

  changeAvatar(user: IUser): Observable<boolean> {
    const data = {
      avatar: user.avatar
    };

    return this.http.patch(`${constants.server}users/me/avatar`, data)
      .map((resp: { ok: boolean, error?: string }) => {
        if (resp.ok) {
          return resp.ok;
        }
        throw resp.error;
      });
  }
  
  deleteUser(id: number): Observable<boolean> {
    return this.http.delete(`${constants.server}users/${id}`)
      .catch((error: HttpErrorResponse) =>
        Observable.throw(
          `Error trying to get events. Server returned ${error.message}`
        )
      )
      .map((resp: IResponse) => {
        if (!resp.error) {
          return true;
        }
        throw resp.errorMessage;
      });
  }
}


