import { IMenu } from './../../interfaces/i-menu';
import { IGenre } from './../../interfaces/i-genre';
import { IConsole } from './../../interfaces/i-consoles';
import { IPlatform } from './../../interfaces/i-platform';
import { HttpClient, HttpErrorResponse} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { IGame } from '../../interfaces/i-game';
import { constants } from '../../constants';
import { IResponse } from '../../interfaces/i-response';
import { ISteam } from '../../interfaces/i-steam';
import { RequestMethod, Http } from '@angular/http';

/*
  Generated class for the GamesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class GamesProvider {

  constructor(private http: HttpClient, private httpA: Http) {
  }

  getAmountGames(): Observable<any> {
    return this.http.get(`${constants.server}games/amount`)
      .catch(error => Observable.throw(error));
  }

  getAllGames(): Observable<IGame[]> {
    return this.http.get(`${constants.server}games`)
      .map((response: { games: IGame[], ok: boolean }) => {
        if (response.ok) {
          response.games.map(game => {
            game.cover = `${constants.imgGames}${game.cover}`;
          });
          return response.games;
        } else {
          return [];
        }
      }).catch(error => Observable.throw(error));
  }

  getAllMenuImages(): Observable<IMenu []> {
    return this.http.get(`${constants.server}menus`)
      .map((response: { images: IMenu[], ok: boolean }) => {
        if (response.ok) {
          response.images.map(menu => {
            menu.image = `${constants.imgUsers}${menu.image}`;
          });
          console.log(response.images);
          return response.images;
        } else {
          return [];
        }
      }).catch(error => Observable.throw(error));
  }
  getAllPlatforms(): Observable<IPlatform[]> {
    return this.http.get(`${constants.server}games/platform`)
      .map((response: { platforms: IPlatform[], ok: boolean }) => {
        if (response.ok) {
          response.platforms.map(plat => {
            plat.img_platform = `${constants.imgPlatfomrs}${plat.img_platform}`;
          });
          return response.platforms;
        } else {
          return [];
        }
      }).catch(error => Observable.throw(error));
  }

  getAllConsoles(): Observable<IConsole[]> {
    return this.http.get(`${constants.server}games/consoles`)
      .map((response: { consoles: IConsole[], ok: boolean }) => {
        if (response.ok) {
          response.consoles.map(conso => {
            conso.img_console = `${constants.imgConsoles}${conso.img_console}`;
          });
          return response.consoles;
        } else {
          return [];
        }
      }).catch(error => Observable.throw(error));
  }

  getAllGenres(): Observable<IGenre[]> {
    return this.http.get(`${constants.server}games/genres`)
      .map((response: { genres: IGenre, ok: boolean }) => {
        if (response.ok) {
          return response.genres;
        } else {
          return [];
        }
      }).catch(error => Observable.throw(error));
  }

  getAllConsolesByPlatform(id: number): Observable<IConsole[]> {
    return this.http.get(`${constants.server}games/platform/${id}`)
      .map((response: { consoles: IConsole[], ok: boolean }) => {
        if (response.ok) {
          response.consoles.map(conso => {
            conso.img_console = `${constants.imgConsoles}${conso.img_console}`;
          });
          return response.consoles;
        } else {
          return [];
        }
      }).catch(error => Observable.throw(error));
  }

  getAllGamesByConsole(id: number): Observable<IGame[]> {
    return this.http.get(`${constants.server}games/console/${id}`)
      .map((response: { games: IGame[], ok: boolean }) => {
        if (response.ok) {
          response.games.map(game => {
            game.cover = `${constants.imgGames}${game.cover}`;
          });
          return response.games;
        } else {
          return [];
        }
      }).catch(error => Observable.throw(error));
  }

  getGameById(id: number): Observable<IGame> {
    return this.http.get(`${constants.server}games/${id}`)
      .map((response: { game: IGame, ok: boolean }) => {
        if (response.ok) {
          response.game.cover = `${constants.imgGames}${response.game.cover}`;
          return response.game;
        } else {
          return [];
        }
      }).catch(error => Observable.throw(error));
  }

  getMenuImageById(id: number): Observable<string> {
    return this.http.get(`${constants.server}menus/${id}`)
      .map((response: { images: IMenu, ok: boolean }) => {
        if (response.ok) {
          response.images.image = `${constants.imgMenu}${response.images.image}`;
          return response.images.image;
        } else {
          return [];
        }
      }).catch(error => Observable.throw(error));
  }

  getGamesScan(id: any): Observable<IGame> {
    return this.http.get(`${constants.server}games/scan/${id}`)
      .map((response: { game: IGame, ok: boolean }) => {
        if (response.ok) {
          response.game.cover = `${constants.imgGames}${response.game.cover}`;
          return response.game;
        } else {
          return [];
        }
      }).catch(error => Observable.throw(error));
  }
  getSteamGames(id: any) :Observable <any>{
    return this.httpA.get(`https://api.steampowered.com/IPlayerService/GetOwnedGames/v0001/?key=610DF6B101CA1B4AF3741D741702A841&include_played_free_games=0&include_appinfo=1&format=json&steamid=${id}`).map(steam => {
    return steam;


      });
    }

  addGame(game: IGame): Observable<any> {
    return this.http
      .post(`${constants.server}games`, game)
      .catch((resp: HttpErrorResponse) =>
        Observable.throw(
          `Error doing register!
            . Server returned code ${resp.status}, message was: ${resp.message}`
        )
      ).map(res => {
        return res.game;
      })
  }

  updateGame(game: IGame): Observable<IGame> {
    const data = {
      id_console: game.id_console,
      id_genre: game.id_genre,
      title: game.title,
      companyia: game.companyia,
      year: game.year,
      description: game.description,
      cover: game.cover,
      region: game.region
    };
    return this.http.put(`${constants.server}games/${game.id_game}`, data)
      .catch(error => Observable.throw("Error trying to update user. Detail: " + error))
      .map((user: IResponse) => {
        if (!user.error) {
          return user.result;
        }
        throw user.errorMessage;
      });
  }

  deleteGame(id: number): Observable<boolean> {
    return this.http.delete(`${constants.server}games/${id}`)
      .catch((error: HttpErrorResponse) =>
        Observable.throw(
          `Error trying to get events. Server returned ${error.message}`
        )
      )
      .map((resp: IResponse) => {
        if (!resp.error) {
          return true;
        }
        throw resp.errorMessage;
      });
  }
}
